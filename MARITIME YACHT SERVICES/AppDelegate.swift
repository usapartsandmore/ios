 //
//  AppDelegate.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 03/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import UserNotifications
import GooglePlaces
import GooglePlacePicker
import GoogleMaps
import PWSwitch
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import Fabric
import Crashlytics

import Firebase
import FirebaseDatabase

var myTeamAvailable = ""

var isSharingUser = Bool()
var sharedUser_id = ""
var isInventoryAvailable = Bool()

var partDataMain = [String:Any]()


var FCMToken : String = ""
var  PostData = [String: Any]()
//var selectedDeckNames = [String]()
let kConstantObj = kConstant()
var scan_Code = ""
let GoogleAPI = "AIzaSyCzZDptqtUCNcNrD7JuukQUcXH9l6XU8hQ"
let channelRef = Database.database().reference()
//AIzaSyCzZDptqtUCNcNrD7JuukQUcXH9l6XU8hQ
//AIzaSyBA3w86s_iUF3gaVdhaC628UJlfLX1--Pg

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate,MessagingDelegate {
    
    var window: UIWindow?
    var strDeviceToken : String = ""
    var PushView: UIView!
    var DataDict: NSDictionary!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //FireBase Chat
        FirebaseApp.configure()  
        FirebaseApp.initialize()
        Messaging.messaging().delegate = self
        
        Fabric.with([Crashlytics.self])
        
        registerForRemoteNotification()
        
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
            // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
        application.applicationIconBadgeNumber = 0
        
       // UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        IQKeyboardManager.sharedManager().enable = true
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        FCMToken = token ?? ""
        defaults.setValue(token, forKey: kAPP_DEVICE_ID)
        
        //PayPal Integration
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: PAYPAL_CLIENT_ID_Live,PayPalEnvironmentSandbox: PAYPAL_CLIENT_ID_Sanbox])
        
        if defaults.bool(forKey: kAPPLOGIN_IN)
        {
            if let window = window{
                let mainVcIntial = kConstantObj.SetIntialMainViewController("HomeVC")
                window.rootViewController = mainVcIntial
            }else{
                window = UIWindow(frame: UIScreen.main.bounds)
                let mainVcIntial = kConstantObj.SetIntialMainViewController("HomeVC")
                self.window?.rootViewController = mainVcIntial
                window?.makeKeyAndVisible()
            }
        }else{
            let mainVcIntial = kConstantObj.SetIntialMainViewController("IntroScreenVC")
            self.window?.rootViewController = mainVcIntial
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        application.applicationIconBadgeNumber = 0
        
        if defaults.bool(forKey: kAPPLOGIN_IN)
        {
             PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "device_token":webService_obj.Retrive("Device_token") as! String]
            
            webService_obj.fetchDataFromServer(alertMsg: false, header: "badge_update", withParameter: PostData as NSDictionary, inVC: UIViewController(), showHud: false) { (response, message, status) in
                if status{
                    
                }
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate .sharedInstance() .application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    // MARK: Remote Notification Methods // <= iOS 9.x
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        NSLog("Device Token =\(token)")
       // defaults.setValue(token, forKey: kAPP_DEVICE_ID)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        NSLog("Device Token FailToRegister=\(kAPP_DEVICE_ID)")
        NSLog("Device Token Error=\(error.localizedDescription)")
        
        defaults.setValue("", forKey: kAPP_DEVICE_ID)
    }
    
    
    // MARK: UNUserNotificationCenter Delegate // >= iOS 10
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .badge, .sound])
        
        guard notification.request.content.userInfo["gcm.notification.type"] != nil else {
            return
        }
        
        NSLog("User Info Normal= \(notification.request.content.userInfo)")
        
        
        
        let data = notification.request.content.userInfo["gcm.notification.type"] as! String
        NSLog("Push Data Recieve Rachit: \(String(describing: data))")
        
        //        guard let key = data["type"] as? String else {
        //            return
        //        }
        //
        //        if (key == "1")
        //        {
        //            NSLog("Push Data Recieve Rachit return type: \(String(describing: data["type"]))")
        //
        //            _=kConstantObj.SetIntialMainViewController("MyTeamListing")
        //        }
        
        //        var userInfo: [AnyHashable: Any]? = notification.request.content.userInfo
        //        print("User Info %@= ",userInfo!)
        //
        //        if UIApplication.shared.applicationState == .inactive
        //        {
        //            print("Inactive")
        //        }
        //        else if UIApplication.shared.applicationState == .active
        //        {
        //            print("Active")
        //            print("Active type",userInfo?["aps"] as! NSDictionary)
        //        }
        //        else if UIApplication.shared.applicationState == .background
        //        {
        //            print("background")
        //        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        //Request = 1
        //Chat = 2
        //default = 0
        
        guard userInfo["gcm.notification.type"] != nil else {
            return
        }
        
        NSLog("User Info = \(userInfo["gcm.notification.type"] ?? "")")
        NSLog("User Info = \(userInfo["aps"] as! [String: String])")
        
        let data = String(describing: userInfo["gcm.notification.type"] ?? "")
        NSLog("Push Data Recieve Rachit: \(String(describing: data))")
        
        if data == "1"
        {
            NSLog("Push Data Recieve Rachit return type: \(String(describing: data))")
            
            _=kConstantObj.SetIntialMainViewController("MyTeamListing")
        }
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      
        completionHandler()
        guard response.notification.request.content.userInfo["gcm.notification.type"] != nil else {
            return
        }
        
        NSLog("User Info Normal= \(response.notification.request.content.userInfo)")
        
        let data =  String(describing: response.notification.request.content.userInfo["gcm.notification.type"] ?? "")
                  
        if data == "1"
        {
            NSLog("Push Data Recieve Rachit return type: \(String(describing: data))")
            
            _=kConstantObj.SetIntialMainViewController("MyTeamListing")
        }
    }
    
    // MARK: Class Methods
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    // MARK:- FCM Token Genration And Handling..
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        let token = fcmToken
        print("FCM token: \(token)")
        FCMToken = token
        defaults.setValue(token, forKey: kAPP_DEVICE_ID)
     }
    
   
}


