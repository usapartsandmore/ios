/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import Photos
import Firebase
import JSQMessagesViewController

//
//extension String {
//    func heightOfString(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
//
//        return ceil(boundingBox.height)
//    }
//
//    func widthOfString(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
//        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
//
//        return ceil(boundingBox.width)
//    }
//}


final class ChatViewController: JSQMessagesViewController,CLLocationManagerDelegate {
    
    
    // MARK: Properties
    private let imageURLNotSetKey = "NOTSET"
    
    var channelRef: DatabaseReference?
    
    private lazy var messageRef: DatabaseReference = self.channelRef!.child("messages")
    fileprivate lazy var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://maritime-yacht-services.appspot.com")
    
    private var newMessageRefHandle: DatabaseHandle?
    private var updatedMessageRefHandle: DatabaseHandle?
    
    private var messages: [JSQMessage] = []
    private var photoMessageMap = [String: JSQPhotoMediaItem]()
    
    //Navigation title
    var channel: Channel? {
        didSet {
            if isfromMyTeam == true{
                title = channel?.name
            }else{
                if isforProduct == true{
                    title = self.productTitle
                }else{
                    title = channel?.name
                }
            }
        }
    }
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    var customer_image = ""
    var channel_name = ""
    
    var allData = [[String:Any]]()
    
    var locationManger: CLLocationManager?
    var coreLocationController:LocationGet?
    var currentlat = Double ()
    var currentlong = Double ()
    
    var isfromMyTeam = Bool()
    var teamName = ""
    
    var isforProduct = Bool()
    var productTitle = ""
    
    var pushiOSData = [String]()
    var pushAndroidData = [String]()
    
    var partData = [String:Any]()
    var isforPartView = Bool()
    
    @IBOutlet weak var collectionVC: UICollectionView!
    // MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isforPartView = false
        partDataMain.removeAll()
        
        
        if isfromMyTeam == true{
            //self.inputToolbar.contentView.leftBarButtonItem = nil
        }else{
            self.coreLocationController = LocationGet()
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse: break
            default:
                locationManger = CLLocationManager()
                locationManger?.delegate = self
                locationManger?.startUpdatingLocation()
                locationManger?.requestWhenInUseAuthorization()
            }
        }
        
        self.senderId = webService_obj.Retrive("User_Id") as! String
        
        observeMessages()
        
        // No avatars
        // collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        
        collectionView.backgroundColor = UIColor(colorLiteralRed: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
        
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault+10, height:kJSQMessagesCollectionViewAvatarSizeDefault+10)
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        self.collectionView.delegate = self
        automaticallyScrollsToMostRecentMessage = true
        self.collectionView?.reloadData()
        self.collectionView?.layoutIfNeeded()
        self.collectionView?.setNeedsLayout()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if isfromMyTeam == false{
            NotificationCenter.default.addObserver(self, selector: #selector(AddLoationExpenseVC.locationAvailable(_:)), name: NSNotification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil)
        }
    }
    
//    override func viewDidLayoutSubviews() {
//        self.collectionView.frame = self.collectionVC.frame
//        
//        self.collectionView.frame = CGRect(x: self.collectionView.frame.origin.x, y: self.collectionView.frame.origin.y, width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height-bottomPadding!)
//
//        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    // MARK: Collection view data source (and related) methods
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
 
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = messages[indexPath.item]
        if message.senderId == senderId { // 1
            cell.textView?.textColor = UIColor.white // 2
 
            cell.cellBottomLabel.text = JSQMessagesTimestampFormatter.shared().time(for: message.date)
        } else {
            cell.textView?.textColor = UIColor.black // 3
 
            cell.cellBottomLabel.text = JSQMessagesTimestampFormatter.shared().time(for: message.date)
        }
        
        for v in cell.subviews{
            if (v .isKind(of: UIButton.self) || v.restorationIdentifier == "editbutton" ) {
                        v.removeFromSuperview()
                    }
            else if (v .isKind(of: UIButton.self) || v.restorationIdentifier == "addbutton" ) {
                v.removeFromSuperview()
                    }
        }
        
        if isfromMyTeam == false{
        let completeData = allData[indexPath.row]
        if completeData["part_name"] as? String != "" && completeData["part_price"] as? String != ""{
            if message.senderId != senderId &&  cell.textView.text.length != 0 {
                // let editButton = UIButton(frame: CGRect(x:cell.contentView.frame.origin.x+cell.contentView.frame.size.width+35, y:15, width:40,height:40))
                let editButton = UIButton(frame: CGRect(x:incomingBubbleImageView.messageBubbleImage.size.width-10, y:-7, width:40,height:40))
                editButton.restorationIdentifier = "editbutton"
                editButton.tag = indexPath.row
                let origImage = UIImage(named: "shoppingcart")
                let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
                editButton.setImage(tintedImage, for: UIControlState.normal)
                editButton.tintColor = UIColor(colorLiteralRed: 47.0/255.0, green: 157.0/255.0, blue: 1.0, alpha: 1.0)
                editButton.addTarget(self, action: #selector(editButtonTapped(_:)), for: .touchUpInside)
                cell.addSubview(editButton)
            }
        }
        else if completeData["part_name"] as? String != "" && completeData["part_price"] as? String == ""{
            let addButton = UIButton(frame: CGRect(x:self.view.frame.size.width-45, y:-7, width:40,height:40))
                addButton.restorationIdentifier = "addbutton"
                addButton.tag = indexPath.row
                addButton.setImage(UIImage(named: "betterInfoIcon"), for: UIControlState.normal)
                addButton.addTarget(self, action: #selector(addPartButtonTapped(_:)), for: .touchUpInside)
                cell.addSubview(addButton)
        }
            }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        let message = messages[indexPath.item]
        
        if message.senderId != senderId {
            let avatarImage = JSQMessagesAvatarImage(avatarImage: nil, highlightedImage: nil, placeholderImage: #imageLiteral(resourceName: "noimage"))
            
            let completeData = allData[indexPath.row]
            
            if completeData["sender_id"] as? String != senderId
            {
                let str = completeData["sender_image"] as! String
                let url:NSURL = NSURL(string :"\(String(describing: str))")!
                
                if str == ""
                {
                    return avatarImage
                }
                
                UIImageView().af_setImage(withURL: url as URL, placeholderImage: #imageLiteral(resourceName: "noimage"), completion: { (response) in
                    
                    if let image = response.result.value {
                        let circularImage = JSQMessagesAvatarImageFactory.circularAvatarImage(image, withDiameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
                        avatarImage!.avatarImage = circularImage
                    }
                })
                
                return avatarImage
            }
            else {
                return nil
            }
        }else {
            return nil
        }
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 18
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every DAY
         */
        let currentMessage = self.messages[indexPath.item]
        
        if indexPath.item == 0 {
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: currentMessage.date)
        }
        
        let currentDateMessage = Calendar.current.startOfDay(for: self.messages[indexPath.item].date)
        let previousDateMessage = Calendar.current.startOfDay(for: self.messages[indexPath.item - 1].date)
        
        let qtyDay = Calendar.current.dateComponents([.day], from: previousDateMessage, to: currentDateMessage).day!
        if( qtyDay >= 1){
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: currentMessage.date)
        }
        
        return nil;
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellTopLabelAt indexPath: IndexPath) -> CGFloat {
        /**
         *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
         */
        
        /**
         *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
         *  The other label height delegate methods should follow similarly
         *
         *  Show a timestamp for every DAY
         */
        
        if indexPath.item == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        let currentDateMessage = Calendar.current.startOfDay(for: self.messages[indexPath.item].date)
        let previousDateMessage = Calendar.current.startOfDay(for: self.messages[indexPath.item - 1].date)
        
        let qtyDay = Calendar.current.dateComponents([.day], from:previousDateMessage , to:currentDateMessage ).day!
        if( qtyDay >= 1){
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        
        return 0.0
    }
    
    
    public func composerTextView(_ textView: JSQMessagesComposerTextView!, shouldPasteWithSender sender: Any!) -> Bool {
        if (UIPasteboard.general.image != nil) {
            // If there's an image in the pasteboard, construct a media item with that image and `send` it.
            
            //            let item = JSQPhotoMediaItem(image: UIPasteboard.generalPasteboard().image)
            //
            //            let message = JSQMessage(senderId: self.senderId, senderDisplayName: self.senderDisplayName, date: NSDate(), media: item)
            //            self.messageArray.append(message)
            //            self.finishSendingMessage()
            
            return false
        }
        
        return true
    }
 
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        
        let message = messages[indexPath.row]
            if message.isMediaMessage == true, let mediaItem = message.media as? JSQLocationMediaItem {
                self.openMap(for: mediaItem.coordinate)
            }
            
        if message.isMediaMessage == true, let item = message.media as? JSQPhotoMediaItem{
            if item.image != nil{
                 let img = self.fixImageOrientation(item.image)
                let newImageView = UIImageView(image:img)
                let viewNew = self.collectionView.superview!.superview!.superview!.superview!.superview!
            
                //newImageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
                //newImageView.transform = newImageView.transform.rotated(by: CGFloat(M_PI_2))
                newImageView.frame = viewNew.frame
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                viewNew.addSubview(newImageView)
            }
        }
    }
    
    func fixImageOrientation(_ image: UIImage)->UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? image
    }
 
     @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
    
    //MARK: Buy services Method
    func editButtonTapped(_ sender: UIButton){
        let completeData = allData[sender.tag]
        print("Hello Edit Button")
        if completeData["part_name"] as? String != "" && completeData["part_price"] as? String != ""{
            
            print("Buy service integration here-->\(completeData.toJson())")
            
            let str = completeData["part_image_url"] as? String
            
            var picked_image = UIImage()
            
            if str != "NOTSET"{
                let storageRef = Storage.storage().reference(forURL: str!)
                storageRef.getData(maxSize: INT64_MAX){ (data, error) in
                    picked_image = UIImage(data: data!)!
                    
                    PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                                "user_id":completeData["sender_id"] ?? "",
                                "name":completeData["part_name"] ?? "",
                                "retail_price":completeData["part_price"] ?? "",
                                "offer_details":completeData["part_desc"] ?? "",
                                "fine_print":completeData["part_price"] ?? ""] as [String : Any]
                    
                    webService_obj.profileDataFromServer(header:"add_chat_service", withParameter: PostData as NSDictionary, coverdata: picked_image.fixed!, inVC: self, showHud: true) { (responce,staus) in
                        if staus {
                            let data = responce.value(forKey: "data") as! [String:String]
                            self.btnBuyNowClick(service_id: data["id"]!, user_id: (completeData["sender_id"])! as! String)
                        }
                    }
                }
            }
        }
    }
    
     //MARK: Add Part button click Method
    func addPartButtonTapped(_ sender: UIButton){
        let completeData = allData[sender.tag]
        
        if completeData["part_name"] as? String != "" && completeData["part_price"] as? String == ""{
            partDataMain = completeData
            let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"AddNewPartVC") as! AddNewPartVC
            controller.isfromchat = true
            // controller.partData = completeData
            controller.push()
            //  }
        }
    }
    
    
    //Buy Service Web-Service call
    func btnBuyNowClick(service_id:String,user_id:String) {
        
        PostData = ["customer_id": webService_obj.Retrive("User_Id") as! String,
                    "user_id":user_id,
                    "user_services_id":service_id]
        
        webService_obj.fetchDataFromServer(header:"service_add_to_cart", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let actionOk = UIAlertAction(title: "GO TO CART", style: .default) { (action:UIAlertAction) in
                    let st = UIStoryboard.init(name: "Cart", bundle: nil)
                    let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
                    controller.push()
                }
                let actionCancel = UIAlertAction(title: "CONTINUE", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }

    
    // MARK: Firebase related methods
    private func observeMessages() {
        messageRef = channelRef!.child("messages")
        let messageQuery = messageRef.queryLimited(toLast:25)
        
        // We can use the observe method to listen for new
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! Dictionary<String, Any>
            
            //Show images
            self.allData.append(messageData)
            
            //Convert Timestap to Date
            let timeStamp = messageData["time"] as? Double
            let date = self.convertTimestamp(serverTimestamp:timeStamp!)
            
            //let timeStamp = messageData["time"] as? Int64
            //let date = Date(timeIntervalSince1970: TimeInterval(timeStamp!))
            
            if let id = messageData["sender_id"] as! String!, let name = messageData["sender_name"] as! String!, let text = messageData["message"] as! String!, text.characters.count > 0 {
                
                self.addMessage(withId: id, name: name, date: date, text: text)
                self.finishReceivingMessage()
            }
            else if let id = messageData["sender_id"] as! String!, let name = messageData["sender_name"] as! String!,messageData["part_text"] != nil ,let text = messageData["part_text"] as? String, text.characters.count > 0 {
           
                let strtext = "\(text) $\(messageData["part_price"] as! String)"
                self.addMessage(withId: id, name: name, date: date, text: strtext)
                self.finishReceivingMessage()
            }

            else if let coordinate = messageData["mapModel"] as? [String: Any], let latitude = coordinate["latitude"], let longitude = coordinate["longitude"] {
                let location = CLLocation(latitude: self.getDouble(for: latitude), longitude: self.getDouble(for: longitude))
                let locationItem = self.buildItem(for: location)
                self.addMedia(withId: messageData["sender_id"] as! String!, name: messageData["sender_name"] as! String!, date: date, media: locationItem)
                self.finishReceivingMessage()
            }
            else if let id = messageData["sender_id"] as! String!, let photoURL = messageData["photoURL"] as! String! {
                if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: id == self.senderId) {
                    self.addPhotoMessage(withId: id, key: snapshot.key, mediaItem: mediaItem)
                    
                    if photoURL.hasPrefix("gs://") {
                        self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                    }
                }
            }
            else {
                print("Error! Could not decode message data")
            }
        })
        
        // We can also use the observer method to listen for
        // changes to existing messages.
        // We use this to be notified when a photo has been stored
        // to the Firebase Storage, so we can update the message data
        updatedMessageRefHandle = messageRef.observe(.childChanged, with: { (snapshot) in
            let key = snapshot.key
            let messageData = snapshot.value as! Dictionary<String, Any>
            
            if let photoURL = messageData["photoURL"] as! String! {
                // The photo has been updated.
                if let mediaItem = self.photoMessageMap[key] {
                    self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: key)
                }
            }
        })
    }
    
    private func fetchImageDataAtURL(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?)
    {
        if photoURL != "NOTSET"{
        let storageRef = Storage.storage().reference(forURL: photoURL)
        storageRef.getData(maxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            //storageRef.downloadURL(completion: { (url, error) in
            //if let error = error {
            //print("Error downloading metadata: \(error)")
            //  return
            //}
            //
            //if let data = try? Data(contentsOf: url!){
            //                     mediaItem.image = UIImage.init(data: data)
            //}
            //self.collectionView.reloadData()
            //guard key != nil else {
            //return
            //}
            //self.photoMessageMap.removeValue(forKey: key!)
            //})
            
            storageRef.getMetadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                
                if (metadata?.contentType == "image/gif") {
                    mediaItem.image = UIImage.gifWithData(data!)
                } else {
                    mediaItem.image = UIImage.init(data: data!)
                }
                self.collectionView.reloadData()
                
                guard key != nil else {
                    return
                }
                self.photoMessageMap.removeValue(forKey: key!)
            })
        }
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        // 1
        let itemRef = messageRef.childByAutoId()
        
        // 2
        let messageItem = [
            "sender_id": senderId!,
            "sender_name": senderDisplayName!,
            "message": text!,
            "receiver_id":channel?.id ?? "",
            "msg_channel":self.channel_name,
            "sender_image": self.customer_image,
            "part_name":"",
            "part_price":"",
            "time":ServerValue.timestamp() //Int64(Date().timeIntervalSince1970)
            ] as [String : Any]

        // 3
        itemRef.setValue(messageItem)
        
        // 4
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        //Push notification for single one to one chat
        if isfromMyTeam == true{
 
            let textStr = "You received a message from \(self.senderDisplayName!) (\(self.teamName)): \(text!)"
            
            // 5 Send push notification
            if self.pushiOSData.count > 0 {
                self.sendNotificationiOS(data: [:], firebaseToken: self.pushiOSData, msg: textStr)
            }
            if self.pushAndroidData.count > 0 {
                self.sendNotificationAndroid(data: [:], firebaseToken: self.pushAndroidData, msg: textStr)
            }
        }
        
        // 6
        finishSendingMessage()
    }
    
    func sendPhotoMessage() -> String? {
        let itemRef = messageRef.childByAutoId()
        
        var messageItem = [String : Any]()
        
        if self.isforPartView == true{
             messageItem = [
                "message":"",
                "part_desc":self.partData["part_desc"] as? String,
                "part_image_url":imageURLNotSetKey,
                "part_minimum_quantity":self.partData["part_minimum_quantity"] as? String,
                "part_name":self.partData["part_name"]!,
                "part_price":"",
                "part_quantity":self.partData["part_quantity"] as? String,
                "part_text":"",
                "part_bar_code":self.partData["part_bar_code"] as? String,
                "part_area":self.partData["part_area"] as? String,
                "part_unit_cost":self.partData["part_unit_cost"] as? String,
                "sender_id": senderId!,
                "sender_name": senderDisplayName!,
                "receiver_id":channel?.id ?? "",
                "msg_channel":self.channel_name,
                "sender_image": self.customer_image,
                "time":ServerValue.timestamp(), //Int64(Date().timeIntervalSince1970)
                "photoURL": imageURLNotSetKey
                ] as [String : Any]
            
        }else{
            messageItem = [
                "sender_id": senderId!,
                "sender_name": senderDisplayName!,
                "receiver_id":channel?.id ?? "",
                "msg_channel":self.channel_name,
                "sender_image": self.customer_image,
                "time":ServerValue.timestamp(), //Int64(Date().timeIntervalSince1970)
                "photoURL": imageURLNotSetKey,
                "part_name":"",
                "part_price":""] as [String : Any]
        }
        
        itemRef.setValue(messageItem)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        //Push notification for single one to one chat
        if isfromMyTeam == true{
            let textStr = "You received an Image from \(self.senderDisplayName!) (\(self.teamName))"
            
            if self.pushiOSData.count > 0 {
                self.sendNotificationiOS(data: [:], firebaseToken: self.pushiOSData, msg:textStr)
            }
            if self.pushAndroidData.count > 0 {
                self.sendNotificationAndroid(data: [:], firebaseToken: self.pushAndroidData, msg: textStr)
            }
        }
        
        finishSendingMessage()
        return itemRef.key
    }
    
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        if self.isforPartView == true{
            let itemRef = messageRef.child(key)
            itemRef.updateChildValues(["photoURL": url])
            itemRef.updateChildValues(["part_image_url": url])
        }else{
            let itemRef = messageRef.child(key)
            itemRef.updateChildValues(["photoURL": url])
        }
    }
    
    
    fileprivate func sendLocationMessage(with coordinate: CLLocationCoordinate2D) {
        let itemRef = messageRef.childByAutoId()
        
        let messageItem = [
            "sender_id": senderId!,
            "sender_name": senderDisplayName!,
            "mapLocUrl":"https://www.google.co.in/maps/@26.857114,75.8127086,15z?hl=en",
            "receiver_id":channel?.id as Any,
            "msg_channel":self.channel_name,
            "sender_image": self.customer_image,
            "time":ServerValue.timestamp(), //Int64(Date().timeIntervalSince1970)
            "mapModel": ["latitude": "\(coordinate.latitude)",
                "longitude": "\(coordinate.longitude)"],
            "part_name":"",
            "part_price":""
            ]
        
        // 3
        itemRef.setValue(messageItem)
        
        // 4
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
//        let textStr = "\(self.senderDisplayName!): Location"
//        
//        if self.pushiOSData.count > 0 {
//            self.sendNotificationiOS(data: [:], firebaseToken: self.pushiOSData, msg:textStr)
//        }
//        if self.pushAndroidData.count > 0 {
//            self.sendNotificationAndroid(data: [:], firebaseToken: self.pushAndroidData, msg: textStr)
//        }
        
        
        // 5
        finishSendingMessage()
    }
    
    
    // MARK: - Firebase PushNotification Message send Methods
    func sendNotificationiOS(data:[String:Any], firebaseToken:[String], msg:String)
    {
        
        let headers = [
            "content-type": "application/json",
            "authorization": "key=AIzaSyBc5_wm9qGwDp3ZhZlktKgR9rC4t1jyXg0"
        ]
        let parameters = [
            "registration_ids": firebaseToken,
            "notification": [
                "body":msg,
                "badge": 0,
                "sound": "default"
            ],
            "data": data
            ] as [String : Any]
        
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        let request = NSMutableURLRequest(url: NSURL(string: "https://fcm.googleapis.com/fcm/send")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error.debugDescription)
            } else
            {
                let httpResponse = response as? HTTPURLResponse
                print("sucess")
            }
        })
        dataTask.resume()
    }
    
    
    func sendNotificationAndroid(data:[String:Any], firebaseToken:[String], msg:String)
    {
        let headers = [
            "content-type": "application/json",
            "authorization":"key=AIzaSyBc5_wm9qGwDp3ZhZlktKgR9rC4t1jyXg0"
        ]
        let parameters = [
            "registration_ids": firebaseToken,
            "data": ["message":msg,
                     "badge": 0,
                     "type":"3"]
            ] as [String : Any]
        
        let postData = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        let request = NSMutableURLRequest(url: NSURL(string: "https://fcm.googleapis.com/fcm/send")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error.debugDescription)
            } else
            {
                let httpResponse = response as? HTTPURLResponse
                print("sucess")
            }
        })
        dataTask.resume()
    }
    
    
    fileprivate func buildItem(for location: CLLocation) -> JSQLocationMediaItem {
        
        let locationItem = JSQLocationMediaItem()
        locationItem.setLocation(location) {
            self.collectionView!.reloadData()
        }
        
        return locationItem
    }
    
    // MARK: UI and User Interaction
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        //        let picker = UIImagePickerController()
        //        picker.delegate = self
        //        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
        //            picker.sourceType = UIImagePickerControllerSourceType.camera
        //        } else {
        //            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //        }
        //
        //        present(picker, animated: true, completion:nil)
        
        self.AlertController()
    }
    
    private func addMessage(withId id: String, name: String, date: Date, text: String) {
        if let message = JSQMessage(senderId: id, senderDisplayName: name, date: date, text: text) {
            messages.append(message)
        }
    }
    
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            messages.append(message)
            
            if (mediaItem.image == nil) {
                photoMessageMap[key] = mediaItem
            }
            
            collectionView.reloadData()
        }
    }
    
    fileprivate func addMedia(withId id: String, name: String, date: Date, media: JSQMediaItem) {
        
        if let message = JSQMessage(senderId: id, senderDisplayName: name, date: date, media: media) {
            self.messages.append(message)
        }
    }
    
    
    // MARK: UITextViewDelegate methods
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        
    }
    
    
    // MARK: - Method Alert Controller
    func AlertController()
    {
        let picker = UIImagePickerController()
        picker.delegate = self
        
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
             self.isforPartView = false
            if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
                picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(picker, animated: true, completion:nil)
            }
        })
        let saveAction = UIAlertAction(title: "Photo Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
             self.isforPartView = false
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(picker, animated: true, completion:nil)
        })
        
        let locationAction = UIAlertAction(title: "Location", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.inputToolbar.contentView!.textView!.resignFirstResponder()
            
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "Location Services not enabled", message:"Please go to Settings and turn on Location Services to enable.", preferredStyle: .alert)
                    
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                        if let url = settingsUrl {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                            } else {
                                UIApplication.shared.openURL(url as URL)
                            }
                        }
                    }
                    let cancelActi = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertMessage.addAction(settingsAction)
                    alertMessage.addAction(cancelActi)
                    self.present(alertMessage, animated: true, completion: nil)
                    
                case .authorizedAlways, .authorizedWhenInUse:
                    let coordinate = CLLocationCoordinate2D(latitude: self.currentlat, longitude: self.currentlong)
                    self.sendLocationMessage(with: coordinate)
                }
            } else {
                
                //Show alert Message
                let alertMessage = UIAlertController(title: "Location Services not enabled", message:"Please go to Settings and turn on Location Services to enable.", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                        } else {
                            UIApplication.shared.openURL(url as URL)
                        }
                    }
                }
                let cancelActi = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                alertMessage.addAction(settingsAction)
                alertMessage.addAction(cancelActi)
                self.present(alertMessage, animated: true, completion: nil)
            }
        })
        
        let addPartAction = UIAlertAction(title: "Add Part", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Add Part")
            let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"AddNewPartVC") as! AddNewPartVC
            
            controller.block = { dict in
                print(dict)
                if dict.count != 0 {
                    self.partData.removeAll()
                    self.partData = dict
                    
                    self.isforPartView = true
                    
                    let pickedImage = self.partData["part_image"]
                    
                    if let key = self.sendPhotoMessage() {
                         //2
                        var data = NSData()
                        data = UIImageJPEGRepresentation(pickedImage! as! UIImage, 0.8)! as NSData
                        let filePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000))"
                        let metaData = StorageMetadata()
                        metaData.contentType = "image/jpg"
                        
                        // 3
                        self.storageRef.child(filePath).putData(data as Data, metadata: metaData){(metaData,error) in
                            if let error = error {
                                print("Error uploading photo: \(error.localizedDescription)")
                                return
                            }
                            // 4
                            self.setImageURL(self.storageRef.child((metaData?.path)!).description, forPhotoMessageWithKey: key)
                        }
                        
                        //5
//                        self.didPressSend(nil, withMessageText: "Click to above image to view part details.", senderId: self.senderId!, senderDisplayName: self.senderDisplayName!, date:Date())
                    }
                }
                else{
                }
            }
           // controller.partData = self.partData
            controller.push()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        if isfromMyTeam == true{
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
        }else{
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(locationAction)
            optionMenu.addAction(addPartAction)
            optionMenu.addAction(cancelAction)
        }
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    func getDouble(for value: Any) -> Double {
        
        if let val = value as? Double {
            return val
        } else if let val = value as? String {
            return Double(val) ?? 0
        } else if let val = value as? Int {
            return Double(val)
        } else {
            return value as? Double ?? 0.0
        }
    }
    
    func convertTimestamp(serverTimestamp: Double) -> Date{
        let x = serverTimestamp / 1000
        let date = NSDate(timeIntervalSince1970: x)
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .medium
        
        //return formatter.string(from: date as Date)
        return date as Date
    }
    
    func openMap(for coordinates: CLLocationCoordinate2D) {
        
        let regionDistance:CLLocationDistance = 10000
        
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.openInMaps(launchOptions: options)
    }
    
    // MARK: Location Available Method
    func locationAvailable(_ notification:Notification) -> Void
    {
        let userInfo = notification.userInfo as! Dictionary<String,String>
        load.hide(delegate: self)
        let  Currentlats = userInfo["latitude"]!
        let  Currentlots = userInfo["longitude"]!
        
        currentlat =    Double(Currentlats)!
        currentlong =    Double(Currentlots)!
        
        
        // NSNotification.removeObserver("")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil);
    }
}

// MARK: Image Picker Delegate
extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion:nil)
        
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
       
        let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        let pickedImage = self.fixImageOrientation(img!)
        // 1
        if let key = sendPhotoMessage() {
            
            //2
            var data = NSData()
            data = UIImageJPEGRepresentation(pickedImage, 0.8)! as NSData
            // set upload path
            // let filePath = "\(Auth.auth().currentUser!.uid)/\(Int(Date.timeIntervalSinceReferenceDate * 1000))/\("userPhoto")"
            let filePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000))"
            let metaData = StorageMetadata()
            metaData.contentType = "image/jpg"
            
            // 3
            self.storageRef.child(filePath).putData(data as Data, metadata: metaData){(metaData,error) in
                if let error = error {
                    print("Error uploading photo: \(error.localizedDescription)")
                    return
                }
                // 4
                self.setImageURL(self.storageRef.child((metaData?.path)!).description, forPhotoMessageWithKey: key)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion:nil)
    }
}

//extension ChatViewController {
//
//    func sendNotification(with message: String, to firebaseToken: String) {
//
//        let name = Constants.kAppDelegate.user!.fullName!
//        let parameters = ["to": firebaseToken,
//                          "notification": ["body": "\(name) sent you a message."],
//                          "data": ["type": "chatMessageByEmployee",
//                                   "channel_name": user!.channelName!,
//                                   "sender_id": Constants.kAppDelegate.user!.id!,
//                                   "sender_device_token": Constants.kDeviceId,
//                                   "sender_name": name,
//                                   "sender_image": Constants.kAppDelegate.user!.picture!]] as [String : Any]
//
//        if let request = API.FCM.request(with: parameters) {
//
//            request.responseJSON { response in
//
//                API.FCM.validatedResponse(response, completionHandler: { (jsonObject, error) -> Void in
//
//                    guard error == nil else {
//                        return
//                    }
//
//
//                })
//            }
//        }
//    }
//}
