//
//  BestFavoritesVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class bestFavoritesCell: UICollectionViewCell {
    @IBOutlet weak var lbl_PName: UILabel!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var img_Logo: UIImageView!
    @IBOutlet weak var view_Rating: FloatRatingView!
    @IBOutlet weak var lbl_Price: UILabel!
}

class BestFavoritesVC: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var btnProducts: UIButton!
    @IBOutlet weak var btnServices: UIButton!
    @IBOutlet var collectionViewSeller: UICollectionView!
    
    @IBOutlet var verticalSpacefromTopview: NSLayoutConstraint!
    @IBOutlet var productTypesViewHeightConst: NSLayoutConstraint!
    
    @IBOutlet var Btn_topSearch: UIButton!
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet var btn_NewArrivals: UIButton!
    @IBOutlet var btn_FlashDeals: UIButton!
    @IBOutlet var btn_Featured: UIButton!
    @IBOutlet var viewSelector3: UIView!
    @IBOutlet var viewSelector2: UIView!
    @IBOutlet var viewSelector1: UIView!
    @IBOutlet var btnTopRight: UIButton!
    
    @IBOutlet var viewProductTypes: UIView!
    
    @IBOutlet var vandorListTableview: UITableView!
    
    var filteredData = [[String:String]]()
    var webServiceData = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.vandorListTableview.isHidden = true
        self.collectionViewSeller.isHidden = false
        self.vandorListTableview.tableFooterView = UIView()
        self.vandorListTableview.estimatedRowHeight = 44.0
        self.vandorListTableview.rowHeight = UITableViewAutomaticDimension
        
        viewProductTypes.isHidden = false
        productTypesViewHeightConst.constant = 45
        self.verticalSpacefromTopview.constant = 0.0
        self.searchBar.tag = 0
         self.searchBar.text = ""
        self.searchBar.isHidden = true
        self.Btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
        
        btnProducts.isSelected = true
        btnServices.isSelected = false
        btnProducts.backgroundColor = UIColor(red: 36/255, green: 86/255, blue: 125/255, alpha: 1.0)
        btnProducts.setTitleColor(Whitecolor, for: UIControlState.selected)
        
        btnServices.backgroundColor = UIColor(red: 116/255, green: 156/255, blue: 182/255, alpha: 1.0)
        btnServices.setTitleColor(Graycolor, for: UIControlState.selected)
        
        btn_Featured.isSelected = true
        btn_FlashDeals.isSelected = false
        btn_NewArrivals.isSelected = false
        viewSelector1.backgroundColor = Greencolor
        viewSelector2.backgroundColor = Whitecolor
        viewSelector3.backgroundColor = Whitecolor
        btn_Featured.setTitleColor(Greencolor, for: UIControlState.selected)
        btn_FlashDeals.setTitleColor(Graycolor, for: UIControlState.selected)
        btn_NewArrivals.setTitleColor(Graycolor, for: UIControlState.selected)
        
        self.fetchAllFavListWebServices(searchtxt: "",type: "featured product")
    }
    
    
    // MARK: FETCH ORDER DATA WEB SERVICES
    @IBAction func btnFetchOrderListWebServices (_ sender:UIButton)
    {
         if sender == btn_Featured {
            btn_Featured.isSelected = true
            btn_FlashDeals.isSelected = false
            btn_NewArrivals.isSelected = false
            viewSelector1.backgroundColor = Greencolor
            viewSelector2.backgroundColor = Whitecolor
            viewSelector3.backgroundColor = Whitecolor
            self.fetchAllFavListWebServices(searchtxt: "",type: "featured product")
        }
        else if sender == btn_FlashDeals{
            btn_Featured.isSelected = false
            btn_FlashDeals.isSelected = true
            btn_NewArrivals.isSelected = false
            viewSelector1.backgroundColor = Whitecolor
            viewSelector2.backgroundColor = Greencolor
            viewSelector3.backgroundColor = Whitecolor
            self.fetchAllFavListWebServices(searchtxt: "",type: "flash")
        }
        else if sender == btn_NewArrivals {
            btn_Featured.isSelected = false
            btn_FlashDeals.isSelected = false
            btn_NewArrivals.isSelected = true
            viewSelector1.backgroundColor = Whitecolor
            viewSelector2.backgroundColor = Whitecolor
            viewSelector3.backgroundColor = Greencolor
            self.fetchAllFavListWebServices(searchtxt: "",type: "new arrivals")
        }
        
        sender.setTitleColor(Greencolor, for: UIControlState.selected)
        sender.setTitleColor(Graycolor, for: UIControlState.normal)
        self.collectionViewSeller.reloadData()
    }
    
    @IBAction func btnProductandServicesClicked (_ sender:UIButton)
    {
        if sender == btnProducts {
            self.vandorListTableview.isHidden = true
            self.collectionViewSeller.isHidden = false
            
            viewProductTypes.isHidden = false
            productTypesViewHeightConst.constant = 45
            btnProducts.isSelected = true
            btnServices.isSelected = false
            btnProducts.backgroundColor = UIColor(red: 36/255, green: 86/255, blue: 125/255, alpha: 1.0)
            btnProducts.setTitleColor(Whitecolor, for: UIControlState.selected)
            btnServices.backgroundColor = UIColor(red: 116/255, green: 156/255, blue: 182/255, alpha: 1.0)
            btnServices.setTitleColor(Graycolor, for: UIControlState.normal)
            
            if self.searchBar.tag != 0
            {
                self.searchBar.text = ""
                self.productTypesViewHeightConst.constant = 0
                self.viewProductTypes.isHidden = true
                self.webServiceData.removeAll()
                self.collectionViewSeller.reloadData()
            }else{
                if btn_Featured.isSelected == true
                {
                    self.fetchAllFavListWebServices(searchtxt: "",type: "featured product")
                }
                else if btn_FlashDeals.isSelected == true{
                    self.fetchAllFavListWebServices(searchtxt: "",type: "flash")
                }
                else{
                    self.fetchAllFavListWebServices(searchtxt: "",type: "new arrivals")
                }
            }
        }
        else{
            self.vandorListTableview.isHidden = false
            self.collectionViewSeller.isHidden = true
            
            self.productTypesViewHeightConst.constant = 0
            self.viewProductTypes.isHidden = true
            self.btnServices.isSelected = true
            self.btnProducts.isSelected = false
            self.btnServices.backgroundColor = UIColor(red: 36/255, green: 86/255, blue: 125/255, alpha: 1.0)
            self.btnServices.setTitleColor(Whitecolor, for: UIControlState.selected)
            self.btnProducts.backgroundColor = UIColor(red: 116/255, green: 156/255, blue: 182/255, alpha: 1.0)
            self.btnProducts.setTitleColor(Graycolor, for: UIControlState.normal)
            
            if self.searchBar.tag != 0
            {
                self.searchBar.text = ""
                self.webServiceData.removeAll()
                self.vandorListTableview.reloadData()
            }else{
                self.fetchAllFavServicesListWebServices(searchtxt: "")
            }
        }
    }
    
    @IBAction func btnSearchClick(_ sender: UIButton) {
        if self.searchBar.tag == 0
        {
            self.webServiceData.removeAll()
            if btnProducts.isSelected == true
            {
                self.collectionViewSeller.reloadData()
            }else{
                self.vandorListTableview.reloadData()
            }
            
            productTypesViewHeightConst.constant = 0
            viewProductTypes.isHidden = true
            self.verticalSpacefromTopview.constant = 55.0
            self.searchBar.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchBar.tag = 1
            self.Btn_topSearch.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
             self.verticalSpacefromTopview.constant = 0.0
            self.searchBar.text = ""
            self.searchBar.isHidden = true
            self.searchBar.endEditing(true)
            self.Btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchBar.tag = 0
            
            if btnProducts.isSelected == true
            {
                self.viewProductTypes.isHidden = false
                self.productTypesViewHeightConst.constant = 45
                
                if btn_Featured.isSelected == true
                {
                    self.fetchAllFavListWebServices(searchtxt: "",type: "featured product")
                }
                else if btn_FlashDeals.isSelected == true{
                    self.fetchAllFavListWebServices(searchtxt: "",type: "flash")
                }
                else{
                    self.fetchAllFavListWebServices(searchtxt: "",type: "new arrivals")
                }
               }else{
                self.fetchAllFavServicesListWebServices(searchtxt: "")
            }
         }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func GoToCartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return webServiceData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "bestFavoritesCell", for: indexPath) as! bestFavoritesCell
        
        let rData = self.webServiceData[indexPath.row]
        
        cellA.lbl_PName.text = rData["name"] ?? ""
        cellA.lbl_Price.text = "$\(rData["price"] ?? "")"
        cellA.view_Rating.rating = (rData["rating"]! as NSString).floatValue
        
        
        let strLogo1 = rData["logo"] ?? ""
        if strLogo1 != ""{
        let urlLogo1:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo1))")!
        cellA.img_Logo.af_setImage(withURL: urlLogo1 as URL)
        }
        
        if rData["image"] == ""
        {
            cellA.img_Product.image = UIImage(named:"noimage")
        }else{
            let str = rData["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cellA.img_Product.af_setImage(withURL: url as URL)
        }
        
        return cellA
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        let fData = self.webServiceData[indexPath.row]
        let p_id = fData["product_id"]
        let variant_id = fData["product_variant_id"]
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
        controller.product_ID = p_id!
        controller.product_variant_ID = variant_id!
        controller.push()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // return CGSize(width: self.view.frame.size.width/2, height:185);
        
        let width = (self.view.frame.size.width - 30) / 2
        let height = width * 1.3
        return CGSize(width: width, height: height);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.webServiceData.count
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceProviderListCell", for: indexPath)
        cell.selectionStyle = .none
        let imageView = cell.viewWithTag(1) as! UIImageView
        let title = cell.viewWithTag(2) as! UILabel
        let description = cell.viewWithTag(3) as! UILabel
        let ratingView = cell.viewWithTag(1005) as! FloatRatingView
        
        let data = self.webServiceData[indexPath.row]
        let str = data["image"]!
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
        imageView.af_setImage(withURL: url as URL)
        title.text = data["company_name"]!
        description.text = data["description"]!
        ratingView.rating = Float(data["rating"] ?? "") ?? 0.0
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vendorId = self.webServiceData[indexPath.row]["user_id"]
        let st = UIStoryboard.init(name: "Services", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
        controller.selectedVendorId = vendorId!
        controller.push()
     }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: SearchBar Delegate Method
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            webServiceData.removeAll()
        }
        else
        {
            if btnProducts.isSelected == true
            {
                self.fetchAllFavListWebServices(searchtxt: searchText,type:"")
            }else{
                self.fetchAllFavServicesListWebServices(searchtxt: searchText)
                
            }
         }
        
        
        if btnProducts.isSelected == true
        {
            self.collectionViewSeller.reloadData()
        }else{
            self.vandorListTableview.reloadData()
        }
    }
    
    
    func fetchAllFavListWebServices(searchtxt: String, type: String)  {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "search":searchtxt,
                    "page":"-1",
                    "type": type]
        
        webService_obj.fetchDataFromServer(alertMsg:false, header:"favoritesProductList", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.webServiceData.removeAll()
                self.webServiceData = responce.value(forKey: "data") as! [[String:String]]
                self.collectionViewSeller.reloadData()
            }else{
                self.webServiceData.removeAll()
                self.collectionViewSeller.reloadData()
            }
        }
     }
    
    func fetchAllFavServicesListWebServices(searchtxt: String)  {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "search":searchtxt,
                    "page":"-1"]
        
        webService_obj.fetchDataFromServer(alertMsg:false, header:"favoritesUserList", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                self.webServiceData.removeAll()
                self.webServiceData = responce.value(forKey: "data") as! [[String:String]]
                self.vandorListTableview.reloadData()
            }else{
                self.webServiceData.removeAll()
                self.vandorListTableview.reloadData()
            }
        }
    }

    
    

}
