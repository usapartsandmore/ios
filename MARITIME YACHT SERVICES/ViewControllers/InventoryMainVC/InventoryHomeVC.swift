//
//  InventoryHomeVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 30/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class InventoryHomeVC: UIViewController {
    
    var isfirstTime = ""
    var deck_name = ""
    var bluePrintDeckId:String = ""
    @IBOutlet weak var lblHeaderTxt: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!

    let arrImgIcons:[UIImage] = [#imageLiteral(resourceName: "storageDesk"),#imageLiteral(resourceName: "maindesk"),#imageLiteral(resourceName: "sundesk"),#imageLiteral(resourceName: "lowerdesk"),#imageLiteral(resourceName: "boatdesk")]
    let arrImageNames = ["Storage","Main Deck","Sun Deck","Lower Deck","Boat Deck"]


    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set header Image and icon
        let name = self.deck_name
        self.lblHeaderTxt.text = name
        let index = arrImageNames.index(of: name) ?? 0
        let iconImage = arrImgIcons[index]
        self.imgHeader.image = iconImage
        
        if isfirstTime == "true"
        {
            UserDefaults.standard.set(true, forKey: "isFirstTimeOnView")
            UserDefaults.standard.synchronize()
        }else
        {
            UserDefaults.standard.set(false, forKey: "isFirstTimeOnView")
            UserDefaults.standard.synchronize()
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        
        //sideMenuVC.toggleMenu()
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func shoppingCartClicked(_ sender: Any) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    @IBAction func UploadBluePrintClicked(_ sender: UIButton) {
        
       // let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddDeck_1VC")
       // controller?.push()
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddDeck_2VC") as! AddDeck_2VC
        controller.deck_name = self.deck_name
        controller.bluePrintDeckId = self.bluePrintDeckId
        controller.push()
    }
    @IBAction func loadDecksFromTemplates(_ sender: UIButton) {
        
        //push SelectVesselFromTemplate AddDeck_1VC
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"TemplateVC") as! SelectVesselTemplateViewController
        controller.deck_name = self.deck_name
        controller.bluePrintDeckId = self.bluePrintDeckId
        controller.push()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
