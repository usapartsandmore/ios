//
//  SelectVesselTemplateViewController.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac114 on 26/10/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import SDWebImage

class SelectVesselTemplateViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    //MARK: Constants
    let labelTagInHeader = 100
    let imageViewTagInHeader = 500
    
    let lableTagInCell = 200
    let imageViewTagInCell = 600
    
    var deck_name = ""
    var bluePrintDeckId:String = ""
    @IBOutlet weak var lblHeaderTxt: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!
    let arrImgIcons:[UIImage] = [#imageLiteral(resourceName: "storageDesk"),#imageLiteral(resourceName: "maindesk"),#imageLiteral(resourceName: "sundesk"),#imageLiteral(resourceName: "lowerdesk"),#imageLiteral(resourceName: "boatdesk")]
    let arrImageNames = ["Storage","Main Deck","Sun Deck","Lower Deck","Boat Deck"]
    
    //MARK: Properties
    var arrTemplatesSections : NSMutableArray? = NSMutableArray()
    
    //MARK: ViewController Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set header Image and icon
        let name = self.deck_name
        self.lblHeaderTxt.text = name
        let index = arrImageNames.index(of: name) ?? 0
        let iconImage = arrImgIcons[index]
        self.imgHeader.image = iconImage

        // Do any additional setup after loading the view.
        tableViewList.dataSource = self
        tableViewList.delegate = self
        
        //get all templates from server
        getAllTemplates()
        
    }

    
    //MARK: Actions
    @IBAction func showSideMenu(_ sender: UIButton) {
        
        //sideMenuVC.toggleMenu()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: TableviewDatasource and Delegate methods
    //sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return (arrTemplatesSections?.count)!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "templateHeaderCell")!
        
        let headerLbl = headerCell.contentView.viewWithTag(labelTagInHeader) as! UILabel
        let headerImgView = headerCell.contentView.viewWithTag(imageViewTagInHeader) as! UIImageView
        
        let templateDict =  arrTemplatesSections?.object(at: section) as! NSDictionary
        headerLbl.text = templateDict.object(forKey: "name") as? String
        
        let imgSubUrl = templateDict.object(forKey: "image") as! String
        //using alomafire
        let imgUrl:NSURL = NSURL(string :"\(imageURL)\(imgSubUrl)")!
        
        headerImgView.sd_setImage(with: imgUrl as URL, placeholderImage: nil, options: SDWebImageOptions(rawValue: 0)) { (img, err, type, url) in
            
            headerImgView.image = img
            
        }
        
        return headerCell.contentView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    
    //cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRows = 0
        
        let templateDict = arrTemplatesSections?.object(at: section) as! NSDictionary
        let arrBoats = templateDict.object(forKey: "template_list") as! NSArray
        
        return arrBoats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "templateContentCell")!
        
        let cellLbl = cell.contentView.viewWithTag(lableTagInCell) as! UILabel
        let cellImgView = cell.contentView.viewWithTag(imageViewTagInCell) as! UIImageView
        cellImgView.isHidden = true
        
        let templateDict = arrTemplatesSections?.object(at: indexPath.section) as! NSDictionary
        let arrBoats = templateDict.object(forKey: "template_list") as! NSArray
        
        let cellDict = arrBoats.object(at: indexPath.row) as! NSDictionary
        let cellText = cellDict.object(forKey: "name") as! String
        
        let totalStr = cellDict.object(forKey: "total_deck") as! String
        let count = Int(totalStr)!
        
        cellLbl.text = cellText
        
        if count > 0 {
            cellImgView.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //get count of deck for selected row
        let templateDict = arrTemplatesSections?.object(at: indexPath.section) as! NSDictionary
        let arrBoats = templateDict.object(forKey: "template_list") as! NSArray
        
        let cellDict = arrBoats.object(at: indexPath.row) as! NSDictionary
        let deckCount = Int(cellDict.object(forKey: "total_deck") as! String)!
        
        guard deckCount > 0 else {
            return
        }
        
        //open deck list view
        //Just navigate user to selectionview for deck
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"selectDecksViewController") as! selectDecksViewController
        controller.templateId = cellDict.object(forKey: "id") as? String
        controller.deck_name = self.deck_name
        controller.bluePrintDeckId = self.bluePrintDeckId
        controller.push()
        
    }
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         
    }
 
    
    //MARK: Utility methods
    func getAllTemplates() {
        
        PostData = ["name":self.deck_name]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "deck_template_list", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                 //getting templates
                self.arrTemplatesSections = NSMutableArray(array: response.object(forKey: "data") as! NSArray)
                
                DispatchQueue.main.async {
                     self.tableViewList.reloadData()
                 }
            }
        }
    }

    
}



