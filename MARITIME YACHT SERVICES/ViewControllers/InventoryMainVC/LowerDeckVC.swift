//
//  LowerDeckVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 31/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import AudioToolbox
import ActionSheetPicker_3_0

@IBDesignable
class MYSTextfield : UITextField{
    
    @IBInspectable var insetX : CGFloat = 0
    @IBInspectable var insetY : CGFloat = 0
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
    
    // textRect
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: insetX, dy: insetY)
    }
}

extension UIImage {
    struct RotationOptions: OptionSet {
        let rawValue: Int
        
        static let flipOnVerticalAxis = RotationOptions(rawValue: 1)
        static let flipOnHorizontalAxis = RotationOptions(rawValue: 2)
    }
    
    @available(iOS 10.0, *)
    func rotated(by rotationAngle: Measurement<UnitAngle>, options: RotationOptions = []) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        let rotationInRadians = CGFloat(rotationAngle.converted(to: .radians).value)
        let transform = CGAffineTransform(rotationAngle: rotationInRadians)
        var rect = CGRect(origin: .zero, size: self.size).applying(transform)
        rect.origin = .zero
        
        let renderer = UIGraphicsImageRenderer(size: rect.size)
        return renderer.image { renderContext in
            renderContext.cgContext.translateBy(x: rect.midX, y: rect.midY)
            renderContext.cgContext.rotate(by: rotationInRadians)
            
            let x = options.contains(.flipOnVerticalAxis) ? -1.0 : 1.0
            let y = options.contains(.flipOnHorizontalAxis) ? 1.0 : -1.0
            renderContext.cgContext.scaleBy(x: CGFloat(x), y: CGFloat(y))
            
            let drawRect = CGRect(origin: CGPoint(x: -self.size.width/2, y: -self.size.height/2), size: self.size)
            renderContext.cgContext.draw(cgImage, in: drawRect)
        }
    }
}

class LowerDeckVC: UIViewController,UITextFieldDelegate {
    
    //MARK: - Outlet Connection
    @IBOutlet weak var scrollViewToShow: UIScrollView!
    @IBOutlet weak var viewTransBack: UIView!
    @IBOutlet weak var viewAddDescriptionToPin: UIView!
    @IBOutlet weak var txtFldPinDescription: UITextField!
    @IBOutlet var bigButton: UIButton!
    @IBOutlet var viewChartTable: UIView!
    @IBOutlet weak var btnAddLocation: UIButton!
    @IBOutlet weak var btnTopNavigationCheck: UIButton!
    @IBOutlet weak var btnShowTutorial: UIButton!
    //   @IBOutlet weak var btnRemovePin: UIButton!
    @IBOutlet var pinchGestureToZoomInOut: UIPinchGestureRecognizer!
    @IBOutlet weak var lblHeader: UILabel!
    
    //MARK: - Global properties
    var dictRecord = NSDictionary()
    var image:UIImage?
    var rotatedImage:UIImage?
    var imgViewToShow : UIImageView?
    let originalScale:CGFloat = 1
    let maximumScale:CGFloat = 1.0
    var currentScale:CGFloat = 0.0
    var selectedPinIndex : Int = 0
    var selectedPreviousPinIndex : Int = 0
    var tapGestureToRemoveCallout:UITapGestureRecognizer? = nil
    var calloutBackView:UIView? = nil
    var flagToScaleDown = false
    var isNewPinTapped:Bool = false
    var isPreviousPinTapped:Bool = false
    var isPinDropped : Bool = false
    var longPressGesture:UILongPressGestureRecognizer? = nil
    
    var isToRotat = true
    
    var imgString = ""
    
    //MARK: - Properites to manage pins
    var arrPins:NSMutableArray? = NSMutableArray()
    var arrPinDetails:NSMutableArray? = NSMutableArray()
    var arrAlreadyAvailablePins:NSMutableArray? = NSMutableArray()
    var stackOfScales:Stack = Stack()
    
    var decknames_NoLocationDict = [[String:String]]()
    @IBOutlet var btnAddDeckNames: UIButton!
    var selectedPinID = ""
    
    @IBOutlet weak var imgHeader: UIImageView!
    let arrImgIcons:[UIImage] = [#imageLiteral(resourceName: "storageDesk"),#imageLiteral(resourceName: "maindesk"),#imageLiteral(resourceName: "sundesk"),#imageLiteral(resourceName: "lowerdesk"),#imageLiteral(resourceName: "boatdesk")]
    let arrImageNames = ["Storage","Main Deck","Sun Deck","Lower Deck","Boat Deck"]
    
    //MARK:- ViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewChartTable.isHidden=true
        
        // Do any additional setup after loading the view.
        //image = UIImage(named: "lowerdeskbig")
 
        //Hide the add description view
        viewTransBack.isHidden = true
        viewAddDescriptionToPin.isHidden = true
        txtFldPinDescription.text = ""
        
        btnAddLocation.isHidden = false
        btnShowTutorial.isHidden = false
        btnTopNavigationCheck.isHidden = true
        // btnRemovePin.isHidden = true
        
        UserDefaults.standard.set(true, forKey: "isFirstTimeOnView")
        UserDefaults.standard.synchronize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //only once process : start
        
        let url:NSURL = NSURL(string :"\(imageURL)\(imgString)")!
        let loadedImageData = NSData(contentsOf: url as URL)
        // DispatchQueue.main.async {
        if let imageData = loadedImageData  {
            self.image = UIImage(data: imageData as Data)
            
            // if self.isToRotat{
            //  if #available(iOS 10.0, *) {
            self.rotatedImage = self.image!//.rotated(by: Measurement(value: -90.0, unit: .degrees))
            //  } else {
            // Fallback on earlier versions
            // }
        }
        //else{
        //self.rotatedImage = self.image!
        //}
        //    }
 
        self.decknames_NoLocationDict.removeAll()
        
        //Below process should be done only for once, when user comes to view
        self.refershUIToUpdate()
    }
    
    func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        // ImagviewFortutorial.isHidden=true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    @IBAction func tutorialHide(_ sender: UIButton) {
        //ImagviewFortutorial.isHidden=true
        bigButton.isHidden = true
    }
    @IBAction func btnTopNavigationCheckTapped(_ sender: UIButton) {
        
        if self.decknames_NoLocationDict.count == 0{
            self.btnAddDeckNames.isHidden = true
            self.selectedPinID = ""
        }else{
            self.btnAddDeckNames.isHidden = false
        }
        
        //open Add Location form
        isNewPinTapped = true
        viewTransBack.isHidden = false
        viewAddDescriptionToPin.isHidden = false
        
    }
    
    @IBAction func tutorialShow(_ sender: UIButton) {
        //ImagviewFortutorial.isHidden=false
        bigButton.isHidden=false
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        // _ = self.navigationController?.popViewController(animated:true)
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DeckLevelList.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func OpenLowerDeckBoxList(_ sender: UISegmentedControl) {
        //        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ExpandableTable") as! ExpandableTable
        //
        //        let dictModified = NSMutableDictionary()
        //        dictModified.setObject(dictRecord, forKey: "data" as NSCopying)
        //
        //        controller.dictPinRecord = dictModified
        //        controller.push()
        //
        //        sender.selectedSegmentIndex = 0
    }
    
    
    func btnViewProductTappedOnCallout(sender:UIButton){
        
        //MnageInventListVC
        //        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        //        let controller = st.instantiateViewController(withIdentifier:"MnageInventListVC") as! MnageInventListVC
        //
        //        let dictForSelectedPin = arrAlreadyAvailablePins?.object(at: selectedPreviousPinIndex) as! NSMutableDictionary
        //
        //        controller.deckId = dictRecord.object(forKey: "id") as! String
        //        controller.selectedPinId = dictForSelectedPin.object(forKey: "id") as! String
        //
        //        controller.push()
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ViewPinsDataVC") as! ViewPinsDataVC
        
        //Get Selected Pin
        let dictForSelectedPin = arrAlreadyAvailablePins?.object(at: selectedPreviousPinIndex) as! NSMutableDictionary
        let id = dictForSelectedPin.object(forKey: "id") as! String
        
        let pinName = dictForSelectedPin.object(forKey: "title") as! String
        
        //Remove unselected Pin from Data
        var newDictionary = [String:Any]()
        
        for (_key, value) in dictRecord{
            let key = _key as! String
            if key == "pin"{
                let oldArray = value as! [[String:Any]]
                let newArray = oldArray.filter{ keyValue in
                    return (keyValue["id"] as! String) == id
                }
                newDictionary[key] = newArray
            }else{
                newDictionary[key] = value
            }
        }
        
        let dictModified = NSMutableDictionary()
        dictModified.setObject(NSMutableDictionary(dictionary: newDictionary), forKey: "data" as NSCopying)
        
        controller.dictPinRecord = dictModified
        controller.bluePrintDeckId = newDictionary["id"] as! String
        controller.pinID = id
        controller.pinName = pinName
        controller.push()
    }
    
    func btnAddProductTappedOnCallout (sender:UIButton){
        
        //instantiate view controller and push
        
        //storyboard id : "AddItemVC"
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"AddItemVC") as! AddItemVC
        
        let dictForSelectedPin = arrAlreadyAvailablePins?.object(at: selectedPreviousPinIndex) as! NSMutableDictionary
        
        controller.deck_id = dictRecord.object(forKey: "id") as! String
        controller.pin_id = dictForSelectedPin.object(forKey: "id") as! String
        
        
        controller.push()
    }
    
    func editButtonOnCalloutTapped(sender:UIButton) {
        removeCallOut()
        
        viewTransBack.isHidden = false
        viewAddDescriptionToPin.isHidden = false
        // btnRemovePin.isHidden = false
        
        var description = ""
        
        if isNewPinTapped {
            //According to flow of app, Edit will never happen on new pin
            
            //            let dictRecord = arrPinDetails?.object(at: selectedPinIndex) as! ImagePin
            //            let description = dictRecord.pinDescription
        }
        else if isPreviousPinTapped{
            let dict = arrAlreadyAvailablePins?.object(at: selectedPreviousPinIndex) as! NSMutableDictionary
            let pinTitle = dict.object(forKey: "title") as? String
            
            description = pinTitle!
        }
        
        txtFldPinDescription.text = description
    }
    
    @IBAction func btnSelectPinClicked(_ sender: UIButton) {
        
        let nameArr = self.decknames_NoLocationDict.map{ ($0["name"]!)}
        
        ActionSheetStringPicker.show(withTitle: "Select Location", rows:nameArr, initialSelection: 0, doneBlock: {
            picker,index,value in
            self.txtFldPinDescription.text = value as? String
            self.selectedPinID = self.decknames_NoLocationDict[index]["id"]!
            return
        }, cancel: {
            ActionStringCancelBlock in return
        }, origin: sender)
    }
    
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        txtFldPinDescription.resignFirstResponder()
        
        viewAddDescriptionToPin.isHidden = true
        viewTransBack.isHidden = true
        
        
        //local validation
        guard txtFldPinDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines).length != 0 else {
            
            //Show error
            let alertVC = UIAlertController(title: "Error", message: "Provide pin description", preferredStyle: .alert)
            
            let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                alertVC.dismiss(animated: true, completion: nil)
            })
            
            alertVC.addAction(alertActionOK)
            
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        
        //Add description to selected pin's description
        if isNewPinTapped {
            let selectedPinRecord = arrPinDetails?.object(at: selectedPinIndex) as! ImagePin
            selectedPinRecord.setDescription(description: txtFldPinDescription.text!)
            
            //Call API to add pin
            //Getting original location
            let imgPin = arrPinDetails?.object(at: 0) as! ImagePin
            let originalLocation = imgPin.pinCurrentPointLocation!
            let originalLocationDescription = NSStringFromCGPoint(originalLocation)
            
            //Getting original size
            //let originalSize = imgPin.pinSuperviewOriginalSize!
            let originalSize = stackOfScales.arrPins.last!.imageSize!
            let originalSizeDescription = NSStringFromCGSize(originalSize)
            
            //Getting blueprint_deck_id
            let bluePrintDeckId = dictRecord.object(forKey: "id") as! String
            
            //Getting pin title
            let pinTitle = txtFldPinDescription.text!
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "blueprint_deck_id":bluePrintDeckId,
                        "title":pinTitle,
                        "size":originalSizeDescription,
                        "points":originalLocationDescription,
                        "id":self.selectedPinID]
            
            //Making addPin Param
            if isSharingUser == true
            {
                PostData["create_by"] = sharedUser_id
            }else{
                PostData["create_by"] = webService_obj.Retrive("User_Id") as! String
            }
            
            
            webService_obj.fetchDataFromServer(alertMsg: false,header:addPinToDeck, withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    //Show alert of sucess and on completion , refresh all the pins on screen
                    let alertVC = UIAlertController(title: "Success", message: "Pin added successfully!", preferredStyle: .alert)
                    
                    let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        //call code to refresh UI
                        // self.refershUIToUpdate()
                        
                        let data = responce.object(forKey: "data") as? [String:String]
                        
                        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ViewPinsDataVC") as! ViewPinsDataVC
                        
                        //Get Selected Pin
                        
                        let id = data?["pin_id"]!
                        let pinName = data?["pin_name"]!
                        
                        controller.bluePrintDeckId = bluePrintDeckId
                        controller.pinID = id!
                        controller.pinName = pinName!
                        controller.push()
                        
                        
                        //dismiss the alert
                        alertVC.dismiss(animated: true, completion: nil)
                    })
                    
                    alertVC.addAction(alertActionOK)
                    
                    self.present(alertVC, animated: true, completion: nil)
                }
                else
                {
                    //TODO: handle fail api response
                }
            }
        }
        else if isPreviousPinTapped{
            //Edit button 
            
            //Call API to Edit title of pin
            //Getting pin id
            let dict = arrAlreadyAvailablePins?.object(at: selectedPreviousPinIndex) as! NSMutableDictionary
            let pinId = dict.object(forKey: "id") as? String
            
            //Getting pin new title
            let pinTitle = txtFldPinDescription.text!
            
            //Making editPin Param
            let editPinParamDict = ["customer_id":webService_obj.Retrive("User_Id") as? String,"decks_pin_id":pinId,"title":pinTitle]
            
            webService_obj.fetchDataFromServer(alertMsg: false,header:editPinToDeck, withParameter: editPinParamDict as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
                if staus{
                    print("pin edited successfully with title")
                    
                    //Show alert of sucess and on completion , refresh all the pins on screen
                    let alertVC = UIAlertController(title: "Success", message: "Pin updated successfully!", preferredStyle: .alert)
                    
                    let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        //call code to refresh UI
                        self.refershUIToUpdate()
                        
                        //dismiss the alert
                        alertVC.dismiss(animated: true, completion: nil)
                    })
                    
                    alertVC.addAction(alertActionOK)
                    
                    self.present(alertVC, animated: true, completion: nil)
                }
                else
                {
                    //TODO: handle fail api response
                }
            }
        }
        
        txtFldPinDescription.text = ""
    }
    
    @IBAction func btnRemovePinTapped(_ sender: UIButton) {
        
        txtFldPinDescription.resignFirstResponder()
        
        viewAddDescriptionToPin.isHidden = true
        viewTransBack.isHidden = true
        
        //Confirm from user to remove pinView
        let alert = UIAlertController(title: "Confirm", message: "Do you want to continue?", preferredStyle: .alert)
        
        let alertActionCance = UIAlertAction(title: "CANCEL", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        let alertActionOk = UIAlertAction(title: "OK", style: .default) { (action) in
            
            alert.dismiss(animated: true, completion: {
                
            })
            
            //Call API to delet pin
            //Getting pin id
            let dict = self.arrAlreadyAvailablePins?.object(at: self.selectedPreviousPinIndex) as! NSMutableDictionary
            let pinId = dict.object(forKey: "id") as? String
            
            
            //Making editPin Param
            let editPinParamDict = ["customer_id":webService_obj.Retrive("User_Id") as? String,
                                    "decks_pin_id":pinId]
            
            webService_obj.fetchDataFromServer(alertMsg: false,header:removeDeckPin, withParameter: editPinParamDict as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    print("pin removed successfully")
                    
                    //Show alert of sucess and on completion , refresh all the pins on screen
                    let alertVC = UIAlertController(title: "Success", message: "Pin removed successfully!", preferredStyle: .alert)
                    
                    let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        //call code to refresh UI
                        self.refershUIToUpdate()
                        
                        //dismiss the alert
                        alertVC.dismiss(animated: true, completion: nil)
                    })
                    
                    alertVC.addAction(alertActionOK)
                    
                    self.present(alertVC, animated: true, completion: nil)
                }
                else
                {
                    //TODO: handle remove pin fail api response
                }
            }
            
            
        }
        
        alert.addAction(alertActionCance)
        alert.addAction(alertActionOk)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnCrossTapped(_ sender: UIButton) {
        //hide addlocation form
        txtFldPinDescription.resignFirstResponder()
        //  btnRemovePin.isHidden = true
        
        viewAddDescriptionToPin.isHidden = true
        viewTransBack.isHidden = true
        txtFldPinDescription.text = ""
    }
    
    
    @IBAction func btnZoomMinusTapped(_ sender: UIButton) {
        //scale down the image to last scale, pop from stack
        if let oldImageSizeNode = stackOfScales.pop()
        {
            if stackOfScales.arrPins.count == 0 {
                stackOfScales.push(newImageSizeNode: oldImageSizeNode)
            }
            
            if stackOfScales.arrPins.count > 1 {
                
                
            }
            let newImageSizeNode = stackOfScales.arrPins.last!
            
            let scaleX = (newImageSizeNode.imageSize?.width)!/(oldImageSizeNode.imageSize?.width)!
            let scaleY = (newImageSizeNode.imageSize?.height)!/(oldImageSizeNode.imageSize?.height)!
            
            UIView.animate(withDuration: 0.2, animations: {
                
                //setup pin to new position
                self.imgViewToShow?.frame.size = newImageSizeNode.imageSize!
                
                //manage content view size
                self.scrollViewToShow.contentSize = newImageSizeNode.imageSize!
                self.imgViewToShow?.center.x = (newImageSizeNode.imageSize?.width)!/2
                self.imgViewToShow?.center.y = (newImageSizeNode.imageSize?.height)!/2
                
                //manage already available pins position
                let arrCount = self.arrPins?.count
                for i in 0 ..< arrCount! {
                    
                    let pinView = self.arrPins?.object(at: i) as! UIView
                    
                    //setup pin to new postion
                    let pindetail = self.arrPinDetails?.object(at: i) as! ImagePin
                    let newX = (pindetail.pinCurrentPointLocation?.x)! * scaleX
                    let newY = (pindetail.pinCurrentPointLocation?.y)! * scaleY
                    
                    pinView.center.x = newX + pinView.frame.size.width/2
                    pinView.center.y = newY + pinView.frame.size.height/2
                    
                    //update pinview's currentLocation
                    pindetail.setCurrentPointLocation(currentLocation: CGPoint(x: newX, y: newY))
                    
                    print("\n\n------ Zoom Minus status ------\nCurrent Size: \(newImageSizeNode.imageSize!.debugDescription)\nOriginal size: \(String(describing: self.stackOfScales.arrPins.first?.imageSize!.debugDescription))")
                    print("\n\n Pin current location: \(pindetail.pinCurrentPointLocation.debugDescription)")
                }
                
                //run a loop for all pins on screen(previously added)
                let arrPreviousPinCount = self.arrAlreadyAvailablePins!.count
                for j in 0 ..< arrPreviousPinCount {
                    
                    let pinDict = self.arrAlreadyAvailablePins!.object(at: j) as! NSDictionary
                    
                    if pinDict.object(forKey: "size") as! String != ""{
                        let pinView = pinDict.object(forKey: "pinView") as! UIView
                        let newX = scaleX * pinView.frame.origin.x
                        let newY = scaleY * pinView.frame.origin.y
                        pinView.center.x = newX + 20
                        pinView.center.y = newY + 20
                        //update current location of pinView
                    }
                }
                
                //changing scrollview offset to zoom image from its center
                let offsetX = (self.scrollViewToShow.contentSize.width - self.scrollViewToShow.frame.size.width)/2
                let offsetY = (self.scrollViewToShow.contentSize.height - self.scrollViewToShow.frame.size.height)/2
                
                self.scrollViewToShow.contentOffset.x = offsetX
                self.scrollViewToShow.contentOffset.y = offsetY
                
                self.currentScale = self.stackOfScales.arrPins.last!.imageScale!
                
            })
            
        }
    }
    
    @IBAction func btnZoomPlusTapped(_ sender: UIButton) {
        
        //Do nothing if stack is full
        guard !(stackOfScales.isFull) else {
            return
        }
        
        if imgViewToShow?.frame != nil{
            
            //scale up the image by 20% upto full zoom
            imgViewToShow?.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            // _ = imgViewToShow!.bounds.size
            flagToScaleDown = true
            
            currentScale += (2/100)
            
            var newScale = currentScale
            
            if newScale > maximumScale {
                newScale = maximumScale
                currentScale = maximumScale
            }
            
            //        let transform:CGAffineTransform = CGAffineTransform(scaleX: newScale, y: newScale)
            //        imgViewToShow?.transform = transform
            //
            //        let currentTransform = imgViewToShow?.transform
            //        let newImgSize = __CGSizeApplyAffineTransform(oldImgSize, currentTransform!)
            
            let newWidth = (imgViewToShow?.frame.size.width)! + ((imgViewToShow?.frame.size.width)! * currentScale)
            let newHeight = (imgViewToShow?.frame.size.height)! + ((imgViewToShow?.frame.size.height)! * currentScale)
            let newImgSize = CGSize(width: newWidth, height: newHeight)
            
            UIView.animate(withDuration: 0.2) {
                self.imgViewToShow?.frame = CGRect(x: (self.imgViewToShow?.frame.origin.x)!, y: (self.imgViewToShow?.frame.origin.y)!, width: newWidth, height: newHeight)
                
                //manage content view size
                self.scrollViewToShow.contentSize = newImgSize
                
                self.imgViewToShow?.center.x = newImgSize.width/2
                self.imgViewToShow?.center.y = newImgSize.height/2
                
                //Managing zoom scaling on all the pins available on view
                
                //run a loop for all pins on screen to update their locations (for newly added pins)
                let arrCount = self.arrPins?.count
                for i in 0 ..< arrCount! {
                    
                    let pinView = self.arrPins?.object(at: i) as! UIView
                    let newSize = newImgSize
                    let oldSizeNode = self.stackOfScales.arrPins.last!
                    
                    let scaleX = newSize.width/(oldSizeNode.imageSize?.width)!
                    let scaleY = newSize.height/(oldSizeNode.imageSize?.height)!
                    
                    //setup pin to new postion
                    let pindetail = self.arrPinDetails?.object(at: i) as! ImagePin
                    let newX = (pindetail.pinCurrentPointLocation?.x)! * scaleX
                    let newY = (pindetail.pinCurrentPointLocation?.y)! * scaleY
                    
                    
                    pinView.center.x = newX + pinView.frame.size.width/2
                    pinView.center.y = newY + pinView.frame.size.height/2
                    
                    //update pinview's currentLocation
                    pindetail.setCurrentPointLocation(currentLocation: CGPoint(x: newX, y: newY))
                    print("\n\n------ Zoom Plus status ------\nCurrent Size: \(newSize.debugDescription)\nOriginal size: \(String(describing: self.stackOfScales.arrPins.first?.imageSize!.debugDescription))")
                    print("\n\n Pin current location: \(pindetail.pinCurrentPointLocation.debugDescription)")
                }
                
                //run a loop for all pins on screen(previously added)
                let arrPreviousPinCount = self.arrAlreadyAvailablePins!.count
                for j in 0 ..< arrPreviousPinCount {
                    
                    let pinDict = self.arrAlreadyAvailablePins!.object(at: j) as! NSDictionary
                    
                    if pinDict.object(forKey: "size") as! String != ""{
                        
                        let pinView = pinDict.object(forKey: "pinView") as! UIView
                        
                        let newSize = newImgSize
                        let oldSizeNode = self.stackOfScales.arrPins.last!
                        
                        let scaleX = newSize.width/(oldSizeNode.imageSize?.width)!
                        let scaleY = newSize.height/(oldSizeNode.imageSize?.height)!
                        
                        let newX = scaleX * pinView.frame.origin.x
                        let newY = scaleY * pinView.frame.origin.y
                        
                        pinView.center.x = newX + 20
                        pinView.center.y = newY + 20
                        
                        //update current location of pinView
                    }
                }
                
                //changing scrollview offset to zoom image from its center
                let offsetX = (self.scrollViewToShow.contentSize.width - self.scrollViewToShow.frame.size.width)/2
                let offsetY = (self.scrollViewToShow.contentSize.height - self.scrollViewToShow.frame.size.height)/2
                
                self.scrollViewToShow.contentOffset.x = offsetX
                self.scrollViewToShow.contentOffset.y = offsetY
                
            }
            
            //add this imgSize to stack
            var newImageSizeNode = ImageSizeNode()
            
            newImageSizeNode.imageSize = newImgSize
            newImageSizeNode.imageScale = currentScale
            newImageSizeNode.imageBoundsSize = imgViewToShow?.bounds.size
            
            stackOfScales.push(newImageSizeNode: newImageSizeNode)
            
        }
    }
    
    @IBAction func btnAddLocationTapped(_ sender: UIButton) {
        
        //Add longPress gesture to scrollview and that should create the pin on scrollview
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(createPin(gestureRecognizer:)))
        
        scrollViewToShow.addGestureRecognizer(longPressGesture!)
        
        //disable addlocation button
        btnAddLocation.isUserInteractionEnabled = false
        btnAddLocation.isHidden = true
        btnShowTutorial.isHidden = true
        //btnTopNavigationCheck.isHidden = false
        isPinDropped = false
        
    }
    
    
    @IBAction func manageZoomInOutByGesture(_ sender: UIPinchGestureRecognizer) {
        
        //Check for pinch In/Out
        let zoomFlag = (sender.scale > 1)
        let btn = UIButton(type: .custom)
        
        if zoomFlag {
            
            self.btnZoomPlusTapped(btn)
            
        }else {
            
            self.btnZoomMinusTapped(btn)
        }
    }
    
    func cleanUp()
    {
        //remove longPressGesture from scrollview
        if let gesture = longPressGesture {
            scrollViewToShow.removeGestureRecognizer(gesture)
        }
        
        //clear all prevouly available pins
        for i in 0 ..< arrAlreadyAvailablePins!.count{
            let pinDict = arrAlreadyAvailablePins!.object(at: i) as! NSDictionary
            
            if pinDict.object(forKey: "size") as! String != ""{
                var pinView = pinDict.object(forKey: "pinView") as? UIView
                pinView!.removeFromSuperview()
                pinView = nil
            }
        }
        arrAlreadyAvailablePins?.removeAllObjects()
        
        //clear all newly added pins
        for i in 0 ..< arrPins!.count{
            var pinView = arrPins?.object(at: i) as? UIView
            pinView!.removeFromSuperview()
            pinView = nil
        }
        arrPins?.removeAllObjects()
        arrPinDetails?.removeAllObjects()
    }
    
    func redrawPins()
    {
        //enable AddLocation button
        btnAddLocation.isUserInteractionEnabled = true
        btnAddLocation.isHidden = false
        btnShowTutorial.isHidden = false
        btnTopNavigationCheck.isHidden = true
        
        //draw updated pins
        let arrAlreadyPins = dictRecord.object(forKey: "pin") as! NSArray
        if arrAlreadyPins.count > 0 {
            //there are already avaliable pins for deck, so place them on deck
            
            arrAlreadyAvailablePins = arrAlreadyPins.mutableCopy() as? NSMutableArray
            
            drawAvailablePinsToDeck()
        }
    }
    //# Nitesh vyas - start
    //MARK: - Utility methods
    func refershUIToUpdate()
    {
        //call API to get latest deckRecord
        let bluePrintDeckId = dictRecord.object(forKey: "id") as! String
        let dictParam = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                         "blueprint_deck_id":bluePrintDeckId]
        
        webService_obj.fetchDataFromServer(header: getDeckRecordForId, withParameter: dictParam as NSDictionary, inVC: self, showHud: true) { (responseDict, msg, status) in
            if status{
                //fill the array and reload the list
                
                //Create dictionary to make list
                
                //  DispatchQueue.main.async {
                //reload table
                print("Updated data got")
                //update dictRecord
                let recordArray = responseDict.object(forKey: "data") as! NSArray
                let record = recordArray.object(at: 0) as! NSDictionary
                self.dictRecord = NSDictionary(dictionary: record)
                
                let arrAlreadyPins = self.dictRecord.object(forKey: "pin") as! NSArray
                if arrAlreadyPins.count > 0 {
                    self.bigButton.isHidden = true
                }else{
                    self.bigButton.isHidden = false
                }
                
                if UserDefaults.standard.bool(forKey: "isFirstTimeOnView") {
                    
                    self.imgViewToShow = UIImageView(frame: CGRect(x: 0, y: 0, width: self.scrollViewToShow.frame.size.width, height: self.scrollViewToShow.frame.size.height))
                    
                    
                    self.imgViewToShow?.image = self.rotatedImage!
                    self.imgViewToShow?.contentMode = UIViewContentMode.scaleAspectFit
                    self.scrollViewToShow.addSubview(self.imgViewToShow!)
                    
                    let originalSize = self.scrollViewToShow.frame.size
                    var origianlImageSizeNode = ImageSizeNode()
                    
                    origianlImageSizeNode.imageSize = originalSize
                    origianlImageSizeNode.imageScale = self.currentScale
                    origianlImageSizeNode.imageBoundsSize = self.imgViewToShow?.bounds.size
                    
                    self.stackOfScales.push(newImageSizeNode: origianlImageSizeNode)
                    
                    //Header Text
                    
                    let deckName = self.dictRecord.object(forKey: "name") as! String
                    self.lblHeader.text = deckName
                    
                    //Set header Image and icon
                    let name = deckName
                    self.lblHeader.text = name
                    let index = self.arrImageNames.index(of: name) ?? 0
                    let iconImage = self.arrImgIcons[index]
                    self.imgHeader.image = iconImage
                    
                    //Draw pins if any for deck
                    // let arrAlreadyPins = self.dictRecord.object(forKey: "pin") as! NSArray
                    if arrAlreadyPins.count > 0 {
                        //there are already avaliable pins for deck, so place them on deck
                        self.arrAlreadyAvailablePins = arrAlreadyPins.mutableCopy() as? NSMutableArray
                        self.drawAvailablePinsToDeck()
                    }
                    
                    UserDefaults.standard.set(false, forKey: "isFirstTimeOnView")
                    UserDefaults.standard.synchronize()
                }
                
                //Do cleanup process
                self.cleanUp()
                
                //Redraw pins
                self.redrawPins()
                // }
            }
            else{
                //TODO: handle error or show user to alert
            }
        }
    }
    
    func createPin(gestureRecognizer:UIGestureRecognizer)
    {
        guard gestureRecognizer.state == UIGestureRecognizerState.began else {
            return
        }
        
        guard !isPinDropped else {
            
            //get the touch location
            let view = gestureRecognizer.view;
            let point:CGPoint = gestureRecognizer.location(in: view); //point is according to scrollview content offset also
            
            
            //move the pin to location with animation
            //Getting pinView to move
            let pinView = arrPins?.object(at: 0) as! UIView
            
            
            UIView.animate(withDuration: 0.3, animations: { 
                
                //changing center with animation
                pinView.center.x = point.x
                pinView.center.y = point.y
                
            }, completion: { (status) in
                
                if status{
                    
                    //since pin has changed its current position, so update that
                    let imgPinDetail = self.arrPinDetails?.object(at: 0) as! ImagePin
                    imgPinDetail.setCurrentPointLocation(currentLocation: CGPoint(x: point.x - 20, y: point.y - 20))
                    
                    //update the currentPosition of pin
                    let originalImageSizeNode = self.stackOfScales.arrPins.first!
                    
                    let newWidth = (self.imgViewToShow?.frame.size.width)! + ((self.imgViewToShow?.frame.size.width)! * self.currentScale)
                    let newHeight = (self.imgViewToShow?.frame.size.height)! + ((self.imgViewToShow?.frame.size.height)! * self.currentScale)
                    let newSize = CGSize(width: newWidth, height: newHeight)
                    
                    let scaleX = newSize.width/(originalImageSizeNode.imageSize?.width)!
                    let scaleY = newSize.height/(originalImageSizeNode.imageSize?.height)!
                    
                    //setup pin to new postion
                    
                    let updatedX = ((imgPinDetail.pinCurrentPointLocation?.x)!) * scaleX
                    let updatedY = ((imgPinDetail.pinCurrentPointLocation?.y)!) * scaleY
                    
                    imgPinDetail.setLocationAtOriginalScale(initialScaleLocation: CGPoint(x: updatedX, y: updatedY))
                }
            })
            return
        }
        
        //play viberate
        AudioServicesPlayAlertSound(UInt32(kSystemSoundID_Vibrate))
        
        //get touch point
        let point = gestureRecognizer.value(forKey: "_startPointScreen") as! CGPoint
        
        //create a imageview and add longpress gesture to that
        let correctX = (point.x + scrollViewToShow.contentOffset.x) - (20 + scrollViewToShow.frame.origin.x)
        let correctY = (point.y + scrollViewToShow.contentOffset.y) - (20 + scrollViewToShow.frame.origin.y)
        
        let pinView = UIView(frame: CGRect(x: correctX, y: correctY, width: 40, height: 40))
        
        let pinImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        pinImgView.image = UIImage(named: "locationicon")
        pinView.addSubview(pinImgView)
        
        //adding longpress gesture to pinview
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(managePinsPosition(sender:)))
        longPressGesture.minimumPressDuration = 0.5
        pinView.addGestureRecognizer(longPressGesture)
        
        //        //adding tapGesture on pinView
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(manageTapOnPin(sender:)))
        //        pinView.addGestureRecognizer(tapGesture)
        
        scrollViewToShow.addSubview(pinView)
        
        //add pin to arrPins
        arrPins?.add(pinView)
        
        //Generating details of pin
        let imgPin:ImagePin = ImagePin()
        imgPin.setPinOriginalViewSize(originalViewSize: self.scrollViewToShow.frame.size)
        imgPin.setCurrentPointLocation(currentLocation: CGPoint(x: pinView.frame.origin.x, y: pinView.frame.origin.y))
        //imgPin.setCurrentPointLocation(currentLocation: CGPoint(x: (pinView.frame.origin.x + (pinView.frame.size.width/2)), y: (pinView.frame.origin.y + pinView.frame.size.height)))
        //imgPin.setCurrentPointLocation(currentLocation: CGPoint(x: correctX, y: correctY))
        
        
        //calculating the imgPin original location
        let originalImageSizeNode = stackOfScales.arrPins.first!
        
        let newWidth = (imgViewToShow?.frame.size.width)! + ((imgViewToShow?.frame.size.width)! * currentScale)
        let newHeight = (imgViewToShow?.frame.size.height)! + ((imgViewToShow?.frame.size.height)! * currentScale)
        let newSize = CGSize(width: newWidth, height: newHeight)
        
        let scaleX = newSize.width/(originalImageSizeNode.imageSize?.width)!
        let scaleY = newSize.height/(originalImageSizeNode.imageSize?.height)!
        
        //setup pin to new postion
        let newX = (imgPin.pinCurrentPointLocation?.x)! * scaleX
        let newY = (imgPin.pinCurrentPointLocation?.y)! * scaleY
        
        imgPin.setLocationAtOriginalScale(initialScaleLocation: CGPoint(x: newX, y: newY))
        print(imgPin.pinCurrentPointLocation.debugDescription)
        
        arrPinDetails?.add(imgPin)
        
        // -- Now don't remove longPressGesture from scrollview, because now it will be used to move pin, so just change isPinDropped value to true
        //scrollViewToShow.removeGestureRecognizer(gestureRecognizer)
        btnTopNavigationCheck.isHidden = false
        isPinDropped = true
    }
    
    func getIndexOf(view:UIView,fromArray:NSMutableArray) -> Int
    {
        var indexOfSelectedPinView: Int = 0
        
        //getting index of selected pinview from array
        for pinViewRecord in arrPins! {
            let oneOfThePinView = pinViewRecord as! UIView
            
            if oneOfThePinView == view{
                indexOfSelectedPinView = (arrPins?.index(of: oneOfThePinView))!
                break
            }
        }
        return indexOfSelectedPinView
    }
    
    func getIndexOfPreviouslyAdded(view:UIView,fromArray:NSArray) -> Int
    {
        var indexOfSelectedPinView: Int = 0
        
        //getting index of selected pinview from array
        for i in 0 ..< fromArray.count{
            let record = fromArray.object(at: i) as! NSDictionary
            
            if record.object(forKey: "size") as! String != ""{
                let oneOfThePinView = record.object(forKey: "pinView") as! UIView
                if oneOfThePinView == view{
                    indexOfSelectedPinView = i
                    break
                }
            }
        }
        
        return indexOfSelectedPinView
    }
    
    func manageTapOnPin(sender:UITapGestureRecognizer)
    {
        isNewPinTapped = true
        isPreviousPinTapped = false
        
        let view = sender.view
        
        let indexOfSelectedPinView = getIndexOf(view: view!, fromArray: arrPins!)
        
        selectedPinIndex = indexOfSelectedPinView
        
        //check if selected pin has description or not
        let pinRecordForSelectedPin = arrPinDetails?.object(at: selectedPinIndex) as! ImagePin
        if let desc = pinRecordForSelectedPin.pinDescription {
            if desc.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).length == 0 {
                //No title yet
                //open Add Location form
                viewTransBack.isHidden = false
                viewAddDescriptionToPin.isHidden = false
            }
            else{
                //description found
                //Show description in callout
                showCalloutView(description: desc, indexToEdit: indexOfSelectedPinView, frame: (view?.frame)!)
            }
        }
        else
        {
            //no description,
            //open Add Location form
            viewTransBack.isHidden = false
            viewAddDescriptionToPin.isHidden = false
        }
    }
    
    func manageTapOnAlreadyAvailablePin(sender:UITapGestureRecognizer)
    {
        isPreviousPinTapped = true
        isNewPinTapped = false
        
        let view = sender.view
        let indexOfSelectedPinView = getIndexOfPreviouslyAdded(view: view!, fromArray: arrAlreadyAvailablePins!)
        
        selectedPreviousPinIndex = indexOfSelectedPinView
        
        //check if selected pin has description or not
        let dict = arrAlreadyAvailablePins?.object(at: selectedPreviousPinIndex) as! NSMutableDictionary
        let pinTitle = dict.object(forKey: "title") as? String
        if let desc = pinTitle {
            if desc.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).length == 0 {
                //No title yet
                //open Add Location form
                viewTransBack.isHidden = false
                viewAddDescriptionToPin.isHidden = false
            }
            else{
                //description found
                //Show description in callout
                
                
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"ViewPinsDataVC") as! ViewPinsDataVC
                
                //Get Selected Pin
                let dictForSelectedPin = arrAlreadyAvailablePins?.object(at: selectedPreviousPinIndex) as! NSMutableDictionary
                let id = dictForSelectedPin.object(forKey: "id") as! String
                
                let pinName = dictForSelectedPin.object(forKey: "title") as! String
                
                //Remove unselected Pin from Data
                var newDictionary = [String:Any]()
                
                for (_key, value) in dictRecord{
                    let key = _key as! String
                    if key == "pin"{
                        let oldArray = value as! [[String:Any]]
                        let newArray = oldArray.filter{ keyValue in
                            return (keyValue["id"] as! String) == id
                        }
                        newDictionary[key] = newArray
                    }else{
                        newDictionary[key] = value
                    }
                }
                
                let dictModified = NSMutableDictionary()
                dictModified.setObject(NSMutableDictionary(dictionary: newDictionary), forKey: "data" as NSCopying)
                
                controller.dictPinRecord = dictModified
                controller.bluePrintDeckId = newDictionary["id"] as! String
                controller.pinID = id
                controller.pinName = pinName
                controller.push()
                
                //  showCalloutView(description: desc, indexToEdit: indexOfSelectedPinView, frame: (view?.frame)!)
            }
        }
        else
        {
            //no description,
            //open Add Location form
            viewTransBack.isHidden = false
            viewAddDescriptionToPin.isHidden = false
        }
        
        print("Tap on previously added pin")
    }
    
    func showCalloutView(description:String,indexToEdit:Int,frame:CGRect)
    {
        //RectX = pinview.x - (Rectwidth/2 - pinview.width/2)
        //Check for valid range
        //RectX >= 10 && RectX + RectWidth <= screenWidth - 10
        //RectX < 10, make RectX:10
        //RectX + RectWidth <= screenWidth - 10, find gap: i.e. RectX+RectWidth - (screenWidth - 10), update RectX by RectX - gap
        
        calloutBackView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        calloutBackView?.backgroundColor = UIColor.clear
        
        let calloutWidth = scrollViewToShow.frame.size.width/1.3
        let calloutHeight:CGFloat = 45
        let calloutY = frame.origin.y - 60 + (scrollViewToShow.frame.origin.y - scrollViewToShow.contentOffset.y)
        var calloutX = (frame.origin.x - (calloutWidth/2 - frame.size.width/2)) - scrollViewToShow.contentOffset.x
        var downArrowX:CGFloat = (frame.origin.x - (15 - frame.size.width/2)) - scrollViewToShow.contentOffset.x
        
        //check for valid calloutX
        if !(calloutX >= 10 && (calloutX+calloutWidth) <= (scrollViewToShow.frame.size.width - 10)) {
            //since calloutX is not valid, so finding the cause
            if calloutX < 10 {
                calloutX = 10
            }
            else{
                let gap = (calloutX + calloutWidth) - (scrollViewToShow.frame.size.width - 10)
                calloutX = calloutX - gap
            }
        }
        if !(downArrowX >= 10 && (downArrowX+30) <= (scrollViewToShow.frame.size.width - 10)) {
            //since downArrowX is not valid, so finding the cause
            if downArrowX < 10 {
                downArrowX = 10
            }
            else{
                downArrowX = calloutX + calloutWidth - 30
            }
        }
        
        
        //now create callout view
        let calloutView = UIView(frame: CGRect(x: calloutX, y: calloutY, width: calloutWidth, height: calloutHeight))
        calloutView.backgroundColor = UIColor.white
        calloutView.layer.borderColor = UIColor.darkGray.cgColor
        calloutView.layer.borderWidth = 1.0
        calloutView.layer.cornerRadius = 8.0
        calloutView.clipsToBounds = true
        
        calloutBackView?.addSubview(calloutView)
        
        //Creating point view to show direction
        let downArrowView = UIView(frame: CGRect(x: downArrowX, y: (calloutY + calloutHeight - 1), width: 30, height: 15))
        downArrowView.backgroundColor = UIColor.clear
        
        let downArrowImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
        downArrowImageView.image = #imageLiteral(resourceName: "downArrow")
        downArrowView.addSubview(downArrowImageView)
        
        //create imageview to hold arrow image
        
        calloutBackView?.addSubview(downArrowView)
        
        
        
        //create uilabel and edit button
        
        //create Edit Button
        let btnEdit = UIButton(type: .custom)
        btnEdit.frame = CGRect(x: calloutView.frame.size.width - 40, y: 0, width: 40, height: 45)
        btnEdit.setImage(UIImage(named: "editicon")!, for: .normal)
        btnEdit.addTarget(self, action: #selector(editButtonOnCalloutTapped(sender:)), for: .touchUpInside)
        calloutView.addSubview(btnEdit)
        
        //create add product button
        //        let btnAddProduct = UIButton(type: .custom)
        //        btnAddProduct.frame = CGRect(x: btnEdit.frame.origin.x - 40, y: 0, width: 40, height: 45)
        //        btnAddProduct.setImage(UIImage(named: "addProductInventory")!, for: .normal)
        //        btnAddProduct.addTarget(self, action: #selector(LowerDeckVC.btnAddProductTappedOnCallout(sender:)), for: .touchUpInside)
        //        calloutView.addSubview(btnAddProduct)
        
        //create view product button
        let btnViewProduct = UIButton(type: .custom)
        btnViewProduct.frame = CGRect(x: btnEdit.frame.origin.x - 40, y: 0, width: 40, height: 45)
        btnViewProduct.setImage(UIImage(named: "viewIconInventory")!, for: .normal)
        btnViewProduct.addTarget(self, action: #selector(LowerDeckVC.btnViewProductTappedOnCallout(sender:)), for: .touchUpInside)
        calloutView.addSubview(btnViewProduct)
        
        //create label to show pinTitle
        let lbl:UILabel = UILabel(frame: CGRect(x: 8, y: 8, width:(btnViewProduct.frame.origin.x - 8), height: calloutView.frame.size.height - 16)) //8+8+40 //8: trailing space,8: space between label and buttonToEdit, 40: width of button
        lbl.text = description
        lbl.numberOfLines = 1
        lbl.textColor = UIColor.gray
        lbl.font = UIFont(name: "Poppins-Regular", size: 17.0)
        lbl.adjustsFontSizeToFitWidth = true;
        lbl.minimumScaleFactor = 0.5;
        //lbl.font = UIFont.boldSystemFont(ofSize: 15)
        calloutView.addSubview(lbl)
        
        self.view.addSubview(calloutBackView!)
        
        tapGestureToRemoveCallout = UITapGestureRecognizer(target: self, action: #selector(removeCallOut))
        calloutBackView?.addGestureRecognizer(tapGestureToRemoveCallout!)
    }
    
    func managePinsPosition(sender:UILongPressGestureRecognizer){
        let view = sender.view;
        let point:CGPoint = sender.location(in: view);
        var indexOfSelectedPinView = 0
        
        indexOfSelectedPinView = getIndexOf(view: view!, fromArray: arrPins!)
        
        if sender.state == UIGestureRecognizerState.began {
            //play viberate
            AudioServicesPlayAlertSound(UInt32(kSystemSoundID_Vibrate))
        }
        
        if (sender.state == UIGestureRecognizerState.changed) {
            
            let oldDetail = arrPinDetails?.object(at: indexOfSelectedPinView)  as! ImagePin
            let oldOrigin = oldDetail.pinCurrentPointLocation!
            let pinView = arrPins?.object(at: indexOfSelectedPinView) as! UIView
            
            //Getting direction
            var newX = oldOrigin.x
            var newY = oldOrigin.y
            
            newX = pinView.center.x + point.x
            newY = pinView.center.y + point.y
            
            view?.frame = CGRect(x:newX , y:newY , width: 40, height: 40)
            
            oldDetail.setCurrentPointLocation(currentLocation: CGPoint(x: newX, y: newY))
            //oldDetail.setCurrentPointLocation(currentLocation: CGPoint(x: newX + 20, y: newY+40))
            //oldDetail.setCurrentPointLocation(currentLocation: CGPoint(x: newX, y: newY))
        }
        
        
        if sender.state == UIGestureRecognizerState.ended {
            
            //need to update image pin original location
            let originalImageSizeNode = stackOfScales.arrPins.first!
            
            let newWidth = (imgViewToShow?.frame.size.width)! + ((imgViewToShow?.frame.size.width)! * currentScale)
            let newHeight = (imgViewToShow?.frame.size.height)! + ((imgViewToShow?.frame.size.height)! * currentScale)
            let newSize = CGSize(width: newWidth, height: newHeight)
            
            let scaleX = newSize.width/(originalImageSizeNode.imageSize?.width)!
            let scaleY = newSize.height/(originalImageSizeNode.imageSize?.height)!
            
            //setup pin to new postion
            let imgPin = arrPinDetails?.object(at: indexOfSelectedPinView) as! ImagePin
            
            let newX = ((imgPin.pinCurrentPointLocation?.x)!) * scaleX
            let newY = ((imgPin.pinCurrentPointLocation?.y)!) * scaleY
            
            imgPin.setLocationAtOriginalScale(initialScaleLocation: CGPoint(x: newX, y: newY))
        }
    }
    
    func removeCallOut()
    {
        print("callout removed")
        scrollViewToShow.removeGestureRecognizer(tapGestureToRemoveCallout!)
        calloutBackView?.removeFromSuperview()
        calloutBackView = nil
    }
    
    func getAllPinDetails(_ forPin:UIView) -> (title:String?,originalLocation:String,originalSize:String)
    {
        let indexOfPin = getIndexOf(view: forPin, fromArray: arrPins!)
        let pinDetailDict = arrPinDetails?.object(at: indexOfPin) as! ImagePin
        
        let pinTitle = pinDetailDict.pinDescription
        let pinOriginalSize = pinDetailDict.pinSuperviewOriginalSize!
        let pinOriginalLocation = pinDetailDict.pinLocationOnOriginalScale!
        
        return (pinTitle,NSStringFromCGPoint(pinOriginalLocation),NSStringFromCGSize(pinOriginalSize))
    }
    
    func drawAvailablePinsToDeck()
    {
        //running loop to calculate position of pins with respect to screen size, and putting them on the deck
        for i in 0 ..< arrAlreadyAvailablePins!.count
        {
            let dictPinDetail = arrAlreadyAvailablePins?.object(at: i) as! NSDictionary
            
            if dictPinDetail.object(forKey: "size") as! String != ""{
                
                //Calculating latest location with respect to screen size
                let originalSizeDescription = dictPinDetail.object(forKey: "size") as! String
                let originalSize = CGSizeFromString(originalSizeDescription)
                
                let newWidth = (imgViewToShow?.frame.size.width)! + ((imgViewToShow?.frame.size.width)! * currentScale)
                let newHeight = (imgViewToShow?.frame.size.height)! + ((imgViewToShow?.frame.size.height)! * currentScale)
                var newSize = CGSize(width: newWidth, height: newHeight)
                
                newSize = stackOfScales.arrPins.last!.imageSize!
                
                let scaleX = newSize.width/originalSize.width
                let scaleY = newSize.height/originalSize.height
                
                let originalPointDescription = dictPinDetail.object(forKey: "points") as! String
                let originalPoint = CGPointFromString(originalPointDescription)
                
                //subtract 20 from x and 40 from y if pin is not moved
                //subtract only 20 from y if pin is dragged
                let newX = (originalPoint.x) * scaleX
                let newY = (originalPoint.y) * scaleY
                
                //Create a PinView using new location
                let pinView = UIView(frame: CGRect(x:newX, y:newY, width: 40, height: 40))
                
                let pinImgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                pinImgView.image = UIImage(named: "locationicon")
                pinView.addSubview(pinImgView)
                
                //adding tapGesture on pinView
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(manageTapOnAlreadyAvailablePin(sender:)))
                pinView.addGestureRecognizer(tapGesture)
                
                scrollViewToShow.addSubview(pinView)
                
                //add the pinView to array of pins at same index
                let updatedDict = dictPinDetail.mutableCopy() as! NSMutableDictionary
                updatedDict.setObject(pinView, forKey: "pinView" as NSCopying)
                updatedDict.setObject(NSStringFromCGPoint(CGPoint(x: newX, y: newY)), forKey: "pinOriginalLocation" as NSCopying)
                
                arrAlreadyAvailablePins?.replaceObject(at: i, with: updatedDict)
            }else{
                
                let data = ["name":dictPinDetail.object(forKey: "title") as! String,
                            "id":dictPinDetail.object(forKey: "id") as! String]
                
                let mainID = dictPinDetail.object(forKey: "id") as! String
                let idAvailableArr = self.decknames_NoLocationDict.map{ ($0["id"]!)}
                
                if idAvailableArr.contains(mainID){
                }else{
                    self.decknames_NoLocationDict.append(data)
                }
            }
        }
        
        print("Pins without location Array-->\(self.decknames_NoLocationDict)")
    }
    
    
}
