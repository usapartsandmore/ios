//
//  CropDeckLevelVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 30/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

extension CGPoint {
    
    /// Creates a frame given another point.
    ///
    /// - Parameter point: other point
    /// - Returns: frame
    public func frame(to point: CGPoint) -> CGRect {
        let rx = min(x, point.x)
        let ry = min(y, point.y)
        let rw = fabs(x - point.x)
        let rh = fabs(y - point.y)
        
        return CGRect(x: rx, y: ry, width: rw, height: rh)
    }
    
}

class CropDeckLevelVC: UIViewController {
    
    //MARK: - Properties
    var imagePicked:UIImage?
    var imgView : UIImageView?
    var overlayView1 : UIView!
    var overlayView2 : UIView!
    var overlayView3 : UIView!
    var overlayView4 : UIView!
    var isImageFullyZoomed  = false
    var isImageAtOriginalScale = true
    var isFirstTimeOnView = true
    
    var btnCropHandle1,btnCropHandle2 : UIButton!
    var lblIndicator1,lblIndicator2,lblIndicator3,lblIndicator4 : UILabel!
    
    var deck_name = ""
    var bluePrintDeckId:String = ""
    
    
    var visibleRectFrame: CGRect = .zero{
        didSet{
            
            guard self.lblIndicator1 != nil else {
                return
            }
            
            var fr = visibleRectFrame
            fr.size.height = 1
            self.lblIndicator1.frame = fr
            
            fr = visibleRectFrame
            fr.origin.y = fr.origin.y+fr.size.height
            fr.size.height = 1
            self.lblIndicator2.frame = fr
            
            fr = visibleRectFrame
            fr.size.width = 1
            self.lblIndicator3.frame = fr
            
            fr = visibleRectFrame
            fr.origin.x = fr.origin.x+fr.width
            fr.size.width = 1
            self.lblIndicator4.frame = fr
            
            fr = btnCropHandle1.frame
            fr.origin = lblIndicator1.frame.origin
            //btnCropHandle1.frame = fr
            
            fr = btnCropHandle2.frame
            fr.origin = CGPoint(x: lblIndicator1.frame.origin.x + lblIndicator1.frame.width, y: lblIndicator1.frame.origin.y + lblIndicator1.frame.height)
            //btnCropHandle1.frame = fr
            
        }
    }
    
    
    private func update(with point1: CGPoint, point2:CGPoint){
        btnCropHandle1.center = point1
        btnCropHandle2.center = point2
    }
    
    
    //MARK:- Properties used for API Call
    var imageToSave:UIImage?
    var titleToSave:String?
    
    //MARK: - Outlets of UIElements
    @IBOutlet var tutorialButton: UIButton!
    @IBOutlet var btnRightsideNavigationbar: UIButton!
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var btnCrop: UIButton!
    
    @IBOutlet weak var scrollViewForImg: UIScrollView!
    @IBOutlet var pinchGesture: UIPinchGestureRecognizer!
    
    var isForIndividual = ""
    
    //MARK: - ViewController Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialCropOverlaySetup()
        btnRightsideNavigationbar.setImage(UIImage(named: "greenTickCircle"),for: .normal)
        btnRightsideNavigationbar.setTitle("", for: .normal)
        
        
        
        // Do any additional setup after loading the view.
        
        //setup userdefaults to set firstTimeOnThisScreen = YES
        // UserDefaults.standard.set(true, forKey: "isFirstTimeOnView")
        //  UserDefaults.standard.synchronize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //only load the image when user is first time on the view
        
        // if UserDefaults.standard.bool(forKey: "isFirstTimeOnView") {
        getImage()
        btnFinish.isHidden = true
        // UserDefaults.standard.set(true, forKey: "isFirstTimeOnView")
        //UserDefaults.standard.synchronize()
        //  }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //adding current view as observer to notification
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(notification:)), name: Notification.Name("setupViewAccordingToOperation"), object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("setupViewAccordingToOperation"), object: nil)
    }
    
    //MARK: - IB Actions
    @IBAction func hideTutorialclicked
        (_ sender: UIButton) {
        tutorialButton.isHidden = true
        
        btnRightsideNavigationbar.isHidden = true
        //btnFinish.isHidden = false
        btnCrop.isHidden = false
        
        // initialCropOverlaySetup()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func topRightBtnClicked(_ sender: UIButton) {
        let img:UIImage?
        if isImageAtOriginalScale {
            //img = cropOriginalImage()
            img = cropZoomedImage() //This works perfect with photoAlbum images not with camera images
        }
        else{
            img = cropZoomedImage()
            //img = cropOriginalImage()
        }
        
        if self.isForIndividual == "true"
        {
            let imgDict = ["image":img]
            let paramDict = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                             "blueprint_deck_id":self.bluePrintDeckId]
            
            
            Config().callAPiFor(subApiName: addBluePrintUrl, parameter: paramDict, images: imgDict, view: self.view!, inVC: self) { (response, msg, status) in
                print(msg)
                if status {
                    let data = ["customer_id": webService_obj.Retrive("User_Id") as! String,
                                "id":self.bluePrintDeckId,
                                "name":self.deck_name]
                    
                    let controller = self.storyboard?.instantiateViewController(withIdentifier:"LowerDeckVC") as! LowerDeckVC
                    let img = response?.object(forKey: "image_url") as! String
                    controller.dictRecord = data as NSDictionary
                    controller.imgString = img
                    let name = controller.dictRecord.object(forKey: "name") as! String
                    if  name == "Storage" {
                        controller.isToRotat = false
                    }
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                else{
                    //TODO: error alert to user
                }
            }
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddDeckVC") as! AddDeckVC
            controller.pickedImg = img
            controller.push()
        }
    }
    
    @IBAction func btnCropTapped(_ sender: UIButton) {
        self.uiSetupForCrop(isCrop: btnCrop.isSelected)
    }
    
    @IBAction func btnFinishTapped(_ sender: UIButton) {
        //Just navigate user to listview
        
        if UserDefaults.standard.bool(forKey: "isFirstTimeOnView") {
            //            let controller = self.storyboard?.instantiateViewController(withIdentifier:"DeckLevelList") as! DeckLevelList
            //            controller.isToManageOrFirstTime = false
            //            UserDefaults.standard.set(false, forKey: "isFirstTimeOnView")
            //            UserDefaults.standard.synchronize()
            //            controller.push()
            _=kConstantObj.SetMainViewController("HomeVC")
        }else
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DeckLevelList.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }else
                {
                }
            }
        }
    }
    
    @IBAction func zoomImageInOut(_ sender: UIPinchGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.ended || sender.state == UIGestureRecognizerState.changed {
            
            //adjust anchor point of imageview
            imgView?.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            // self.getAnchorPointForZoom()
            isImageFullyZoomed = false
            isImageAtOriginalScale = true
            
            let oldImgSize = imgView?.bounds.size
            let currentScale:CGFloat = (imgView?.frame.size.width)!/(imgView?.bounds.size.width)!
            var newScale = currentScale * sender.scale
            
            if newScale < 1 {
                newScale = 1
            }
            else if newScale > 3 {
                newScale = 3
                isImageFullyZoomed = true
            }
            else if newScale > 1{
                isImageAtOriginalScale = false
            }
            
            let transform:CGAffineTransform = CGAffineTransform(scaleX: newScale, y: newScale)
            imgView?.transform = transform
            sender.scale = 1
            
            let currentTransform = imgView?.transform
            let newImgSize = __CGSizeApplyAffineTransform(oldImgSize!, currentTransform!)
            
            scrollViewForImg.contentSize = newImgSize
            
            imgView?.center.x = newImgSize.width/2
            imgView?.center.y = newImgSize.height/2
        }
    }
    
    func btnCropHandle1Dragged(control:UIControl!,touchEvent:UIEvent!)
    {
        let lastTouch = touchEvent.allTouches?.reversed().first
        
        
        let p2 = btnCropHandle2.center
        let p1 = lastTouch!.location(in: self.view)
        
        update(with: p1, point2: p2)
        
        visibleRectFrame = p1.frame(to: p2)
        
        _ = (lastTouch?.location(in: self.view).y)!
        
        //getting correct bounds to update frame
        //if touchYPos >= self.scrollViewForImg.frame.origin.y + 15 && touchYPos <= btnCropHandle2.frame.origin.y - 30  {
        //valid range, so update frame here.
        
        //visibleRect frame updation
        //visibleRectFrame = CGRect(x: visibleRectFrame.origin.x, y: touchYPos, width: visibleRectFrame.size.width, height: overlayView2.frame.origin.y - touchYPos)
        
        //overlayView1 frame updation
        let heightForOverlay1 = visibleRectFrame.origin.y - scrollViewForImg.frame.origin.y
        _ = CGRect(x: visibleRectFrame.origin.x, y: scrollViewForImg.frame.origin.y, width: visibleRectFrame.size.width, height: heightForOverlay1)
        //overlayView1.frame = rectForOverlay1
        
        //            //lblIndicator frame updation
        //            lblIndicator1.frame = CGRect(x: visibleRectFrame.origin.x, y: visibleRectFrame.origin.y-1, width: visibleRectFrame.size.width, height: 1)
        //
        //            //btnCropHandle 1 frame updation
        //            btnCropHandle1.frame = CGRect(x: self.scrollViewForImg.frame.size.width - 60, y: lblIndicator1.center.y - 15, width: 30, height: 30)
        //
        //            lblIndicator3.frame = CGRect(x: lblIndicator3.frame.origin.x, y: lblIndicator1.center.y - 15, width: 1, height: visibleRectFrame.size.height)
        //}
        
    }
    
    func btnCropHandle2Dragged(control:UIControl!,touchEvent:UIEvent!)
    {
        let lastTouch = touchEvent.allTouches?.reversed().first
        
        _ = (lastTouch?.location(in: self.view).x)!
        _ = (lastTouch?.location(in: self.view).y)!
        
        let p1 = btnCropHandle1.center
        let p2 = lastTouch!.location(in: self.view)
        
        
        update(with: p1, point2: p2)
        
        visibleRectFrame = p1.frame(to: p2)
        
        //getting correct bounds to update frame
        //if touchYPos <= self.scrollViewForImg.frame.size.height - 15 + self.scrollViewForImg.frame.origin.y && touchYPos >= btnCropHandle1.frame.origin.y + 30 + 30 {
        //valid range, so update frame here.
        
        //visibleRect frame updation
        //visibleRectFrame = CGRect(x: visibleRectFrame.origin.x, y: visibleRectFrame.origin.y, width: touchXPos - visibleRectFrame.size.width, height: touchYPos - visibleRectFrame.origin.y)
        
        //overlayView2 frame updation
        let heightForOverlay2 = scrollViewForImg.frame.size.height - visibleRectFrame.origin.y + visibleRectFrame.size.height
        let rectForOverlay2 = CGRect(x: visibleRectFrame.origin.x, y: visibleRectFrame.origin.y + visibleRectFrame.size.height, width: visibleRectFrame.size.width, height: heightForOverlay2)
        overlayView2.frame = rectForOverlay2
        
        //            //lblIndicator frame updation
        //            lblIndicator2.frame = CGRect(x: visibleRectFrame.origin.x, y: visibleRectFrame.origin.y + visibleRectFrame.size.height, width: visibleRectFrame.size.width, height: 1)
        //
        //            //btnCropHandle 2 frame updation
        //            btnCropHandle2.frame = CGRect(x: 30, y: lblIndicator2.center.y - 15, width: 30, height: 30)
        //}
    }
    
    
    //MARK: - Utility Methods
    func uiSetupForCrop(isCrop:Bool){
        
        //if crop is on: show overlay, hide Finish button,show crop handles, Hide crop button, show confirm button
        //crop is off: Hide overlay, show Finish button, show crop handles, show crop button , hide confirm button
        btnRightsideNavigationbar.isHidden = isCrop
        
        if !isFirstTimeOnView{
            btnFinish.isHidden = !isCrop
        }
        //btnCrop.isHidden = isCrop
        
        //Handle userinteraction on crop handles
        btnCropHandle1.isUserInteractionEnabled = isCrop
        btnCropHandle2.isUserInteractionEnabled = isCrop
        
        //Handle visibility of overlayviews
        // overlayView1.isHidden = isCrop
        //overlayView2.isHidden = isCrop
        
        //Handle userinteraction on scrollview
        scrollViewForImg.isUserInteractionEnabled = isCrop
        
        btnCrop.isSelected = !btnCrop.isSelected
    }
    
    func handleNotification(notification: Notification){
        //Take Action on Notification
        
        DispatchQueue.main.async {
            self.isFirstTimeOnView = false
            let cropFlag = notification.userInfo?["isCrop"] as! Bool
            self.uiSetupForCrop(isCrop: cropFlag)
            
            self.imageToSave = notification.userInfo?["img"] as? UIImage
            self.titleToSave = notification.userInfo?["title"] as? String
        }
    }
    
    //MARK: - ImageCrop Utility Method
    func getImage()
    {
        let img = imagePicked
        imgView = UIImageView(image: img)
        imgView?.contentMode = .scaleAspectFit
        imgView?.frame = CGRect(x: 0, y: 0, width: scrollViewForImg.frame.size.width, height: scrollViewForImg.frame.size.height)
        
        //add imageview to scrollview
        scrollViewForImg.addSubview(imgView!)
        
        //add pinch gesture to scrollview
        scrollViewForImg.addGestureRecognizer(pinchGesture)
        
        //add double tap gesture to scrollview
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(zoomImageForDoubleTaps))
        doubleTapGesture.numberOfTapsRequired = 2
        scrollViewForImg.addGestureRecognizer(doubleTapGesture)
    }
    
    func initialCropOverlaySetup()
    {
        
        //rect for visible area: will be used to add overlay to mainview
        let visibleRectHeight = self.scrollViewForImg.frame.size.height/4
        visibleRectFrame = CGRect(x: 25, y: self.scrollViewForImg.frame.size.height/2 - visibleRectHeight/2 + scrollViewForImg.frame.origin.y, width: self.scrollViewForImg.frame.size.width-50, height: visibleRectHeight)
        
        //overlay views creation
        //overlay 1
        let heightForOverlay1 = visibleRectFrame.origin.y - scrollViewForImg.frame.origin.y
        let rectForOverlay1 = CGRect(x: visibleRectFrame.origin.x, y: scrollViewForImg.frame.origin.y, width: visibleRectFrame.size.width, height: heightForOverlay1)
        overlayView1 = UIView(frame: rectForOverlay1)
        // overlayView1.backgroundColor = UIColor.black
        // overlayView1.alpha = 0.5
        
        //overlay 2
        let heightForOverlay2 = scrollViewForImg.frame.size.height - visibleRectFrame.origin.y + visibleRectFrame.size.height
        let rectForOverlay2 = CGRect(x: visibleRectFrame.origin.x, y: visibleRectFrame.origin.y + visibleRectFrame.size.height, width: visibleRectFrame.size.width, height: heightForOverlay2)
        overlayView2 = UIView(frame: rectForOverlay2)
        overlayView2.backgroundColor = UIColor.black
        overlayView2.alpha = 0.5
        
        
        let backColor = UIColor(patternImage: UIImage(named: "cropLine")!)
        let backColor2 = UIColor(patternImage: UIImage(named: "cropLineHorizontal")!)
        //setup of indicators
        lblIndicator1 = UILabel(frame: CGRect(x: visibleRectFrame.origin.x, y: visibleRectFrame.origin.y-1, width: visibleRectFrame.size.width, height: 1))
        //lblIndicator1.backgroundColor = UIColor.orange
        lblIndicator1.backgroundColor = backColor
        
        lblIndicator2 = UILabel(frame: CGRect(x: visibleRectFrame.origin.x, y: visibleRectFrame.origin.y + visibleRectFrame.size.height, width: visibleRectFrame.size.width, height: 1))
        //lblIndicator2.backgroundColor = UIColor.orange
        lblIndicator2.backgroundColor = backColor
        
        lblIndicator3 = UILabel(frame: CGRect(x: 45, y: visibleRectFrame.origin.y-1, width:1, height:visibleRectFrame.size.height ))
        //lblIndicator3.backgroundColor = UIColor.orange
        lblIndicator3.backgroundColor = backColor2
        
        lblIndicator4 = UILabel(frame: CGRect(x: visibleRectFrame.size.width - 45, y: visibleRectFrame.origin.y-1, width: 1, height: visibleRectFrame.size.height))
        //lblIndicator4.backgroundColor = UIColor.orange
        lblIndicator4.backgroundColor = backColor2
        
        
        //setup of cropHandles
        btnCropHandle1 = UIButton(type: .custom)
        btnCropHandle1.frame = CGRect(x: self.scrollViewForImg.frame.size.width - 60, y: lblIndicator1.center.y - 15, width: 30, height: 30)
        //btnCropHandle1.layer.cornerRadius = 15
        //btnCropHandle1.clipsToBounds = true
        btnCropHandle1.addTarget(self, action:#selector(btnCropHandle1Dragged(control:touchEvent:)), for: UIControlEvents.touchDragInside)
        //btnCropHandle1.backgroundColor = UIColor.orange
        let img = UIImage(named: "cropHandleImg")
        btnCropHandle1.setImage(img, for: .normal)
        
        
        btnCropHandle2 = UIButton(type: .custom)
        btnCropHandle2.frame = CGRect(x: 30, y: lblIndicator2.center.y - 15, width: 30, height: 30)
        //btnCropHandle2.layer.cornerRadius = 15
        //btnCropHandle2.clipsToBounds = true
        btnCropHandle2.addTarget(self, action: #selector(btnCropHandle2Dragged(control:touchEvent:)), for: UIControlEvents.touchDragInside)
        btnCropHandle2.setImage(img, for: .normal)
        //btnCropHandle2.backgroundColor = UIColor.orange
        
        
        
        //initially no need to show overlayView1 and overlayView2
        overlayView1.isHidden = true
        overlayView2.isHidden = true
        
        //adding overlay views to mainview
        self.view.addSubview(overlayView1)
        self.view.addSubview(overlayView2)
        
        //adding indicator labels to mainview
        self.view.addSubview(lblIndicator1)
        self.view.addSubview(lblIndicator2)
        self.view.addSubview(lblIndicator3)
        self.view.addSubview(lblIndicator4)
        
        //adding cropHandles to mainview
        self.view.addSubview(btnCropHandle1)
        self.view.addSubview(btnCropHandle2)
        
        
        //bringing the crop button in front of all views
        self.view.bringSubview(toFront: self.btnCrop)
        
        overlayView1.isHidden = true
        overlayView2.isHidden = true
        
        let fr = visibleRectFrame
        
        visibleRectFrame = fr
        
        tutorialButton.isHidden = false
    }
    
    //supporint function to get image cropped when image is not zoomed - Start
    func cropOriginalImage() -> UIImage
    {
        //crop image comes in the visibleRect and pass image to nextviewController to show image
        
        var cropArea:CGRect{
            get{
                let factor = (imgView?.image!.size.width)!/scrollViewForImg.frame.width
                let scale = 1/scrollViewForImg.zoomScale
                let imageFrame = imgView?.frame
                let x = (scrollViewForImg.contentOffset.x + visibleRectFrame.origin.x - (imageFrame?.origin.x)!) * scale * factor
                
                let y = (scrollViewForImg.contentOffset.y + visibleRectFrame.origin.y - (imageFrame?.origin.y)! - scrollViewForImg.frame.origin.y ) * scale * factor
                let width = visibleRectFrame.size.width * scale * factor
                let height = visibleRectFrame.size.height * scale * factor
                return CGRect(x: x, y: y, width: width, height: height)
            }
        }
        
        let croppedCGImage = imgView?.image?.cgImage?.cropping(to: cropArea)
        
        let croppedImage = UIImage(cgImage: croppedCGImage!)
        return croppedImage
    }
    //End
    
    //supporting function to get image cropped when image is zoomed - Start
    func cropZoomedImage() -> UIImage{
        
        let Fi_iv = self.frameForImage(image: (imgView?.image)!, imageView: imgView!)
        let Fiv_sv = imgView?.frame
        let Fi_sv = CGRect(x: Fi_iv.origin.x + (Fiv_sv?.origin.x)!, y: Fi_iv.origin.y + (Fiv_sv?.origin.y)!, width: Fi_iv.size.width, height: Fi_iv.size.height)
        let offset = scrollViewForImg.contentOffset
        
        let Fi_of = CGRect(x: Fi_sv.origin.x - offset.x, y: Fi_sv.origin.y - offset.y, width: Fi_sv.size.width, height: Fi_sv.size.height)
        
        let scale = (imgView?.image?.size.width)! / Fi_of.size.width
        
        let Fcrop_ios = CGRect(x: (visibleRectFrame.origin.x - Fi_of.origin.x) * scale, y: (visibleRectFrame.origin.y - Fi_of.origin.y - scrollViewForImg.frame.origin.y)*scale, width: visibleRectFrame.size.width * scale, height: visibleRectFrame.size.height*scale)
        
        let image = getCroppedImage(image: (imgView?.image)!, cropRect: Fcrop_ios)
        return image
    }
    
    func frameForImage(image:UIImage,imageView:UIImageView) -> CGRect {
        let imageRatio:CGFloat = image.size.width / image.size.height
        let viewRatio: CGFloat = imageView.frame.size.width / imageView.frame.size.height
        
        if imageRatio < viewRatio {
            let scale: CGFloat = imageView.frame.size.height / image.size.height
            let width: CGFloat = scale * image.size.width
            let topLeftX:CGFloat = (imageView.frame.size.width - width) * 0.5
            return CGRect(x: topLeftX, y: 0, width: width, height: imageView.frame.size.height)
        }
        else
        {
            let scale:CGFloat = imageView.frame.size.width / image.size.width
            let height:CGFloat = scale * image.size.height
            let topLeftY: CGFloat = (imageView.frame.size.height - height) *  0.5
            return CGRect(x: 0, y: topLeftY, width: imageView.frame.size.width, height: height)
        }
    }
    
    func getCroppedImage(image:UIImage,cropRect:CGRect) -> UIImage {
        let imageRef = image.cgImage?.cropping(to: cropRect)
        let croppedImage = UIImage(cgImage: imageRef!)
        return croppedImage
    }
    //End
    func getAnchorPointForZoom() -> CGPoint
    {
        let touch1 = pinchGesture.location(ofTouch: 0, in: imgView)
        let touch2 = pinchGesture.location(ofTouch: 1, in: imgView)
        var midPoint = touch1
        
        midPoint.x = ((touch2.x - touch1.x) / 2) + touch1.x
        midPoint.y = ((touch2.y - touch1.y) / 2) + touch1.y
        
        var anchorPoint = touch1
        anchorPoint.x = midPoint.x/(self.imgView?.frame.size.width)!
        anchorPoint.y = midPoint.y/(self.imgView?.frame.size.height)!
        
        return anchorPoint
    }
    
    func zoomImageForDoubleTaps()
    {
        //if image isnot zoomed fully, full zoom image
        //if image is already zoomed, zoom down to original
        
        if !isImageFullyZoomed {
            //image is not fully zoomed, so full zoom the image
            let transform:CGAffineTransform = CGAffineTransform(scaleX: 3, y: 3)
            imgView?.transform = transform
            pinchGesture.scale = 1
            
            isImageFullyZoomed = true
            isImageAtOriginalScale = false
        }
        else
        {
            //image is fully zoomed, so zoom in down to original
            let transform:CGAffineTransform = CGAffineTransform(scaleX: 1, y: 1)
            imgView?.transform = transform
            pinchGesture.scale = 1
            
            isImageFullyZoomed = false
            isImageAtOriginalScale = true
        }
        
        //set frame of scrollview's contentsize
        let currentTransform = imgView?.transform
        let newImgSize = __CGSizeApplyAffineTransform(CGSize(width: self.scrollViewForImg.frame.size.width, height: self.scrollViewForImg.frame.size.height)
            , currentTransform!)
        scrollViewForImg.contentSize = newImgSize
        imgView?.center.x = newImgSize.width/2
        imgView?.center.y = newImgSize.height/2
    }
    
}



extension UIImageView{
    
    func imageFrame()->CGRect{
        let imageViewSize = self.frame.size
        guard let imageSize = self.image?.size else{return CGRect.zero}
        let imageRatio = imageSize.width / imageSize.height
        let imageViewRatio = imageViewSize.width / imageViewSize.height
        if imageRatio < imageViewRatio {
            let scaleFactor = imageViewSize.height / imageSize.height
            let width = imageSize.width * scaleFactor
            let topLeftX = (imageViewSize.width - width) * 0.5
            return CGRect(x: topLeftX, y: 0, width: width, height: imageViewSize.height)
        }else{
            let scalFactor = imageViewSize.width / imageSize.width
            let height = imageSize.height * scalFactor
            let topLeftY = (imageViewSize.height - height) * 0.5
            return CGRect(x: 0, y: topLeftY, width: imageViewSize.width, height: height)
        }
    }
}

