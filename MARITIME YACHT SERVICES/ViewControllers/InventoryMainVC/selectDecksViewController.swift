//
//  selectDecksViewController.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac114 on 01/11/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class selectDecksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DeckLiseDelegate {
    
    var templateId : String? = nil
    var arrDecks : NSMutableArray? = NSMutableArray()
    var arrSelectedDecks = ""
    
    var deck_name = ""
    var bluePrintDeckId:String = ""
    @IBOutlet weak var lblHeaderTxt: UILabel!
    @IBOutlet weak var imgHeader: UIImageView!
    let arrImgIcons:[UIImage] = [#imageLiteral(resourceName: "storageDesk"),#imageLiteral(resourceName: "maindesk"),#imageLiteral(resourceName: "sundesk"),#imageLiteral(resourceName: "lowerdesk"),#imageLiteral(resourceName: "boatdesk")]
    let arrImageNames = ["Storage","Main Deck","Sun Deck","Lower Deck","Boat Deck"]
    
    @IBOutlet weak var tableViewDeckSelection: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set header Image and icon
        let name = self.deck_name
        self.lblHeaderTxt.text = name
        let index = arrImageNames.index(of: name) ?? 0
        let iconImage = arrImgIcons[index]
        self.imgHeader.image = iconImage
        
        // Do any additional setup after loading the view.
        print("Selected template id: " + templateId!)
        
        getDecksForTemplateId(templateId!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDoneTapped(_ sender: UIButton) {
        if (self.arrDecks?.count)! > 0 {
            
            guard arrSelectedDecks != "" else {
                //TODO: show alert to user to select one of the deck
                let alertController = UIAlertController(title: "Sorry!", message: "Please select template.", preferredStyle: .alert)
                let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    alertController.dismiss(animated: true, completion: nil)
                })
                alertController.addAction(alertActionOK)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            // call webservice to upload selected templates and on success response navigate to list view of decks
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "deck_ids":arrSelectedDecks,
                        "blueprint_deck_id":self.bluePrintDeckId]
            
            webService_obj.fetchDataFromServer(header:"use_template_deck", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    //                    let controller = self.storyboard?.instantiateViewController(withIdentifier:"DeckLevelList") as! DeckLevelList
                    //                    controller.isforFirstTime = false
                    //                    controller.isToManageOrFirstTime = true
                    //                    controller.push()
                    
                    
                    let data = ["customer_id": webService_obj.Retrive("User_Id") as! String,
                                "id":self.bluePrintDeckId,
                                "name":self.deck_name]
                    
                    let controller = self.storyboard?.instantiateViewController(withIdentifier:"LowerDeckVC") as! LowerDeckVC
                    let img = responce.object(forKey: "image") as! String
                    controller.dictRecord = data as NSDictionary
                    controller.imgString = img
                    let name = controller.dictRecord.object(forKey: "name") as! String
                    if  name == "Storage" {
                        controller.isToRotat = false
                    }
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
            }
        }else{
           _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func backToTemplates(_ sender: UIButton) {
        self.navigationController?.popViewController(animated:true)
    }
    
    //MARK: API Methods
    func getDecksForTemplateId(_ templateId:String)
    {
        
        let paramDict = ["customer_id":webService_obj.Retrive("User_Id") as! String ,
                         "template_id":templateId,
                         "name":self.deck_name]
        
        Config().fetchDataFromServer(header: "template_deck_list", withParameter: paramDict as NSDictionary, inVC: self, showHud: true) { (responseData, msg, status) in
            if status {
                self.arrDecks = NSMutableArray(array: responseData.object(forKey: "data") as! NSArray)
                DispatchQueue.main.async {
                    self.tableViewDeckSelection.reloadData()
                }
            }else{
                
            }
         }
     }
    
    //MARK: Tableview datasource and delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (arrDecks?.count)!
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "deckSelectionCell") as! SelectDecksTableViewCell
        
        let dictRecord = arrDecks!.object(at: indexPath.row) as! NSDictionary
        
        cell.cellSetupForRecord(dict: dictRecord)
        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell as UITableViewCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 180.0
        
    }
    
    //MARK: Tableview cell delegate
    func btnDeckListCheckboxTapped(selectedDeckId: String) {
        
        arrSelectedDecks = selectedDeckId
        
        //check if id in array, remove from array else add into array
//        if let indexOfId = arrSelectedDecks.index(of: selectedDeckId){
//            
//            // id already avialbe, so remove it
//            arrSelectedDecks.remove(at: indexOfId)
//        }
//        else{
//            
//            //id not found, so append it
//            arrSelectedDecks.append(selectedDeckId)
//            
//        }
        print(arrSelectedDecks)
    }
}
