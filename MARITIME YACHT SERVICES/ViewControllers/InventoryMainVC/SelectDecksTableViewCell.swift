//
//  SelectDecksTableViewCell.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac114 on 01/11/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import SDWebImage

protocol DeckLiseDelegate {
    
    func btnDeckListCheckboxTapped(selectedDeckId: String) -> Void
    
}


class SelectDecksTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDeckName: UILabel!
    @IBOutlet weak var imgViewDeckImg: UIImageView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var btnCheckbox: UIButton!
    
    var delegate : DeckLiseDelegate?
    var selectedDict : NSDictionary?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if selected{
            btnCheckbox.setImage(UIImage(named: "squareselect"), for: .normal)
            
            delegate?.btnDeckListCheckboxTapped(selectedDeckId: selectedDict?.object(forKey: "id") as! String)
 
        }else{
            btnCheckbox.setImage(UIImage(named: "squareunselect"), for: .normal)
        }
    }
    
    @IBAction func btnCheckBoxTapped(_ sender: Any) {
        
//        let btn = sender as! UIButton
//        
//        btn.isSelected = !btn.isSelected
        
        //delegate?.btnDeckListCheckboxTapped(selectedDeckId: selectedDict?.object(forKey: "id") as! String)
        
    }
    
    func cellSetupForRecord(dict: NSDictionary) {
    
        selectedDict = NSDictionary(dictionary: dict)
        
        // Deck name
        lblDeckName.text = dict.object(forKey: "name") as! String
        
        // Deck image
        let imageSubUrl = dict.object(forKey: "image") as! String
        
        let url:NSURL = NSURL(string :"\(imageURL)\(imageSubUrl)")!
        
        
        self.imgViewDeckImg.sd_setImage(with: url as URL, placeholderImage: nil, options: SDWebImageOptions(rawValue: 0)) { (img, err, type, url) in
            self.imgViewDeckImg.image = img
            self.loadingView.stopAnimating()
            self.loadingView.isHidden = true
        }

    
    }

}
