//
//  GroupExpensesListVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 24/11/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class CustomeGroupExpenseCell: UITableViewCell {
    @IBOutlet weak var expenseTitle: UILabel!
    @IBOutlet weak var expenseDate: UILabel!
    @IBOutlet weak var expensesPrice: UILabel!
}

class GroupExpensesListVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var expeseTableView: UITableView!
    var expensesData = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
          getGroupExpensesWebService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - Table view Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.expensesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomeGroupExpenseCell", for: indexPath) as! CustomeGroupExpenseCell
        
        let data = self.expensesData[indexPath.row]
        
        cell.expenseTitle.text = data["name"]
        cell.expenseDate.text = data["date"]
        
        
        if data["amount"] == "" || data["amount"] == "0"
        {
             cell.expensesPrice.text = "$0.00"
            
        }else
        {
            let amt = (data["amount"]! as NSString).doubleValue
        
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value:amt))
            
            cell.expensesPrice.text =  ("$") +  (formattedNumber!)
         }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ExpenseDetailVC") as! ExpenseDetailVC
         let data = self.expensesData[indexPath.row]
        
        controller.group_id = data["id"]!
        controller.push()
    }
    
    @IBAction func bntAddClick(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"GroupExpensesVC") as! GroupExpensesVC
        controller.isFromGroup = "true"
         controller.push()
    }
    
    // MARK: - Web-Service Methods
    func getGroupExpensesWebService() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "group_expense_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.expensesData = response.value(forKey: "data") as! [[String: String]]
                self.expeseTableView.reloadData()
            }
        }
    }
}
