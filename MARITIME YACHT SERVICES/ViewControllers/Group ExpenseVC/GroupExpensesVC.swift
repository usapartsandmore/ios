//
//  GroupExpensesVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 30/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class GroupExpensesVC: UIViewController,UITextViewDelegate {
  
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtComments: UITextView!
    @IBOutlet weak var btnSave: UIButton!
    
    var selectedExpenses = [String]()
    
    var isFromGroup = ""
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetPlaceHoder()
        
        txtComments.text = "Comment"
        txtComments.textColor = UIColor.lightGray
        
        self.btnSave.layer.masksToBounds = true
        self.btnSave.layer.cornerRadius = 5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetPlaceHoder()
    {
        txtDate.addPaddingView(width: 10, onSide: 0)
        txtName.addPaddingView(width: 10, onSide: 0)
        txtComments.addPaddingView(width: 10, onSide: 0)
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - TextView Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Comment"
            textView.textColor = UIColor.lightGray
        }
    }

    
     @IBAction func btnDateClick(_ sender: Any)
     {
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date, doneBlock: {
            picker,index,value in
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = index as! Date
            self.txtDate.text = dateFormatter.string(from: date)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        datePicker?.show()
    }
    
    @IBAction func saveBtnClick(_ sender: Any) {
        
        if txtName.text! .isEmpty
        {
            self.AlertMessage("Please enter name.")
        }
        else if txtDate.text! .isEmpty
        {
            self.AlertMessage("Please select date.")
        }
//        else if txtComments.textColor == UIColor.lightGray
//        {
//            self.AlertMessage("Please enter comment.")
//        }
        else
        {
            self.addGroupExpensesWebService()
        }
    }
    
    // MARK: - Web-Service Methods
    func addGroupExpensesWebService() {
        
        var comments = ""
        if txtComments.textColor == UIColor.lightGray
        {
            comments = ""
        }else{
            comments = self.txtComments.text
        }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "date":self.txtDate.text!,
                    "name":self.txtName.text!,
                    "expenses":self.selectedExpenses,
                    "comment":comments]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "add_group_expense", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                    if self.isFromGroup == "true"
                    {
                    _ = self.navigationController?.popViewController(animated:true)
                    }else{
                        _=kConstantObj.SetMainViewController("ExpensesMainVC")
                    }
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
}
