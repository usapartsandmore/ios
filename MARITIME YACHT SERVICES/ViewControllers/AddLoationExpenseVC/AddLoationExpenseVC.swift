 //
 //  AddLoationExpenseVC.swift
 //  MARITIME YACHT SERVICES
 //
 //  Created by Rachit Sharma on 23/11/17.
 //  Copyright © 2017 OctalSoftware. All rights reserved.
 //
 
 import UIKit
 import GooglePlaces
 import GooglePlacePicker
 import GoogleMaps
 
 class PlaceCell: UITableViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeTitleLabel: UILabel!
    @IBOutlet weak var placeSubtitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    private static let backColor = UIColor(white: 241.0/255, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 2.5
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        containerView.layer.shadowRadius = 1.0
        containerView.layer.shadowOpacity = 0.75
        containerView.backgroundColor = UIColor.white
        contentView.backgroundColor = PlaceCell.backColor
        //selectedBackgroundView = UIView()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
 }
 
 
 class AddLoationExpenseVC: UIViewController , UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate{
    
    var locationManger = CLLocationManager()
    var currentlat = 0.0
    var currentlong = 0.0
    var currentAddress = ""
    
    @IBOutlet weak var txt_Search: UITextField!
    @IBOutlet weak var tblView_Search: UITableView!
    
    var PostData = [[String: String]]()
    var onAdressSelection: (([String: String])->Void)?
    var myLocationData: [String: String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManger.delegate = self
        
        //  txt_Search.addPaddingView(width: 10, onSide: 0)
        txt_Search.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.tblView_Search.rowHeight = UITableViewAutomaticDimension;
        self.tblView_Search.estimatedRowHeight = 44.0;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(AddLoationExpenseVC.locationAvailable(_:)), name: NSNotification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil)
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        let keyword: String = txt_Search.text!.replacingOccurrences(of: " ", with: "+")
        let todoEndpoint: String =  "https://maps.googleapis.com/maps/api/place/textsearch/json?query=\(keyword)&key=\(GoogleAPI)"
        print(todoEndpoint)
        
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let request = URLRequest(url: url)
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            guard let data = data else {
                return
            }
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    //let routes = json["routes"] as! NSArray
                    
                    let returnedPlaces: NSArray? = json["results"] as? NSArray
                    
                    if returnedPlaces != nil {
                        
                        var newPostData = [[String: String]]()
                        
                        if (self.myLocationData != nil)
                        {
                            newPostData.append(self.myLocationData)
                        }
                        
                        for index in 0..<returnedPlaces!.count {
                            
                            if let returnedPlace = returnedPlaces?[index] as? NSDictionary {
                                
                                var placeName = ""
                                var formated_address = ""
                                var placeId = ""
                                var lat = 0.0
                                var long = 0.0
                                
                                if let name = returnedPlace["name"] as? NSString {
                                    placeName = name as String
                                }
                                
                                if let adrress = returnedPlace["formatted_address"] as? NSString {
                                    formated_address = adrress as String
                                }
                                if let id = returnedPlace["place_id"] as? NSString {
                                    placeId = id as String
                                }
                                
                                if let geometry = returnedPlace["geometry"] as? [String: Any] {
                                    if let location = geometry["location"] as? [String: Double]{
                                        lat = location["lat"] ?? 0.0
                                        long = location["lng"] ?? 0.0
                                        
                                        if lat == 0 || long == 0{
                                            print("found")
                                        }
                                    }
                                }
                                
                                // self.PostData.updateValue("place_name", forKey: placeName)
                                
                                let dict = ["place_name":placeName,
                                            "address":formated_address,
                                            "place_id":placeId,
                                            "lat": "\(lat)",
                                    "long": "\(long)"]
                                newPostData.append(dict)
                            }
                        }
                        
                        DispatchQueue.main.sync{
                            self.PostData = newPostData
                            self.tblView_Search.reloadData()
                        }
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
    
    // MARK: - IBAction Button Click Methods
    @IBAction func btn_Back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_CloseSearch(_ sender: Any) {
        
        txt_Search.text = ""
        
        self.PostData.removeAll()
        
        if myLocationData != nil{
            
            self.PostData.append(self.myLocationData)
            self.tblView_Search.reloadData()
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PostData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell") as! PlaceCell
        let data = PostData[indexPath.row]
        cell.placeTitleLabel?.text = data["place_name"]
        cell.placeSubtitleLabel?.text = data["address"]
        
        cell.placeImageView.image = #imageLiteral(resourceName: "mapicon")
        
        if indexPath.row == 0{
            cell.placeImageView.image = #imageLiteral(resourceName: "locationmap")
        }else{
            cell.placeImageView.image = #imageLiteral(resourceName: "mapicon")
        }
        
        return cell
    }
    
    
    // MARK:  UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        let data = PostData[indexPath.row]
        
        if indexPath.row == 0{
            let address = data["address"]  as? String ?? ""
            let place_name = data["place_name"] as? String ?? ""
            print(place_name)
            PostData[indexPath.row]["place_name"] = address+place_name
            onAdressSelection?(PostData[indexPath.row])
        }else{
            onAdressSelection?(data)
        }
    }
    
    
    // MARK: Location Available Method
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        
        switch status {
        case .authorizedAlways:
            
            manager.startUpdatingLocation()
            
        case .authorizedWhenInUse:
            
            manager.startUpdatingLocation()
            
        case .notDetermined:
            
            manager.requestWhenInUseAuthorization()
            
        case .denied, .restricted:
            
            let alert = UIAlertController.init(title: "Access Denied", message: "Please allow access for location from device settings.", preferredStyle: .alert)
            
            let actionCancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                alert.dismiss(animated: true, completion: nil)
            })
            
            let actionSettings = UIAlertAction.init(title: "Settings", style: .default, handler: { (action) in
                
                UIApplication.shared.open(URL(string:UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                alert.dismiss(animated: true, completion: nil)
                
            })
            
            alert.addAction(actionCancel)
            alert.addAction(actionSettings)
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func locationAvailable(_ notification:Notification) -> Void
    {
        let userInfo = notification.userInfo as! Dictionary<String,String>
        load.hide(delegate: self)
        let  Currentlats = userInfo["latitude"]!
        let  Currentlots = userInfo["longitude"]!
        
        currentlat =    Double(Currentlats)!
        currentlong =    Double(Currentlots)!
        _ = NSMutableString(string: userInfo["postalCode"]!)
        //str_ZipCode = postalCode as String
        let str = "\(userInfo["name"]!),\(userInfo["Area"]!), \(userInfo["city"]!), \(userInfo["postalCode"]!), \(userInfo["state"]!)"
        
        currentAddress = "\(userInfo["Area"]!), \(userInfo["city"]!), \(userInfo["postalCode"]!), \(userInfo["state"]!)"
        //currentCity = userInfo["city"]!
        
        
        NSLog("Location Data -->\(str) Lat--> \(currentlat)  Long--> \(currentlong)")
        
        self.myLocationData = ["place_name":"Current Location","address":currentAddress, "place_id":"", "lat": "\(Currentlats)", "long": "\(Currentlots)"]
        
        if (self.myLocationData != nil)
        {
            PostData.append(myLocationData)
        }
         
        self.tblView_Search.reloadData()
        
        // NSNotification.removeObserver("")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil);
    }
 }
