//
//  CategoriesVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 28/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Text: UILabel!
    @IBOutlet weak var img_Categories: UIImageView!
    
}

class CategoriesVC: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var constSearchCollectionV: NSLayoutConstraint!
    @IBOutlet var collectionViewCategories: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var Btn_topSearch: UIButton!
    var filteredData = [[String:Any]]()
    var webServiceData = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.constSearchCollectionV.constant = 8.0
        // Do any additional setup after loading the view.
       
        self.searchBar.tag = 0
        self.searchBar.isHidden = true
        
        self.getCategoriesWebServiceMethod()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCartClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    func getCategoriesWebServiceMethod () {
        webService_obj.fetchDataFromServer(header:"productCat", withParameter: [:], inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                 self.webServiceData = responce.value(forKey: "data") as! [[String:Any]]
                  self.filteredData = self.webServiceData
                 self.collectionViewCategories.reloadData()
             }
        }
    }
    
    @IBAction func btnCameraClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Product", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ProductScanVC")
        controller.push()
    }

    
    @IBAction func btnSearchClick(_ sender: UIButton) {
        if self.searchBar.tag == 0
        {
            self.constSearchCollectionV.constant = 44.0
            self.searchBar.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchBar.tag = 1
            self.Btn_topSearch.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
            self.constSearchCollectionV.constant = 8.0
            self.searchBar.text = ""            
            self.searchBar.isHidden = true
            self.searchBar.endEditing(true)
            self.Btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchBar.tag = 0
            
            collectionViewCategories.reloadData()
        }
    }
     
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        let data = filteredData[indexPath.row]
        
        let str = data["image"]!
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
        cellA.img_Categories.af_setImage(withURL: url as URL)
        cellA.lbl_Text.text = data["name"] as? String
        cellA.img_Categories.contentMode = .scaleAspectFill
        cellA.img_Categories.clipsToBounds = true
        
        return cellA
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let width = (self.view.frame.size.width - 40) / 2
        let height = width * 0.8
        return CGSize(width: width, height: height);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10.0
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
         let data = filteredData[indexPath.row]
        
        let st = UIStoryboard.init(name: "Product", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"SubCategoryVC") as! SubCategoryVC
        controller.cat_id = data["id"] as! String
        controller.cat_name = data["name"] as! String
        controller.cat_img_str = data["image"] as! String
        controller.push()
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData.removeAll()
        
        if searchText.isEmpty {
            //searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            self.filteredData = webServiceData
        }
        else
        {
            if searchText.length >= 3
            {
                let arr = webServiceData.filter({ (fruit) -> Bool in
                    return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
                })
                filteredData = arr
            }
        }
        collectionViewCategories.reloadData()
    }

}
