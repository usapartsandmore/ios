//
//  AddDebitCardVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 08/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class AddDebitCardVC: UIViewController {


    @IBOutlet var btn_SaveCard: UIButton!
    @IBOutlet var txt_SecurityCode: UITextField!
    @IBOutlet var txt_Year: UITextField!
    @IBOutlet var txt_Month: UITextField!
    @IBOutlet var txt_CardNum: UITextField!
    @IBOutlet var txt_Name: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSaveCardClick(_ sender: UIButton) {
        
    }
    @IBAction func btnDonePressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
