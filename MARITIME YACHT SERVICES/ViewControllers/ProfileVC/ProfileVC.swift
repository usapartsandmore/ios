//
//  ProfileVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 03/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit

class ProfilecustomeCell: UITableViewCell {
    
    @IBOutlet var lbl_Email: UILabel!
}

class ProfileVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tableview: UITableView!
    @IBOutlet var lbl_ServiceBooked: UILabel!
    @IBOutlet var lbl_CurrentInventory: UILabel!
    @IBOutlet var lbl_TotalOrder: UILabel!
    @IBOutlet var lbl_MemberFrom: UILabel!
    @IBOutlet var lbl_userName: UILabel!
    @IBOutlet var bigBackProfileImg: UIImageView!
    @IBOutlet var img_UserProfile: UIImageView!
    @IBOutlet var viewBlackFordeletePopup: UIView!
    @IBOutlet var viewDeletePopup: UIView!
    var kConstantObj = kConstant()

    
    var arrNames = NSArray()
    var arrImgs  = NSArray()
    var data = [String:Any] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.tableFooterView = UIView()
        viewBlackFordeletePopup.isHidden=true
        viewDeletePopup.isHidden=true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // address list web service method
       self.geProfileDetailWebService()
        
    }
    
    func geProfileDetailWebService()
    {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"get_profile", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                 self.data = responce.value(forKey: "data") as! [String:Any]
                self.lbl_ServiceBooked.text = self.data["service_booked"] as? String
                self.lbl_CurrentInventory.text = self.data["current_inventory"] as? String
                self.lbl_TotalOrder.text = self.data["total_orders"] as? String
                self.lbl_MemberFrom.text = self.data["created"] as? String
                self.lbl_userName.text = "\(String(describing: self.data["first_name"]as! String)) \(String(describing: self.data["last_name"]as! String))"
                let str = self.data["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                self.img_UserProfile.af_setImage(withURL: url as URL)
                self.bigBackProfileImg.af_setImage(withURL: url as URL)
                
                let isvendor = responce.value(forKey: "is_vendor") as! String
                
                if isvendor == "1"{
                    
                    self.arrNames = ["Account","Change Password","Addresses","Switch Vendor"]
                    self.arrImgs = ["account","password","address","switch"]
                    
                    //self.arrNames = ["Account","Wallet","Change Password","Addresses","Switch Vendor"]
                    //self.arrImgs = ["account","wallet","password","address","switch"]
                }else{
                    
                    self.arrNames = ["Account","Change Password","Addresses"]
                    self.arrImgs = ["account","password","address"]
                    
                  //  self.arrNames = ["Account","Wallet","Change Password","Addresses"]
                  //  self.arrImgs = ["account","wallet","password","address"]
                }
                
                self.tableview.reloadData()
            }
        }
    }
    
    @IBAction func btnDeleteProfile(_ sender: UIButton) {
        viewBlackFordeletePopup.isHidden = false
        viewDeletePopup.isHidden = false
    }
    @IBAction func btnConfirmDeletePressed(_ sender: UIButton) {
        //confirm profile delete Webservice Call
        viewBlackFordeletePopup.removeFromSuperview()
        viewDeletePopup.removeFromSuperview()
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"customer_deleted", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                defaults.removeObject(forKey: kAPPLOGIN_IN)
                self.kConstantObj = kConstant()
                
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
                
                let mainVcIntial = self.kConstantObj.SetIntialMainViewController("IntroScreenVC")
                UIApplication.shared.delegate?.window??.rootViewController = mainVcIntial
                
            }
        }
  
    }
    @IBAction func btnCancelPressed(_ sender: UIButton) {
        viewBlackFordeletePopup.isHidden=true
        viewDeletePopup.isHidden=true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilecustomeCell", for: indexPath) as! ProfilecustomeCell
        
        if indexPath.row == 0
        {
            cell.lbl_Email.text = self.data["email"] as? String
        }
//        else if indexPath.row == 1
//        {
//            cell.lbl_Email.text = "Master ending xx92211"
//        }
        else
        {
            cell.lbl_Email.text = ""
        }
        
        let imgMenu : UIImageView = cell .viewWithTag(10) as! UIImageView
        
        let lblNames : UILabel = cell.viewWithTag(11) as! UILabel
        //let lblDetail : UILabel = cell.viewWithTag(12) as! UILabel
        imgMenu.image = UIImage(named: arrImgs[indexPath.row] as! String)
        lblNames.text = arrNames[indexPath.row] as? String
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.contentView.backgroundColor = UIColor.clear
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
        //let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
        // cell.contentView.backgroundColor = imgUser.backgroundColor
        if indexPath.row == 0 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"ProfileAccountInfoVC")
            controller?.push()
        }
//        else if indexPath.row == 1 {
//            // let controller = self.storyboard?.instantiateViewController(withIdentifier:"WalletVC")
//            // controller?.push()
//            //Show alert Message
//            let alertMessage = UIAlertController(title: "MYS", message:"This functionality is under development.", preferredStyle: .alert)
//            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//                
//            }
//            alertMessage.addAction(action)
//            self.present(alertMessage, animated: true, completion: nil)
//        }
        else if indexPath.row == 1 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"ChangePasswordVC")
            controller?.push()
        }
        else if indexPath.row == 2 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"ProfileAddressVC")
            controller?.push()
        }
        else if indexPath.row == 3 {
            let st = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "SwitchVendorVC")
             vc.push()
        }
        
    }
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        
        // _ = kConstantObj.SetIntialMainViewController("HomeVC")
        sideMenuVC.toggleMenu()
        
    }
    
    
}
