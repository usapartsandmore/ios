//
//  ChangePasswordVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 08/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet var txt_CurrentPwd: UITextField!
    @IBOutlet var txt_NewPwd: UITextField!
    @IBOutlet var txt_CNewPwd: UITextField!
    var PostData = [String: String]()
    
    var sharing_user = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }

    @IBAction func btnSaveClick(_ sender: UIButton) {
        self.textFieldCheck()
    }
    // MARK: - CallLoginService
    func textFieldCheck()
    {
        self.view.endEditing(true)
        if txt_CurrentPwd.text! .isEmpty
        {
            self.AlertMessage("Please enter current password.")
        }
        else if txt_NewPwd.text! .isEmpty
        {
            self.AlertMessage("Please enter new password.")
        }
        else if txt_CNewPwd.text! .isEmpty
        {
            self.AlertMessage("Please enter confirm password.")
        }
        else if (txt_NewPwd.text!.length) < 6
        {
            self.AlertMessage("Minimum of 6 character needed for password.")
        }
        else if txt_NewPwd.text! != txt_CNewPwd.text!
        {
            self.AlertMessage("New password must match confirm password.")
        }
        else
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "old_password":txt_CurrentPwd.text!,
                        "new_password":txt_NewPwd.text!]
            
            webService_obj.fetchDataFromServer(header:"change_password", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    let data = responce.value(forKey: "data") as! NSDictionary
                    
                   let email = data["email"] as? String
                    
                    if self.sharing_user == "true"
                    {
                        let st = UIStoryboard.init(name: "Main", bundle: nil)
                        let vc = st.instantiateViewController(withIdentifier:"SignUpVC") as! SignUpVC
                        
                        vc.isfromFacebook = false
                        vc.email_id = email!
                        self.navigationController?.pushViewController(vc, animated: true)
                        
           
                        
                    }else{
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "Alert", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                    }
                }
            }
        }
    }

}
