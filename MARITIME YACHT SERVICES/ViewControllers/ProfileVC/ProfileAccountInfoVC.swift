//
//  ProfileAccountInfoVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 09/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import AlamofireImage

extension ProfileAccountInfoVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        
        let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        pickedImage = self.fixImageOrientation(img!)
        img_Profile.image = pickedImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func fixImageOrientation(_ image: UIImage)->UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(at: .zero)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? image
    }
}

class VendorEditDetailCell : UITableViewCell{
    @IBOutlet var txtVendor: UITextField!
    @IBOutlet var txtAccNumber: UITextField!
    @IBOutlet var btnVendor: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
}

class ProfileAccountInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var img_Profile: UIImageView!
    @IBOutlet var txt_FName: UITextField!
    @IBOutlet var txt_LName: UITextField!
    @IBOutlet var txt_Dob: UITextField!
    @IBOutlet var txt_BoatName: UITextField!
    @IBOutlet var txt_Gender: UITextField!
    @IBOutlet var txt_MMSI: UITextField!
    @IBOutlet var txt_VasselSize: UITextField!
    
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtFeet: UITextField!
    //@IBOutlet var txt_Vendor: UITextField!
   // @IBOutlet var txt_AccNumber: UITextField!
     let imagePicker = UIImagePickerController()
    var Dict_Profile : NSMutableDictionary!
    var pickedImage: UIImage!
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var tableViewHeightCont: NSLayoutConstraint!
    
    var vendorsDetails = [[String:String]]()
    var selectedVendorsDetails = [[String:String]]()
    
    var arrVendorName = [String]()
    var arrSelectedVendorName = [String]()
    
    var vendorAccountArr = [String:String]()
    var vendorSelectedAccountArr = [String]()
    
    var vendorsIDArr = [String:String]()
    var numberOfRows : Int = 1
    
    var arrRecSelectedVendorName = [String:String]()
    
    var isFromMinusClick = ""
    
    var email = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUserDetailWebService()
        
        isFromMinusClick = "false"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: DOB Button Click
    @IBAction func btnDOBClick(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date, doneBlock: {
            picker,index,value in
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let date = index as! Date
            
            let dateOfBirth = date
            let today = NSDate()
            let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])
            if age.year! < 18 {
                self.AlertMessage("Date of birth should not be less than 18 years.")
                // user is under 18
            }else{
                self.txt_Dob.text = dateFormatter.string(from: date)
            }
            
 
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        //        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        datePicker?.maximumDate = Date()
        datePicker?.show()
        
    }
    
    // MARK: Feet Button Click
    @IBAction func btnFeetClick(_ sender: UIButton) {
        self.view.endEditing(true)
        ActionSheetStringPicker.show(withTitle: "Select Vessel Type", rows: ["Feet","Meters"], initialSelection: 0, doneBlock: {
            picker,index,value in
            self.txtFeet.text = value as? String
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
    }
    
    // MARK: Gender Button Click
    @IBAction func btnGenderClick(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select Gender", rows: ["Male","Female"], initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txt_Gender.text = value as? String
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
        
    }
    
    
    @IBAction func btnProfileImageClick(_ sender: UIButton) {
         self.AlertController()
    }

    // MARK: - Method Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imageCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imageGallery()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnEditProfileClick(_ sender: UIButton) {
        if img_Profile.image == UIImage.init(named:"complete_profile")
        {
            self.AlertMessage("Please select image.")
        }
       else if txt_FName.text! .isEmpty
        {
            self.AlertMessage("Please enter first name.")
        }
        else if txt_LName.text! .isEmpty
        {
            self.AlertMessage("Please enter last name.")
        }
        else if txt_Dob.text! .isEmpty
        {
            self.AlertMessage("Please select date of birth.")
        }
        else if txtPhoneNumber.text! .isEmpty
        {
            self.AlertMessage("Please enter Phone number.")
        }
        else if txt_BoatName.text! .isEmpty
        {
            self.AlertMessage("Please enter boat name.")
        }
        else if txt_Gender.text! .isEmpty
        {
            self.AlertMessage("Please select gender.")
        }
        else if txt_MMSI.text! .isEmpty
        {
            self.AlertMessage("Please enter MMSI.")
        }
        else if txt_VasselSize.text! .isEmpty
        {
            self.AlertMessage("Please enter vassel size.")
        }
        else if txtFeet.text! .isEmpty
        {
            self.AlertMessage("Please select vessel size type.")
        }
        else if self.vendorsIDArr.count == 0 && self.vendorAccountArr.count != 0
        {
            self.AlertMessage("Please select vendor.")
        }
        else if self.vendorAccountArr.count == 0 && self.vendorsIDArr.count != 0
        {
            self.AlertMessage("Please enter vendor account number.")
        }

        else
        {
            self.updateUserDetailWebService()
        }
        
    }
    // MARK: - CallWebService
    func updateUserDetailWebService()
    {
        
        var arrVID = [String]()
        for indexPath in 0..<self.numberOfRows
        {
            let dict = self.vendorsIDArr["\(indexPath)"]
            if dict != nil {
                arrVID.append(dict!)
            }
        }
        
        var arrVaccount = [String]()
        for indexPath in 0..<self.numberOfRows
        {
            let dict = self.vendorAccountArr["\(indexPath)"]
            if dict != nil {
                arrVaccount.append(dict!)
            }
        }

        let image = UIImagePNGRepresentation(self.img_Profile.image!) as NSData?
        self.pickedImage = UIImage(data: image! as Data)
        
        //Convert in date format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let myDate = dateFormatter.date(from: self.txt_Dob.text!)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dob =  dateFormatter.string(from: myDate!)
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "email":self.email,
                    "first_name":txt_FName.text!,
                    "last_name":txt_LName.text!,
                    "dob":dob,
                    "boat_name":txt_BoatName.text!,
                    "gender":txt_Gender.text ?? "",
                    "mmsi":txt_MMSI.text ?? "",
                    "vessel_size":txt_VasselSize.text!,
                    "vessel_size_type":self.txtFeet.text!,
                    "contact_number":self.txtPhoneNumber.text!,
                    "vendors_id":arrVID ?? [],
                    "vendors_account_number":arrVaccount ?? []]
        
        
        webService_obj.profileDataFromServer(header:"edit_customer", withParameter: PostData as NSDictionary, coverdata: self.pickedImage.fixed!, inVC: self, showHud: true) { (responce,staus) in
            if staus{
                
                //Show alert Message
                let alertMessage = UIAlertController(title: "Alert", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    _ = self.navigationController?.popViewController(animated: true)
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    func getUserDetailWebService()
    {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"get_profile", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                let data = responce.value(forKey: "data") as! NSDictionary
                
                self.txt_FName.text = data["first_name"] as? String
                self.txt_LName.text = data["last_name"] as? String
 
                //Convert in date format
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let myDate = dateFormatter.date(from: (data["dob"] as? String)!)
                dateFormatter.dateFormat = "MMM dd, yyyy"
                self.txt_Dob.text =  dateFormatter.string(from: myDate!)
                self.txt_BoatName.text = data["boat_name"] as? String
                self.txt_Gender.text = data["gender"] as? String
                self.txtPhoneNumber.text = data["contact_number"] as? String
                self.txtFeet.text = data["vessel_size_type"] as? String
                self.txt_MMSI.text = data["mmsi"] as? String
                self.txt_VasselSize.text = data["vessel_size"] as? String
                
                self.email = (data["email"] as? String)!
                
                let str = data["image"] as! String
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                self.img_Profile.af_setImage(withURL: url as URL)
                
                let isvendor = responce.value(forKey: "is_vendor") as! String
                 if isvendor == "1"{
                  self.selectedVendorsDetails = data["vendor"] as! [[String:String]]
                    self.arrSelectedVendorName = self.selectedVendorsDetails.map{ $0["name"]! }
                    self.vendorSelectedAccountArr = self.selectedVendorsDetails.map{ $0["account_number"]! }
                    self.numberOfRows = self.arrSelectedVendorName.count
                    self.tableView.reloadData()
                    self.tableView.layoutIfNeeded()
                    self.tableViewHeightCont.constant = self.tableView.contentSize.height
                }

                
//                let arr =  data["vendor"] as! [[String:String]]
//                let arr2 = arr.map{ $0["name"]! }
//                let arr3 = arr.map{ $0["account_number"]! }
//                
//                let strVname = (arr2.map{String($0)}).joined(separator: ",")
//                let strANumbers = (arr3.map{String($0)}).joined(separator: ",")
//                
//                self.txt_Vendor.text = strVname
//                self.txt_AccNumber.text = strANumbers
                
                
                self.vendorListWebService()
            }
        }
    }
    
    
    // MARK: UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VendorEditDetailCell", for: indexPath) as! VendorEditDetailCell
        
        
        if self.selectedVendorsDetails.count != 0 && self.selectedVendorsDetails.count == numberOfRows {
            if isFromMinusClick == "false"
            {
                cell.txtVendor.text = self.arrSelectedVendorName[indexPath.row]
                cell.txtAccNumber.text = self.vendorSelectedAccountArr[indexPath.row]
                
                self.vendorsIDArr["\(indexPath.row)"] =  self.selectedVendorsDetails[indexPath.row]["id"]!
                self.vendorAccountArr["\(indexPath.row)"] = self.selectedVendorsDetails[indexPath.row]["account_number"]!
                
                self.arrRecSelectedVendorName["\(indexPath.row)"] =  self.selectedVendorsDetails[indexPath.row]["name"]!
                
                NSLog("Account Numbers data-->\(self.vendorAccountArr)")
                NSLog("Vendor Name data-->\(self.vendorsIDArr)")
            }
        }
        
        
        cell.txtVendor.text =   self.arrRecSelectedVendorName["\(indexPath.row)"] ?? ""
        cell.txtAccNumber.text = self.vendorAccountArr["\(indexPath.row)"] ?? ""
        
      
//        if numberOfRows == self.vendorsIDArr.count && numberOfRows == self.vendorAccountArr.count{
//            var arrSelectedVName = [String]()
//            for indexPath in 0..<self.numberOfRows
//            {
//                let dict = self.arrRecSelectedVendorName["\(indexPath)"]
//                arrSelectedVName.append(dict!)
//            }
//            
//            var arrSelectedVaccount = [String]()
//            for indexPath in 0..<self.numberOfRows
//            {
//                let dict =  self.vendorAccountArr["\(indexPath)"]
//                arrSelectedVaccount.append(dict!)
//            }
//            
//            cell.txtVendor.text =   arrSelectedVName[indexPath.row]
//            cell.txtAccNumber.text = arrSelectedVaccount[indexPath.row]
//        }
//        else
//        {
//            if indexPath.row == self.vendorsIDArr.count
//            {
//                cell.txtVendor.text =   ""
//                cell.txtAccNumber.text = ""
//            }
//        }
         
        if numberOfRows == 0
        {
            cell.btnMinus.isHidden = true
        }else
        {
            cell.btnMinus.isHidden = false
        }
        
        
        cell.txtVendor.tag = indexPath.row
        cell.btnVendor?.tag = indexPath.row
        cell.btnMinus?.tag = indexPath.row
        cell.btnVendor?.addTarget(self, action: #selector(selectVendorClicked(_:)), for: .touchUpInside)
        cell.btnMinus?.addTarget(self, action: #selector(btnMinusClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
    
    // MARK: Pluse Button Click
    @IBAction func btnPlusClick(_ sender: UIButton) {
        
        
        if self.vendorsIDArr.count == numberOfRows && self.vendorAccountArr.count == numberOfRows {
             numberOfRows += 1
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.tableViewHeightCont.constant = self.tableView.contentSize.height
        }else{
            if self.vendorsIDArr.count != numberOfRows
            {
                self.AlertMessage("Please select vendor.")
            }else
            {
                self.AlertMessage("Please enter vendor account number.")
            }
        }
    }
    
    // MARK: Minus Button Click Method
    @IBAction func btnMinusClicked(_ sender: UIButton)
    {
        
        if numberOfRows > 0 {
            
            self.vendorsIDArr["\(sender.tag)"] =  nil
            self.vendorAccountArr["\(sender.tag)"] =  nil
            self.arrRecSelectedVendorName["\(sender.tag)"] =  nil
            
            isFromMinusClick = "true"
            
            var temp1 = [String:String]()
            for (key, value) in self.arrRecSelectedVendorName
            {
                if Int(key)! < sender.tag{
                    temp1[key] = value
                }
                else{
                    temp1["\(Int(key)!-1)"] = value
                }
            }
            
            self.arrRecSelectedVendorName = temp1
            
            
            var temp3 = [String:String]()
            for (key, value) in self.vendorsIDArr
            {
                if Int(key)! < sender.tag{
                    temp3[key] = value
                }
                else{
                    temp3["\(Int(key)!-1)"] = value
                }
            }
            
            self.vendorsIDArr = temp3
            
            
            var temp2 = [String:String]()
            for (key, value) in self.vendorAccountArr
            {
                if Int(key)! < sender.tag{
                    temp2[key] = value
                }
                else{
                    temp2["\(Int(key)!-1)"] = value
                }
            }
            
            self.vendorAccountArr = temp2
            
            
            
            numberOfRows -= 1
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.tableViewHeightCont.constant = self.tableView.contentSize.height
            
        }else{
            
        }
    }
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        if string == ""{ //BackSpace
            return true
        }else if str!.characters.count < 3{
            if str!.characters.count == 1{
                txtPhoneNumber.text = "("
            }
        }else if str!.characters.count == 5{
            txtPhoneNumber.text = txtPhoneNumber.text! + ") "
        }else if str!.characters.count == 10{
            txtPhoneNumber.text = txtPhoneNumber.text! + "-"
        }else if str!.characters.count > 20{
            return false
        }
        return true
    }
    
    // MARK: - Method Alert Controll
    func textField (_ textField :  UITextField, shouldChangeCharactersIn range:  NSRange, replacementString string:  String  )  ->  Bool {

        if textField == txtPhoneNumber
        {
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            if textField == txtPhoneNumber{
                return checkEnglishPhoneNumberFormat(string: string, str: str)
            }else{
                return true
            }
        }
        return true;
    }
    
    // MARK: UITextFieldDelegate Method
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField != txtPhoneNumber
        {
            let pointInTable = textField.convert(textField.bounds.origin, to: self.tableView)
            let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
            
            if textField.text != ""
            {
                self.vendorAccountArr["\(String(describing: textFieldIndexPath!.row))"] =  (textField.text!)
                
                NSLog("data arr -->\(self.vendorAccountArr)")
            }else
            {
                self.vendorAccountArr.removeValue(forKey: String(textFieldIndexPath!.row))
            }
        }
        return true
    }
    
     // MARK: Select Vendor Button Click Method
    @IBAction func selectVendorClicked(_ sender: UIButton)
    {
        guard let cell = tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? VendorEditDetailCell else {
            return
        }
        
        ActionSheetStringPicker.show(withTitle: "Select Vendor", rows: self.arrVendorName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            cell.txtVendor.text = value as? String
            
            self.arrRecSelectedVendorName["\(sender.tag)"] = (value as? String)!
            self.vendorsIDArr["\(sender.tag)"] =  self.vendorsDetails[index]["id"]!
            
            
            NSLog("data arr -->\(self.vendorsIDArr)")
            
            return
        }, cancel: {
             ActionStringCancelBlock in return
         }, origin: sender)
        
    }
    
    
    // MARK: - Call Fetch Vendors List WebService
    func vendorListWebService()
    {
        webService_obj.fetchDataFromServer(header:"vendor_list", withParameter: [:], inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                self.vendorsDetails = responce.value(forKey: "data") as! [[String:String]]
                self.arrVendorName = self.vendorsDetails.map{ $0["username"]! }
            }
        }
    }
}
