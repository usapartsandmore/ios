//
//  WalletVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 08/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class  WalletViewCell : UITableViewCell {
    @IBOutlet var btnRadioCheck: UIButton!
    @IBOutlet var img_cardType: UIImageView!
    @IBOutlet var lbl_cardDetail: UILabel!
    @IBOutlet var lbl_cardName: UILabel!
}
class WalletVC: UIViewController, UITableViewDelegate,UITableViewDataSource {


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }

    @IBAction func btnAddNewPaymentCard(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddDebitCardVC")
        controller?.push()
    }
    @IBAction func btnDeleteClick(_ sender: UIButton) {
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletViewCell", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
        }
    }
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
