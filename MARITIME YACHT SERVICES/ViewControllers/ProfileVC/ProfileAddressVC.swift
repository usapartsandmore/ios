//
//  ProfileAddressVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 09/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class AddressCustomeCell: UITableViewCell {
    @IBOutlet var btnSelect: UIButton!
    @IBOutlet var lbl_address : UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected{
            btnSelect.setImage(UIImage(named: "squareselect"), for: .normal)
        }else{
            btnSelect.setImage(UIImage(named: "squareunselect"), for: .normal)
        }
    }
}

class ProfileAddressVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var addlists = [[String:String]]()
    var defaultData = [String:String] ()
    var newData = [[String:String]]()
    @IBOutlet var lbl_defaultAddress: UILabel!
    @IBOutlet var defaultbtnSelect: UIButton!
    
    @IBOutlet var tableView: UITableView!
    var completeAdd = ""
    var deleted_id = ""
    var default_Id = ""
    
    var isforCart = Bool()
    var block: ((String)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        // address list web service method
        self.addressListWebServicec()
        
    }
    
    func addressListWebServicec () {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"customer_shipping_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.addlists = responce.value(forKey: "data") as! [[String:String]]
                self.newData = self.addlists.filter{ dict in
                    if dict["is_default"] == "1"{
                        self.defaultData = dict
                        let add1 = self.defaultData["address1"]!
                        let add2 = self.defaultData["address2"]!
                        let cityname = self.defaultData["city_name"]!
                        let statename = self.defaultData["state_name"]!
                        let countryname = self.defaultData["country_name"]!
                        let zipcode = self.defaultData["zipcode"]!
                        self.completeAdd = "\(add1) \(add2) \(",") \(cityname) \(",") \(statename) \("-") \(zipcode) \(countryname)"
                        self.lbl_defaultAddress.text = self.completeAdd
                        self.defaultbtnSelect.setImage(UIImage(named: "squareselect"), for: .normal)
                        return false
                    }
                    else{
                        
                        return true
                    }
                }
                
            }
            self.tableView.reloadData()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnAddShippingAddress(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Profile", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ShippingAddressVC") as! ShippingAddressVC
        controller.isfromServices = "false"
        controller.push()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.newData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCustomeCell", for: indexPath) as! AddressCustomeCell
        let dict = newData[indexPath.row] as NSDictionary
        
        cell.selectionStyle = .none
        
        let add1 = dict["address1"] as! String
        let add2 = dict["address2"] as!String
        let cityname = dict["city_name"] as!String
        let statename = dict["state_name"] as!String
        let countryname = dict["country_name"] as!String
        let zipcode = dict["zipcode"] as!String
        completeAdd = "\(add1) \(add2) \(",") \(cityname) \(",") \(statename) \("-") \(zipcode) \(countryname)"
        
        cell.lbl_address.text = completeAdd
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.default_Id = self.newData[indexPath.row]["id"]!
        
        if isforCart == true{
            
            let dict = newData[indexPath.row] as NSDictionary
 
            let add1 = dict["address1"] as! String
            let add2 = dict["address2"] as!String
            let cityname = dict["city_name"] as!String
            let statename = dict["state_name"] as!String
            let countryname = dict["country_name"] as!String
            let zipcode = dict["zipcode"] as!String
            let address  = "\(add1) \(add2) \(",") \(cityname) \(",") \(statename) \("-") \(zipcode) \(countryname)"

             block?(address)
            _=self.navigationController?.popViewController(animated: true)
        }
        
        self.makeDefaultAddressWebserviceCall()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            print("more button tapped")
            self.deleted_id = self.newData[index.row]["id"]!
            self.newData.remove(at: index.row)
            self.deleteWebserviceCall()
        }
        delete.backgroundColor = .red
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func deleteWebserviceCall() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "shipping_id":self.deleted_id]
        
        webService_obj.fetchDataFromServer(header:"delete_shipping", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.tableView.reloadData()
            }
        }
        
        
    }
    func makeDefaultAddressWebserviceCall() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "shipping_id":self.default_Id]
        
        webService_obj.fetchDataFromServer(header:"default_shipping", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.addressListWebServicec()
                
            }
        }
    }
}
