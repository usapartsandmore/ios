//
//  AddNewItemVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 10/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

extension AddNewItemVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        img_Profile.image = pickedImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

class AddNewItemVC: UIViewController, UIScrollViewDelegate,UITextViewDelegate {
    @IBOutlet weak var txt_Title: UITextField!
    @IBOutlet weak var txt_Description: UITextView!
    @IBOutlet weak var txt_SerialNo: UITextField!
    @IBOutlet weak var txt_UPCNumber: UITextField!
    
    @IBOutlet weak var lbl_CurrentStock: UILabel!
    @IBOutlet weak var lbl_MinLevel: UILabel!
    @IBOutlet weak var lbl_MaxLevel: UILabel!
    
    @IBOutlet weak var txt_Notes: UITextField!
    @IBOutlet weak var txt_Pin: UITextField!
    @IBOutlet weak var txt_Boxes: UITextField!
    @IBOutlet weak var txt_lockers: UITextField!
    
    @IBOutlet var img_Profile: UIImageView!
    let imagePicker = UIImagePickerController()
    var pickedImage: UIImage!
    
    var deck_id = ""
    var pin_id = ""
    var locker_id = ""
    var box_id = ""
    var isForPin = ""
    var isFromQuickView = ""
    
    var islockerAvailable = Bool()
    var isboxAvailable = Bool()
    
    
    @IBOutlet weak var lockerBtnHeightConst: NSLayoutConstraint!
    @IBOutlet weak var lockerTxtHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var boxBtnHeightConst: NSLayoutConstraint!
    @IBOutlet weak var boxTxtHeightConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetPlaceHoder()
        txt_Description.text = "Description"
        txt_Description.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
        
        self.getLockerBoxInfoWebService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetPlaceHoder()
    {
        txt_Title.addPaddingView(width: 10, onSide: 0)
        txt_Description.addPaddingView(width: 10, onSide: 0)
        txt_SerialNo.addPaddingView(width: 10, onSide: 0)
        txt_UPCNumber.addPaddingView(width: 10, onSide: 0)
        txt_Notes.addPaddingView(width: 10, onSide: 0)
        txt_Pin.addPaddingView(width: 10, onSide: 0)
        txt_Boxes.addPaddingView(width: 10, onSide: 0)
        txt_lockers.addPaddingView(width: 10, onSide: 0)
    }
    
    @IBAction func btnAddImgClick(_ sender: Any) {
        self.AlertController()
    }
    
    // MARK: - Button Click Methods
    @IBAction func btnBackClick(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnCurrentSPlusClick(_ sender: UIButton) {
        lbl_CurrentStock.text = self.pluseBtnClick(number: lbl_CurrentStock.text!,filterValue: 0)
    }
    
    @IBAction func btnCurrentSMinusClick(_ sender: UIButton) {
        lbl_CurrentStock.text = self.minusBtnClick(number: lbl_CurrentStock.text!,filterValue: 0)
    }
    
    @IBAction func btnMinLevlPlusClick(_ sender: UIButton) {
        lbl_MinLevel.text = self.pluseBtnClick(number: lbl_MinLevel.text!,filterValue: 1)
    }
    
    @IBAction func btnMinLevlMinusClick(_ sender: UIButton) {
        lbl_MinLevel.text = self.minusBtnClick(number: lbl_MinLevel.text!,filterValue: 0)
    }
    
    @IBAction func btnMaxLevlPlusClick(_ sender: UIButton) {
        lbl_MaxLevel.text = self.pluseBtnClick(number: lbl_MaxLevel.text!,filterValue: 0)
    }
    
    @IBAction func btnMaxLevlMinusClick(_ sender: UIButton) {
        lbl_MaxLevel.text = self.minusBtnClick(number: lbl_MaxLevel.text!,filterValue: 2)
    }
    
    @IBAction func btnPinclick(_ sender: Any) {
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier: "ChooseRoomVC") as? ChooseRoomVC
        controller?.isfromVC = "pin"
        controller?.deck_id = self.deck_id
        controller?.block = { dict in
            self.pin_id = dict["id"]!
            self.txt_Pin.text = dict["name"]!
        }
        controller?.push()
    }
    
    @IBAction func btnLockerclick(_ sender: Any) {
        
        if self.txt_Pin.text == ""
        {
            self.AlertMessage("Please first select location.")
        }else
        {
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier: "ChooseRoomVC") as? ChooseRoomVC
            controller?.isfromVC = "locker"
            controller?.deck_id = self.deck_id
            controller?.pin_id = self.pin_id
            controller?.block = { dict in
                self.locker_id = dict["id"]!
                self.txt_lockers.text = dict["name"]!
            }
            controller?.push()
        }
    }
    
    @IBAction func btnBoxesClick(_ sender: Any) {
        if self.txt_Pin.text == ""
        {
            self.AlertMessage("Please first select location.")
        }
        else if self.txt_lockers.text == ""
        {
            self.AlertMessage("Please first select locker.")
        }
        else{
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier: "ChooseRoomVC") as? ChooseRoomVC
            controller?.isfromVC = "boxes"
            controller?.deck_id = self.deck_id
            controller?.pin_id = self.pin_id
            controller?.locker_id = self.locker_id
            controller?.block = { dict in
                self.box_id = dict["id"]!
                self.txt_Boxes.text = dict["name"]!
            }
            controller?.push()
        }
    }
    
    @IBAction func btnNotesClick(_ sender: Any) {
        if self.txt_Pin.text == ""
        {
            self.AlertMessage("Please first select location.")
        }
        else if self.txt_lockers.text == ""
        {
            self.AlertMessage("Please first select locker.")
        }
        else if self.txt_Boxes.text == ""
        {
            self.AlertMessage("Please first select box.")
        }
        else
        {
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier: "NotesVC") as? NotesVC
            controller?.deck_id = self.deck_id
            controller?.pin_id = self.pin_id
            controller?.locker_id = self.locker_id
            controller?.box_id = self.box_id
            controller?.push()
        }
    }
    
    //MAKR:- Utility methods
    //filterValue :
    //0 - No filter
    //1 - validation1
    //2 - validation2
    
    func pluseBtnClick(number:String,filterValue:Int) -> String {
        var temp = Int(number) ?? 0
        temp += 1
        
        if filterValue == 1 {
            guard validation1(valueToCheck: temp) else {
                temp -= 1
                return String(temp)
            }
        }
        
        
        return String(temp)
    }
    func minusBtnClick(number:String,filterValue: Int) -> String {
        var temp = Int(number) ?? 0
        if temp <= 0 {
            return "0"
        }
        else {
            temp -= 1
        }
        
        if filterValue == 2 {
            guard validation2(valueToCheck: temp) else {
                
                temp += 1
                return String(temp)
            }
        }
        
        return String(temp)
    }
    
    func validation1(valueToCheck: Int) -> Bool{
        
        let maximumVal = Int(lbl_MaxLevel.text!)!
        return valueToCheck <= maximumVal
    }
    
    func validation2(valueToCheck: Int) -> Bool{
        
        let minimumVal = Int(lbl_MinLevel.text!)!
        return minimumVal <= valueToCheck
    }
    
    // MARK: - TextView Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func btnAddItemClick(_ sender: UIButton)
    {
        if img_Profile.image == UIImage.init(named:"addphoto")
        {
            self.AlertMessage("Please select image.")
        }
        else if txt_Title.text! .isEmpty
        {
            self.AlertMessage("Please enter item title.")
        }
        else if txt_Description.textColor == UIColor.lightGray
        {
            self.AlertMessage("Please enter description.")
        }
//        else if txt_SerialNo.text! .isEmpty
//        {
//            self.AlertMessage("Please enter serial numbers.")
//        }
//        else if txt_UPCNumber.text! .isEmpty
//        {
//            self.AlertMessage("Please enter UPC code.")
//        }
//        else if txt_lockers.text! .isEmpty
//        {
//            self.AlertMessage("Please select lockers.")
//        }
        else
        {
            self.addProductManuallyWebService()
        }
        
    }
    
    // MARK: - Web-Service Methods
    func getLockerBoxInfoWebService() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "blueprint_deck_id":deck_id,
                    "pins_locker_id":self.locker_id,
                    "locker_box_id":self.box_id,
                    "decks_pin_id":pin_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"count_pin_box", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                let pinsData = responce.value(forKey: "data") as! [String: String]
                self.txt_Pin.text = pinsData["pin_name"]
                self.txt_Boxes.text = pinsData["box_name"] ?? "Choose Box"
                self.txt_lockers.text = pinsData["locker_name"] ?? "Choose Locker"
                
                if self.isForPin == "true"
                {
                    self.islockerAvailable = false
                    self.lockerBtnHeightConst.constant = 0
                    self.lockerTxtHeightConst.constant = 0
                    
                    self.isboxAvailable = false
                    self.boxBtnHeightConst.constant = 0
                    self.boxTxtHeightConst.constant = 0
                    
                }else{
                    if pinsData["lockers"] == "0"
                    {
                        self.islockerAvailable = false
                        self.lockerBtnHeightConst.constant = 0
                        self.lockerTxtHeightConst.constant = 0
                    }
                    else{
                        self.islockerAvailable = true
                        self.lockerBtnHeightConst.constant = 36
                        self.lockerTxtHeightConst.constant = 37
                    }
                    
                    if pinsData["boxes"] == "0"
                    {
                        self.isboxAvailable = false
                        self.boxBtnHeightConst.constant = 0
                        self.boxTxtHeightConst.constant = 0
                    }
                    else{
                        self.isboxAvailable = true
                        self.boxBtnHeightConst.constant = 36
                        self.boxTxtHeightConst.constant = 37
                    }
                }
            }
        }
    }
    
    
    func addProductManuallyWebService() {
        
        if isSharingUser == true
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "blueprint_deck_id":deck_id,
                        "decks_pin_id":pin_id,
                        "pins_locker_id":locker_id,
                        "locker_box_id":box_id,
                        "name":self.txt_Title.text!,
                        "description":self.txt_Description.text!,
                        "serial_number":self.txt_SerialNo.text!,
                        "upc":self.txt_UPCNumber.text!,
                        "qty":self.lbl_CurrentStock.text!,
                        "min_qty":self.lbl_MinLevel.text!,
                        "max_qty":self.lbl_MaxLevel.text!,
                        "create_by":sharedUser_id]
        }else{
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "blueprint_deck_id":deck_id,
                        "decks_pin_id":pin_id,
                        "pins_locker_id":locker_id,
                        "locker_box_id":box_id,
                        "name":self.txt_Title.text!,
                        "description":self.txt_Description.text!,
                        "serial_number":self.txt_SerialNo.text!,
                        "upc":self.txt_UPCNumber.text!,
                        "qty":self.lbl_CurrentStock.text!,
                        "min_qty":self.lbl_MinLevel.text!,
                        "max_qty":self.lbl_MaxLevel.text!,
                        "create_by":webService_obj.Retrive("User_Id") as! String]
        }

        
        
        webService_obj.profileDataFromServer(header:"add_item", withParameter: PostData as NSDictionary, coverdata: self.pickedImage.fixed!, inVC: self, showHud: true) { (responce,staus) in
            if staus{
                if self.isFromQuickView == "true"
                {
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: MnageInventListVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }else{
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: ViewPinsDataVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
            }
        }
    }
    
    
    // MARK: - Method Image Picker Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imageCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imageGallery()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}
