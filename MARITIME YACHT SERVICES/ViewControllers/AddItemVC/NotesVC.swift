//
//  NotesVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 16/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class AllNotesCustomeCell: UITableViewCell {
    @IBOutlet var lbCelllHeader: UILabel!
    @IBOutlet var lblDate: UILabel!
}
class NotesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var notesData = [[String:String]]()
    @IBOutlet weak var lbl_NoData: UILabel!
    @IBOutlet weak var lbl_product_Name: UILabel!
    @IBOutlet var tblView: UITableView!
    var reqData = [String:String]()
    
    
    var deck_id = ""
    var pin_id = ""
    var locker_id = ""
    var box_id = ""
    var product_id = ""
    var product_varient_id = ""
    var product_name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_product_Name.text = self.product_name
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lbl_NoData.isHidden = true
        self.fetchNotesWebServiceCall()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addNotesBtnClick(_ sender: Any) {
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier: "AddNewNotesVC") as? AddNewNotesVC
        controller?.deck_id = self.deck_id
        controller?.pin_id = self.pin_id
        controller?.locker_id = self.locker_id
        controller?.box_id = self.box_id
        controller?.product_id = self.product_id
        controller?.product_varient_id = self.product_varient_id
        controller?.product_name = self.product_name
        controller?.isfrom = "add"
        controller?.push()
    }
    
    // MARK: - Method Button click
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllNotesCustomeCell", for: indexPath) as! AllNotesCustomeCell
        let data = self.notesData[indexPath.row]
        
        cell.lbCelllHeader.text = data["name"]
        cell.lblDate.text = data["created"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let data = self.notesData[indexPath.row]
        
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier: "AddNewNotesVC") as? AddNewNotesVC
        controller?.deck_id = self.deck_id
        controller?.pin_id = self.pin_id
        controller?.locker_id = self.locker_id
        controller?.box_id = self.box_id
        controller?.product_id = self.product_id
        controller?.product_varient_id = self.product_varient_id
        controller?.product_name = self.product_name
        controller?.isfrom = "view"
        controller?.product_title = data["name"]!
        controller?.product_description = data["description"]!
        controller?.push()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
             return true
     }
 
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
 
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            let data = self.notesData[indexPath.row]
            
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier: "AddNewNotesVC") as? AddNewNotesVC
            controller?.deck_id = self.deck_id
            controller?.pin_id = self.pin_id
            controller?.locker_id = self.locker_id
            controller?.box_id = self.box_id
            controller?.product_id = self.product_id
            controller?.product_varient_id = self.product_varient_id
            controller?.product_name = self.product_name
            controller?.isfrom = "edit"
            controller?.product_title = data["name"]!
            controller?.product_description = data["description"]!
            controller?.note_id = data["id"]!
            controller?.push()
          
            print("edit button tapped")
        }
        edit.backgroundColor = UIColor.blue
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            let data = self.notesData[indexPath.row]
            let note_id = data["id"] ?? ""
            self.deleteSelectedNotesWebService(note_id:note_id )
            print("delete button tapped")
        }
        delete.backgroundColor = UIColor.red
 
            return [edit,delete]
       
    }
    
    func deleteSelectedNotesWebService(note_id:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "note_id":note_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_note", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.fetchNotesWebServiceCall()
            }
        }
    }

    
    func fetchNotesWebServiceCall() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "blueprint_deck_id":deck_id,
                    "decks_pin_id":pin_id,
                    "pins_locker_id":locker_id,
                    "locker_box_id":box_id,
                    "product_id":product_id,
                    "product_variant_id":product_varient_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"note_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.notesData.removeAll()
                self.notesData = responce["data"] as! [[String:String]]
                self.tblView.reloadData()
            }else
            {
                self.notesData.removeAll()
                self.tblView.reloadData()
            }
            if self.notesData.count == 0
            {
                 self.lbl_NoData.isHidden = false
            }
        }
    }
}
