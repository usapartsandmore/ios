//
//  AddItemVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 10/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import SwiftyJSON


class AddItemVC: UIViewController,UISearchBarDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource {
 
     @IBOutlet var collectionViewSearch: UICollectionView!
    @IBOutlet var btn_camera: UIButton!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var view_ManuallyAddItem: UIView!
    @IBOutlet weak var searchTopConst: NSLayoutConstraint!
    
    var filteredData = [[String:String]]()
    
    var deck_id = ""
    var pin_id = ""
    var box_id = ""
    var locker_id = ""
     var isForPin = ""
    var isFromQuickView = ""
    
    
    //Camera Search 
    var type = ""
    let imagePicker = UIImagePickerController()
    var pickedImage: UIImage!
    var visionImgSearchArray = [String]()
    var googleAPIKey = "AIzaSyBBxfwGLfhDccha-7or8FTxn4rOp0mRx8Y"
    var googleURL: URL {
        return URL(string: "https://vision.googleapis.com/v1/images:annotate?key=\(googleAPIKey)")!
    }
    let session = URLSession.shared
    @IBOutlet var btn_ImgCamera: UIButton!
    var imagesearchOn = ""
    @IBOutlet var view_Search: UIView!
    @IBOutlet var collViewSearched_keywords: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchTopConst.constant = 0
        //Camera Search
        collViewSearched_keywords.isHidden = true
        self.btn_ImgCamera.tag = 0
        self.view_Search.isHidden = true
        self.btn_ImgCamera.isHidden = true
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.white
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if imagesearchOn == "Y"
        {
            
        }else{
            
            if scan_Code.length != 0
            {
                self.btn_camera.setImage(UIImage(named: "cross"), for: UIControlState.normal)
                type = "barcode"
                self.view_Search.isHidden = false
                self.fetchSearchdataWebServiceCall(searchText: scan_Code)
                self.btn_camera.tag = 1
                self.btn_ImgCamera.tag = 1
                self.searchTopConst.constant = 0
                
            }else
            {
                self.btn_camera.setImage(UIImage(named: "chatcamera"), for: UIControlState.normal)
                self.btn_camera.tag = 0
                self.view_Search.isHidden = true
                type = "home"
                searchBar.text = ""
            }
        }
        
       // self.filteredData.removeAll()
        //self.searchBar.text = ""
        self.view_ManuallyAddItem.isHidden = true
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Method Button click
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnCameraClick(_ sender: UIButton) {
        self.searchTopConst.constant = 53
        if self.btn_ImgCamera.tag == 1
        {
            collViewSearched_keywords.isHidden = true
            //self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_camera.setImage(UIImage(named: "chatcamera"), for: UIControlState.normal)
            self.btn_camera.tag = 0
            scan_Code = ""
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            type = "home"
            self.imagesearchOn = "N"
        }else
        {
            collViewSearched_keywords.isHidden = false
            
            filteredData.removeAll()
            visionImgSearchArray.removeAll()
            collViewSearched_keywords.reloadData()
            collectionViewSearch.reloadData()
            
            //self.btn_ImgCamera.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = false
            type = "image_search"
            self.imagesearchOn = "Y"
            self.view_Search.isHidden = false
            self.AlertController()
            self.btn_ImgCamera.tag = 1
        }
    }
    
    @IBAction func cartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    @IBAction func btnAddMyInventory(_ sender: UIButton) {
         let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "AddNewItemVC") as? AddNewItemVC
        vc?.deck_id = self.deck_id
        vc?.pin_id = self.pin_id
        vc?.box_id = self.box_id
        vc?.locker_id = self.locker_id
        vc?.isForPin = self.isForPin
        vc?.isFromQuickView = self.isFromQuickView
         vc?.push()
      }
    
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionViewSearch
        {
            return filteredData.count
        }
        else
        {
            return visionImgSearchArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
         if collectionView == self.collViewSearched_keywords
        {
            let cellS = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchKeywordCell", for: indexPath) as! SearchKeywordCell
            
            cellS.lbl_Keywords.text = visionImgSearchArray[indexPath.row]
            
            cellS.btn_Close.tag = indexPath.row
            cellS.btn_Close.addTarget(self, action: #selector(deleteSearchedKeyWord(_:)), for: .touchUpInside)
            
            return cellS
        }
        else
        {
            let cellD = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsMainCell", for: indexPath) as! ProductsMainCell
            
            let sData = self.filteredData[indexPath.row]
            
            cellD.lbl_PName.text = sData["name"] ?? ""
            
            let strLogo2 = sData["logo"]!
            let urlLogo2:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo2))")!
            cellD.img_Logo.af_setImage(withURL: urlLogo2 as URL)
            
            cellD.lbl_Price.text = "$\(sData["price"] ?? "")"
            
            cellD.view_Rating.rating = (sData["rating"]! as NSString).floatValue
            
            if sData["image"] == ""
            {
                cellD.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = sData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellD.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellD
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
       if collectionView == self.collViewSearched_keywords
        {
        }
       else{
        self.searchBar.endEditing(true)
        
        let selectedRecord = self.filteredData[indexPath.row]
        
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ManageInventoryDetailProductVC") as! ManageInventoryDetailProductVC
        
        controller.dictProductDetail = selectedRecord as NSDictionary
        controller.deck_id = self.deck_id
        controller.pin_id = self.pin_id
        controller.box_id = self.box_id
        controller.locker_id = self.locker_id
        controller.isViewToAddProduct = true
        controller.isForPin = self.isForPin
        controller.isFromQuickView = self.isFromQuickView
        controller.push()
        
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewSearch
        {
            let width = (self.view.frame.size.width - 40) / 2
            let height = width * 1.2
            return CGSize(width: width, height: height)
        }
        else if collectionView == collViewSearched_keywords
        {
            let width = (visionImgSearchArray[indexPath.row] as NSString).size(attributes: nil)
            let height = CGFloat(30)
            return CGSize(width: width.width+40, height: height)
        }
        else
        {
            let width = (self.view.frame.size.width - 40) / 2.2
            let height = CGFloat(170)
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 10.0
    }
    
    // MARK: - Delete Searched KeyWord Method
    @IBAction func deleteSearchedKeyWord(_ sender: UIButton)
    {
        if self.visionImgSearchArray.count == 1
        {
            self.collViewSearched_keywords.isHidden = true
            // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            self.type = "home"
            self.imagesearchOn = "N"
        }else{
            self.visionImgSearchArray.remove(at: sender.tag)
            print("Updated Keyword Array----> \(self.visionImgSearchArray)")
            self.collViewSearched_keywords.reloadData()
            self.fetchSearchdataWebServiceCall(searchText: "")
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    // MARK: - searchBar Delegate Method
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.view_Search.isHidden = false
        filteredData.removeAll()
        
        self.searchTopConst.constant = 0
        
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            self.view_Search.isHidden = true
            self.view_ManuallyAddItem.isHidden = true
        }
        else
        {
            if searchText.length >= 3
            {
                type = "home"
                self.fetchSearchdataWebServiceCall(searchText: searchText)
                
            }
            //            if searchText.length >= 3
            //            {
            //                let arr = featuredData.filter({ (fruit) -> Bool in
            //                    return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
            //                })
            //                filteredData = arr
            //            }
        }
        collectionViewSearch.reloadData()
    }

    
    func fetchSearchdataWebServiceCall(searchText :String) {
        if searchText == ""
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "search":self.visionImgSearchArray,
                        "type" : "image_search",
                        "page":-1]
        }else
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "search":searchText,
                        "type" : type,
                        "page":-1]
        }
        load.hide(delegate: self)
        
        webService_obj.fetchDataFromServer(alertMsg: true,header:"product_searching", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                let searchData = responce.value(forKey: "data") as! [String: Any]
                let product = searchData["product"] as! [[String:String]]
                
                if self.type == "home"
                {
                    let arr = product.filter({ (fruit) -> Bool in
                        return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
                    })
                    self.filteredData = arr
                }else
                {
                    self.filteredData = product
                }
                
                self.collViewSearched_keywords.reloadData()
                self.collectionViewSearch.reloadData()
            }else
            {
                if self.imagesearchOn == "Y"
                {
                    self.collViewSearched_keywords.isHidden = true
                    //self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
                    self.btn_ImgCamera.isHidden = true
                    self.btn_ImgCamera.tag = 0
                    self.view_Search.isHidden = true
                    self.type = "home"
                    self.imagesearchOn = "N"
                }else{
                    self.btn_camera.setImage(UIImage(named: "chatcamera"), for: UIControlState.normal)
                    self.btn_camera.tag = 0
                    scan_Code = ""
                }
                
                self.filteredData.removeAll()
                self.collectionViewSearch.reloadData()
                self.searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
                self.view_ManuallyAddItem.isHidden = false
            }
        }
    }
 
    @IBAction func btnCameraImageClick(_ sender: UIButton) {
        
        if self.btn_ImgCamera.tag == 1
        {
            collViewSearched_keywords.isHidden = true
            // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            type = "home"
            self.imagesearchOn = "N"
        }else
        {
            collViewSearched_keywords.isHidden = false
            
            filteredData.removeAll()
            visionImgSearchArray.removeAll()
            collViewSearched_keywords.reloadData()
            collectionViewSearch.reloadData()
            
            // self.btn_ImgCamera.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = false
            type = "image_search"
            self.imagesearchOn = "Y"
            self.view_Search.isHidden = false
            self.AlertController()
            self.btn_ImgCamera.tag = 1
        }
    }
    
    // MARK: - Method Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let ScanAction = UIAlertAction(title: "Bar Code Scan", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Bar Code Scan")
            
            self.collViewSearched_keywords.isHidden = true
            // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            self.type = "home"
            self.imagesearchOn = "N"
            
            if self.btn_camera.tag == 1
            {
                self.btn_camera.setImage(UIImage(named: "chatcamera"), for: UIControlState.normal)
                self.btn_camera.tag = 0
                self.view_Search.isHidden = true
                self.type = "home"
                scan_Code = ""
            }else
            {
                let st = UIStoryboard.init(name: "Product", bundle: nil)
                
                let vc3 = st.instantiateViewController(withIdentifier: "ProductScanVC") as? ProductScanVC
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController!.present(vc3!, animated: true, completion: nil)
                self.type = "barcode"
            }
            
        })
        
        let imageAction = UIAlertAction(title: "Image Recognition", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
            
            let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                print("Camera")
                self.imageCamera()
            })
            
            let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                print("Gallery")
                self.imageGallery()
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                print("Cancelled")
                
                self.collViewSearched_keywords.isHidden = true
                //  self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
                self.btn_ImgCamera.isHidden = true
                self.btn_ImgCamera.tag = 0
                self.view_Search.isHidden = true
                self.type = "home"
                self.imagesearchOn = "N"
            })
            
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
            
            // 5
            self.present(optionMenu, animated: true, completion: nil)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
            
            self.collViewSearched_keywords.isHidden = true
            // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            self.type = "home"
            self.imagesearchOn = "N"
        })
        
        // 4
        optionMenu.addAction(ScanAction)
        optionMenu.addAction(imageAction)
        optionMenu.addAction(cancelAction)
        
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
        
    }
}

extension AddItemVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        //img_Profile.image = pickedImage
        
        // Base64 encode the image and create the request
        let binaryImageData = base64EncodeImage(pickedImage)
        createRequest(with: binaryImageData)
        load.show(views: self.view)
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(_ imageSize: CGSize, image: UIImage) -> Data {
        UIGraphicsBeginImageContext(imageSize)
        image.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        let resizedImage = UIImagePNGRepresentation(newImage!)
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    func analyzeResults(_ dataToParse: Data) {
        
        // Update UI on the main thread
        DispatchQueue.main.async(execute: {
            
            // Use SwiftyJSON to parse results
            let json = JSON(data: dataToParse)
            let errorObj: JSON = json["error"]
            
            // Check for errors
            if (errorObj.dictionaryValue != [:]) {
                // self.lblLableAnotation.text = "Error code \(errorObj["code"]): \(errorObj["message"])"
            } else {
                // Parse the response
                print(json)
                let responses: JSON = json["responses"][0]
                
                self.visionImgSearchArray.removeAll()
                
                // Get logoAnnotations
                let logoAnnotations: JSON = responses["logoAnnotations"]
                let numLogoLabels: Int = logoAnnotations.count
                if numLogoLabels > 0 {
                    //var labelResultsText:String = "Logo Annotations Found: "
                    for index in 0..<numLogoLabels {
                        let label = logoAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                }
                
                // Get textAnnotations
                let txtAnnotations: JSON = responses["textAnnotations"]
                let numtxtLabels: Int = txtAnnotations.count
                if numtxtLabels > 0 {
                    //  var labelResultsText:String = "Text Annotations Found: "
                    for index in 0..<numtxtLabels {
                        let label = txtAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                }
                
                // Get label annotations
                let labelAnnotations: JSON = responses["labelAnnotations"]
                let numLabels: Int = labelAnnotations.count
                if numLabels > 0 {
                    for index in 0..<numLabels {
                        let label = labelAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                }
                
                // Get webDetection
                let webAnnotations: JSON = responses["webDetection"]["webEntities"]
                let numWebLabels: Int = webAnnotations.count
                if numWebLabels > 0 {
                    for index in 0..<numWebLabels {
                        let label = webAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                    print("Complete Array----> \(self.visionImgSearchArray.count)")
                    
                    let uniqueStrings = self.uniqueElementsFrom(array:self.visionImgSearchArray)
                    
                    self.visionImgSearchArray = uniqueStrings
                    
                    print("Complete Array uniqueStrings----> \(self.visionImgSearchArray.count)")
                    
                    self.type = "image_search"
                    self.view_Search.isHidden = false
                    self.fetchSearchdataWebServiceCall(searchText: "")
                }
                
            }
        })
    }
    
    func uniqueElementsFrom(array: [String]) -> [String] {
        //Create an empty Set to track unique items
        var set = Set<String>()
        let result = array.filter {
            guard !set.contains($0) else {
                //If the set already contains this object, return false
                //so we skip it
                return false
            }
            //Add this item to the set since it will now be in the array
            set.insert($0)
            //Return true so that filtered array will contain this item.
            return true
        }
        return result
    }
}


extension AddItemVC {
    func base64EncodeImage(_ image: UIImage) -> String {
        var imagedata = UIImagePNGRepresentation(image)
        
        // Resize the image if it exceeds the 2MB API limit
        if (imagedata?.count > 2097152) {
            let oldSize: CGSize = image.size
            let newSize: CGSize = CGSize(width: 800, height: oldSize.height / oldSize.width * 800)
            imagedata = resizeImage(newSize, image: image)
        }
        
        return imagedata!.base64EncodedString(options: .endLineWithCarriageReturn)
    }
    
    func createRequest(with imageBase64: String) {
        // Create our request URL
        
        var request = URLRequest(url: googleURL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(Bundle.main.bundleIdentifier ?? "", forHTTPHeaderField: "X-Ios-Bundle-Identifier")
        
        // Build our API request
        let jsonRequest = [
            "requests": [
                "image": [
                    "content": imageBase64
                ],
                "features": [
                    [
                        "type": "LABEL_DETECTION",
                        "maxResults": 10
                    ],
                    [
                        "type": "TEXT_DETECTION",
                        "maxResults": 10
                    ],
                    [
                        "type": "LOGO_DETECTION",
                        "maxResults": 10
                    ],
                    [
                        "type": "WEB_DETECTION",
                        "maxResults": 10
                    ]
                    
                ]
            ]
        ]
        let jsonObject = JSON(jsonDictionary: jsonRequest)
        
        // Serialize the JSON
        guard let data = try? jsonObject.rawData() else {
            return
        }
        
        request.httpBody = data
        
        // Run the request on a background thread
        DispatchQueue.global().async { self.runRequestOnBackgroundThread(request) }
    }
    
    func runRequestOnBackgroundThread(_ request: URLRequest) {
        // run the request
        
        let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }
            
            
            self.analyzeResults(data)
        }
        
        task.resume()
    }
}


// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


