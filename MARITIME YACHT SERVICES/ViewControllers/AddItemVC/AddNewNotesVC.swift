//
//  AddNewNotesVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 16/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class AddNewNotesVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var txt_Title: UITextField!
    @IBOutlet weak var txt_Description: UITextView!
    @IBOutlet weak var lbl_product_Name: UILabel!
    @IBOutlet weak var btn_Done: UIButton!
    
    var deck_id = ""
    var pin_id = ""
    var locker_id = ""
    var box_id = ""
    var product_id = ""
    var product_varient_id = ""
    var product_name = ""
    var product_title = ""
    var product_description = ""
    
    var note_id = ""
    var isfrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isfrom == "add"
        {
            self.btn_Done.isHidden = false
            txt_Description.text = "Write your note here."
            txt_Description.textColor = UIColor.lightGray
        }else if isfrom == "view"
        {
            self.btn_Done.isHidden = true
            self.txt_Title.text = self.product_title
            self.txt_Description.text = self.product_description
            txt_Description.textColor = UIColor.darkGray
            
            self.txt_Title.isUserInteractionEnabled = false
            self.txt_Description.isUserInteractionEnabled = false
        }else{
            self.btn_Done.isHidden = false
            self.txt_Title.text = self.product_title
            self.txt_Description.text = self.product_description
            txt_Description.textColor = UIColor.darkGray
        }
        
        self.lbl_product_Name.text = self.product_name
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Click Methods
    @IBAction func btnBackClick(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnDoneClick(_ sender: Any) {
        if isfrom == "add"
        {
             self.addNotesWebServiceCall()
        }else
        {
             self.editNotesWebServiceCall()
        }
     }
    
    func addNotesWebServiceCall() {
        if txt_Title.text == ""
        {
            self.AlertMessage("Please enter title.")
        }
        else if txt_Description.text == "Write your note here."
        {
            self.AlertMessage("Please enter notes.")
        }
        else
        {
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "blueprint_deck_id":deck_id,
                        "decks_pin_id":pin_id,
                        "pins_locker_id":locker_id,
                        "locker_box_id":box_id,
                        "product_id":product_id,
                        "product_variant_id":product_varient_id,
                        "name":self.txt_Title.text!,
                        "description":self.txt_Description.text!]
            
            webService_obj.fetchDataFromServer(alertMsg: false,header:"add_note", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    _ = self.navigationController?.popViewController(animated:true)
                }
            }
        }
    }
    
    func editNotesWebServiceCall() {
        if txt_Title.text == ""
        {
            self.AlertMessage("Please enter title.")
        }
        else if txt_Description.text == "Write your note here."
        {
            self.AlertMessage("Please enter notes.")
        }
        else
        {
             PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "note_id":note_id,
                        "name":self.txt_Title.text!,
                        "description":self.txt_Description.text!]
 
            webService_obj.fetchDataFromServer(alertMsg: false,header:"edit_note", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        _ = self.navigationController?.popViewController(animated:true)
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    // MARK: - TextView Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write your note here."
            textView.textColor = UIColor.lightGray
        }
    }
    
    
}
