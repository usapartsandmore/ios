//
//  CalendarVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Narottam on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

class FilterTagCell : UICollectionViewCell{
    @IBOutlet weak var label: UILabel!
    var color: UIColor!{
        didSet{
            if isSelected{
                label.backgroundColor = color
                label.textColor = .white
            }else{
                label.borderColor = color
                label.borderWidth = 1
                label.backgroundColor = .white
                label.textColor = color
            }
        }
    }
    
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            if newValue{
                label.backgroundColor = color
                label.textColor = .white
            }else{
                label.borderColor = color
                label.borderWidth = 1
                label.backgroundColor = .white
                label.textColor = color
            }
        }
        
        get{
            return super.isSelected
        }
    }
}

class CalendarFirstCell: UITableViewCell {
    
    @IBOutlet weak var lbl_ViewPriority: UILabel!
    @IBOutlet weak var lblTaskEventTitle: UILabel!
    @IBOutlet weak var lblTaskEventNotes: UILabel!
    @IBOutlet weak var lblCreatedDateAndCreator: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
}

class CalendarSecondCell: UITableViewCell {
    
    @IBOutlet weak var lbl_ViewPriority: UILabel!
    @IBOutlet weak var lblTaskEventTitle: UILabel!
    @IBOutlet weak var lblTaskEventNotes: UILabel!
    @IBOutlet weak var lblCreatedDateAndCreator: UILabel!
    @IBOutlet weak var lblTimeReq: UILabel!
}


class CalendarVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lbl_topHeader: UILabel!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblViewSearch: UITableView!
    @IBOutlet var btn_topSearch: UIButton!
    
    var isfrom = ""
    var filterType = ""
    var headerTxt = ""
    var team_id = ""
    var team_member_id = ""
    
    var sectionData = [String]()
    var topDictionary = [String: [Any]]()
    let filterTag = ["All  ","Open", "Ongoing", "Past Due", "Completed"]
    //let filterTag = ["All  ", "Overdue", "Ongoing", "Completed"]  
     let filterColors = [#colorLiteral(red: 0.9647058824, green: 0.818066895, blue: 0.1022731587, alpha: 1),#colorLiteral(red: 0.2868720591, green: 0.6213208437, blue: 0.8930993676, alpha: 1),#colorLiteral(red: 0.9456083179, green: 0.443831563, blue: 0.01756197214, alpha: 1),#colorLiteral(red: 0.9393905401, green: 0.1034754142, blue: 0.002637132071, alpha: 1),#colorLiteral(red: 0.2609895766, green: 0.827819407, blue: 0.1192210391, alpha: 1)]
    var isteamData = ""
    
    var selectedDate = ""
    
     override func viewDidLoad() {
        super.viewDidLoad()
       // view.bringSubview(toFront: topView)
        topView.layer.shadowColor = UIColor.lightGray.cgColor
        topView.layer.shadowOffset = CGSize(width:0, height:5)
        topView.layer.shadowRadius = 10
        topView.layer.shadowOpacity = 0.5
        
        let fullDateArr = self.selectedDate.characters.split{$0 == "-"}.map(String.init)
        let monthNumber = Int(fullDateArr[1])
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        let month = fmt.monthSymbols[monthNumber! - 1]
        self.lbl_topHeader.text = "\(month), \(fullDateArr[0])"
      
       
         self.filterType = "calendar"
        
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
        self.collectionView.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: [])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.team_member_id != ""
        {
            self.isteamData = "1"
        }
        
        //Clear Search Data and set it Default
        self.searchView.tag = 0
        self.searchView.isHidden = true
        self.btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
        self.searchBar.text = ""
        tblViewSearch.reloadData()
        
        self.getEventTaskListsWebService()
    }
     
    //MARK: Search Button Click Methods
    @IBAction func btnSearchClick(_ sender: UIButton) {
        

        
            if self.searchView.tag == 0
            {
                
                self.sectionData.removeAll()
                self.topDictionary.removeAll()
                
                self.searchView.isHidden = false
                self.searchBar.becomeFirstResponder()
                self.searchView.tag = 1
                self.btn_topSearch.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
                
               
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // in half a second...
                    self.tblViewSearch.reloadData()
                }
            }
            else {
                
                self.sectionData.removeAll()
                self.topDictionary.removeAll()
                
                self.searchBar.text = ""
                self.searchView.isHidden = true
                self.searchBar.endEditing(true)
                self.btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
                self.searchView.tag = 0
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // in half a second...
                    self.tblViewSearch.reloadData()
                }
                self.getEventTaskListsWebService()
            }
            
        
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: SearchBar Delegate Method
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.tblViewSearch.isHidden = false
        
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            self.tblViewSearch.isHidden = true
        }
        else
        {
            //if searchText.length >= 3
            //{
            self.fetchSearchdataWebServiceCall(searchText: searchText)
            // }
        }
        tblViewSearch.reloadData()
    }
    
    @IBAction func filterOpen(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnTopRightAdd(_ sender: Any) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil , preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Calendar Events", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CalendarEventVC") as! CalendarEventVC
            controller.isforupdate = false
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.push()
        })
        
        let saveAction = UIAlertAction(title: "Task Events", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CreateTaskVC") as! CreateTaskVC
            controller.isforupdate = false
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.push()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
         sideMenuVC.toggleMenu()
     }
    
    //MARK: UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionData.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
 
        if sectionData.count != 0 {
            let label = UILabel(frame: CGRect(x:0, y:0, width: tableView.bounds.width-0, height: 44))
            label.backgroundColor = UIColor(colorLiteralRed: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1.0)
            label.text =  "  \(sectionData[section])"
            
            label.font = UIFont(name: "Poppins-Regular", size: 14)
            return label
        }else{
            let label = UILabel(frame: CGRect(x:0, y:0, width: tableView.bounds.width-0, height: 44))
            label.text = ""
            return label
        }
   
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionTitle: String = sectionData[section]
        let sectiondata = topDictionary[sectionTitle]
        return (sectiondata?.count)!
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if sectionData.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarFirstCell", for: indexPath) as! CalendarFirstCell
            return cell

        }
        
        let sectionTitle: String = sectionData[indexPath.section] 
        let sectiondata = topDictionary[sectionTitle]
        let data = sectiondata?[indexPath.row] as? [String:Any]
        
        
        if data?["duration_from"] as? String != "Full day"{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarFirstCell", for: indexPath) as! CalendarFirstCell
            cell.selectionStyle = .none
           // cell.accessoryType = .disclosureIndicator
            
            cell.lblTaskEventTitle.text = data?["title"] as? String
            
            cell.lblCreatedDateAndCreator.text = data?["created_full"] as? String
            cell.lblStartTime.text = data?["duration_from"] as? String
            cell.lblEndTime.text = data?["duration_to"] as? String
            
            if self.isfrom == "task"
            {
                cell.lblTaskEventNotes.text = data?["note"] as? String
            }else{
                cell.lblTaskEventNotes.text = data?["notes"] as? String
            }
            
            let status = data?["status"] as? String
            
            switch status {
            case "0"?: cell.lbl_ViewPriority.backgroundColor = UIColor.onGoingFilterColor
                break
            case "1"?: cell.lbl_ViewPriority.backgroundColor = UIColor.overDueFilterColor
                break
             case "3"?: cell.lbl_ViewPriority.backgroundColor = UIColor.completedFilterColor
                break
            case "4"?: cell.lbl_ViewPriority.backgroundColor = UIColor.openFilterColor
                break
            default: print("default")
            }
            
            
            let priority = data?["priority"] as? String
            
            switch priority {
            case "1"?: cell.lbl_ViewPriority.text = "!"
                break
            case "2"?: cell.lbl_ViewPriority.text = "!!"
                break
            case "3"?: cell.lbl_ViewPriority.text = "!!!"
                break
            default: print("default")
            }
            
            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarSecondCell", for: indexPath) as! CalendarSecondCell
            cell.selectionStyle = .none
           // cell.accessoryType = .disclosureIndicator
            
            cell.lblTaskEventTitle.text = data?["title"] as? String
            cell.lblCreatedDateAndCreator.text = data?["created_full"] as? String
            cell.lblTimeReq.text = data?["duration_from"] as? String
            if self.isfrom == "task"
            {
                cell.lblTaskEventNotes.text = data?["note"] as? String
            }else{
                cell.lblTaskEventNotes.text = data?["notes"] as? String
            }
            
            
            let status = data?["status"] as? String
            
            switch status {
            case "0"?: cell.lbl_ViewPriority.backgroundColor = UIColor.onGoingFilterColor
                break
            case "1"?: cell.lbl_ViewPriority.backgroundColor = UIColor.overDueFilterColor
                break
            case "3"?: cell.lbl_ViewPriority.backgroundColor = UIColor.completedFilterColor
                break
            case "4"?: cell.lbl_ViewPriority.backgroundColor = UIColor.openFilterColor
                break
            default: print("default")
            }
            
            
            let priority = data?["priority"] as? String
            
            switch priority {
            case "1"?: cell.lbl_ViewPriority.text = "!"
                break
            case "2"?: cell.lbl_ViewPriority.text = "!!"
                break
            case "3"?: cell.lbl_ViewPriority.text = "!!!"
                break
            default: print("default")
            }

            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        
        let sectionTitle: String = sectionData[indexPath.section]
        let sectiondata = topDictionary[sectionTitle]
        let data = sectiondata?[indexPath.row] as? [String:Any]
        
        if self.isfrom == "event"
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CalendarEventVC") as! CalendarEventVC
            controller.selectedDataDict = data!
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.isforupdate = true
            controller.push()
        }else
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CreateTaskVC") as! CreateTaskVC
            controller.selectedDataDict = data!
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.isforupdate = true
            controller.push()
        }
    }

    
    
    //MARK: UICollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterTag.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FilterTagCell
        cell.label.text = filterTag[indexPath.row]
        cell.label.cornerRadius = 2.5
        cell.color = filterColors[indexPath.row]
        
//        if self.filterType == "calendar"
//        {
//            if indexPath.row == 0
//            {
//                cell.isSelected = true
//            }
//        }

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
         if filterTag[indexPath.row] == "All  " {
            self.filterType = "calendar"
        }
        else if filterTag[indexPath.row] == "Open" {
            self.filterType = "open"
        }
        else if filterTag[indexPath.row] == "Past Due" {
            self.filterType = "overdue"
        }
        else if filterTag[indexPath.row] == "Ongoing" {
            self.filterType = "ongoing"
        }
        else if filterTag[indexPath.row] == "Completed" {
            self.filterType = "completed"
        }
        else{ //"Upcoming Task"
            self.filterType = "upcoming"
        }
        
        self.getEventTaskListsWebService()
    }
    
    
    private let _font = UIFont(name: "Poppins-Regular", size: 12)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let w = filterTag[indexPath.row].width(withConstraintedHeight: collectionView.bounds.height, font: _font!)
        return CGSize(width: w+41, height: collectionView.bounds.height)
    }
    
    // MARK: - Web-Service Methods
    func getEventTaskListsWebService() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "data_type":self.isfrom,
                    "type":self.filterType,
                    "team_id":self.team_id,
                    "team_member_id":self.team_member_id,
                    "date":self.selectedDate,
                    "team_data":self.isteamData]
            
         
        webService_obj.fetchDataFromServer(alertMsg: false, header: "get_calender_by_date", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.sectionData.removeAll()
                self.topDictionary.removeAll()
                
                let completeData = response.value(forKey: "data") as! [[String: Any]]
                
                self.sectionData = completeData.map{ ($0["date"]! as! String) }
                
                let collection: [[String: [Any]]] = completeData.map({ (dictionary) -> [String: [Any]] in
                    let eventDate = dictionary["date"]! as! String
                    let eventArray = dictionary["event_task"] as! [Any]
                    
                    return [eventDate: eventArray]
                })
                
                collection.forEach{ dict in
                    dict.forEach{
                        self.topDictionary[$0.key] = $0.value
                    }
                }
                
                print("Table Data->\((self.topDictionary as Dictionary<String, Any>).toJson())")
  
                self.tableView.reloadData()
 
            }else
            {
                self.sectionData.removeAll()
                self.topDictionary.removeAll()
                 self.tableView.reloadData()
            }
        }
    }
    
    func fetchSearchdataWebServiceCall(searchText :String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "data_type":self.isfrom,
                    "type":"calendar",
                    "team_id":self.team_id,
                    "team_member_id":self.team_member_id,
                    "team_data":self.isteamData,
                    "date":self.selectedDate,
                    "search":searchText]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"search_calender_by_date", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (response, message, status) in
            if status{
                
                self.sectionData.removeAll()
                self.topDictionary.removeAll()
                
                let completeData = response.value(forKey: "data") as! [[String: Any]]
                
                self.sectionData = completeData.map{ ($0["date"]! as! String) }
                
                let collection: [[String: [Any]]] = completeData.map({ (dictionary) -> [String: [Any]] in
                    let eventDate = dictionary["date"]! as! String
                    let eventArray = dictionary["event_task"] as! [Any]
                    
                    return [eventDate: eventArray]
                })
                
                collection.forEach{ dict in
                    dict.forEach{
                        self.topDictionary[$0.key] = $0.value
                    }
                }
                
                print("Table Data->\((self.topDictionary as Dictionary<String, Any>).toJson())")
                
              
                self.tblViewSearch.reloadData()
                
            }else
            {
                self.sectionData.removeAll()
                self.topDictionary.removeAll()
                self.tblViewSearch.reloadData()
            }
        }
    }


}
