//
//  CalendarAndTaskVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Narottam on 23/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import FSCalendar
import MessageUI
import Foundation


extension UIColor {
    
    static var calenderFilterColor: UIColor {
        return #colorLiteral(red: 0.9642209411, green: 0.818066895, blue: 0.1022731587, alpha: 1)
    }
    static var overDueFilterColor: UIColor {
        return #colorLiteral(red: 0.9393905401, green: 0.1034754142, blue: 0.002637132071, alpha: 1)
    }
    static var onGoingFilterColor: UIColor {
        return #colorLiteral(red: 0.9456083179, green: 0.443831563, blue: 0.01756197214, alpha: 1)
    }
    static var completedFilterColor: UIColor {
        return #colorLiteral(red: 0.2609895766, green: 0.827819407, blue: 0.1192210391, alpha: 1)
    }
    static var openFilterColor: UIColor {
        return #colorLiteral(red: 0.2868720591, green: 0.6213208437, blue: 0.8930993676, alpha: 1)
    }
}

class EventTaskCell: UITableViewCell {
    
    @IBOutlet weak var lbl_ViewPriority: UILabel!
    @IBOutlet weak var lblTaskEventTitle: UILabel!
   // @IBOutlet weak var lblTaskEventNotes: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblCreatedName: UILabel!
    @IBOutlet weak var lbl_AssignedTo: UILabel!
    
    var selectedTag: String = ""
    
    func cellSetupFor(dictRecord: NSDictionary, isEvent:Bool){
        
        lblTaskEventTitle.text = dictRecord.object(forKey: "title") as? String ?? ""
        lblCreatedDate.text = dictRecord.object(forKey: "created") as? String ?? ""
        lblCreatedName.text = dictRecord.object(forKey: "created_by") as? String ?? ""
        lbl_AssignedTo.text = dictRecord.object(forKey: "assigned_to") as? String ?? ""
//        if isEvent == true
//        {
//            lblTaskEventNotes.text = dictRecord.object(forKey: "notes") as? String ?? ""
//        }else
//        {
//            lblTaskEventNotes.text = dictRecord.object(forKey: "note") as? String ?? ""
//        }
        
        
        let status = dictRecord.object(forKey: "status") as? String ?? ""
        
        switch status {
        case "0": self.lbl_ViewPriority.backgroundColor = UIColor.onGoingFilterColor
            break
        case "1": self.lbl_ViewPriority.backgroundColor = UIColor.overDueFilterColor
            break
        case "3": self.lbl_ViewPriority.backgroundColor = UIColor.completedFilterColor
            break
        case "4": self.lbl_ViewPriority.backgroundColor = UIColor.openFilterColor
            break
        default: print("default")
        }
        
        
        let priority = dictRecord.object(forKey: "priority") as? String ?? ""
        
        switch priority {
        case "1": self.lbl_ViewPriority.text = "!"
            break
        case "2": self.lbl_ViewPriority.text = "!!"
            break
        case "3": self.lbl_ViewPriority.text = "!!!"
            break
        default: print("default")
        }
    }
}

class CalendarAndTaskVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance,UISearchBarDelegate, MFMailComposeViewControllerDelegate  {
    
    @IBOutlet weak var containtViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var containtViewH: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet var calendarHeaderView: UIView!
    @IBOutlet var taskHeaderView: UIView!
    @IBOutlet weak var btn_TopCalendar: UIButton!
    @IBOutlet weak var btn_TopTask: UIButton!
    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblViewSearch: UITableView!
    @IBOutlet var btn_topSearch: UIButton!
    let screenW:CGFloat = UIScreen.main.bounds.size.width
    let screenH:CGFloat = UIScreen.main.bounds.size.height
    var cellCount1:Int = 0
    var onceTime: Bool = true
    //OPEN , ONGOING , PAST DUE , COMPLETED.
    
   // let filterTag = ["All  ","Overdue", "Ongoing", "Completed"]
    
    let filterTag = ["All  ","Open", "Ongoing", "Past Due", "Completed"]
    
    let filterColors = [#colorLiteral(red: 0.9647058824, green: 0.8196078431, blue: 0.1019607843, alpha: 1),#colorLiteral(red: 0.2868720591, green: 0.6213208437, blue: 0.8930993676, alpha: 1),#colorLiteral(red: 0.9456083179, green: 0.443831563, blue: 0.01756197214, alpha: 1),#colorLiteral(red: 0.9393905401, green: 0.1034754142, blue: 0.002637132071, alpha: 1),#colorLiteral(red: 0.2609895766, green: 0.827819407, blue: 0.1192210391, alpha: 1)]
    var arrEvents = [[String:Any]]()
    var arrTasks = [[String:Any]]()
    var filteredData = [[String:Any]]()
    
    var calendarDateLabel = ""
    var filterType = ""
    var selectedDate = ""
    var topFilter = ""
    
    var team_id = ""
    var team_member_id = ""
    var team_name = ""
    
    var header_name = ""
    var isfromTeam = Bool()
    var isteamData = ""
    var selectedTaskDates = [String]()
    var selectedEventDates = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.bringSubview(toFront: topView)
        calendarView.delegate = self
        calendarView.dataSource = self
        calendarView.placeholderType = .none
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        self.selectedDate = dateFormater.string(from: Date())
        self.filterType = "calendar"
        
        //Set Top Calendar/Task Buttons
        if self.topFilter == ""
        {
            self.lbl_header.text = "Events & Task"
            self.btn_back.setImage(UIImage(named: "menu"), for: .normal)
            
            self.btn_TopCalendar.tag = 1
            self.btn_TopTask.tag = 0
            self.btn_TopCalendar.backgroundColor = UIColor.openFilterColor
            self.btn_TopCalendar.setTitleColor(UIColor.white, for: .normal)
            self.btn_TopTask.backgroundColor = UIColor.white
            self.btn_TopTask.setTitleColor(UIColor.darkGray, for: .normal)
            self.topFilter = "calendar_event"
        }else{
            
            self.lbl_header.text = self.header_name
            self.btn_back.setImage(UIImage(named: "back"), for: .normal)
            
            if self.topFilter == "calendar_event"
            {
                self.btn_TopCalendar.tag = 1
                self.btn_TopTask.tag = 0
                self.btn_TopCalendar.backgroundColor = UIColor.openFilterColor
                self.btn_TopCalendar.setTitleColor(UIColor.white, for: .normal)
                self.btn_TopTask.backgroundColor = UIColor.white
                self.btn_TopTask.setTitleColor(UIColor.darkGray, for: .normal)
            }else{
                self.btn_TopCalendar.tag = 0
                self.btn_TopTask.tag = 1
                self.btn_TopTask.backgroundColor = UIColor.openFilterColor
                self.btn_TopTask.setTitleColor(UIColor.white, for: .normal)
                self.btn_TopCalendar.backgroundColor = UIColor.white
                self.btn_TopCalendar.setTitleColor(UIColor.darkGray, for: .normal)
            }
        }
        
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
        self.collectionView.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: [])
        
        dateFormater.dateFormat = "MMMM dd, yyyy"
        let string = dateFormater.string(from: Date())
        calendarDateLabel = string
        
        
    }
    
    func layoutSetting() {
        
        let cH:Int = Int(tableView.rowHeight)
       // let cH1:Int = Int(tblcheckout1.rowHeight)
        self.topView.frame = CGRect(x: 0, y: 0, width: screenW, height: 400)
        self.tableView.frame = CGRect(x: 0, y: self.topView.frame.origin.y + self.topView.frame.size.height , width: screenW, height: (380 + CGFloat(cellCount1*cH)))
        //self.tblcheckout1.frame = CGRect(x: 0, y: self.tblCheckout.frame.origin.y + self.tblCheckout.frame.size.height, width: screenW, height: 350 + CGFloat(cellCount2*cH1))
        
        /*if onceTime {
         self.mainScrollView.contentSize = CGSize(width: screenW, height: self.tblCheckout.frame.size.height + self.tblcheckout1.frame.size.height - 400)
         }
         else{*/
        
        self.scroll.contentSize = CGSize(width: screenW, height: self.tableView.frame.size.height+50)
        
        //}
        
        self.containtViewHeight.constant =  (self.scroll.contentSize.height)
        
    }
    
    
//
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        var sizeOfContent: Float = 0
//        var answerTbHight = Int(self.tableView.contentSize.height+400)
//        sizeOfContent = Float(answerTbHight)
//        scroll.contentSize = CGSize(width: scroll.frame.size.width, height: CGFloat(sizeOfContent))
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if self.team_member_id != ""
        {
            self.isteamData = "1"
        }
        
        //Clear Search Data and set it Default
        self.searchView.tag = 0
        self.searchView.isHidden = true
        self.btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
        self.searchBar.text = ""
        filteredData.removeAll()
        tblViewSearch.reloadData()
       // self.tableView.delaysContentTouches = true
        //call webservice to get all the tasks & events
        self.getEventTaskListsWebService()
    }
    
    //MARK: Search Button Click Methods
    @IBAction func btnSearchClick(_ sender: UIButton) {
        if self.searchView.tag == 0
        {
            self.searchView.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchView.tag = 1
            self.btn_topSearch.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
            self.searchBar.text = ""
            filteredData.removeAll()
            
            self.searchView.isHidden = true
            self.searchBar.endEditing(true)
            self.btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchView.tag = 0
            tblViewSearch.reloadData()
            
            self.getEventTaskListsWebService()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: SearchBar Delegate Method
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.tblViewSearch.isHidden = false
        filteredData.removeAll()
        
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            self.tblViewSearch.isHidden = true
        }
        else
        {
            //if searchText.length >= 3
            //{
            self.fetchSearchdataWebServiceCall(searchText: searchText)
            // }
        }
        tblViewSearch.reloadData()
    }
    
    //MARK: Button Click Methods
    @IBAction func topRightAddBtnClicked(_ sender: Any) {
        
        if self.topFilter == "calendar_event"
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CalendarEventVC") as! CalendarEventVC
            controller.isforupdate = false
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.team_name = self.team_name
            controller.push()
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CreateTaskVC") as! CreateTaskVC
            controller.isforupdate = false
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.team_name = self.team_name
            controller.push()
        }
    }
    
    @IBAction func btnTopCalendarandTaskClicked(_ sender: Any) {
        if self.btn_TopCalendar.tag == 1
        {
            self.btn_TopCalendar.tag = 0
            self.btn_TopTask.tag = 1
            
            self.btn_TopTask.backgroundColor = UIColor.openFilterColor
            self.btn_TopTask.setTitleColor(UIColor.white, for: .normal)
            
            self.btn_TopCalendar.backgroundColor = UIColor.white
            self.btn_TopCalendar.setTitleColor(UIColor.darkGray, for: .normal)
            self.topFilter = "task_event"
            
            DispatchQueue.main.async {
               self.tableView.reloadData()
                 self.calendarView.reloadData()
            }
           
            
        }else{
            self.btn_TopCalendar.tag = 1
            self.btn_TopTask.tag = 0
            
            self.btn_TopCalendar.backgroundColor = UIColor.openFilterColor
            self.btn_TopCalendar.setTitleColor(UIColor.white, for: .normal)
            
            self.btn_TopTask.backgroundColor = UIColor.white
            self.btn_TopTask.setTitleColor(UIColor.darkGray, for: .normal)
            self.topFilter = "calendar_event"
            DispatchQueue.main.async {
                self.tableView.reloadData()
                 self.calendarView.reloadData()
            }
           
        }
        
    }
    
    @IBAction func btnTopShowEventsClicked(_ sender: Any) {
        if self.topFilter == "calendar_event"
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CalendarVC") as! CalendarVC
            controller.isfrom = "event"
            controller.selectedDate = self.selectedDate
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.push()
        }else
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"CalendarVC") as! CalendarVC
            controller.isfrom = "task"
            controller.selectedDate = self.selectedDate
            controller.team_id = self.team_id
            controller.team_member_id = self.team_member_id
            controller.push()
        }
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        if self.isfromTeam == true
        {
            _=navigationController?.popViewController(animated: true)
        }else{
            sideMenuVC.toggleMenu()
        }
    }
    
    //MARK: Export File
    @IBAction func downloadPDFBtnClicked(_ sender: Any) {
        
        if self.topFilter == "calendar_event"
        {
            self.fetchEventorTaskPDFFileWebService(webservice: "export_calendar_event")
        }else{
            self.fetchEventorTaskPDFFileWebService(webservice: "export_calendar_task")
        }
    }

    
    //MARK: UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblViewSearch
        {
            return filteredData.count
        }else{
            
            if self.topFilter == "calendar_event"
            {
                cellCount1 = self.arrEvents.count
                return self.arrEvents.count
            }else
            {
                
                return self.arrTasks.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTaskCell", for: indexPath) as! EventTaskCell
        cell.selectionStyle = .none
        
        if tableView == self.tblViewSearch
        {
            if self.topFilter == "calendar_event"
            {
                cell.cellSetupFor(dictRecord: filteredData[indexPath.row] as NSDictionary, isEvent: true)
            }else
            {
                cell.cellSetupFor(dictRecord: filteredData[indexPath.row] as NSDictionary, isEvent: false)
            }
            
        }else{
            if self.topFilter == "calendar_event"
            {
                print(arrEvents)
                DispatchQueue.main.async {
                    cell.cellSetupFor(dictRecord: self.arrEvents[indexPath.row] as NSDictionary, isEvent: true)
                }
               
            }else
            {
                DispatchQueue.main.async {
                    cell.cellSetupFor(dictRecord: self.arrTasks[indexPath.row] as NSDictionary, isEvent: false)
                }
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            var data = NSDictionary()
            
            if tableView == self.tblViewSearch
            {
                data = filteredData[indexPath.row] as NSDictionary
            }else{
                if self.topFilter == "calendar_event"
                {
                    data = arrEvents[indexPath.row] as NSDictionary
                }else
                {
                    data = arrTasks[indexPath.row] as NSDictionary
                }
            }
            
            
            if data["customer_id"] as? String == webService_obj.Retrive("User_Id") as? String
            {
                if tableView == self.tblViewSearch
                {
                    if self.topFilter == "calendar_event"
                    {
                        self.deleteEventorTaskWebService(event_id: data["id"]! as! String, type: "event", search: true)
                    }else{
                        self.deleteEventorTaskWebService(event_id: data["id"]! as! String, type: "task", search: true)
                    }

                }
                else{
                    if self.topFilter == "calendar_event"
                    {
                        self.deleteEventorTaskWebService(event_id: data["id"]! as! String, type: "event", search: false)
                    }else{
                        self.deleteEventorTaskWebService(event_id: data["id"]! as! String, type: "task", search: false)
                    }
                }
            }else{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        if tableView == self.tblViewSearch
        {
            if self.topFilter == "calendar_event"
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"CalendarEventVC") as! CalendarEventVC
                controller.selectedDataDict = filteredData[indexPath.row]
                controller.isforupdate = true
                controller.team_id = self.team_id
                controller.team_member_id = self.team_member_id
                controller.push()
                
            }else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"CreateTaskVC") as! CreateTaskVC
                controller.selectedDataDict = filteredData[indexPath.row]
                controller.isforupdate = true
                controller.team_id = self.team_id
                controller.team_member_id = self.team_member_id
                controller.push()
            }
            
        }else{
            if self.topFilter == "calendar_event"
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"CalendarEventVC") as! CalendarEventVC
                controller.selectedDataDict = arrEvents[indexPath.row]
                controller.isforupdate = true
                controller.team_id = self.team_id
                controller.team_member_id = self.team_member_id
                controller.push()
                
            }else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"CreateTaskVC") as! CreateTaskVC
                controller.selectedDataDict = arrTasks[indexPath.row] 
                controller.isforupdate = true
                controller.team_id = self.team_id
                controller.team_member_id = self.team_member_id
                controller.push()
            }
        }
    }
    
    
    
    func deleteEventorTaskWebService(event_id:String, type:String, search:Bool) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "id":event_id,
                    "type":type]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_calendar", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
               
                if search == true
                {
                    self.searchBar.text = ""
                    self.filteredData.removeAll()
                    
                    self.searchView.isHidden = true
                    self.searchBar.endEditing(true)
                    self.btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
                    self.searchView.tag = 0
                    self.tblViewSearch.reloadData()
                }
                     self.getEventTaskListsWebService()
                
            }
        }
    }
    
    
    func fetchEventorTaskPDFFileWebService(webservice:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "date":self.selectedDate,
                    "type":self.filterType]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: webservice, withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                let data = response.value(forKey: "file_url") as! String
                
                let url = URL(string: data)
                let xldata = try? Data(contentsOf: url!)
                
                let fileManager = FileManager.default
                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("report.xlsx")
                
                fileManager.createFile(atPath: paths as String, contents: xldata, attributes: nil)
                
                if let fileData = NSData(contentsOfFile: paths){
                    
                    //yatindra
                    
                    if MFMailComposeViewController.canSendMail() {
                        let composeVC = MFMailComposeViewController()
                        composeVC.mailComposeDelegate = self
                            as? MFMailComposeViewControllerDelegate
                        // Configure the fields of the interface.
                        composeVC.setToRecipients([""])
                        composeVC.setSubject("Task and Events Report.")
                        composeVC.setMessageBody("Hello, \n Please find the attached Events and Tasks Report.", isHTML: false)
                        composeVC.addAttachmentData(fileData as Data, mimeType: "application/vnd.ms-excel", fileName: "report.xlsx")
                        
                        DispatchQueue.main.async {
                            self.present(composeVC, animated: true, completion: nil)
                        }
                    } else {
                        
                        let alertMessage = UIAlertController(title: "MYS", message:"Mail services are not available", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                        
                        return
                    }
              
                    // Present the view controller modally.
                }
            }
        }
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    //MARK: UICollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterTag.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FilterTagCell
        cell.label.text = filterTag[indexPath.row]
        cell.label.cornerRadius = 2.5
        cell.color = filterColors[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
         if filterTag[indexPath.row] == "All  " {
            self.filterType = "calendar"
        }
        else if filterTag[indexPath.row] == "Open" {
            self.filterType = "open"
        }
        else if filterTag[indexPath.row] == "Ongoing" {
            self.filterType = "ongoing"
        }
        else if filterTag[indexPath.row] == "Past Due" {
            self.filterType = "overdue"
        }
        else if filterTag[indexPath.row] == "Completed" {
            self.filterType = "completed"
        }
        else{ //"Upcoming Task"
            self.filterType = "upcoming"
        }
        
        self.getEventTaskListsWebService()
    }
    
    private let _font = UIFont(name: "Poppins-Regular", size: 11)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let w = filterTag[indexPath.row].width(withConstraintedHeight: collectionView.bounds.height, font: _font!)
        return CGSize(width: w+41, height: collectionView.bounds.height)
    }
    
    
    // MARK: - Web-Service Methods
    func getEventTaskListsWebService() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "date":self.selectedDate,
                    "type":self.filterType,
                    "team_id":self.team_id,
                    "team_member_id":self.team_member_id,
                    "team_data":self.isteamData]
        
        webService_obj.fetchCalendarDataFromServer(alertMsg: false, header: "get_calender", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                let completeData = response.value(forKey: "data") as! [String: Any]
                self.arrTasks.removeAll()
                self.arrEvents.removeAll()
                self.selectedTaskDates.removeAll()
                self.selectedEventDates.removeAll()
                
                
                let dateData = response.value(forKey: "data_list") as! [String:[String]]
                //Set Event and Task date background color
                self.selectedTaskDates = dateData["task_date"] ?? []
                self.selectedEventDates = dateData["event_date"] ?? []
                self.calendarView.reloadData()
                
                
                self.arrTasks = completeData["task"] as! [[String:Any]]
                self.arrEvents = completeData["event"] as! [[String:Any]]
                    self.tableView.reloadData()
                
                 self.layoutSetting()
                // self.collectionView.reloadData()
            }else
            {
                self.selectedTaskDates.removeAll()
                self.selectedEventDates.removeAll()
                self.arrTasks.removeAll()
                self.arrEvents.removeAll()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
                let dateData = response.value(forKey: "data_list") as! [String:[String]]
                //Set Event and Task date background color
                self.selectedTaskDates = dateData["task_date"] ?? []
                self.selectedEventDates = dateData["event_date"] ?? []
                self.calendarView.reloadData()
            }
        }
    }
    
    
    func fetchSearchdataWebServiceCall(searchText :String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "search":searchText,
                    "team_data":self.isteamData,
                    "team_member_id":self.team_member_id,
                    "team_id":self.team_id]
        
        
        if self.topFilter == "calendar_event"
        {
            PostData["type"] = "event"
        }else{
            PostData["type"] = "task"
        }
        
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"search_calender", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                self.filteredData = responce.value(forKey: "data") as! [[String:Any]]
                self.tblViewSearch.reloadData()
            }
        }
    }
    
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    
    //MARK: FSCalendarDelegate, FSCalendarDataSource
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MMMM dd, yyyy"
        let string = dateFormater.string(from: date)
        calendarDateLabel = string
        
        
        //Convert in date format
        dateFormater.dateFormat = "yyyy-MM-dd"
        self.selectedDate = dateFormater.string(from: date)
        
        self.getEventTaskListsWebService()
     }
    
     func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        
        let currentYear = calendar.year(of: calendar.currentPage)
        let currentMonth = calendar.month(of: calendar.currentPage)
        self.selectedDate = "\(currentYear)-\(currentMonth)-1"
        print("this is the current Date--> \(self.selectedDate)")
        
        self.getEventTaskListsWebService()
    }
    
    //Show default dates color
    public func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor?{
        
        let dateString = self.dateFormatter1.string(from: date)
        let currentDate = self.dateFormatter1.string(from: Date())
        
        if self.topFilter == "calendar_event"
        {
            if self.selectedEventDates.contains(dateString) {
                
                if dateString == currentDate
                {
                    return UIColor.black
                }else{
                    
                    if self.filterType == "overdue"
                    {
                        return UIColor.overDueFilterColor
                    }else if self.filterType == "open"
                    {
                        return UIColor.openFilterColor
                    }else if self.filterType == "ongoing"
                    {
                        return UIColor.onGoingFilterColor
                    }else if self.filterType == "completed"
                    {
                        return UIColor.completedFilterColor
                    }else{
                        return UIColor(red: 183/255, green: 229/255, blue: 247/255, alpha: 1.0)
                    }
                }
            }
            else
            {
                return nil
            }
        }else{
            if self.selectedTaskDates.contains(dateString) {
                if dateString == currentDate
                {
                    return UIColor.black
                }else{
                     if self.filterType == "overdue"
                    {
                        return UIColor.overDueFilterColor
                    }else if self.filterType == "open"
                    {
                        return UIColor.openFilterColor
                    }else if self.filterType == "ongoing"
                    {
                        return UIColor.onGoingFilterColor
                    }else if self.filterType == "completed"
                    {
                        return UIColor.completedFilterColor
                    }else{
                        return UIColor(red: 183/255, green: 229/255, blue: 247/255, alpha: 1.0)
                    }
                }
            }
            else
            {
                return nil
            }
        }
    }
    
    //Show default dates title color
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor?
    {
        let dateString = self.dateFormatter1.string(from: date)
        let currentDate = self.dateFormatter1.string(from: Date())
        
        if self.topFilter == "calendar_event"
        {
            if self.selectedEventDates.contains(dateString) {
                if dateString == currentDate
                {
                    return UIColor.yellow
                }else{
                    return UIColor.white
                }
            }
            else{
                return nil
            }
            
        }else{
            if self.selectedTaskDates.contains(dateString) {
                if dateString == currentDate
                {
                    return UIColor.yellow
                }else{
                    return UIColor.white
                }
            }else{
                return nil
            }
        }
    }
    
    //Set date Selected
    // for dates in self.selectedDatess {
    // // self.calendarView.select(self.dateFormatter1.date(from: dates))
    // }
    //Show selected dates color
    //    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
    //
    //        let dateString = self.dateFormatter1.string(from: date)
    //
    //        if self.selectedDatess.contains(dateString) {
    //            return UIColor.green
    //        }
    //
    //        return appearance.selectionColor
    //    }
    
}
