 //
//  ExpenseVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac102 on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class CustomeExpenseCell: UITableViewCell {
    @IBOutlet weak var expenseImg: SLImageView!
    @IBOutlet weak var expenseImgBtn: UIButton!
    
    @IBOutlet weak var expenseTitle: UILabel!
    @IBOutlet weak var expenseDate: UILabel!
    @IBOutlet weak var expenseAddress: UILabel!
    @IBOutlet weak var expensesCategory: UILabel!
    @IBOutlet weak var expensesPrice: UILabel!
    
    @IBOutlet weak var userPosition: UILabel!
}

class ExpenseVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var expeseTableView: UITableView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnGroup: UIButton!
    @IBOutlet weak var topRightAddButtonConstaint: NSLayoutConstraint!
    @IBOutlet weak var lblNoExpenses: UILabel!
    var expensesData = [[String:String]]()
    var isfromMyTeam = Bool()
    var team_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
         self.lblNoExpenses.isHidden = true
        
        if self.isfromMyTeam == true
        {
            self.btnGroup.isHidden = true
            topRightAddButtonConstaint.constant = 0
            getTeamExpensesWebService(team_id:self.team_id)
        }else{
            self.btnGroup.isHidden = false
            topRightAddButtonConstaint.constant = 44
            getIndividualExpensesWebService()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.expensesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomeExpenseCell", for: indexPath) as! CustomeExpenseCell
        
        let data = self.expensesData[indexPath.row]
        
        cell.expenseTitle.text = data["sub_expense_type"]
        cell.expenseDate.text = data["date"]
        cell.expensesCategory.text = data["vendor"]
        cell.expenseAddress.text = "\(String(describing: data["address"] ?? "")) -"
        cell.userPosition.text = data["position"]
        
        let amt = (data["amount"]! as NSString).doubleValue
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:amt))
        
        cell.expensesPrice.text =  ("$") +  (formattedNumber!)
        
        if data["image"] != ""
        {
            cell.expenseImg.isHidden = false
            
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.expenseImg.af_setImage(withURL: url as URL)
            
            //Button Expense Image 
            cell.expenseImgBtn.tag = indexPath.row
            cell.expenseImgBtn.addTarget(self, action: #selector(expenseImgBtnClicked(_:)), for: .touchUpInside)
            
        }else{
            cell.expenseImg.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            let data = self.expensesData[indexPath.row]
            
            if data["customer_id"] == webService_obj.Retrive("User_Id") as? String
            {
                self.deleteExpenseWebService(expense_id: data["id"]!)
            }else{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
             }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.isfromMyTeam == true
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddExpenceVC") as! AddExpenceVC
            controller.isFromAdd = "false"
            controller.expense_detail = self.expensesData[indexPath.row]
            controller.isfromMyTeam = true
            controller.team_id = self.team_id
            controller.push()
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddExpenceVC") as! AddExpenceVC
            controller.isFromAdd = "false"
            controller.expense_detail = self.expensesData[indexPath.row]
            controller.isfromMyTeam = false
            controller.team_id = ""
            controller.group_id = self.expensesData[indexPath.row]["group_id"]!
            controller.push()
        }
    }
    
    @IBAction func expenseImgBtnClicked(_ sender: UIButton)
    {
        //let cell = expeseTableView.dequeueReusableCell(withIdentifier: "CustomeExpenseCell", for: [0, sender.tag]) as! CustomeExpenseCell
        
        let cell = sender.superview!.superview as! CustomeExpenseCell
        
        let newImageView = UIImageView(image: cell.expenseImg.image)
        let viewNew = expeseTableView.superview!.superview!.superview!.superview!.superview!
        
        newImageView.frame = viewNew.frame
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        viewNew.addSubview(newImageView)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
 
    
    @IBAction func bntFolderClick(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ExpenseListVC") as! ExpenseListVC
        controller.isFromGroup = "false"
        controller.push()
    }
    
    @IBAction func bntAddClick(_ sender: AnyObject) {
        if self.isfromMyTeam == true
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddExpenceVC") as! AddExpenceVC
            controller.isFromAdd = "true"
            controller.isfromMyTeam = true
            controller.team_id = self.team_id
            controller.push()
            
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddExpenceVC") as! AddExpenceVC
            controller.isFromAdd = "true"
            controller.isfromMyTeam = false
            controller.team_id = ""
            controller.push()
         }
    }
    
    // MARK: - Web-Service Methods
    func getIndividualExpensesWebService() {
         PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "page":"-1"]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "get_expenses_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.expensesData.removeAll()
                self.expensesData = response.value(forKey: "data") as! [[String: String]]
                
                 if self.expensesData.count == 0
                {
                    self.lblNoExpenses.isHidden = false
                }else{
                    self.lblNoExpenses.isHidden = true
                }
                
                self.expeseTableView.reloadData()
             }else
            {
                self.lblNoExpenses.isHidden = false
                
                self.expensesData.removeAll()
                 self.expeseTableView.reloadData()
            }
        }
    }
  
    func getTeamExpensesWebService(team_id:String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "page":"-1",
                    "team_id":team_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "get_expenses_list_according_team_id", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.expensesData.removeAll()
                self.expensesData = response.value(forKey: "data") as! [[String: String]]
                self.expeseTableView.reloadData()
            }else
            {
                self.expensesData.removeAll()
                self.expeseTableView.reloadData()
            }
        }
    }

    
    func deleteExpenseWebService(expense_id:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "group_id":"",
                    "expense_id":expense_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_expense", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                if self.isfromMyTeam == true
                {
                    self.getTeamExpensesWebService(team_id:self.team_id)
                }else{
                    self.getIndividualExpensesWebService()
                }
            }
        }
    }

}
