 //
 //  ExpenseDetailVC.swift
 //  MARITIME YACHT SERVICES
 //
 //  Created by Mac102 on 22/08/17.
 //  Copyright © 2017 OctalSoftware. All rights reserved.
 //
 
 import UIKit
 
 class cellExpenceDetail: UITableViewCell {
    
    @IBOutlet weak var expenseImg: UIImageView!
    @IBOutlet weak var expenseImgBtn: UIButton!
    @IBOutlet weak var expenseTitle: UILabel!
    @IBOutlet weak var expenseDate: UILabel!
    @IBOutlet weak var expenseAddress: UILabel!
    @IBOutlet weak var expensesCategory: UILabel!
    @IBOutlet weak var expensesPrice: UILabel!
 }
 
 class ExpenseDetailVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var detailTableView: UITableView!
    
    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_Comment: UILabel!
    @IBOutlet weak var lblNoExpenses: UILabel!
    
    var group_id = ""
    var expense_id = ""
    var completeData = [String:Any]()
    var expensesData = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.lblNoExpenses.isHidden = true
        self.getGroupDetailWebService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.expensesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellExpenceDetail", for: indexPath) as! cellExpenceDetail
        
        let data = self.expensesData[indexPath.row]
 
        cell.expenseTitle.text = data["sub_expense_type"]
        cell.expenseDate.text = data["date"]
        cell.expensesCategory.text = data["vendor"]
        cell.expenseAddress.text = "\(String(describing: data["address"] ?? "")) -"
        //cell.expensesPrice.text = "$\(String(describing: data["amount"] ?? "0"))"
        
        let amt = (data["amount"]! as NSString).doubleValue
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:amt))
        
        cell.expensesPrice.text =  ("$") +  (formattedNumber!)
        
        if data["image"] != ""
        {
            cell.expenseImg.isHidden = false
        }else{
            cell.expenseImg.isHidden = true
        }
        
        if data["image"] != ""
        {
            cell.expenseImg.isHidden = false
            
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.expenseImg.af_setImage(withURL: url as URL)
            
            //Button Expense Image
            cell.expenseImgBtn.tag = indexPath.row
            cell.expenseImgBtn.addTarget(self, action: #selector(expenseImgBtnClicked(_:)), for: .touchUpInside)
            
        }else{
            cell.expenseImg.isHidden = true
        }

        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let data = self.expensesData[indexPath.row]
            self.deleteExpenseWebService(expense_id: data["id"]!)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddExpenceVC") as! AddExpenceVC
        controller.isFromAdd = "false"
        controller.group_id = self.group_id
        controller.expense_detail = self.expensesData[indexPath.row]
        controller.push()
    }
    
    
    @IBAction func expenseImgBtnClicked(_ sender: UIButton)
    {
        //let cell = expeseTableView.dequeueReusableCell(withIdentifier: "CustomeExpenseCell", for: [0, sender.tag]) as! CustomeExpenseCell
        
        let cell = sender.superview!.superview as! cellExpenceDetail
        
        let newImageView = UIImageView(image: cell.expenseImg.image)
        let viewNew = detailTableView.superview!.superview!.superview!.superview!.superview!
        
        newImageView.frame = viewNew.frame
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        viewNew.addSubview(newImageView)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
    
    
    @IBAction func btnAddClick(_ sender: AnyObject) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil , preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Add New Expense", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Add New Expense click")
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddExpenceVC") as! AddExpenceVC
            controller.isFromAdd = "true"
            controller.group_id = self.group_id
            controller.push()
        })
        
        let saveAction = UIAlertAction(title: "Add From Existing Expense", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Add From Existing Expense click")
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"ExpenseListVC") as! ExpenseListVC
            
            controller.isFromGroup = "true"
            controller.group_id = self.group_id
            controller.push()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    // MARK: - Web-Service Methods
    func getGroupDetailWebService() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "group_id":self.group_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "group_expense_info", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                
                self.completeData.removeAll()
                self.expensesData.removeAll()
                
                self.completeData = response.value(forKey: "data") as! [String:Any]
                
                self.lbl_header.text = self.completeData["name"] as? String
                self.lbl_Name.text = self.completeData["customer_name"] as? String
                self.lbl_Date.text = self.completeData["date"] as? String
                self.lbl_Comment.text = self.completeData["comment"] as? String
                 if self.completeData["amount"] as? String == ""
                {
                    self.lbl_Price.text = "$0.00"
                 }else
                {
                     let amt = (self.completeData["amount"]! as! NSString).doubleValue
                    let numberFormatter = NumberFormatter()
                    numberFormatter.numberStyle = NumberFormatter.Style.decimal
                    let formattedNumber = numberFormatter.string(from: NSNumber(value:amt))
                    
                    self.lbl_Price.text =  ("$") +  (formattedNumber!)
                }
                
                
                self.expensesData = self.completeData["expense"] as! [[String:String]]
                if self.expensesData.count == 0
                {
                    self.lblNoExpenses.isHidden = false
                }else{
                    self.lblNoExpenses.isHidden = true
                }
                
                self.detailTableView.reloadData()
            }else
            {
                 self.lblNoExpenses.isHidden = false
                self.completeData.removeAll()
                self.expensesData.removeAll()
                self.detailTableView.reloadData()
            }
        }
    }
    
    func deleteExpenseWebService(expense_id:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "group_id":self.group_id,
                    "expense_id":expense_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_expense", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.getGroupDetailWebService()
            }
        }
    }
    
 }
