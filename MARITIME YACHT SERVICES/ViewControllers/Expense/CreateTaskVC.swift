//
//  CreateTaskVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac102 on 04/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class CreateTaskVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var txtFldTitle: UITextField!
    @IBOutlet weak var txtFldCategory: UITextField!
    @IBOutlet weak var txtFldAlert: UITextField!
    @IBOutlet weak var txtFldRepete: UITextField!
    @IBOutlet weak var txtFldPort: UITextField!
    @IBOutlet weak var txtFldLocationOnVessel: UITextField!
    @IBOutlet weak var textviewNotes: UITextView!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    
    @IBOutlet weak var collectionViewImg: UICollectionView!
    @IBOutlet weak var btnEditImage: UIButton!
    @IBOutlet weak var lbl_NoImage: UILabel!
    var imageCollection = [Any]()
    var partData = [String:Any]()
     var imageUpdateData = [[String:String]]()
    @IBOutlet weak var partAddedorNotConst: NSLayoutConstraint!
    @IBOutlet weak var btnPartImage: UIButton!
     @IBOutlet weak var btnpartGear: UIButton!
    
    var startDate = "",endDate = "",selectedStatus = "",selectedVesselPinID = "", selectedCategoryID = ""
    var startDateToUser = "", endDateToUser = ""
    var dateObjectStartDate: NSDate? = nil
    var dateObjectEndDate: NSDate? = nil
    var selectedPriority = ""
    var portLocation = "",portLatitude = "",portLongitude = ""
    
    let backColor = UIColor(colorLiteralRed: 47.0/255.0, green: 157.0/255.0, blue: 1.0, alpha: 1.0)
        
    let overdueColor = #colorLiteral(red: 0.9393905401, green: 0.1034754142, blue: 0.002637132071, alpha: 1)
    let ongoingColor = #colorLiteral(red: 0.9456083179, green: 0.443831563, blue: 0.01756197214, alpha: 1)
    let completedColor = #colorLiteral(red: 0.2609895766, green: 0.827819407, blue: 0.1192210391, alpha: 1)
    
    let filterColors = [#colorLiteral(red: 0.2868720591, green: 0.6213208437, blue: 0.8930993676, alpha: 1),#colorLiteral(red: 0.9456083179, green: 0.443831563, blue: 0.01756197214, alpha: 1),#colorLiteral(red: 0.9393905401, green: 0.1034754142, blue: 0.002637132071, alpha: 1),#colorLiteral(red: 0.2609895766, green: 0.827819407, blue: 0.1192210391, alpha: 1)]
    
    //let openColor = UIColor(colorLiteralRed: 246.0/255.0, green: 209.0/255.0, blue: 26.0/255.0, alpha: 1.0)
    
    var selectedDataDict = [String:Any]()
    var isforupdate = Bool()
    var team_id = ""
    var team_member_id = ""
    var team_name = ""
    var teamData = [[String:String]]()
    
  
    //Assign To
    @IBOutlet weak var lbl_assignMembers: UILabel!
    @IBOutlet weak var assignHeightConst: NSLayoutConstraint!
    @IBOutlet weak var assignArrowImg: UIImageView!
    @IBOutlet weak var lblAssignTo: UILabel!
    var calendarTeamMembersDict = [[String:String]]()
    var membersDict = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let btnCompletedEvent = self.view.viewWithTag(1010) as! UIButton
        let btnOverdueEvent = self.view.viewWithTag(1011) as! UIButton
        let btnOngoingEvent = self.view.viewWithTag(1012) as! UIButton
        let btnOpenEvent = self.view.viewWithTag(1015) as! UIButton
        
        let arr = [btnOpenEvent, btnOngoingEvent, btnOverdueEvent, btnCompletedEvent]
        
        for (index,btn) in arr.enumerated() {
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(filterColors[index], for: .normal)
        }
   
        if isforupdate == true
        {
            //Part Data
            self.partData = self.selectedDataDict["part"] as! [String : Any]
            if self.partData.count > 0
            {
                self.partAddedorNotConst.constant = 85
                self.btnPartImage.isHidden = false
                self.btnpartGear.isHidden = false
                
                if self.partData["part_image"] is UIImage{
                    self.btnPartImage.setImage(self.partData["part_image"] as? UIImage, for: .normal)
                }else{
                    let str = self.partData["part_image"] as! String
                    let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                    self.btnPartImage.af_setImage(for: .normal, url: url as URL)
                }
                
            }else{
                self.partAddedorNotConst.constant = 1
                self.btnPartImage.isHidden = true
                self.btnpartGear.isHidden = true
            }
            

            //Image Data
            self.imageUpdateData = self.selectedDataDict["image"] as! [[String : String]]
            self.imageCollection = self.imageUpdateData.map{ ($0["image"]! ) }
            if self.imageCollection.count > 0
            {
                lbl_NoImage.isHidden = true
            }else{
                lbl_NoImage.isHidden = false
            }
            
            //Assign To
 
            let team_idtxt = self.selectedDataDict["team_id"] as? String
            
            if team_idtxt == "" || team_idtxt == "0"{
                self.lbl_assignMembers.text = self.selectedDataDict["assigned_to"] as? String
            }else{
                self.team_name = self.selectedDataDict["assigned_to"] as! String
                self.team_id = self.selectedDataDict["team_id"] as? String ?? ""
            }

            
            let members = self.selectedDataDict["assigned_to_ids"] as! [[String : String]]
            self.membersDict = members.map{ ($0["customer_id"]! ) }
            self.calendarTeamMembersDict = members
            
            self.txtFldTitle.text = selectedDataDict["title"] as? String
            self.txtFldCategory.text = selectedDataDict["cat_id"] as? String
           // self.selectedCategoryID = (selectedDataDict["cat_id"] as? String)!
            self.txtFldAlert.text = selectedDataDict["alert"] as? String
            self.txtFldRepete.text = selectedDataDict["repeat"] as? String
            self.txtFldPort.text = selectedDataDict["port"] as? String
            self.txtFldLocationOnVessel.text = selectedDataDict["vessel_name"] as? String
            self.selectedVesselPinID = (selectedDataDict["location_of_vessel"] as? String)!
            self.textviewNotes.text = selectedDataDict["note"] as? String
            
            //Set Start Date
            let startdate = selectedDataDict["start_date"]!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: startdate as! String)
            
            self.startDate = dateFormatter.string(from: date!)//Start date
            self.dateObjectStartDate = date! as NSDate
            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
            self.startDateToUser = dateFormatter.string(from: date!)
            self.lblFromDate.text = self.startDateToUser
            
          //Set End Date
            let enddate = selectedDataDict["end_date"]!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date2 = dateFormatter.date(from: enddate as! String)!
            
            self.endDate = dateFormatter.string(from: date2)//Start date
            self.dateObjectEndDate = date2 as NSDate
            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
            self.endDateToUser = dateFormatter.string(from: date2)
            self.lblToDate.text = self.endDateToUser
            
            //Set Priority
            let priority = Int((selectedDataDict["priority"] as! String))
            switch (priority){
            case 1?:
                //By default normal priority should be selected
                let btnNormal = self.view.viewWithTag(1004) as! UIButton
                btnNormal.backgroundColor = backColor
                btnNormal.setTitleColor(UIColor.white, for: .normal)
                selectedPriority = "1"
 
            case 2?:
                let btnMedium = self.view.viewWithTag(1005) as! UIButton
                btnMedium.backgroundColor = backColor
                btnMedium.setTitleColor(UIColor.white, for: .normal)
                selectedPriority = "2"
                
            case 3?:
                let btnHigh = self.view.viewWithTag(1006) as! UIButton
                btnHigh.backgroundColor = backColor
                btnHigh.setTitleColor(UIColor.white, for: .normal)
                selectedPriority = "3"
                
            default: selectedPriority = "1"
            }
            
            //Set Current Status
            let status = Int((selectedDataDict["status"] as! String))
            
            switch (status){
            case 3?:
                //By default normal priority should be selected
                let btnCompletedEvent = self.view.viewWithTag(1010) as! UIButton
                btnCompletedEvent.backgroundColor = completedColor
                btnCompletedEvent.setTitleColor(UIColor.white, for: .normal)
                selectedStatus = "3"
                
            case 4?:
                //By default normal priority should be selected
                let btnOpenEvent = self.view.viewWithTag(1015) as! UIButton
                btnOpenEvent.backgroundColor = backColor
                btnOpenEvent.setTitleColor(UIColor.white, for: .normal)
                selectedStatus = "4"
                
            case 1?:
                let btnOverdueEvent = self.view.viewWithTag(1011) as! UIButton
                btnOverdueEvent.backgroundColor = overdueColor
                btnOverdueEvent.setTitleColor(UIColor.white, for: .normal)
                selectedStatus = "1"
                
            case 0?:
                let btnOngoingEvent = self.view.viewWithTag(1012) as! UIButton
                btnOngoingEvent.backgroundColor = ongoingColor
                btnOngoingEvent.setTitleColor(UIColor.white, for: .normal)
                selectedStatus = "0"
                
            default: selectedStatus = "1"
            }

            
         }else{
            
            //Part Data
            if self.partData.count > 0
            {
                self.partAddedorNotConst.constant = 85
                self.btnPartImage.isHidden = false
                self.btnpartGear.isHidden = false
                
                if self.partData["part_image"] is UIImage{
                    self.btnPartImage.setImage(self.partData["part_image"] as? UIImage, for: .normal)
                }else{
                    let str = self.partData["part_image"] as! String
                    let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                    self.btnPartImage.af_setImage(for: .normal, url: url as URL)
                }
                
            }else{
                self.partAddedorNotConst.constant = 1
                self.btnPartImage.isHidden = true
                self.btnpartGear.isHidden = true
            }

            // Do any additional setup after loading the view.
            //self.view.backgroundColor = UIColor.yellow
            
            //By Default OPEN Status Selected
            selectedStatus = "4"
            let btnOpenEvent = self.view.viewWithTag(1015) as! UIButton
            btnOpenEvent.backgroundColor = backColor
            btnOpenEvent.setTitleColor(UIColor.white, for: .normal)
            
            //By default normal priority should be selected
            let btnNormal = self.view.viewWithTag(1004) as! UIButton
            btnNormal.backgroundColor = backColor
            btnNormal.setTitleColor(UIColor.white, for: .normal)
            selectedPriority = "1"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        //Part Data
        if self.partData.count > 0
        {
            self.partAddedorNotConst.constant = 85
            self.btnPartImage.isHidden = false
            self.btnpartGear.isHidden = false
            
            if self.partData["part_image"] is UIImage{
                self.btnPartImage.setImage(self.partData["part_image"] as? UIImage, for: .normal)
            }else{
                let str = self.partData["part_image"] as! String
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                self.btnPartImage.af_setImage(for: .normal, url: url as URL)
            }
            
        }else{
            self.partAddedorNotConst.constant = 1
            self.btnPartImage.isHidden = true
            self.btnpartGear.isHidden = true
        }
        
        if self.team_id == "" || self.team_id == "0"{
            if self.membersDict.count > 0{
                self.lbl_assignMembers.isHidden = false
                self.assignHeightConst.constant = 70
                
            }else{
                self.lbl_assignMembers.isHidden = true
                self.assignHeightConst.constant = 40
            }
        }else{
            self.lbl_assignMembers.isHidden = false
            self.assignHeightConst.constant = 70
            self.lbl_assignMembers.text = self.team_name
            self.assignArrowImg.isHidden = true
            self.lblAssignTo.text = "Assign To"
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAssignToClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        if self.team_id == "" || self.team_id == "0"{
            let st = UIStoryboard.init(name: "Expense", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"ExpenseTeamList") as! ExpenseTeamList
            controller.isfromCalendar = true
            
            controller.block = { dict in
                if dict.count != 0 {
                    self.calendarTeamMembersDict.removeAll()
                    self.membersDict.removeAll()
                    
                    self.calendarTeamMembersDict = dict
                    self.membersDict = self.calendarTeamMembersDict.map{ ($0["customer_id"]! ) }
                    
                    let membersNameArray = self.calendarTeamMembersDict.map{ ($0["name"]!) }
                    let stringRepresentation = membersNameArray.joined(separator: ", ")
                    self.lbl_assignMembers.text = stringRepresentation
                    
                    print("Selected Members-->\(self.calendarTeamMembersDict)")
                }
                else{
                     self.membersDict.removeAll()
                    self.calendarTeamMembersDict.removeAll()
                }
            }
            controller.calendarTeamMembersDict = self.calendarTeamMembersDict
            controller.push()
        }else{
            
        }
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    override func viewDidLayoutSubviews() {
        
      //  scrollView.translatesAutoresizingMaskIntoConstraints = true
      //  scrollView.contentSize = CGSize(width: scrollView.frame.width, height:800);
    }
    
    //MARK: IBAction
    
    @IBAction func btnAddImageandPartClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Add Images", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if self.imageCollection.count == 7{
                self.AlertMessage("Maximum image upload limit of 6 has been reached.")
            }else{
                self.imagePickerAlert()
            }
        })
        
        let saveAction = UIAlertAction(title: "Add Part", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"AddNewPartVC") as! AddNewPartVC
            
            controller.block = { dict in
                print(dict)
                if dict.count != 0 {
                    self.partData.removeAll()
                    self.partData = dict
                    
                    if self.partData["part_image"] is UIImage{
                        self.btnPartImage.setImage(self.partData["part_image"] as? UIImage, for: .normal)
                    }else{
                        let str = self.partData["part_image"] as! String
                        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                        self.btnPartImage.af_setImage(for: .normal, url: url as URL)
                    }

                }
                else{
                    
                }
            }
            controller.partData = self.partData
            controller.push()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        if self.partData.count > 0
        {
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(cancelAction)
            
        }else{
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
        }

        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnEditImageandPartClicked(_ sender: Any) {
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"AddNewPartVC") as! AddNewPartVC
        
        controller.block = { dict in
            print(dict)
            if dict.count != 0 {
                self.partData.removeAll()
                self.partData = dict
                
                if self.partData["part_image"] is UIImage{
                    self.btnPartImage.setImage(self.partData["part_image"] as? UIImage, for: .normal)
                }else{
                        let str = self.partData["part_image"] as! String
                        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                        self.btnPartImage.af_setImage(for: .normal, url: url as URL)
                }

            }
            else{
                
            }
        }
        controller.partData = self.partData
        controller.push()
    }
    
    
    @IBAction func btnDatePickerTapped(_ sender: UIButton) {
        //1000 : from date
        //1001 : to date
        //1002 : from time
        //1003 : to time
        var title = ""
        var pickerFormat = UIDatePickerMode.date
        var dateFormatToUse = ""
        var dateFormatToDisplay = ""
        
        switch sender.tag {
        case 1000: title = "Select start date"
        pickerFormat = UIDatePickerMode.dateAndTime
        dateFormatToUse = "yyyy-MM-dd HH:mm"
        dateFormatToDisplay = "MM-dd-yyyy HH:mm"
            
        case 1001: title = "Select end date"
        pickerFormat = UIDatePickerMode.dateAndTime
        dateFormatToUse = "yyyy-MM-dd HH:mm"
        dateFormatToDisplay = "MM-dd-yyyy HH:mm"
            
        default: print("Default case")
        }
        
        //Date validations
        if (sender.tag == 1001 && startDate.length == 0){
            
            AlertMessage("First select start date.")
            return
        }
        
        let datePicker = ActionSheetDatePicker(title: title, datePickerMode: pickerFormat, selectedDate: NSDate() as Date, doneBlock: {
            picker,index,value in
            
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = dateFormatToUse
            let date = index as! Date
            
            var flagToChange = false
            
            switch sender.tag {
            case 1000:
                if self.endDate.length != 0 {
                    let oldEndDate = self.dateObjectEndDate! as Date
                    
                    if oldEndDate < date {
                        flagToChange = true
                    }
                }
                
                
                if flagToChange {
                    dateFormatter.dateFormat = dateFormatToUse
                    //change oldEndDate to startDate
                    self.endDate = dateFormatter.string(from: date)//end date
                    self.dateObjectEndDate = date as NSDate
                    dateFormatter.dateFormat = dateFormatToDisplay
                    self.endDateToUser = dateFormatter.string(from: date)
                    self.lblToDate.text = self.endDateToUser
                }
                
                //Set Start Date
                dateFormatter.dateFormat = dateFormatToUse
                self.startDate = dateFormatter.string(from: date)//Start date
                self.dateObjectStartDate = date as NSDate
                dateFormatter.dateFormat = dateFormatToDisplay
                self.startDateToUser = dateFormatter.string(from: date)
                self.lblFromDate.text = self.startDateToUser
                
                
            case 1001:
                dateFormatter.dateFormat = dateFormatToUse
                self.endDate = dateFormatter.string(from: date)//End date
                self.dateObjectEndDate = date as NSDate
                dateFormatter.dateFormat = dateFormatToDisplay
                self.endDateToUser = dateFormatter.string(from: date)
                self.lblToDate.text = self.endDateToUser
                
            default: print("Default case")
            }
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        
        
        //set minimum date validations
        if sender.tag == 1001 {
            datePicker?.minimumDate = dateObjectStartDate! as Date
        }else {
           // datePicker?.minimumDate = Date()
        }
        //        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        
         datePicker?.show()
     }
    
    @IBAction func btnEventStatusTapped(_ sender: UIButton) {
        
        //1010 : Completed Task
        //1011 : Overdue Task
        //1012 : Ongoing Task
        
        let btnBackgroundColor = sender.backgroundColor ?? UIColor.black
        var isAlreadySelected = Bool()
        
        if (btnBackgroundColor.isEqual(UIColor.white))
        {
            isAlreadySelected = false
        }else
        {
            isAlreadySelected = true
        }
        
        let btnCompletedEvent = self.view.viewWithTag(1010) as! UIButton
        let btnOverdueEvent = self.view.viewWithTag(1011) as! UIButton
        let btnOngoingEvent = self.view.viewWithTag(1012) as! UIButton
        let btnOpenEvent = self.view.viewWithTag(1015) as! UIButton
        
        let arr = [btnOpenEvent, btnOngoingEvent, btnOverdueEvent, btnCompletedEvent]
        for (index,btn) in arr.enumerated() {
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(filterColors[index], for: .normal)
        }
        
        guard !isAlreadySelected else {
            
            selectedStatus = ""
            return
        }
        
        switch sender.tag{
            
        case 1010:
            btnCompletedEvent.backgroundColor = completedColor
            btnCompletedEvent.setTitleColor(UIColor.white, for: .normal)
            selectedStatus = "3"
            
        case 1011:
            btnOverdueEvent.backgroundColor = overdueColor
            btnOverdueEvent.setTitleColor(UIColor.white, for: .normal)
            selectedStatus = "1"
            
        case 1012:
            btnOngoingEvent.backgroundColor = ongoingColor
            btnOngoingEvent.setTitleColor(UIColor.white, for: .normal)
            selectedStatus = "0"
            
        case 1015:
            btnOpenEvent.backgroundColor = backColor
            btnOpenEvent.setTitleColor(UIColor.white, for: .normal)
            selectedStatus = "4"

            
        default: selectedStatus = "1"
        }
        
    }
    
    @IBAction func btnPriorityTapped(_ sender: UIButton) {
        
        //1004 : Normal priority
        //1005 : Medium priority
        //1006 : High priority
        
        let btnNormal = self.view.viewWithTag(1004) as! UIButton
        let btnMedium = self.view.viewWithTag(1005) as! UIButton
        let btnHigh = self.view.viewWithTag(1006) as! UIButton
        
        let arr = [btnNormal, btnMedium, btnHigh]
        for btn in arr {
            
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(backColor, for: .normal)
        }
        
        switch sender.tag{
            
        case 1004:
            btnNormal.backgroundColor = backColor
            btnNormal.setTitleColor(UIColor.white, for: .normal)
            selectedPriority = "1"
            
        case 1005:
            btnMedium.backgroundColor = backColor
            btnMedium.setTitleColor(UIColor.white, for: .normal)
            selectedPriority = "2"
            
        case 1006:
            btnHigh.backgroundColor = backColor
            btnHigh.setTitleColor(UIColor.white, for: .normal)
            selectedPriority = "3"
            
        default: selectedPriority = "1"
        }
     }
    
    @IBAction func btnAlertClick(_ sender: Any) {
        self.view.endEditing(true)
        
         let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"CalendarAlertCatRepetVC") as!CalendarAlertCatRepetVC
        controller.block = { dict in
            print(dict)
            if dict.count != 0 {
                
                self.txtFldAlert.text = dict["name"]
            }else{
                self.txtFldAlert.text = ""
            }
        }
        controller.isFrom = "alert"
        controller.selectedRow = self.txtFldAlert.text!
        controller.push()
    }
    
    @IBAction func btnCategoryClick(_ sender: Any) {
        self.view.endEditing(true)
        
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"CalendarAlertCatRepetVC") as!CalendarAlertCatRepetVC
        controller.block = { dict in
            print(dict)
            if dict.count != 0 {
                
                self.txtFldCategory.text = dict["name"]
               // self.selectedCategoryID = dict["id"]!
             }else{
                self.txtFldCategory.text = ""
              //  self.selectedCategoryID = ""
            }
        }
        controller.isFrom = "category"
        controller.selectedRow = self.txtFldCategory.text!
        controller.push()
    }

    @IBAction func btnRepeteClick(_ sender: Any) {
        self.view.endEditing(true)
        
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"CalendarAlertCatRepetVC") as!CalendarAlertCatRepetVC
        controller.block = { dict in
            print(dict)
            if dict.count != 0 {
                 self.txtFldRepete.text = dict["name"]
            }else{
                self.txtFldRepete.text = ""
            }
        }
        controller.isFrom = "repate"
        controller.selectedRow = self.txtFldRepete.text!
        controller.push()
    }
    
    @IBAction func btnLocationVesselClick(_ sender: Any) {
        self.view.endEditing(true)
        
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ChooseLocationInventoryMngViewController") as! ChooseLocationInventoryMngViewController
        
        controller.block = { dict in
            print(dict)
            if dict.count != 0 {
                self.txtFldLocationOnVessel.text = dict["name"]
                self.selectedVesselPinID = dict["id"]!
            }else{
                self.txtFldRepete.text = ""
                self.selectedVesselPinID = ""
            }
        }
         controller.push()
    }
    
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventandTaskImgCell", for: indexPath) as! EventandTaskImgCell
        cell.contentView.layer.cornerRadius = 1.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        cell.contentView.layer.borderWidth = 0.5
        
         if imageCollection[indexPath.row] is UIImage{
            cell.img_Event.image = imageCollection[indexPath.row] as? UIImage
        }else{
            let str = imageCollection[indexPath.row] as! String
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_Event.af_setImage(withURL: url as URL)
        }
        
        cell.btn_delete.tag = indexPath.row
        cell.btn_delete.addTarget(self, action: #selector(btnDeleteImgClicked(_:)), for: .touchUpInside)
        
        if self.imageCollection.count > 0
        {
            lbl_NoImage.isHidden = true
        }else{
            lbl_NoImage.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! EventandTaskImgCell
        
        let newImageView = UIImageView(image: cell.img_Event.image)
        let viewNew = collectionView.superview!.superview!.superview!.superview!.superview!
        
        newImageView.frame = viewNew.frame
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        viewNew.addSubview(newImageView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = 80
        return CGSize(width: size, height: size)
    }
    
    @IBAction func btnDeleteImgClicked(_ sender: UIButton)
    {
        if imageCollection[sender.tag] is UIImage{
            
            self.imageCollection.remove(at: sender.tag)
            self.collectionViewImg.reloadData()
        }else{
            
            let str = imageCollection[sender.tag] as! String
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "image_url":str]
            
            webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_image", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
                if status{
                    self.imageCollection.remove(at: sender.tag)
                    self.collectionViewImg.reloadData()
                }else
                {
                    
                }
            }
            
        }

         if self.imageCollection.count > 0
        {
            lbl_NoImage.isHidden = true
        }else{
            lbl_NoImage.isHidden = false
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
    
    
    // MARK: - Web-Service Methods
    @IBAction func btnCreatePressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.txtFldTitle.text == ""
        {
            AlertMessage("Please enter title.")
        }
        else if startDate == ""
        {
            AlertMessage("Please select start date.")
         }
        else if endDate == ""
        {
            AlertMessage("Please select end date.")
        }
//        else if self.selectedStatus == ""
//        {
//            AlertMessage("Please select current status.")
//        }
//        else if self.txtFldPort.text == ""
//        {
//            AlertMessage("Please select port location.")
//        }
//        else if self.selectedVesselPinID == ""
//        {
//            AlertMessage("Please select location on vessel.")
//        }
//        else if self.textviewNotes.text == "Notes"
//        {
//            AlertMessage("Please enter notes.")
//        }
        else{
            
            let startDateTime = startDate
            let endDateTime = endDate
            
            var notesTXT = ""
            if self.textviewNotes.text == "Notes"
            {
                notesTXT = ""
            }else{
                notesTXT = self.textviewNotes.text
            }
             
            PostData = ["customer_id":self.userId,
                        "title":self.txtFldTitle.text ?? "",
                        "cat_id":self.txtFldCategory.text ?? "",
                        "start_date":startDateTime,
                        "end_date":endDateTime,
                        "priority":selectedPriority,
                        "port":txtFldPort.text ?? "",
                        "location_of_vessel":selectedVesselPinID,
                        "note":notesTXT,
                        "status":self.selectedStatus,
                        "alert":txtFldAlert.text ?? "",
                        "repeat":txtFldRepete.text ?? "",
                        "vessel_name":self.txtFldLocationOnVessel.text ?? ""]
            
            var webserviceHeader = ""
            if isforupdate == true
            {
                webserviceHeader = "edit_task"
                PostData["id"] =  self.selectedDataDict["id"]
            }else{
                webserviceHeader = "add_task"
                PostData["team_id"] =  self.team_id
                PostData["team_member_id"] =  self.team_member_id
            }
            
            if membersDict.count > 0
            {
                PostData["assign_to"] = self.membersDict
            }else{
                var id = [String]()
                 id = [self.userId]
                PostData["assign_to"] = id
            }
            
            var imagesData = [String : UIImage]()
            if imageCollection.count != 0 {
                var strCount : Int = 0
                for (index, img) in imageCollection.enumerated(){
                    if imageCollection[index] is UIImage{
                        strCount += 1
                        imagesData["image\(strCount)"] = img as? UIImage
                    }
                }
                
                if self.partData.count > 0
                {
                    imagesData["part_image"] = self.partData["part_image"] as? UIImage
                    
                    var partsenddata = self.partData
                    partsenddata.removeValue(forKey: "part_image")
                    PostData["part"] = partsenddata
                }
            }else{
                if self.partData.count > 0
                {
                    imagesData["part_image"] = self.partData["part_image"] as? UIImage
                    
                    var partsenddata = self.partData
                    partsenddata.removeValue(forKey: "part_image")
                    PostData["part"] = partsenddata
                }
            }

    
                    
            webService_obj.uploadMultipleImages(header: webserviceHeader, withParameter: PostData, images:imagesData, compressRatio: 0.1, inVC: self, showHud: true) { (response, message, status) in
                if status{
                    
                    _ = self.navigationController?.popViewController(animated:true)
                }
                else
                {
                    //TODO: handle fail api response
                }
            }
           
        }
    }
}

//ImagePicker extension
extension CreateTaskVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        
        if let pImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)?.fixed{
            
            if let imageData = UIImageJPEGRepresentation(pImage, 0.1), let img = UIImage.init(data: imageData) {
                imageCollection.append(img)
                collectionViewImg.reloadData()
            }
        }
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerAlert()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imagePickerCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imagePickerGallery()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}


extension CreateTaskVC: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView.text == TEXTVIEW_PLACEHOLDER{
            textView.text = ""
        }
        
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).length == 0 {
            
            textView.text = TEXTVIEW_PLACEHOLDER
        }
        return true
    }
}

extension CreateTaskVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let st = UIStoryboard.init(name: "Expense", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"AddLoationExpenseVC") as! AddLoationExpenseVC
        controller.onAdressSelection = { [weak self] data in
            
                self?.txtFldPort.text = data["place_name"]
                self?.portLatitude = data["lat"] ?? "0"
                self?.portLongitude = data["long"] ?? "0"
                self?.portLocation = data["place_name"] ?? ""
            
            self?.dismiss(animated: true)
        }
        self.present(controller, animated: true, completion: nil)
        
        return false
    }
}

