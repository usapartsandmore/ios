//
//  ExpenseTeamList.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 22/02/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class TeamExpenseCell: UITableViewCell {
    @IBOutlet weak var teamTitle: UILabel!
    @IBOutlet weak var teamCreateDate: UILabel!
    @IBOutlet weak var teamCreatedBy: UILabel!
}

class ExpenseTeamList: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblviewTeam: UITableView!
    var expensesTeamData = [[String:String]]()
    
    var calendarTeamMembersDict = [[String:String]]()
    var isfromCalendar = Bool()
    var block: (([[String:String]])->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        getExpensesTeamWebService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnBackPressed(_ sender: UIButton) {
        block?(self.calendarTeamMembersDict)
        _ = self.navigationController?.popViewController(animated:true)
    }

    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.expensesTeamData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamExpenseCell", for: indexPath) as! TeamExpenseCell
        
        let data = self.expensesTeamData[indexPath.row]
        
         cell.teamTitle.text = data["name"]
         cell.teamCreateDate.text = data["created"]
         cell.teamCreatedBy.text = data["customer_name"]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.isfromCalendar == true{
            let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"TeamMembersCalendarVC") as! TeamMembersCalendarVC
            let data = self.expensesTeamData[indexPath.row]
            controller.team_id = data["id"]!
            controller.team_name = data["name"]!
            
            controller.block = { dict in
                if dict.count != 0 {
                    self.calendarTeamMembersDict = dict
                    print("Selected Members-->\(self.calendarTeamMembersDict)")
                 }
                else{
                    self.calendarTeamMembersDict.removeAll()
                 }
             }
            controller.selectedRows = self.calendarTeamMembersDict
            controller.push()
         }else{
            let data = self.expensesTeamData[indexPath.row]
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"ExpenseVC") as! ExpenseVC
            controller.isfromMyTeam = true
            controller.team_id = data["id"]!
            controller.push()
        }
    }

    
    // MARK: - Web-Service Methods
    func getExpensesTeamWebService() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"get_team_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.expensesTeamData.removeAll()
                self.expensesTeamData = response.value(forKey: "data") as! [[String: String]]
                self.tblviewTeam.reloadData()
            }else
            {
                self.expensesTeamData.removeAll()
                self.tblviewTeam.reloadData()
            }
        }
    }

}
