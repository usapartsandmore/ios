//
//  AddExpenceVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac102 on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

extension AddExpenceVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        img_Profile.image = pickedImage
        btnImg.setImage(nil, for: .normal)
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}


class AddExpenceVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet var lblHeaderName: UILabel!
    @IBOutlet var btnSave:UIButton!
    var isFromAdd = ""
    
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtVendor: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var txtComments: UITextView!
    @IBOutlet weak var btnImg: UIButton!
    
    @IBOutlet var img_Profile: UIImageView!
    let imagePicker = UIImagePickerController()
    var pickedImage: UIImage!
    var lat = ""
    var long = ""
    var expenseType_id = ""
    var group_id = ""
    
    var isfromMyTeam = Bool()
    var team_id = ""
    
    var expense_detail = [String:String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetPlaceHoder()
        
        txtComments.text = "Comments"
        txtComments.textColor = UIColor.lightGray
        
        if isFromAdd == "true"
        {
            self.lblHeaderName.text="Add Expense"
            self.btnSave.setTitle("SAVE", for: .normal)
         }else{
            self.lblHeaderName.text="Edit Expense"
            self.btnSave.setTitle("UPDATE", for: .normal)
 
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let myDate = dateFormatter.date(from: expense_detail["date"]!)
            dateFormatter.dateFormat = "MMM dd, yyyy"
           
            self.txtDate.text =  dateFormatter.string(from: myDate!)
            
            //txtAmount.text = expense_detail["amount"]
            
            let amt =  Double(expense_detail["amount"]!)
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            let formattedNumber = numberFormatter.string(from: NSNumber(value:amt!))
            
            txtAmount.text = ("$") +  (formattedNumber!)
            
            txtVendor.text = expense_detail["vendor"]
            txtType.text = expense_detail["sub_expense_type"]
            self.expenseType_id = expense_detail["sub_expense_type_id"]!
            txtLocation.text = expense_detail["address"]
            
            
            if expense_detail["comment"] == ""{
                txtComments.text = "Comments"
                txtComments.textColor = UIColor.lightGray
            }else{
                txtComments.textColor = UIColor.darkGray
                txtComments.text = expense_detail["comment"]
            }
            
            lat = expense_detail["lat"]!
            long = expense_detail["lng"]!
            
            if expense_detail["image"] != ""
            {
                btnImg.setImage(nil, for: .normal)
                 let str = expense_detail["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                img_Profile.af_setImage(withURL: url as URL)
            }

         }
        btnSave.layer.masksToBounds = true
        btnSave.layer.cornerRadius = 5
        //
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func SetPlaceHoder()
    {
        txtDate.addPaddingView(width: 10, onSide: 0)
        txtAmount.addPaddingView(width: 10, onSide: 0)
        txtVendor.addPaddingView(width: 10, onSide: 0)
        txtType.addPaddingView(width: 10, onSide: 0)
        txtLocation.addPaddingView(width: 10, onSide: 0)
        txtComments.addPaddingView(width: 10, onSide: 0)
    }
    
    @IBAction func btnbackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
    
    @IBAction func btnImgClick(_ sender: Any) {
        self.AlertController()
    }
    
    @IBAction func btnDateClick(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date, doneBlock: {
            picker,index,value in
            print("index",index)
              print("picker",picker)
             print("value",value)
             //let datePicker = UIDatePicker()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let date = index as! Date
            self.txtDate.text = dateFormatter.string(from: date )
            self.view.endEditing(true)
            
//
//            //Convert in date format
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MMM dd, yyyy"
//            self.txtDate.text = dateFormatter.string(from: datePicker.date)
        print("*********")
            
           
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        //        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        
        datePicker?.show()
     }
    
    @IBAction func btnLocationClick(_ sender: Any) {
         self.view.endEditing(true)
         let st = UIStoryboard.init(name: "Expense", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"AddLoationExpenseVC") as! AddLoationExpenseVC
         controller.onAdressSelection = { [weak self] data in
            if data["place_name"] == "Current Location"
            {
                self?.txtLocation.text = data["address"]
            }else
            {
                self?.txtLocation.text = data["place_name"]
            }
            
            self?.lat = data["lat"] ?? "0"
            self?.long = data["long"] ?? "0"
            
            self?.dismiss(animated: true)
        }
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func btnTypeClick(_ sender: Any) {
          self.view.endEditing(true)
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ExpenseTypeVC") as! ExpenseTypeVC
        controller.block = { dict in
           // self.positionValue = dict
            self.expenseType_id = dict["id"] as! String
           // self.position_name = self.positionValue["name"] as! String
            
            self.txtType.text =  dict["name"] as? String
          }
        controller.push()
     }
    
    
    @IBAction func saveBtnClick(_ sender: Any) {
        
        if txtDate.text! .isEmpty
        {
            self.AlertMessage("Please select date.")
        }
        else if txtAmount.text! .isEmpty
        {
            self.AlertMessage("Please enter Amount.")
        }
        else if txtVendor.text! .isEmpty
        {
            self.AlertMessage("Please select vendor.")
        }
        else if txtType.text! .isEmpty
        {
            self.AlertMessage("Please enter type")
        }
        else if txtLocation.text! .isEmpty
        {
            self.AlertMessage("Please select location.")
        }
        else
        {
            if self.isfromMyTeam == true
            {
                if isFromAdd == "true"{
                    self.addIndividualExpensesWebService()
                }else{
                    if self.expense_detail["customer_id"] == webService_obj.Retrive("User_Id") as? String
                    {
                        self.editIndividualExpensesWebService()
                    }else{
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                }
            }else{
                if isFromAdd == "true"
                {
                    self.addIndividualExpensesWebService()
                }
                else
                {
                    self.editIndividualExpensesWebService()
                }
            }
        }
     }
    
    // MARK: - Web-Service Methods
    func addIndividualExpensesWebService() {
        
        var str = self.txtAmount.text?.replacingOccurrences(of: ",", with: "")
        str = str?.replacingOccurrences(of: "$", with: "")
         let amt = String(format: "%.2f", Double(str!)!)
        
        
        
        
        
        //Convert in date format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let myDate = dateFormatter.date(from: self.txtDate.text!)
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dob =  dateFormatter.string(from: myDate!)
        print("*****",dob)
        var comntTxt = ""
        if self.txtComments.text == "Comments"{
            comntTxt = ""
        }else{
            comntTxt = self.txtComments.text
        }
        
         PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "date":self.txtDate.text!,
                    "amount":amt,
                    "vendor":self.txtVendor.text!,
                    "sub_expense_type_id":self.expenseType_id,
                    "address":self.txtLocation.text!,
                    "lat":self.lat,
                    "lng":self.long,
                    "group_id":self.group_id,
                    "comment":comntTxt]
        
        if isfromMyTeam == true{
            PostData["team_id"] = self.team_id
        }

        var imgDict = [String:UIImage?]()
        
        if (self.pickedImage?.fixed) != nil {
             imgDict = ["image":self.pickedImage?.fixed]
        }
        
        webService_obj.callAPiFor(subApiName: "add_expenses", parameter: PostData as! [String : String], images: imgDict, view: self.view!, inVC: self) { (response, msg, status) in
            if status{
             _ = self.navigationController?.popViewController(animated:true)
            }
        }
    }
    
  
    func editIndividualExpensesWebService() {
        
        
        //Convert in date format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let myDate = dateFormatter.date(from: self.txtDate.text!)
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dob1 =  dateFormatter.string(from: myDate!)
        print("*****",dob1)
        
        var str = self.txtAmount.text?.replacingOccurrences(of: ",", with: "")
        str = str?.replacingOccurrences(of: "$", with: "")
        let amt = String(format: "%.2f", Double(str!)!)
        var comntTxt = ""
        if self.txtComments.text == "Comments"{
            comntTxt = ""
        }else{
            comntTxt = self.txtComments.text
        }

        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "id":self.expense_detail["id"]!,
                    "date":dob1,
                    "amount":amt,
                    "vendor":self.txtVendor.text!,
                    "sub_expense_type_id":self.expenseType_id,
                    "address":self.txtLocation.text!,
                    "lat":self.lat,
                    "lng":self.long,
                    "group_id":self.group_id,
                    "comment":comntTxt]
        
        if isfromMyTeam == true{
            PostData["team_id"] = self.team_id
        }
        
        var imgDict = [String:UIImage?]()
        
        if (self.pickedImage?.fixed) != nil {
            imgDict = ["image":self.pickedImage?.fixed]
        }
        
        webService_obj.callAPiFor(subApiName: "edit_expenses", parameter: PostData as! [String : String], images: imgDict, view: self.view!, inVC: self) { (response, msg, status) in
            print("***********",response)
            if status{
                _ = self.navigationController?.popViewController(animated:true)
            }
            
        }
    }
    
    
    
    // MARK: - Method Image Picker Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imageCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imageGallery()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    // MARK: UITextFieldDelegate Method
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtAmount{
            if let amt = Double(txtAmount.text!){
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value:amt))
                txtAmount.text = ("$") +  (formattedNumber!)
            }
        }
        return true
    }

    
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        //One Decimal and only two value after decimal
        if range.length == 1{
            return true
        }
        
        if let text = textField.text{
            let array = text.components(separatedBy: ".")
            if array.count > 1 && string == "."{
                return false
            }
            
            if array.count > 1{
                return array[1].length < 2
            }
        }
        
        return true
    }
    
    
    // MARK: - TextView Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Comments"
            textView.textColor = UIColor.lightGray
        }
    }

 }
