//
//  temp.swift
//  MARITIME YACHT SERVICES
//
//  Created by Narottam on 05/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import Foundation

let categoryArray:[[Any]] = [["Anchor & Docking", #imageLiteral(resourceName: "imag_1.jpg")],
                             ["Boat, Motor & Parts", #imageLiteral(resourceName: "imag_2.jpg")],
                             ["Electrical", #imageLiteral(resourceName: "imag_3.jpg")],
                             ["Electonics & Navigation", #imageLiteral(resourceName: "imag_4.jpg")],
                             ["Fishing", #imageLiteral(resourceName: "imag_5.jpg")],
                             ["Gallery & Outdoor", #imageLiteral(resourceName: "imag_6.jpg")],
                             ["Maintenance & Hardware", #imageLiteral(resourceName: "imag_7.jpg")],
                             ["Mens", #imageLiteral(resourceName: "imag_8.jpg")],
                             ["Padding", #imageLiteral(resourceName: "imag_9.jpg")],
                             ["Plumbing & Ventilation", #imageLiteral(resourceName: "imag_10.jpg")],
                             ["Safety", #imageLiteral(resourceName: "imag_11.jpg")],
                             ["Salling", #imageLiteral(resourceName: "imag_12.jpg")],
                             ["Shoes", #imageLiteral(resourceName: "imag_13.jpg")],
                             ["Water Sports", #imageLiteral(resourceName: "imag_14.jpg")],
                             ["Womens", #imageLiteral(resourceName: "imag_15.jpg")]
]

let paintersArray: [[Any]] = [["National Marine Suppliers", "National Marine Suppliers is a worldwide yacht support group providing the most professional service and on-time reliability in the world. We are truly the worlds #1 choice for megayacht owners, captains and crew.", #imageLiteral(resourceName: "img0.png")],
                              ["Pinmar Finishing","From their bases in Palma de Mallorca and Barcelona in Spain, as well as numerous locations throughout Europe, Pinmar Finishing has been finishing and repainting the finest superyachts in the world for over 35 years.", #imageLiteral(resourceName: "image_2.jpg")],
                              ["Premier Yacht Services Ltd", "Premier Yacht Services specialise in yacht painting & varnishing, from minor repairs to complete refinishing, from substrate to top coat. Premier is a fully mobile refinishing operation allowing for work to be completed at the yachts location.", #imageLiteral(resourceName: "image_3.jpg")],
                              ["Hargittai & Shaw", "Hargittai & Shaw is an experienced car, vehicle, and yacht wrapping company based in Davie, FL.", #imageLiteral(resourceName: "image_4.jpg")],
                              ["Thraki Yacht Painting GmbH", "Thraki Yacht Painting TYP is a superyacht finishing company based in Northern Germany, employing a talented and experienced yacht painting team. Whether your yacht needs exterior painting, refinishing or more, TYP ensures exceptional quality.", #imageLiteral(resourceName: "image_5.png")],
                              ["BenyMar Yachtpaint S.L.", "BenyMar Yachtpaint specialises in the painting and repainting of yachts, superyachts and helicopters from its bases in Palma de Mallorca and Varadero Port Denia, Spain.", #imageLiteral(resourceName: "image_6.png")],
                              ["General Coating GmbH", "General Coating GmbH is a marine coating specialist founded in 2001. Its expertise are grounded in complete metal preparation and dressing, corrosion control and yacht finishing, using standard methods for outstanding exterior and interior...", #imageLiteral(resourceName: "image_7.jpg")],
                              ["Topcoat Yacht Painting","Topcoat Yacht Painting works with the industry’s leading products and highly experienced technicians to give any sized yacht or superyacht visiting Antibes, France the best possible finish.", #imageLiteral(resourceName: "image_8.png")],
                              ["Garcia Yachtcoating", "Garcia Yachtcoating is a marine surface protection specialist founded in 2008 using ceramic sealants developed and produced in Germany, to offer highest quality standards in the yacht industry.", #imageLiteral(resourceName: "image_9.jpg")],
                              ["Rolling Stock","Rolling Stock Finishing is a world leading painting company serving the yachting and superyacht industries. Based in Palma de Mallorca and offering worldwide services, their team of experts provide the perfect finish for every paint job.", #imageLiteral(resourceName: "image_10.jpg")]
]
