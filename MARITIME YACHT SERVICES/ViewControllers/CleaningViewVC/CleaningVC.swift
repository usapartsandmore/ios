//
//  CleaningVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class CleaningVC: UIViewController {
    
    @IBOutlet var lbl_header: UILabel!
    @IBOutlet var imageview: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblOfferPrice: UILabel!
    @IBOutlet var lblOriginalPrice: UILabel!
    @IBOutlet var lblServiceNameInOfferDetail: UILabel!
    
    @IBOutlet var lblServicePlace: UILabel!
    @IBOutlet var rateingView: FloatRatingView!
    @IBOutlet var lblTotaRecentlReviews: UILabel!
    @IBOutlet weak var lbl_Offer_detail: UILabel!
    @IBOutlet weak var lbl_FinePrint: UILabel!
    @IBOutlet weak var lblReturnPolicy: UILabel!
    
    @IBOutlet weak var btn_Buy_Services: UIButton!
    
    var ServiceData = [String:String]()
    var localOffer_id = ""
    var isfromMyOrders = Bool()
    
    
    @IBOutlet weak var lbl_Center: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isfromMyOrders ==  true{
            btn_Buy_Services.isHidden = true
        }else{
             btn_Buy_Services.isHidden = false
        }
        
        if ServiceData.count != 0
        {
            let str = ServiceData["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            self.imageview.af_setImage(withURL: url as URL)
            lblTitle.text = ServiceData["name"]!
            lblServiceName.text = ServiceData["vendor_name"]!
            lblTitle.sizeToFit()
            rateingView.rating = Float(ServiceData["rating"] ?? "") ?? 0.0
            lblTotaRecentlReviews.text = "\(ServiceData["review_count"] ?? "")"
            
            if ServiceData["discount_offer"] != "0"{
                lblOriginalPrice.isHidden = false
                lblOfferPrice.text = "$\(ServiceData["offered_price"] ?? "")"
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(ServiceData["retail_price"] ?? "")")
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
               lblOriginalPrice.attributedText = attributeString
                self.lbl_Center.constant = -30
                
            }else{
               lblOfferPrice.text = "$\(ServiceData["retail_price"] ?? "")"
              lblOriginalPrice.isHidden = true
                self.lbl_Center.constant = 0
            }
            self.view.layoutIfNeeded()
            lblServiceNameInOfferDetail.text = ServiceData["vendor_name"]!
            lblServicePlace.text = ServiceData["vendor_address"]!
            lbl_header.text = ServiceData["services_cat_name"]!
            if ServiceData["vendor_return_policies"] == ""
            {
                self.lblReturnPolicy.text = "N/A"
            }else
            {
                do {
                    let str = try NSAttributedString(data:(ServiceData["vendor_return_policies"] ?? "N/A").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                    self.lblReturnPolicy.attributedText = str
                } catch {
                    print(error)
                }
            }
 
            if ServiceData["fine_print"] == ""
            {
                self.lbl_FinePrint.text = "N/A"
            }else
            {
                do {
                    let str = try NSAttributedString(data:(ServiceData["fine_print"] ?? "N/A").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                    self.lbl_FinePrint.attributedText = str
                } catch {
                    print(error)
                }
            }
            
            if ServiceData["fine_print"] == ""
            {
                self.lbl_Offer_detail.text = "N/A"
            }else
            {
                do {
                    let str = try NSAttributedString(data:(ServiceData["offer_details"] ?? "N/A").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                    self.lbl_Offer_detail.attributedText = str
                } catch {
                    print(error)
                }
            }
        }else
        {
            self.localOffersWebServices()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnBuyNowClick(_ sender: UIButton) {
        
        PostData = ["customer_id": webService_obj.Retrive("User_Id") as! String,
                    "user_id":ServiceData["user_id"]!,
                    "user_services_id":ServiceData["id"]!]
        
        webService_obj.fetchDataFromServer(header:"service_add_to_cart", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let actionOk = UIAlertAction(title: "GO TO CART", style: .default) { (action:UIAlertAction) in
                    let st = UIStoryboard.init(name: "Cart", bundle: nil)
                    let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
                    controller.push()
                }
                let actionCancel = UIAlertAction(title: "CONTINUE", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    
    
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: SHOW LOCATION ON MAP
    @IBAction func showLoactionOnMap(_ sender: UIButton) {
        var mapData = [[String:String]]()
        
        let dict = ["latitude":ServiceData["lat"]! ,
                    "longitude":ServiceData["lng"]!,
                    "title":ServiceData["vendor_name"]!]
        mapData.append(dict)
        
        //let st = UIStoryboard.init(name: "Services", bundle: nil)
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"MapVC") as! MapVC
        controller.mapDataDict = mapData
        controller.push()
        
    }
    
    func localOffersWebServices() {
        
        PostData = ["customer_id": webService_obj.Retrive("User_Id") as! String,
                    "localoffer_id": self.localOffer_id]
        
        webService_obj.fetchDataFromServer(header:"get_local_offer", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.ServiceData = responce.value(forKey: "data") as! [String:String]
                
                let str = self.ServiceData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                self.imageview.af_setImage(withURL: url as URL)
                self.lblTitle.text = self.ServiceData["name"]!
                self.lblServiceName.text = self.ServiceData["vendor_name"]!
                self.lblTitle.sizeToFit()
                self.rateingView.rating = Float(self.ServiceData["rating"] ?? "") ?? 0.0
                self.lblTotaRecentlReviews.text = "\(self.ServiceData["review_count"] ?? "")"
                
                if self.ServiceData["discount_offer"] != "0"{
                    self.lblOriginalPrice.isHidden = false
                    self.lblOfferPrice.text = "$\(self.ServiceData["offered_price"] ?? "")"
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(self.ServiceData["retail_price"] ?? "")")
                    attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                    self.lblOriginalPrice.attributedText = attributeString
                    
                }else{
                    self.lblOfferPrice.text = "$\(self.ServiceData["retail_price"] ?? "")"
                    self.lblOriginalPrice.isHidden = true
                }
 
                self.lblServiceNameInOfferDetail.text = self.ServiceData["vendor_name"]!
                self.lblServicePlace.text = self.ServiceData["vendor_address"]!
                self.lbl_header.text = self.ServiceData["services_cat_name"]!
                
                if self.ServiceData["vendor_return_policies"] == ""
                {
                    self.lblReturnPolicy.text = "N/A"
                }else
                {
                    do {
                        let str = try NSAttributedString(data:(self.ServiceData["vendor_return_policies"] ?? "N/A").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                        self.lblReturnPolicy.attributedText = str
                    } catch {
                        print(error)
                    }
                }
                
                
                
                if self.ServiceData["fine_print"] == ""
                {
                    self.lbl_FinePrint.text = "N/A"
                }else
                {
                    do {
                        let str = try NSAttributedString(data:(self.ServiceData["fine_print"] ?? "N/A").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                        self.lbl_FinePrint.attributedText = str
                    } catch {
                        print(error)
                    }
                }
                
                if self.ServiceData["fine_print"] == ""
                {
                    self.lbl_Offer_detail.text = "N/A"
                }else
                {
                    do {
                        let str = try NSAttributedString(data:(self.ServiceData["offer_details"] ?? "N/A").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                        self.lbl_Offer_detail.attributedText = str
                    } catch {
                        print(error)
                    }
                }
            }
        }
    }
}
