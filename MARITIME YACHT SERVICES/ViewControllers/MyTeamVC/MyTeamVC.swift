//
//  MyTeamVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 29/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class MyTeamVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func creatTeam(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"CreateDepartmentVC") as! CreateDepartmentVC
        controller.push()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }


}
