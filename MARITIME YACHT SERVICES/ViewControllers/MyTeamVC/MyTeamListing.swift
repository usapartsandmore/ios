//
//  MyTeamListing.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 16/01/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit
import Firebase


class CustomeTeamCell: UITableViewCell {
    @IBOutlet weak var teamTitle: UILabel!
    
    @IBOutlet var btnTask: UIButton!
    @IBOutlet var btnCalendar: UIButton!
    @IBOutlet var btnInventory: UIButton!
    @IBOutlet var btnExpenses: UIButton!
    @IBOutlet var btnViewTeam: UIButton!
    @IBOutlet var btnViewTeamBYNAME: UIButton!
 }

class RequestPermissionCell: UITableViewCell {
    @IBOutlet weak var teamTitle: UILabel!
    @IBOutlet weak var teamOwner: UILabel!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnDecline: UIButton!
 }


extension UIColor {
    
    static var taskFilterColor: UIColor {
        return #colorLiteral(red: 0.9287363887, green: 0.1087741479, blue: 0.1356621683, alpha: 1)
    }
    static var eventFilterColor: UIColor {
        return #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    static var inventoryFilterColor: UIColor {
        return #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
    }
    static var expensesFilterColor: UIColor {
        return #colorLiteral(red: 0.9642209411, green: 0.818066895, blue: 0.1022731587, alpha: 1)
    }
}

class MyTeamListing: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var teamTableView: UITableView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnadd: UIButton!
    
    var teamData = [[String:Any]]()
    var membersData = [[String:String]]()
    
    private var channels: [Channel] = []
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("channels")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
         }
    
    var customer_name = ""
    var customer_Image = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        self.teamTableView.tableFooterView = UIView()
        self.channels.removeAll()
        getTeamWebService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teamData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.teamData[indexPath.row]
        
        if Int(data["is_request"] as! String) == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestPermissionCell", for: indexPath) as! RequestPermissionCell
            
            cell.teamTitle.text = data["name"] as? String
            cell.teamOwner.text = data["owner_name"] as? String
            
            //Button Accept
            cell.btnAccept.tag = indexPath.row
            cell.btnAccept.addTarget(self, action: #selector(btnAcceptReqClicked(_:)), for: .touchUpInside)
            
            //Button Decline
            cell.btnDecline.tag = indexPath.row
            cell.btnDecline.addTarget(self, action: #selector(btnDeclineReqClicked(_:)), for: .touchUpInside)
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomeTeamCell", for: indexPath) as! CustomeTeamCell
            
            cell.teamTitle.text = data["name"] as? String
            
            let taskAvailable = Int(data["is_task_available"] as! String)
            let eventAvailable = Int(data["is_event_available"] as! String)
            let inventoryShared = Int(data["is_inventory_share"] as! String)
            let expensesAvailable = Int(data["is_expenses_available"] as! String)
            
            //Tasks
            if taskAvailable == 0
            {
                cell.btnTask.backgroundColor = UIColor.white
                cell.btnTask.setTitleColor(UIColor.darkGray, for: .normal)
            }else{
                cell.btnTask.backgroundColor = UIColor.taskFilterColor
                cell.btnTask.setTitleColor(UIColor.white, for: .normal)
            }
            
            //Calendar
            if eventAvailable == 0
            {
                cell.btnCalendar.backgroundColor = UIColor.white
                cell.btnCalendar.setTitleColor(UIColor.darkGray, for: .normal)
            }else{
                cell.btnCalendar.backgroundColor = UIColor.eventFilterColor
                cell.btnCalendar.setTitleColor(UIColor.white, for: .normal)
            }
            
            //Inventory
            if inventoryShared == 0
            {
                cell.btnInventory.backgroundColor = UIColor.white
                cell.btnInventory.setTitleColor(UIColor.darkGray, for: .normal)
            }else{
                cell.btnInventory.backgroundColor = UIColor.inventoryFilterColor
                cell.btnInventory.setTitleColor(UIColor.white, for: .normal)
            }
            
            //Expenses
            if expensesAvailable == 0
            {
                cell.btnExpenses.backgroundColor = UIColor.white
                cell.btnExpenses.setTitleColor(UIColor.darkGray, for: .normal)
            }else{
                cell.btnExpenses.backgroundColor = UIColor.expensesFilterColor
                cell.btnExpenses.setTitleColor(UIColor.white, for: .normal)
            }
            
            //View Team
            cell.btnViewTeam.tag = indexPath.row
            cell.btnViewTeam.addTarget(self, action: #selector(btnViewTeamClicked(_:)), for: .touchUpInside)
            
            cell.btnViewTeamBYNAME.tag = indexPath.row
            cell.btnViewTeamBYNAME.addTarget(self, action: #selector(btnViewTeamClicked(_:)), for: .touchUpInside)
            
            
            //View Task
            cell.btnTask.tag = indexPath.row
            cell.btnTask.addTarget(self, action: #selector(btnViewTaskClicked(_:)), for: .touchUpInside)
            
            //View Events
            cell.btnCalendar.tag = indexPath.row
            cell.btnCalendar.addTarget(self, action: #selector(btnViewEventClicked(_:)), for: .touchUpInside)
            
            //View Inventory
            cell.btnInventory.tag = indexPath.row
            cell.btnInventory.addTarget(self, action: #selector(btnViewInventoryClicked(_:)), for: .touchUpInside)
            
            //View Expenses
            cell.btnExpenses.tag = indexPath.row
            cell.btnExpenses.addTarget(self, action: #selector(btnViewExpensesClicked(_:)), for: .touchUpInside)
            
            
            return cell
         }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        let data = self.teamData[indexPath.row]
        
        if Int(data["is_request"] as! String) == 1
        {
            return false
        }
        else{
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let data = self.teamData[indexPath.row]
        
        if Int(data["is_owner"] as! String) == 1{
            
            let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Do you want to delete this team?", preferredStyle: .alert)
                let actionCancel = UIAlertAction(title: "NO", style: .default) { (action:UIAlertAction) in
                }
                let actionOk = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction) in
                    self.deleteTeamWebService(team_id: data["id"]! as! String)
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
                
            }
            delete.backgroundColor = UIColor.red
            
            return [delete]
        }else{
            
            let leave = UITableViewRowAction(style: .normal, title: "Leave") { action, index in
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Do you want to leave this team?", preferredStyle: .alert)
                let actionCancel = UIAlertAction(title: "NO", style: .default) { (action:UIAlertAction) in
                }
                let actionOk = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction) in
                    self.leaveTeamWebService(selectedTeamId: data["id"]! as! String)
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
            }
            leave.backgroundColor = UIColor.red
            
            return [leave]
        }
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let data = self.teamData[indexPath.row]
        
        if Int(data["is_request"] as! String) == 1
        {
            return 60.0
        }else{
            return 110.0
        }
    }
    
    // Set the spacing between sections
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//         return 100.0
//     }
    
    // MARK: - Member Chat Methods
//    @IBAction func btnMemberChatClicked(_ sender: UIButton)
//    {
//        let data = self.teamData[sender.tag]
//
//        self.channels.append(Channel(id: data["id"] as! String, name:data["name"] as! String))
//        let channel = self.channels[0]
//        self.channelRef.child(data["id"] as! String)
//
//        let main = UIStoryboard(name: "MngInventory", bundle: nil)
//        let controller = main.instantiateViewController(withIdentifier:"ChatViewController")as! ChatViewController
//        controller.isfromMyTeam = true
//        controller.channel = channel
//        controller.senderDisplayName = self.customer_name
//        controller.channelRef = self.channelRef.child(data["id"] as! String)
//        controller.customer_image = self.customer_Image
//        controller.channel_name = data["id"] as! String
//        controller.push()
//
//    }
    
     // MARK: - View Team Methods
    @IBAction func btnViewTeamClicked(_ sender: UIButton)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"TeamDetailVC") as! TeamDetailVC
        controller.team_id = self.teamData[sender.tag]["id"]! as! String
        controller.team_name = self.teamData[sender.tag]["name"]! as! String
        controller.team_Owner = self.teamData[sender.tag]["is_owner"]! as! String
        controller.push()
    }
    
    // MARK: - View Task Methods
    @IBAction func btnViewTaskClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "CalendarAndTaskVC") as? CalendarAndTaskVC
        vc?.team_id = data["id"] as! String
        vc?.team_member_id = ""
        vc?.header_name = data["name"] as! String
        vc?.team_name = data["name"] as! String
        vc?.isfromTeam = true
        vc?.topFilter = "task_event"
        vc?.push()
    }
    
    // MARK: - View Events Methods
    @IBAction func btnViewEventClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "CalendarAndTaskVC") as? CalendarAndTaskVC
        vc?.team_id = data["id"] as! String
        vc?.team_member_id = ""
        vc?.header_name = data["name"] as! String
        vc?.team_name = data["name"] as! String
        vc?.isfromTeam = true
        vc?.topFilter = "calendar_event"
        vc?.push()
    }
    
    // MARK: - View Inventory Methods
    @IBAction func btnViewInventoryClicked(_ sender: UIButton)
    {
         let data = self.teamData[sender.tag]
        
        if data["is_inventory_share"] as! String == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Do you want to unshare your inventory?" as String, preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                self.unShareInventoryWebService(selectedTeamId:data["id"] as! String)
            }
            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(actionCancel)
            alertMessage.addAction(actionOk)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Would you like to share your inventory?" as String, preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
                self.shareInventoryWebService(selectedTeamId: data["id"] as! String)
            }
            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(actionCancel)
            alertMessage.addAction(actionOk)
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
    // MARK: - View Expenses Methods
    @IBAction func btnViewExpensesClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]        
        let st = UIStoryboard.init(name: "Expense", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ExpenseVC") as! ExpenseVC
        controller.isfromMyTeam = true
        controller.team_id = data["id"] as! String
        controller.push()
     }
    
    // MARK: - Accept Request Methods
    @IBAction func btnAcceptReqClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        self.reqAcceptDeclineWebService(team_id: data["team_id"] as! String, status: "accept")
    }
    
    // MARK: - Decline Request Methods
    @IBAction func btnDeclineReqClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        self.reqAcceptDeclineWebService(team_id: data["team_id"]! as! String, status: "decline")
    }
    
    @IBAction func bntAddClick(_ sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"CreateDepartmentVC")
        controller?.push()
    }
    
    // MARK: - Web-Service Methods
    func getTeamWebService() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "get_team_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.teamData.removeAll()
                self.teamData = response.value(forKey: "data") as! [[String: Any]]
                
                self.customer_name = response.value(forKey: "customer_name") as! String
                self.customer_Image = response.value(forKey: "customer_image") as! String
                
                // self.membersData = self.teamData["member"] as! [[String:String]]
                self.teamTableView.reloadData()
            }else
            {
                self.teamData.removeAll()
                self.teamTableView.reloadData()
            }
        }
    }
    
    func deleteTeamWebService(team_id:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":team_id,
                    "member_id":""]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_team", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.getTeamWebService()
            }
        }
    }
    
    func leaveTeamWebService(selectedTeamId:String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":selectedTeamId]
        
        webService_obj.fetchDataFromServer(alertMsg: true, header: "exit_myteam", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                 //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    self.getTeamWebService()
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }

    
    func reqAcceptDeclineWebService(team_id:String,status:String) {
        
        //status :- accept / decline
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":team_id,
                    "status":status]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "push_team_action", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.getTeamWebService()
            }
        }
    }
    
    func shareInventoryWebService(selectedTeamId:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":selectedTeamId,
                    "member_id":""]
        
        webService_obj.fetchDataFromServer(alertMsg: true, header: "share_inventory_team", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    self.getTeamWebService()
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    func unShareInventoryWebService(selectedTeamId:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":selectedTeamId,
                    "member_id":""]
        
        webService_obj.fetchDataFromServer(alertMsg: true, header: "unshare_inventory", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                   self.getTeamWebService()
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
 }
