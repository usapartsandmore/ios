//
//  TeamDetailVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 16/01/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit
import Firebase

class TeamDetailCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var lbl_position: UILabel!
    @IBOutlet weak var btnAddTask: UIButton!
    @IBOutlet weak var btnAddEvents: UIButton!
    @IBOutlet weak var btnShareInventory: UIButton!
    @IBOutlet weak var btnViewTask: UIButton!
    @IBOutlet weak var btnViewEvent: UIButton!
    @IBOutlet weak var btnViewInventory: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var lbl_Pending: UILabel!
    @IBOutlet weak var img_Position: UIImageView!
 }


class TeamDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var teamTableView: UITableView!
    @IBOutlet weak var lbl_TeamName: UILabel!
    @IBOutlet weak var btn_TopRight: UIButton!
    
    @IBOutlet weak var btnAddTeamTask: UIButton!
    @IBOutlet weak var btnAddTeamEvent: UIButton!
    @IBOutlet weak var btnTeamShareInventory: UIButton!
    @IBOutlet weak var btnViewTeamTask: UIButton!
    @IBOutlet weak var btnViewTeamEvents: UIButton!
    @IBOutlet weak var btnViewExpenses: UIButton!
    @IBOutlet weak var lbl_AllMembersName: UILabel!
    
    var teamData = [[String:Any]]()
    var team_id = ""
    var team_name = ""
    var team_Owner = ""
    var allInventoryShared = ""
    
    var customer_name = ""
    var customer_Image = ""
    
    private var channels: [Channel] = []
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("channels")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Inventory Sharing
        isSharingUser = false
        sharedUser_id = ""
        
        self.lbl_TeamName.text = self.team_name
        self.teamTableView.tableFooterView = UIView()
        
        self.channels.removeAll()
          getTeamMDetailWebService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - Team Button Click Methods
    @IBAction func AddTeamTaskClicked(_ sender: Any) {
         let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "CreateTaskVC") as? CreateTaskVC
        vc?.isforupdate = false
        vc?.team_name = self.team_name
        vc?.team_id = self.team_id
        vc?.team_member_id = ""
        vc?.push()
    }
    
    @IBAction func AddTeamEventClicked(_ sender: Any) {
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "CalendarEventVC") as? CalendarEventVC
        vc?.isforupdate = false
        vc?.team_name = self.team_name
        vc?.team_id = self.team_id
        vc?.team_member_id = ""
        vc?.push()
    }
    
    @IBAction func btnShareInventoryClicked(_ sender: Any) {
        
        if self.allInventoryShared == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Do you want to unshare your inventory?" as String, preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                self.unShareInventoryWebService(selectedMembersId: "")
            }
            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(actionCancel)
            alertMessage.addAction(actionOk)
            self.present(alertMessage, animated: true, completion: nil)
             
        }else{
            if self.teamData.count > 0{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Would you like to share your inventory?" as String, preferredStyle: .alert)
                let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    self.shareInventoryWebService(selectedMembersId: "")
                }
                let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
            }else{
                
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Please add team members to share your inventory." as String, preferredStyle: .alert)
                let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                  
                }
                 alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
             }
        }
    }
    
    @IBAction func btnViewTeamTaskClicked(_ sender: Any) {
        
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "CalendarAndTaskVC") as? CalendarAndTaskVC
        vc?.team_id = self.team_id
        vc?.team_member_id = ""
        vc?.header_name = self.team_name
        vc?.team_name = self.team_name
        vc?.isfromTeam = true
        vc?.topFilter = "task_event"
        vc?.push()
    }
    
    @IBAction func btnViewTeamEventClicked(_ sender: Any) {
        
        let st = UIStoryboard.init(name: "Calender", bundle: nil)
        let vc = st.instantiateViewController(withIdentifier: "CalendarAndTaskVC") as? CalendarAndTaskVC
        vc?.team_id = self.team_id
        vc?.team_member_id = ""
        vc?.header_name = self.team_name
        vc?.team_name = self.team_name
        vc?.isfromTeam = true
        vc?.topFilter = "calendar_event"
        vc?.push()
    }
    
    @IBAction func btnViewExpensesClicked(_ sender: Any) {
        let st = UIStoryboard.init(name: "Expense", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ExpenseVC") as! ExpenseVC
        controller.isfromMyTeam = true
        controller.team_id = self.team_id
        controller.push()
     }
    
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teamData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamDetailCell", for: indexPath) as! TeamDetailCell
        
        let data = self.teamData[indexPath.row]
        
        cell.name.text = data["name"] as? String
        
        if data["position_name"] as? String == ""
        {
            cell.lbl_position.text = "N/A"
        }else{
            cell.lbl_position.text = data["position_name"] as? String
        }
        
        let inventoryShared = Int((data["is_inventory_share"] as? String)!)
        let inventoryViewed = Int((data["is_view_inventory"] as? String)!)
        let position_image = data["position_image_name"] as? String
        
        let isPending = data["is_pending"] as? String
        let isOwner = data["is_owner"] as? String
        
        //Owner and Pending
        if isOwner == "0" && isPending == "0"
        {
            cell.lbl_Pending.isHidden = true
        }else{
            cell.lbl_Pending.isHidden = false
            if  isOwner == "1"
            {
                cell.lbl_Pending.text = "Owner"
                cell.lbl_Pending.backgroundColor = UIColor.expensesFilterColor
                cell.lbl_Pending.textColor = UIColor.white
            }
            
            if isPending == "1"
            {
                cell.lbl_Pending.text = "Pending"
                cell.lbl_Pending.backgroundColor = UIColor.taskFilterColor
                cell.lbl_Pending.textColor = UIColor.white
            }
        }
        
        
        //Set Position Image
        if position_image == "Engineering"
        {
         cell.img_Position.image =  UIImage(named:"engrinning")
        }else if position_image == "Deck"
        {
            cell.img_Position.image =  UIImage(named:"desk")
        }else if position_image == "Interior"
        {
            cell.img_Position.image =  UIImage(named:"interior")
        }else if position_image == "Galley"
        {
            cell.img_Position.image =  UIImage(named:"gallery")
        }else{
            cell.img_Position.image =  UIImage(named:"other")
        }
        
        
        //Tasks
        cell.btnAddTask.tag = indexPath.row
        cell.btnAddTask.addTarget(self, action: #selector(btnAddMemberTaskClicked(_:)), for: .touchUpInside)
        
        
        //Calendar
        cell.btnAddEvents.tag = indexPath.row
        cell.btnAddEvents.addTarget(self, action: #selector(btnAddMemberEventClicked(_:)), for: .touchUpInside)
        
        
        //Share Inventory
        if inventoryShared == 0
        {
            cell.btnShareInventory.backgroundColor = UIColor.white
            cell.btnShareInventory.setTitleColor(UIColor.darkGray, for: .normal)
        }else{
            cell.btnShareInventory.backgroundColor = UIColor.inventoryFilterColor
            cell.btnShareInventory.setTitleColor(UIColor.white, for: .normal)
        }
        cell.btnShareInventory.tag = indexPath.row
        cell.btnShareInventory.addTarget(self, action: #selector(btnShareMemberInventoryClicked(_:)), for: .touchUpInside)
        
        
        
        //View Task
        cell.btnViewTask.tag = indexPath.row
        cell.btnViewTask.addTarget(self, action: #selector(btnViewTaskClicked(_:)), for: .touchUpInside)
        
        //View Events
        cell.btnViewEvent.tag = indexPath.row
        cell.btnViewEvent.addTarget(self, action: #selector(btnViewEventClicked(_:)), for: .touchUpInside)
        
        //View Inventory
        if inventoryViewed == 0
        {
            cell.btnViewInventory.backgroundColor = UIColor.white
            cell.btnViewInventory.setTitleColor(UIColor.darkGray, for: .normal)
        }else{
            cell.btnViewInventory.backgroundColor = UIColor.inventoryFilterColor
            cell.btnViewInventory.setTitleColor(UIColor.white, for: .normal)
        }

        cell.btnViewInventory.tag = indexPath.row
        cell.btnViewInventory.addTarget(self, action: #selector(btnViewInventoryClicked(_:)), for: .touchUpInside)
        
        //Member Chat
        cell.btnChat.tag = indexPath.row
        cell.btnChat.addTarget(self, action: #selector(btnMemberChatClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            let data = self.teamData[indexPath.row]
            
            if self.team_Owner == "0"{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }else{
                
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Do you want to delete this team member?", preferredStyle: .alert)
                let actionCancel = UIAlertAction(title: "NO", style: .default) { (action:UIAlertAction) in
                }
                let actionOk = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction) in
                   self.deleteTeamMemberWebService(member_id: (data["customer_id"] as? String)!)
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)

             }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    // MARK: - Add Member Task Methods
    @IBAction func btnAddMemberTaskClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        let isPending = data["is_pending"] as? String
        
        if isPending == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Team member not Joined team yet. Please wait until user accept the team join request.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            
            let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "CreateTaskVC") as? CreateTaskVC
            vc?.isforupdate = false
            vc?.team_name = data["name"] as! String
            vc?.team_id = self.team_id
            vc?.team_member_id = data["customer_id"] as! String
            vc?.push()
        }
    }
    
    // MARK: - Add Member Event Methods
    @IBAction func btnAddMemberEventClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        let isPending = data["is_pending"] as? String
        
        if isPending == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Team member not Joined team yet. Please wait until user accept the team join request.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
             let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "CalendarEventVC") as? CalendarEventVC
            vc?.isforupdate = false
            vc?.team_name = data["name"] as! String
            vc?.team_id = self.team_id
            vc?.team_member_id = data["customer_id"] as! String
            vc?.push()
        }
    }
    
    // MARK: - Share Member Inventory Methods
    @IBAction func btnShareMemberInventoryClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        
        if data["is_inventory_share"] as? String == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Do you want to unshare your inventory?" as String, preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                self.unShareInventoryWebService(selectedMembersId:data["customer_id"] as! String)
            }
            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(actionCancel)
            alertMessage.addAction(actionOk)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Would you like to share your inventory?" as String, preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
            }
            let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                let isPending = data["is_pending"] as? String
                
                if isPending == "1"
                {
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:"Team member not Joined team yet. Please wait until user accept the team join request.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                    
                    
                }else{
                    
                    self.shareInventoryWebService(selectedMembersId: data["customer_id"] as! String)
                }
            }
            alertMessage.addAction(actionCancel)
            alertMessage.addAction(actionOk)
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
     // MARK: - Team Group Chat Methods
//    @IBAction func btnTeamChatClicked(_ sender: Any) {
//
//        self.channels.append(Channel(id: self.team_id, name: self.team_name))
//
//        let channel = self.channels[0]
//        self.channelRef.child(self.team_id)
//
//        let main = UIStoryboard(name: "MngInventory", bundle: nil)
//        let controller = main.instantiateViewController(withIdentifier:"ChatViewController")as! ChatViewController
//        controller.isfromMyTeam = true
//        controller.channel = channel
//        controller.senderDisplayName = self.customer_name
//        controller.channelRef = self.channelRef.child(self.team_id)
//        controller.customer_image = self.customer_Image
//        controller.channel_name = self.team_id
//        controller.push()
//    }
    
    // MARK: - View Task Methods
    @IBAction func btnViewTaskClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        let isPending = data["is_pending"] as? String
        
        if isPending == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Team member not Joined team yet. Please wait until user accept the team join request.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "CalendarAndTaskVC") as? CalendarAndTaskVC
            vc?.team_id = self.team_id
            vc?.team_member_id = data["customer_id"] as! String
            vc?.header_name = data["name"] as! String
            vc?.team_name = data["name"] as! String
            vc?.isfromTeam = true
            vc?.topFilter = "task_event"
            vc?.push()
        }
    }
    
    // MARK: - View Events Methods
    @IBAction func btnViewEventClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        let isPending = data["is_pending"] as? String
        
        if isPending == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Team member not Joined team yet. Please wait until user accept the team join request.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            let st = UIStoryboard.init(name: "Calender", bundle: nil)
            let vc = st.instantiateViewController(withIdentifier: "CalendarAndTaskVC") as? CalendarAndTaskVC
            vc?.team_id = self.team_id
            vc?.team_member_id = data["customer_id"] as! String
            vc?.header_name = data["name"] as! String
            vc?.team_name = data["name"] as! String
            vc?.isfromTeam = true
            vc?.topFilter = "calendar_event"
            vc?.push()
        }
    }
    
    // MARK: - View Inventory Methods
    @IBAction func btnViewInventoryClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        let isPending = data["is_pending"] as? String
        let isviewInventory = data["is_view_inventory"] as? String
        
        if isPending == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Team member not Joined team yet. Please wait until user accept the team join request.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            if isviewInventory == "1"
            {
                //Inventory Sharing
                isSharingUser = true
                
                //  if isInventoryAvailable == true{
                //Already have decks, so now let the user manage the decks
                let st = UIStoryboard(name: "Inventory", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"DeckLevelList") as! DeckLevelList
                controller.isToManageOrFirstTime = true
                controller.sharedCustomer_id = data["customer_id"] as! String
                sharedUser_id = data["customer_id"] as! String
                controller.push()
                //}
                //else {
                //
                ////Don't have decks yet, so let the user add decks
                //let st = UIStoryboard(name: "Inventory", bundle: nil)
                //let controller = st.instantiateViewController(withIdentifier:"DeckTemplatesVC") as! DeckTemplatesVC
                //controller.isforFirstTime = true
                //controller.push()
                //}
            }else{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Team member not shared inventory with you.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
                
            }
        }
    }
    
    // MARK: - Member Chat Methods
    @IBAction func btnMemberChatClicked(_ sender: UIButton)
    {
        let data = self.teamData[sender.tag]
        let isPending = data["is_pending"] as? String
         
        if isPending == "1"
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Team member not Joined team yet. Please wait until user accept the team join request.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            let tokensdata = data["customer_device_token"] as? [[String:String]]
            var iosTokensData = [String]()
            var androidTokensData = [String]()
            
            for (_,dict) in (tokensdata?.enumerated())! {
                if dict["device_type"] == "ios" {
                    iosTokensData.append(dict["device_token"]!)
                }else{
                    androidTokensData.append(dict["device_token"]!)
                }
            }
            
            print("iOS Data--> \(iosTokensData)  Android Data--> \(androidTokensData)")
            
            
            var channel_Name = ""
            
            if Int(webService_obj.Retrive("User_Id") as! String)! > Int(data["customer_id"] as! String)!
            {
                channel_Name = self.team_id+"_"+(webService_obj.Retrive("User_Id") as! String)+"_"+(data["customer_id"] as! String)
            }else{
                channel_Name = self.team_id+"_"+(data["customer_id"] as! String)+"_"+(webService_obj.Retrive("User_Id") as! String)
            }
            
            self.channels.append(Channel(id: data["customer_id"] as! String, name: data["name"] as! String))
            let channel = self.channels[0]
            
            self.channelRef.child(channel_Name)
            
            let main = UIStoryboard(name: "MngInventory", bundle: nil)
            let controller = main.instantiateViewController(withIdentifier:"ChatViewController")as! ChatViewController
            controller.isfromMyTeam = true
            controller.channel = channel
            controller.senderDisplayName = self.customer_name
            controller.channelRef = self.channelRef.child(channel_Name)
            controller.customer_image = self.customer_Image
            controller.channel_name = channel_Name
            controller.pushiOSData = iosTokensData
            controller.pushAndroidData = androidTokensData
            controller.teamName = self.team_name
            controller.push()
        }
    }
    
    
    @IBAction func bntAddClick(_ sender: AnyObject) {
         if self.team_Owner == "0"{
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"you are not authorized for this action.", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            }
            alertMessage.addAction(actionOk)
            self.present(alertMessage, animated: true, completion: nil)

         }else{
            let st = UIStoryboard.init(name: "MyTeam", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"MemberListing") as! MemberListing
            controller.isfromMembers = "true"
            controller.team_id = self.team_id
            controller.team_name = self.team_name
            controller.selectedArr = self.teamData  
            controller.push()
         }
      }
    
       
    
    
    // MARK: - Web-Service Methods
    func getTeamMDetailWebService() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":self.team_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "get_team_member_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.teamData.removeAll()
                self.teamData = response.value(forKey: "data") as! [[String: Any]]
                
                self.customer_name = response.value(forKey: "customer_name") as! String
                self.customer_Image = response.value(forKey: "customer_image") as! String
                
                //Set All Members Name
                let membersNameArray = self.teamData.map{ ($0["name"]!) } as! [String]
                var stringRepresentation = membersNameArray.joined(separator: ", ")
                
                stringRepresentation.append(", \(String(describing: response.value(forKey: "customer_name")!))")
                
                self.lbl_AllMembersName.text = stringRepresentation
                
                let expensesAvailable = Int(response.value(forKey: "is_expenses_available") as! String)
        
                //Expenses
                if expensesAvailable == 0
                {
                    self.btnViewExpenses.backgroundColor = UIColor.white
                    self.btnViewExpenses.setTitleColor(UIColor.darkGray, for: .normal)
                }else{
                    self.btnViewExpenses.backgroundColor = UIColor.expensesFilterColor
                    self.btnViewExpenses.setTitleColor(UIColor.white, for: .normal)
                }

                
                self.allInventoryShared = response.value(forKey: "is_inventory_share") as! String
                
                //Team Inventory
                if self.allInventoryShared == "0"
                {
                    self.btnTeamShareInventory.backgroundColor = UIColor.white
                    self.btnTeamShareInventory.setTitleColor(UIColor.darkGray, for: .normal)
                }else{
                    self.btnTeamShareInventory.backgroundColor = UIColor.inventoryFilterColor
                    self.btnTeamShareInventory.setTitleColor(UIColor.white, for: .normal)
                }
                
                self.teamTableView.reloadData()
            }else
            {
                self.lbl_AllMembersName.text = "N/A"
                self.teamData.removeAll()
                self.teamTableView.reloadData()
            }
        }
    }
    
    func deleteTeamMemberWebService(member_id:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":team_id,
                    "member_id":member_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_team", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.getTeamMDetailWebService()
            }
        }
    }
    
      
    func shareInventoryWebService(selectedMembersId:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":team_id,
                    "member_id":selectedMembersId]
        
        webService_obj.fetchDataFromServer(alertMsg: true, header: "share_inventory_team", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    self.getTeamMDetailWebService()
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    
    func unShareInventoryWebService(selectedMembersId:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":team_id,
                    "member_id":selectedMembersId]
        
        webService_obj.fetchDataFromServer(alertMsg: true, header: "unshare_inventory", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    self.getTeamMDetailWebService()
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
     
}
