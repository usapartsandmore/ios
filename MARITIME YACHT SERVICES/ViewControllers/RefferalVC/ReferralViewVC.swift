//
//  ReferralViewVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 18/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import Social
import MessageUI

class ReferralViewVC: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate  {

    @IBOutlet var lblReferralCode: UILabel!
    @IBOutlet var lblRefereal: UILabel!
    
    var shareTxt = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       self.getRefferalDetailWebService()
    }

    @IBAction func btnInviteFriend(_ sender: UIButton) {
         if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            //mail.setToRecipients(["help@mys.com"])
            mail.title = "MARITIME YACHT SERVICES"
            mail.setSubject("MYS")
            mail.setMessageBody(self.shareTxt, isHTML: false)
            present(mail, animated: true)
        } else {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Simulator Error", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnShareWithFBClick(_ sender: UIButton) {
        if let urlFromStr = URL(string: "fb://") {
            if UIApplication.shared.canOpenURL(urlFromStr) {
                let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
                //vc.add(imageView.image!)
                vc?.add(URL(string: "https://56.octallabs.com/mys/"))
                vc?.setInitialText(self.shareTxt)
                self.present(vc!, animated: true, completion: nil)
            } else {
                
                let textToShare = [ self.shareTxt ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnShareWithTwitterClick(_ sender: UIButton) {
         if let urlFromStr = URL(string: "twitter://") {
            if UIApplication.shared.canOpenURL(urlFromStr) {
                let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
                vc?.add(URL(string: "https://maritimeyachtservices.com/"))
                vc?.setInitialText(self.shareTxt)
                self.present(vc!, animated: true, completion: nil)
            }else {
                // self.AlertMessage("You are not connected to Twitter, Please check phone settings.")
                
                // set up activity view controller
                let textToShare = [ self.shareTxt ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnShareWithMsgClick(_ sender: UIButton) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = self.shareTxt
            //controller.recipients = [""]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Simulator Error", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
        }
     }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backMenuPressed(_ sender: UIButton) {
          sideMenuVC.toggleMenu()
    }
    
    func getRefferalDetailWebService() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "get_refferal", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                 let data = response.value(forKey: "data") as! [String: String]
                
                self.lblRefereal.text = "Share your code and get up to $\(data["refferal_amount"]!) in free shipping for each friend who tries MYS."
                
                self.shareTxt = "Please use this referral code \(data["refferal_code"]!) during the checkout when you place order and get special discount on your next order using MYS app. Install the iPhone app from \(data["appstore"]!) also for Android download the app from \(data["playstore"]!)"
                
                self.lblReferralCode.text = data["refferal_code"]!
            }
        }
    }
}
