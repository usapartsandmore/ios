//
//  CompanyInfoVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 18/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class CompanyInfoVC: UIViewController, UIScrollViewDelegate {
  var vendorCompanyInfo = [String:Any]()
    
    @IBOutlet var lbl_businessSince: UILabel!
    @IBOutlet var lbl_24HourService: UILabel!
    @IBOutlet var lbl_ThursdayTimeing: UILabel!
    @IBOutlet var lbl_WedTimeing: UILabel!
    @IBOutlet var lbl_TuesdayTimeing: UILabel!
    @IBOutlet var lbl_MondayTimeing: UILabel!
    @IBOutlet var lbl_SundayTimeing: UILabel!
    @IBOutlet var SaturdayTimeing: UILabel!
    @IBOutlet var lbl_FridayTimeing: UILabel!
    @IBOutlet var lbl_BussinessDescription: UILabel!
    @IBOutlet var lbl_companyWebsite: UILabel!
    @IBOutlet var lbl_contactNumber: UILabel!
    @IBOutlet var companyAddress: UILabel!
    @IBOutlet var VendorCompanyName: UILabel!
    var tapGestureRecognizer:UITapGestureRecognizer!
    var vendorNumbers:String!
    var vendorWebLink:String!
    
    @IBOutlet weak var view_InformationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var view_Additional: UIView!
    
  //  var vendorCompanyInfo = [String:String] ()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.vendorCompanyInfo)
        self.VendorCompanyName.text = self.vendorCompanyInfo["company_name"] as? String
        self.companyAddress.text = self.vendorCompanyInfo ["company_address"] as? String
        self.lbl_contactNumber.text = self.vendorCompanyInfo ["contact_number"] as? String
        self.lbl_companyWebsite.text = self.vendorCompanyInfo ["website_link"] as? String
        self.lbl_BussinessDescription.text = (self.vendorCompanyInfo ["description"] as? String)
        self.lbl_businessSince.text = self.vendorCompanyInfo ["business_start"] as? String
        
        self.vendorWebLink = self.vendorCompanyInfo ["website_link"] as? String
        self.vendorNumbers = self.vendorCompanyInfo ["contact_number"] as? String
        
        
        
        if self.vendorCompanyInfo ["emergency_service"] as? String == "0" && self.vendorCompanyInfo ["business_start"] as? String == "0" {
            self.lbl_24HourService.text = "N/A"
            view_InformationHeight.constant = 0.0
            view_Additional.isHidden = true
            
        }
        else
        
        {
            view_InformationHeight.constant = 125.0
            view_Additional.isHidden = false
            if self.vendorCompanyInfo ["emergency_service"] as? String == "0"
            {
                self.lbl_24HourService.text = ""
            }
            else
            {
            self.lbl_24HourService.text = "24 hour/ Emergency service:\(self.vendorCompanyInfo ["emergency_service"] as? String ?? "")"
            }
            if self.vendorCompanyInfo ["business_start"] as? String == "0"
            {
               self.lbl_businessSince.text = ""
            }
            else
            {
                self.lbl_businessSince.text = "In Business Since:\(self.vendorCompanyInfo ["business_start"] as? String ?? "")"

            }
  
        }
        self.view.layoutIfNeeded()

        let weekTimeing = self.vendorCompanyInfo ["WorkingHour"] as! [String:String]
        if weekTimeing ["friday"] == "1"
        {
      
            
            
            if weekTimeing ["friday_start_time"] == "Closed"
            {
                self.lbl_FridayTimeing.text = "\(weekTimeing ["friday_start_time"] ?? "")"
                self.lbl_FridayTimeing.textColor = UIColor.red
            }
            else
                
            {
                self.lbl_FridayTimeing.text = "\(weekTimeing ["friday_start_time"] ?? "") to \(weekTimeing ["friday_end_time"] ?? "" )"
            }
           
        }
        else
        {
            self.lbl_FridayTimeing.text = "N/A"
        }
        if weekTimeing ["monday"] == "1"
        {

            
            if weekTimeing ["monday_start_time"] == "Closed"
            {
                self.lbl_MondayTimeing.text = "\(weekTimeing ["monday_start_time"] ?? "")"
                self.lbl_MondayTimeing.textColor = UIColor.red
            }
            else
                
            {
                self.lbl_MondayTimeing.text = "\(weekTimeing ["monday_start_time"] ?? "") to \(weekTimeing ["monday_end_time"] ?? "" )"
            }
            
        }
        else
        {
            self.lbl_MondayTimeing.text = "N/A"
        }
        if weekTimeing ["saturday"] == "1"
        {
            if weekTimeing ["saturday_start_time"] == "Closed"
            {
                self.SaturdayTimeing.text = "\(weekTimeing ["saturday_start_time"] ?? "")"
                self.SaturdayTimeing.textColor = UIColor.red
            }
            else
            
            {
                self.SaturdayTimeing.text = "\(weekTimeing ["saturday_start_time"] ?? "") to \(weekTimeing ["saturday_end_time"] ?? "" )"
            }
        }
        else
        {
            self.SaturdayTimeing.text = "N/A"
        }
        
        
        if weekTimeing ["sunday"] == "1"
        {
            
            if weekTimeing ["sunday_start_time"] == "Closed"
            {
                self.lbl_SundayTimeing.text = "\(weekTimeing ["sunday_start_time"] ?? "")"
                self.lbl_SundayTimeing.textColor = UIColor.red
            }
            else
                
            {
               self.lbl_SundayTimeing.text = "\(weekTimeing ["sunday_start_time"] ?? "") to \(weekTimeing ["sunday_end_time"] ?? "" )"
            }
            
            
        }
        else
        {
            self.lbl_SundayTimeing.text = "N/A"
        }
        
        
        if weekTimeing ["thursday"] == "1"
        
        {
            
            if weekTimeing ["thursday_start_time"] == "Closed"
            {
                self.lbl_ThursdayTimeing.text = "\(weekTimeing ["thursday_start_time"] ?? "")"
                self.lbl_ThursdayTimeing.textColor = UIColor.red
            }
            else
                
            {
                self.lbl_ThursdayTimeing.text = "\(weekTimeing ["thursday_start_time"] ?? "") to \(weekTimeing ["thursday_end_time"] ?? "" )"
            }

            
        }
        else
        {
            self.lbl_ThursdayTimeing.text = "N/A"
        }
        if weekTimeing ["tuesday"] == "1"
        {
            
            
            
            if weekTimeing ["tuesday_start_time"] == "Closed"
            {
                self.lbl_TuesdayTimeing.text = "\(weekTimeing ["tuesday_start_time"] ?? "")"
                self.lbl_TuesdayTimeing.textColor = UIColor.red
            }
            else
                
            {
                self.lbl_TuesdayTimeing.text = "\(weekTimeing ["tuesday_start_time"] ?? "") to \(weekTimeing ["tuesday_end_time"] ?? "" )"
            }

            
        }
        else
        {
            self.lbl_TuesdayTimeing.text = "N/A"
        }
        if weekTimeing ["wednesday"] == "1" {
            
            
            
            if weekTimeing ["wednesday_start_time"] == "Closed"
            {
                self.lbl_WedTimeing.text = "\(weekTimeing ["wednesday_start_time"] ?? "")"
                self.lbl_WedTimeing.textColor = UIColor.red
            }
            else
                
            {
                self.lbl_WedTimeing.text = "\(weekTimeing ["wednesday_start_time"] ?? "") to \(weekTimeing ["wednesday_end_time"] ?? "" )"
            }

            
        }
        else
        {
            self.lbl_WedTimeing.text = "N/A"
        }
        
        
        //MARK:- add guesture on lable
        
        tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(tapOnContactlbl(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        lbl_contactNumber.isUserInteractionEnabled = true
        lbl_contactNumber.addGestureRecognizer(tapGestureRecognizer)
        
        
        tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(tapOnLink(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        lbl_companyWebsite.isUserInteractionEnabled = true
        lbl_companyWebsite.addGestureRecognizer(tapGestureRecognizer)
        
        // Do any additional setup after loading the view.
    }
   
    func tapOnContactlbl(_ sender: Any)
    {
        
    var number = self.vendorNumbers
        number = number?.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        
        if let url = URL(string: "telprompt://"+number!){
            if UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
        
        
        
        print("*******", "is tapable")
    }
    
    
    func tapOnLink(_ sender: Any)
    {
        let url = NSURL(string: self.vendorWebLink)
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url! as URL)
        }
        
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
