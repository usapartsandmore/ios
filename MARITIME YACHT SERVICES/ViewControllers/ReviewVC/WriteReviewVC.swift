//
//  WriteReviewVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class WriteReviewVC: UIViewController, UIScrollViewDelegate {
    var vendorId = ""
    var servicePerform = ""
    var serviceWillAgainUse = ""
    var overallRating = ""
    
    
    @IBOutlet var btn_willUseProvider : UIButton!
    @IBOutlet var btn_WillNeverUseProvider: UIButton!
    @IBOutlet var costOfService: UITextField!
    @IBOutlet var dateOfService: UITextField!

    @IBOutlet var providerDescription: UITextView!
    
    @IBOutlet var providerComment: UITextView!
    @IBOutlet var btn_ServiceNotPerform: UIButton!
    @IBOutlet var btn_ServicePerform: UIButton!
    
    @IBOutlet var rating_professionalism: FloatRatingView!
    @IBOutlet var rating_punctuality: FloatRatingView!
    
    @IBOutlet var rating_responsiveness: FloatRatingView!
    @IBOutlet var rating_Quantity: FloatRatingView!
    @IBOutlet var rating_price: FloatRatingView!
    @IBOutlet var rating_overallGrade: FloatRatingView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func servicePerformedSelectionMethod(sender: UIButton) {
        self.btn_ServicePerform.isSelected = false
        self.btn_ServiceNotPerform.isSelected = false
        sender.isSelected = true
    }
    
    @IBAction func providerSelectionMethod(sender: UIButton) {
        self.btn_willUseProvider.isSelected = false
        self.btn_WillNeverUseProvider.isSelected = false
        sender.isSelected = true
    }
    

    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    @IBAction func btnSubmitReviewClick(_ sender: UIButton) {

        if rating_overallGrade.rating == 0 {
            self.AlertMessage("Please rating overall.")
        }
        else if rating_price.rating == 0 {
             self.AlertMessage("Please rating price.")
        }
        else if rating_Quantity.rating == 0 {
             self.AlertMessage("Please rating quantity.")
        }
        else if rating_responsiveness.rating == 0 {
            self.AlertMessage("Please rating responsiveness.")
        }
        else if rating_punctuality.rating == 0 {
            self.AlertMessage("Please rating punctuality.")
        }
        else if rating_professionalism.rating == 0{
             self.AlertMessage("Please rating professionalism.")
        }
        else if costOfService.text! .isEmpty {
             self.AlertMessage("Please enter cost of service.")
        }
        else if dateOfService.text! .isEmpty {
            self.AlertMessage("Please enter date of service.")
        }
        else if providerDescription.text! .isEmpty {
            self.AlertMessage("Please enter description.")
        }
        else{
            self.writeReviewWebService ()
        }
    }
    
    @IBAction func DatePickerOpen(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date, doneBlock: {
            picker,index,value in
            
            //Convert in date format
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd"
//            let date = index as! Date
//            self.dateOfService.text = dateFormatter.string(from: date)
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            let date = index as! Date
            self.dateOfService.text = dateFormatter.string(from: date)
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        
        datePicker?.show()
        
    }
    
    func writeReviewWebService () {
        if btn_ServicePerform.isSelected {
            servicePerform = "1"
        }
        else{
            servicePerform = "0"
        }
        
        if btn_willUseProvider.isSelected {
            serviceWillAgainUse = "1"
        }
        else {
            serviceWillAgainUse = "0"
        }
        
        //Convert in date format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let myDate = dateFormatter.date(from: self.dateOfService.text!)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateService =  dateFormatter.string(from: myDate!)
  
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "user_id": self.vendorId,
                    "overall_grade":rating_overallGrade.rating,
                    "service_performed": servicePerform,
                    "what_provider_do":self.providerDescription.text ?? "",
                    "how_it_go": self.providerComment.text ?? "",
                    "service_date": dateService,
                    "price": self.costOfService.text ?? "",
                    "use_again":serviceWillAgainUse,
                    "price_rate": rating_price.rating,
                    "quality_rate": rating_Quantity.rating,
                    "responsive_rate": rating_responsiveness.rating,
                    "punctuality_rate":rating_punctuality.rating,
                    "professionalism_rate": rating_professionalism.rating]
      
        webService_obj.fetchDataFromServer(header:"service_review", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                 _ = self.navigationController?.popViewController(animated: true)
//                //Show alert Message
//                let alertMessage = UIAlertController(title: "Alert", message:"", preferredStyle: .alert)
//                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//                    _ = self.navigationController?.popViewController(animated: true)
//                }
//                alertMessage.addAction(action)
//                self.present(alertMessage, animated: true, completion: nil)
            }
       }
    }


}
