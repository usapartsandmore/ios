//
//  LoginVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 04/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
extension String {
    var length: Int {
        return self.characters.count
    }
}
extension String {
    var stringByRemovingWhitespaces: String {
        return components(separatedBy: .whitespaces).joined(separator: "")
    }
}
extension String {
    var stringByRemovingWhitespacesNew: String {
        let components = self.components(separatedBy: .whitespaces)
        return components.joined(separator: "")
    }
}
extension String {
    mutating func replace(_ originalString:String, with newString:String) {
        self = self.replacingOccurrences(of: originalString, with: newString)
    }
}

class LoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var btnFacebookLogin: UIButton!
    @IBOutlet var btnSignIn: UIButton!
    
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    var  fbData = [String: Any]()
    var pickedImage: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPassword.delegate = self
        txtEmail.delegate = self
        //Set Corner Radius and Border
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Back Button Method
    @IBAction func btnBackPressed(_ sender: UIButton) {
        //_ = self.navigationController?.popViewController(animated:true)
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: IntroScreenVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    // MARK: Forgot Password Button Method
    @IBAction func btnForgotPasswordPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ForgotPasswordVC", sender: sender)
    }
    
    
    // MARK: SignIn Button Method
    @IBAction func btnSignInPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtEmail.text! .isEmpty
        {
            self.AlertMessage("Please enter email address.")
            self.txtPassword.text = ""
        }
        else if validateEmail(txtEmail.text!) == false
        {
            self.AlertMessage("Invalid email address.")
            self.txtPassword.text = ""
        }
        else if txtPassword.text! .isEmpty
        {
            self.AlertMessage("Please enter password.")
        }
        else if (txtPassword.text!.length) < 6
        {
            self.AlertMessage("Minimum of 6 characters needed for password.")
            self.txtPassword.text = ""
        }
        else
        {
            
            PostData = ["email":self.txtEmail.text!,
                        "password":self.txtPassword.text!,
                        "device_type":"iOS",
                        "device_token":webService_obj.Retrive("Device_token") as! String]
            
            webService_obj.fetchDataFromServer(header:"customer_login", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                self.txtPassword.text = ""
                if staus{
                    let data = responce.value(forKey: "data") as! NSDictionary
                    
                    let sharing_user = data["is_team_member"] as! String
                    let profile_completed = data["profile_completed"] as! String
                    
                    //Clear Data
                    self.txtEmail.text = ""
                    self.txtPassword.text = ""
                    
                    //Save User Data
                    
                    let User_Id = data["customer_id"]
                    webService_obj.Save(User_Id as! String, keyname: "User_Id")
                    
                    //FireBase Chat
                    Auth.auth().signInAnonymously(completion: { (user, error) in // 2
                        if let err = error { // 3
                            print(err.localizedDescription)
                            return
                        }
                        
                        if sharing_user == "1" && profile_completed == "0"
                        {
                            let controller = self.storyboard?.instantiateViewController(withIdentifier:"ChangePasswordVC") as! ChangePasswordVC
                            controller.sharing_user = "true"
                            controller.push()
                        }else
                        {
                            let mainVcIntial = kConstantObj.SetIntialMainViewController("HomeVC")
                            appDelegate.window?.rootViewController = mainVcIntial
                            defaults.set(true, forKey: kAPPLOGIN_IN)
                        }
                        
                        
                    })
                }
            }
        }
    }
    
    // MARK: Facebook SDK Method
    @IBAction func btnFbPressed(_ sender: Any)
    {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) -> Void in
            if error != nil {
                print(error?.localizedDescription ?? "")
                self.getFBUserInfos()
                self.dismiss(animated: true, completion: nil)
            } else if (result?.isCancelled)! {
                print("Cancelled")
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.getFBUserInfos()
            }
        }
    }
    
    // MARK: - Get User Information login By Facebook
    func getFBUserInfos()
    {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name,email,picture"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                self.loginbyFaceboobk(Details: fbDetails)
            }
        })
    }
    
    // MARK: - CallSocialLoginService
    func loginbyFaceboobk(Details:NSDictionary)
    {
        let pic_Url = ((Details.value(forKey: "picture") as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "url") as! String
        print("pic urlllll",pic_Url)
        
        //Use image's path to create NSData
        let url:NSURL = NSURL(string : pic_Url)!
        //Now use image to create into NSData format
        let imageData:NSData = NSData.init(contentsOf: url as URL)!
        
        pickedImage = UIImage(data: imageData as Data)
        
        PostData = ["fb_id":(Details.value(forKey: "id") as? String)!,
                    "email":(Details.value(forKey: "email") as? String)!,
                    "type":"fb"]
        
        fbData =  ["fb_id":(Details.value(forKey: "id") as? String)!,
                   "email":(Details.value(forKey: "email") as? String)!,
                   "name":(Details.value(forKey: "first_name") as? String)!]
        
        webService_obj.fetchDataFromServer(header:"existEmail", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                let data = responce.value(forKey: "data") as! NSDictionary
                let isexist = data.value(forKey: "isExist")
                let isverified = data.value(forKey: "isFbEmailVerified")
                
                if (isexist! as AnyObject).doubleValue == 1.0
                {
                    if (isverified! as AnyObject).doubleValue == 1.0
                    {
                        //Save User Data
                        defaults.set(true, forKey: kAPPLOGIN_IN)
                        let User_Id = data["customer_id"]
                        webService_obj.Save(User_Id as! String, keyname: "User_Id")
                        
                        //FireBase Chat
                        Auth.auth().signInAnonymously(completion: { (user, error) in // 2
                            if let err = error { // 3
                                print(err.localizedDescription)
                                return
                            }
                            let mainVcIntial = kConstantObj.SetIntialMainViewController("HomeVC")
                            appDelegate.window?.rootViewController = mainVcIntial
                        })
                    }else{
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:"Please verify your email first, then Login.", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                            
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                    vc.isfromFacebook = true
                    vc.facebookData = self.fbData as! [String : String]
                    vc.facebookUserImg = self.pickedImage
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    
    func validateEmail(_ enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
