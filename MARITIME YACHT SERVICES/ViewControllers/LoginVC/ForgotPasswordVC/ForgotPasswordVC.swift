//
//  ForgotPasswordVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 05/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet var blurView: UIView!
    @IBOutlet var btnResetPass: UIButton!
    @IBOutlet var btnEmail: UITextField!

     override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Corner Radius and Border
        btnResetPass.layer.cornerRadius = 5
        let _ : UIColor = UIColor.lightGray
        blurView.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Back Button Method
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func OkBtnClick(_ sender: UIButton) {
        blurView.isHidden = true
        
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: Reset Password Button Method
    @IBAction func btnResetPasswordPressed(_ sender: UIButton) {
        
        if btnEmail.text! .isEmpty
        {
            self.AlertMessage("Please enter email address.")
        }
        else  if validateEmail(btnEmail.text!) == false
        {
            self.AlertMessage("Invalid email address")
        }
        else
        {
            PostData = ["email":self.btnEmail.text!]
            
            webService_obj.fetchDataFromServer(header:"forgot_password", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    self.blurView.isHidden = false

                }
            }
        }
    }
    
    func validateEmail(_ enteredEmail:String) -> Bool {
         let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
}
