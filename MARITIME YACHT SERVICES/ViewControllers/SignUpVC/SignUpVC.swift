//
//  SignUpVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 04/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class VendorDetailCell : UITableViewCell{
    @IBOutlet var txtVendor: UITextField!
    @IBOutlet var txtAccNumber: UITextField!
    @IBOutlet var btnVendor: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
}

extension SignUpVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    func imageCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        img_Profile.image = pickedImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

class SignUpVC: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var img_Profile: UIImageView!
    @IBOutlet var txtFName: UITextField!
    @IBOutlet var txtLName: UITextField!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var txtDob: UITextField!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtBoadName: UITextField!
    @IBOutlet var txtPosition: UITextField!
    @IBOutlet var txtVesselSize: UITextField!
    @IBOutlet var txtMMSI: UITextField!
    @IBOutlet var txtFeet: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var btnDob: UIButton!
    @IBOutlet weak var btnGender: UIButton!
    @IBOutlet var btn_back: UIButton!
    @IBOutlet weak var btn_picture: UIButton!
    let imagePicker = UIImagePickerController()
    var pickedImage: UIImage!
    var facebookUserImg = UIImage()
    
    @IBOutlet weak var tableViewHeightCont: NSLayoutConstraint!
    var vendorsDetails = [[String:String]]()
    var arrVendorName = [String]()
    var arrSelectedVendorName = [String:String]()
   // var arrSelectedAccNum = [String:String]()
    var vendorAccountArr = [String:String]()
    var vendorsIDArr = [String:String]()
    var signUPDataDict = [String:Any]()
    var numberOfRows : Int = 1
    var facebookData = [String: String]()
    var isfromFacebook = Bool()
    
    var positionValue: [String: AnyObject]!
    var position_id = ""
    var position_name = ""
    
    var email_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtEmail.delegate = self
        if isfromFacebook {
            //btn_back.isHidden = true
            img_Profile.image = facebookUserImg
            txtEmail.text = facebookData["email"]
            txtFName.text = facebookData["name"]
            
        }else{
            btn_back.isHidden = false
            if email_id != ""
            {
                txtEmail.text = self.email_id
                txtEmail.isUserInteractionEnabled = false
            }
        }
         self.vendorListWebService()
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews(){
        
        // tableView.reloadData()
    }
    
    // MARK: Feet Button Click
    @IBAction func btnFeetClick(_ sender: UIButton) {
        self.view.endEditing(true)
        ActionSheetStringPicker.show(withTitle: "Select Vessel Type", rows: ["Feet","Meters"], initialSelection: 0, doneBlock: {
            picker,index,value in
             self.txtFeet.text = value as? String
             return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
      }
    
    
    // MARK: Position Button Click
    @IBAction func btnPositionClick(_ sender: UIButton) {
        self.view.endEditing(true)
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"PositionVC") as! PositionVC
        controller.block = { dict in
            self.positionValue = dict
            self.position_id = self.positionValue["id"] as! String
            self.position_name = self.positionValue["name"] as! String
            
            self.txtPosition.text =  self.positionValue["name"] as? String
            
            print(self.position_name)
        }
        controller.push()
        
    }
    
    // MARK: DOB Button Click
    @IBAction func btnDOBClick(_ sender: Any) {
        
       // self.resignFirstResponder()
        
        self.view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date, doneBlock: {
            picker,index,value in
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let date = index as! Date
            
            let dateOfBirth = date
            let today = NSDate()
            let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let age = gregorian.components([.year], from: dateOfBirth, to: today as Date, options: [])
            if age.year! < 18 {
                self.AlertMessage("Date of birth should not be less than 18 years.")
                // user is under 18
            }else{
                 self.txtDob.text = dateFormatter.string(from: date)
            }
 
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!?.superview)
        //        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //        datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        
        datePicker?.maximumDate = Date()
        
        datePicker?.show()
        
    }
    
    // MARK: Gender Button Click
    @IBAction func btnGenderClick(_ sender: Any) {
        self.view.endEditing(true)
        ActionSheetStringPicker.show(withTitle: "Select Gender", rows: ["Male","Female"], initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txtGender.text = value as? String
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
        
    }
    
    
    // MARK: Back Button Method
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: Next Button Method
    @IBAction func btnNextPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if img_Profile.image == UIImage.init(named:"complete_profile")
        {
            self.AlertMessage("Please select image.")
        }
        else if txtFName.text! .isEmpty
        {
            self.AlertMessage("Please enter first name.")
        }
        else if txtLName.text! .isEmpty
        {
            self.AlertMessage("Please enter last name.")
        }
        else if txtDob.text! .isEmpty
        {
            self.AlertMessage("Please select date of birth.")
        }
        else if txtGender.text! .isEmpty
        {
            self.AlertMessage("Please select gender.")
        }
        else if txtEmail.text! .isEmpty
        {
            self.AlertMessage("Please enter email address.")
        }
        else if validateEmail(txtEmail.text!) == false
        {
            self.AlertMessage("Invalid email address.")
            self.txtPassword.text = ""
        }
        else if txtPassword.text! .isEmpty
        {
            self.AlertMessage("Please enter password.")
        }
        else if (txtPassword.text!.length) < 6
        {
            self.AlertMessage("Minimum of 6 characters needed for password.")
            self.txtPassword.text = ""
        }
        else if txtBoadName.text! .isEmpty
        {
            self.AlertMessage("Please enter boat name.")
        }
        else if txtPosition.text! .isEmpty
        {
            self.AlertMessage("Please select your position.")
        }
        else if txtVesselSize.text! .isEmpty
        {
            self.AlertMessage("Please enter vessel size.")
        }
        else if txtFeet.text! .isEmpty
        {
            self.AlertMessage("Please select vessel size type.")
        }
        else if txtMMSI.text! .isEmpty
        {
            self.AlertMessage("Please enter MMSI.")
        }
        else if self.vendorsIDArr.count == 0 && self.vendorAccountArr.count != 0
        {
            self.AlertMessage("Please select vendor.")
        }
        else if self.vendorAccountArr.count == 0 && self.vendorsIDArr.count != 0
        {
            self.AlertMessage("Please enter vendor account number.")
        }
        else
        {
            PostData = ["email":self.txtEmail.text!,
                        "type":"signup"]
            
            webService_obj.fetchDataFromServer(header:"existEmail", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    let data = responce.value(forKey: "data") as! NSDictionary
                    let isexist = data.value(forKey: "isExist")
                        
                    if (isexist! as AnyObject).doubleValue == 1.0
                    {
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                           
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)

                    }else{
                        
                        //Convert in date format
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MMM dd, yyyy"
                        let myDate = dateFormatter.date(from: self.txtDob.text!)
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        
                        let dateDob =  dateFormatter.string(from: myDate!)
                        
                        var dict = ["f_Name":self.txtFName.text!,
                                    "L_Name":self.txtLName.text!,
                                    "dob":dateDob,
                                    "gender":self.txtGender.text!,
                                    "email":self.txtEmail.text! ,
                                    "password":self.txtPassword.text!,
                                    "boat_name":self.txtBoadName.text! ,
                                    "position":self.position_id,
                                    "vessel_size":self.txtVesselSize.text! ,
                                    "vessel_type":self.txtFeet.text!,
                                    "mmsi":self.txtMMSI.text!] as [String : Any]
                        
                        
                        if self.vendorsIDArr.count != 0{
                            var arrVID = [String]()
                            for indexPath in 0..<self.numberOfRows
                            {
                                let dict = self.vendorsIDArr["\(indexPath)"]
                                if dict != nil {
                                    arrVID.append(dict!)
                                }
                            }
                            dict["vendor_id"] = arrVID
                        }
                        
                        if self.vendorAccountArr.count != 0 {
                            var arrVaccount = [String]()
                            for indexPath in 0..<self.numberOfRows
                            {
                                let dict = self.vendorAccountArr["\(indexPath)"]
                                if dict != nil {
                                    arrVaccount.append(dict!)
                                }
                            }
                            dict["vendor_account"] = arrVaccount
                        }

                        
                        if self.isfromFacebook == true
                        {
                            dict["fb_id"]=self.facebookData["fb_id"]!
                        }
                        
                        self.signUPDataDict = dict
                        
                        print("SignUp Complete Data->\(self.signUPDataDict)")
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
                        vc.finalDataDict = self.signUPDataDict
                        vc.isfromFacebook2 = self.isfromFacebook
                        
                        if self.email_id != ""
                        {
                            vc.sharing_user = "true"
                        }
                        
                            if self.isfromFacebook == true
                            {
                                vc.userImg = self.facebookUserImg
                            }
                            else
                            {
                                 vc.userImg = self.pickedImage
                            }
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
 
    // MARK: UITableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VendorDetailCell", for: indexPath) as! VendorDetailCell
        
        
        cell.txtVendor.text =   self.arrSelectedVendorName["\(indexPath.row)"] ?? ""
        cell.txtAccNumber.text = self.vendorAccountArr["\(indexPath.row)"] ?? ""
        
//          if numberOfRows == self.vendorsIDArr.count && numberOfRows == self.vendorAccountArr.count{
//            
//            var arrSelectedVName = [String]()
//            for indexPath in 0..<self.numberOfRows
//            {
//                let dict = self.arrSelectedVendorName["\(indexPath)"]
//                arrSelectedVName.append(dict!)
//            }
//            
//            var arrSelectedVaccount = [String]()
//            for indexPath in 0..<self.numberOfRows
//            {
//                let dict =  self.vendorAccountArr["\(indexPath)"]
//                arrSelectedVaccount.append(dict!)
//            }
//
//            cell.txtVendor.text =   arrSelectedVName[indexPath.row]
//            cell.txtAccNumber.text = arrSelectedVaccount[indexPath.row]
//         }
//        else
//          {
//            if indexPath.row == self.vendorsIDArr.count
//            {
//                cell.txtVendor.text =   ""
//                cell.txtAccNumber.text = ""
//            }
//        }
        
        if numberOfRows == 0
        {
             cell.btnMinus.isHidden = true
        }else
        {
             cell.btnMinus.isHidden = false
        }
        
        cell.txtVendor.tag = indexPath.row
        cell.btnVendor?.tag = indexPath.row
        cell.btnMinus?.tag = indexPath.row
        cell.btnVendor?.addTarget(self, action: #selector(selectVendorClicked(_:)), for: .touchUpInside)
        cell.btnMinus?.addTarget(self, action: #selector(btnMinusClicked(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
    
    
    // MARK: Pluse Button Click
    @IBAction func btnPlusClick(_ sender: UIButton) {
        
        if self.vendorsIDArr.count == numberOfRows && self.vendorAccountArr.count == numberOfRows {
            
             numberOfRows += 1
             self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.tableViewHeightCont.constant = self.tableView.contentSize.height
        }else{
            if self.vendorsIDArr.count != numberOfRows
            {
                self.AlertMessage("Please select vendor.")
            }else
            {
                self.AlertMessage("Please enter vendor account number.")
            }
         }
    }
    
    // MARK: Minus Button Click Method
    @IBAction func btnMinusClicked(_ sender: UIButton)
    {
        guard let cell = tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? VendorDetailCell else {
            return
        }
        
         if numberOfRows > 0 {
            
             self.vendorsIDArr["\(sender.tag)"] =  nil
            self.vendorAccountArr["\(sender.tag)"] =  nil
             self.arrSelectedVendorName["\(sender.tag)"] =  nil
            
            var temp1 = [String:String]()
            for (key, value) in self.arrSelectedVendorName
            {
                if Int(key)! < sender.tag{
                    temp1[key] = value
                }
                else{
                    temp1["\(Int(key)!-1)"] = value
                }
            }
            
            self.arrSelectedVendorName = temp1
            
            
            var temp3 = [String:String]()
            for (key, value) in self.vendorsIDArr
            {
                if Int(key)! < sender.tag{
                    temp3[key] = value
                }
                else{
                    temp3["\(Int(key)!-1)"] = value
                }
            }
            
            self.vendorsIDArr = temp3
            
            
            var temp2 = [String:String]()
            for (key, value) in self.vendorAccountArr
            {
                if Int(key)! < sender.tag{
                    temp2[key] = value
                }
                else{
                    temp2["\(Int(key)!-1)"] = value
                }
            }
            
            self.vendorAccountArr = temp2
            
            
            
             numberOfRows -= 1
             self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.tableViewHeightCont.constant = self.tableView.contentSize.height
            
        }else{
            
        }
    }
    
    // MARK: UITextFieldDelegate Method
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            return true
        }

        let pointInTable = textField.convert(textField.bounds.origin, to: self.tableView)
        let textFieldIndexPath = self.tableView.indexPathForRow(at: pointInTable)
        
         if textField.text != ""
        {
             self.vendorAccountArr["\(String(describing: textFieldIndexPath!.row))"] =  (textField.text!)
            
          //  self.arrSelectedAccNum["\(String(describing: textFieldIndexPath!.row))"] = (textField.text!)
            
            NSLog("data arr -->\(self.vendorAccountArr)")
         }else
         {
           self.vendorAccountArr.removeValue(forKey: String(textFieldIndexPath!.row))
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.txtEmail{
            if validateEmail(txtEmail.text!) == false
            {
                self.AlertMessage("Invalid email address.")
                self.txtPassword.text = ""
            }
        }
    }
    
    // MARK: Select Vendor Button Click Method
    @IBAction func selectVendorClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        guard let cell = tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? VendorDetailCell else {
            return
        }
        
        ActionSheetStringPicker.show(withTitle: "Select Vendor", rows: self.arrVendorName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            cell.txtVendor.text = value as? String
            
            self.arrSelectedVendorName["\(sender.tag)"] = (value as? String)!
            self.vendorsIDArr["\(sender.tag)"] =  self.vendorsDetails[index]["id"]!
            
            
            NSLog("data arr -->\(self.vendorsIDArr)")
            
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
        
    }
    
    
    // MARK: - Call Fetch Vendors List WebService
    func vendorListWebService()
    {
        webService_obj.fetchDataFromServer(header:"vendor_list", withParameter:[:], inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.vendorsDetails = responce.value(forKey: "data") as! [[String:String]]
                self.arrVendorName = self.vendorsDetails.map{ $0["username"]! }
            }
        }
    }
    
    func validateEmail(_ enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
     }
 
    // MARK: - IBAction Picture Button
    @IBAction func btn_Picture(_ sender: Any)
    {
        self.AlertController()
    }
    
    // MARK: - Method Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imageCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imageGallery()
        })
          
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
 }
