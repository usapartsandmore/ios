//
//  PositionVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 11/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class PositionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var tableView: UITableView!
    
    
    var positionArr = [[String:AnyObject]]()
    var subPositionArr = [[String:AnyObject]]()
    
    var arrImgs  = NSArray()
    
    var dictData = [String: [[String : AnyObject]]]()
    var dictKeys = [String]()
    
    var block: (([String : AnyObject])->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrImgs = [#imageLiteral(resourceName: "interior"),#imageLiteral(resourceName: "desk"),#imageLiteral(resourceName: "engrinning"),#imageLiteral(resourceName: "gallery"),#imageLiteral(resourceName: "other")]
        //Fetch Position List Data
        if let path = Bundle.main.path(forResource: "positions", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    positionArr = json as! [[String : AnyObject]]
                    print("positionArr List Data:",positionArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        else {
            print("Invalid filename/path.")
        }
        
        //Fetch SubPosition List Data
        
        if let path = Bundle.main.path(forResource: "sub_positions", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    subPositionArr = json as! [[String : AnyObject]]
                    
                    dictData = [String: [[String : AnyObject]]]()
                    
                    
                    subPositionArr.forEach({ object in
                        let id = "\(object["position_id"]!)"
                        var array = dictData[id] ?? [[String : AnyObject]]()
                        array.append(object)
                        dictData[id] = array
                    })
                    
                    dictKeys = (dictData as NSDictionary).allKeys as! [String];
                    dictKeys.sort()
                    
                    
                    print("subPositionArr List Data:",subPositionArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        else {
            print("Invalid filename/path.")
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func gotoBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true);
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return dictKeys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictData[dictKeys[section]]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = dictData[dictKeys[indexPath.section]]![indexPath.row] //["name": Chief stewardess, "id": 1, "created": 2017-09-08 17:19:27, "position_id": 1]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PositionCustomCell", for: indexPath)
        cell.textLabel?.text=data["name"] as? String
        cell.accessoryType = UITableViewCellAccessoryType.none
        
        
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let rect = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40)
        let view = UIView(frame: rect)
        view.backgroundColor = UIColor.white
        let ImageView = UIImageView(frame: CGRect(x: 15, y: 5, width: 20, height: 30))
        let label = UILabel(frame: CGRect(x: 45, y: 5, width: 150, height: 30))
        label.text = positionArr[section]["name"] as? String
        // label.textColor=UIColor(red: 0.0, green: 164.0, blue: 224.0, alpha: 1.0)
        ImageView.image = arrImgs[section] as? UIImage
        view.addSubview(label)
        view.addSubview(ImageView)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath)
              cell?.accessoryType = UITableViewCellAccessoryType.checkmark
     }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.none
    }
    
    @IBAction func btnDoneClick(_ sender: Any) {
        if let indexPath = tableView.indexPathForSelectedRow{
            let data = dictData[dictKeys[indexPath.section]]![indexPath.row]
            block?(data)
            self.navigationController?.popViewController(animated: true);
        }
     }
    
}
