//
//  AddAddressVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 05/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CoreLocation

class AddAddressVC: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var txtCountry: UITextField!
    @IBOutlet var txtAddress1: UITextField!
    @IBOutlet var txtAddress2: UITextField!
    @IBOutlet var txtCity: UITextField!
    @IBOutlet var txtState: UITextField!
    @IBOutlet var txtPostalCode: UITextField!
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var btnCity: UIButton!
    @IBOutlet var btnState: UIButton!
    var geocoder = CLGeocoder()
    
    var citiesArr = [[String:AnyObject]]()
    var countriesArr = [[String:AnyObject]]()
    var statesArr = [[String:AnyObject]]()
    
    var finalDataDict = [String:Any]()
    var isfromFacebook2 = Bool()
    var arrCityName = [String]()
    var arrStateName = [String]()
    var arrCountryName = [String]()
    
    var selectedCity = ""
    var selectedState = ""
    var selectedCountry = ""
    var stateId = ""
    
    var sharing_user = ""
    
    var userImg = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPostalCode.delegate = self
        
        //Fetch Country List Data
        if let path = Bundle.main.path(forResource: "countries", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    countriesArr = json as! [[String : AnyObject]]
                    self.arrCountryName = self.countriesArr.map{ ($0["name"]! as! String) }
                    //print("Country List Data:",countriesArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        //Fetch State List Data
        if let path = Bundle.main.path(forResource: "states", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    statesArr = json as! [[String : AnyObject]]
                    self.arrStateName = self.statesArr.map{ ($0["name"]! as! String) }
                    
                    //print("State List Data:",statesArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        //Fetch City List Data
        if let path = Bundle.main.path(forResource: "cities", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    citiesArr = json as! [[String : AnyObject]]
                    self.arrCityName = self.citiesArr.map{ ($0["name"]! as! String) }
                    
                    //print("City List Data:",citiesArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Term Of Services Method
    @IBAction func btnTermOfServicesClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        vc.selctButton = "Term Of Services"
        vc.push()
    }
    
    // MARK: Privacy Policy Method
    @IBAction func btnPrivacyPolicyClick(_ sender: UIButton) {
        //        let controller = self.storyboard?.instantiateViewController(withIdentifier:"PrivacyPolicyVC")
        //        controller?.push()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        vc.selctButton = "Privacy Policy"
        vc.push()
    }
    
    // MARK: Cancellation Policy Method
    @IBAction func btnCancellationPolicyClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
        vc.selctButton = "Cancellation Policy"
        vc.push()
    }
    
    // MARK: Back Button Method
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: City Button Click
    @IBAction func btnCityClick(_ sender: Any) {
        self.view.endEditing(true)
        ActionSheetStringPicker.show(withTitle: "Select City", rows:self.arrCityName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txtCity.text = value as? String
            
            self.selectedCity = self.citiesArr[index]["id"]! as! String
            
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
        
    }
    
    // MARK: State Button Click
    @IBAction func btnStateClick(_ sender: Any) {
         self.view.endEditing(true)
        ActionSheetStringPicker.show(withTitle: "Select State", rows:self.arrStateName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txtState.text = value as? String
            
            self.selectedState = self.statesArr[index]["id"]! as! String
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
        
    }
    
    // MARK: Country Button Click
    @IBAction func btnCountryClick(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Select Country", rows:self.arrCountryName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txtCountry.text = value as? String
            self.selectedCountry = self.countriesArr[index]["id"]! as! String
            
            if self.txtCountry.text != "United States"
            {
                self.txtCity.text = ""
                self.txtState.text = ""
                
                self.btnCity.isHidden = true
                self.btnState.isHidden = true
            }else{
                self.btnCity.isHidden = false
                self.btnState.isHidden = false
            }

            
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
    }
    
    // MARK: Sign Up Method
    @IBAction func btnSignUpPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtCountry.text! .isEmpty
        {
            self.AlertMessage("Please select country.")
        }
        else if txtAddress1.text! .isEmpty
        {
            self.AlertMessage("Please enter address.")
        }
        else if txtCity.text! .isEmpty
        {
            self.AlertMessage("Please select city.")
        }
        else if txtState.text! .isEmpty
        {
            self.AlertMessage("Please select state.")
        }
        else if txtPostalCode.text! .isEmpty
        {
            self.AlertMessage("Please enter postal code.")
        }
        else if txtPhoneNumber.text! .isEmpty
        {
            self.AlertMessage("Please enter Phone number.")
        }
        else
        {
            self.signupWebService()
        }
    }
    
    // MARK: - Call Fetch Vendors List WebService
    func signupWebService()
    {
        
        PostData = ["first_name":self.finalDataDict["f_Name"]!,
                    "last_name":self.finalDataDict["L_Name"]!,
                    "dob":self.finalDataDict["dob"]!,
                    "gender":self.finalDataDict["gender"]!,
                    "email":self.finalDataDict["email"]!,
                    "password":self.finalDataDict["password"]!,
                    "boat_name":self.finalDataDict["boat_name"]!,
                    "position":self.finalDataDict["position"]!,
                    "vessel_size":self.finalDataDict["vessel_size"]!,
                    "vessel_size_type":self.finalDataDict["vessel_type"]!,
                    "mmsi":self.finalDataDict["mmsi"]! ,
                    "vendors_id":self.finalDataDict["vendor_id"] ?? [],
                    "vendors_account_number":self.finalDataDict["vendor_account"] ?? [],
                    "country":self.selectedCountry,
                    "address1":self.txtAddress1.text!,
                    "address2":self.txtAddress2.text!,
                    "zipcode":self.txtPostalCode.text!,
                    "contact_number":self.txtPhoneNumber.text!,
                    "device_type":"iOS",
                    "device_token":webService_obj.Retrive("Device_token") as! String] as [String : Any]
     
        if self.selectedState == ""
        {
            PostData["state"] =  self.txtState.text
        }else{
            PostData["state"] =  self.selectedState
        }
        
        if self.selectedCity == ""
        {
            PostData["city"] =  self.txtCity.text
        }else{
            PostData["city"] =  self.selectedCity
        }
        
        if self.isfromFacebook2 == true
        {
            PostData["fb_id"]=self.finalDataDict["fb_id"]!
        }
        
        if self.sharing_user == "true"
        {
            PostData["id"] = webService_obj.Retrive("User_Id") as! String
        }
        
        webService_obj.profileDataFromServer(header:"customer_add", withParameter: PostData as NSDictionary, coverdata: self.userImg.fixed!, inVC: self, showHud: true) { (responce,staus) in
            if staus{
                // let data = responce.value(forKey: "data") as! NSDictionary
                
                if self.sharing_user == "true"
                {
                    let mainVcIntial = kConstantObj.SetIntialMainViewController("HomeVC")
                    appDelegate.window?.rootViewController = mainVcIntial
                    
                }else
                {
                    
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        //Save User Data
                        // defaults.set(true, forKey: kAPPLOGIN_IN)
                        // let User_Id = data["customer_id"]
                        // webService_obj.Save(User_Id as! String, keyname: "User_Id")
                        
                        let mainVcIntial = kConstantObj.SetIntialMainViewController("LoginVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }
            }
        }
    }
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        if string == ""{ //BackSpace
            return true
        }else if str!.characters.count < 3{
            if str!.characters.count == 1{
                txtPhoneNumber.text = "("
            }
        }else if str!.characters.count == 5{
            txtPhoneNumber.text = txtPhoneNumber.text! + ") "
        }else if str!.characters.count == 10{
            txtPhoneNumber.text = txtPhoneNumber.text! + "-"
        }else if str!.characters.count > 20{
            return false
        }
        return true
    }
    
    
    // MARK: - Method Alert Controll
    func textField (_ textField :  UITextField, shouldChangeCharactersIn range:  NSRange, replacementString string:  String  )  ->  Bool {
        
        if textField == txtPostalCode
        {
            let maxLength = 9
            let currentString: NSString = txtPostalCode.text! as NSString
            if txtPostalCode.text?.characters.count != 6
            {
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
        }
        if textField == txtPhoneNumber
        {
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            if textField == txtPhoneNumber{
                return checkEnglishPhoneNumberFormat(string: string, str: str)
            }else{
                return true
            }
        }
        return true;
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            if textField == self.txtPostalCode {
                self.geocoder.geocodeAddressString(self.txtPostalCode.text!) { (placemarks, error) in
                    if (error != nil){
                        
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:"Location not available for this zip code.", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                            self.txtPostalCode.text = ""
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                        
                    }else{
                        
                        let placemark = placemarks?[0] ?? CLPlacemark()
                        let cityname = placemark.locality
                        
                        if self.txtCountry.text == "United States"
                        {
                            for (index,dict) in self.arrCityName.enumerated() {
                                if dict.contains(cityname ?? "") {
                                    self.selectedCity =  self.citiesArr[index]["id"]! as! String
                                    self.txtCity.text = self.citiesArr[index]["name"]! as? String
                                    self.stateId = (self.citiesArr[index]["state_id"]! as? String)!
                                }
                            }
                            
                            for (index,dict) in self.statesArr.enumerated() {
                                if (String(describing: dict["id"]!) == self.stateId) {
                                    self.selectedState =  self.statesArr[index]["id"]! as! String
                                    self.txtState.text = self.statesArr[index]["name"]! as? String
                                }
                            }
                        }else
                        {
                             self.txtCity.text = placemark.addressDictionary!["SubAdministrativeArea"] as? String
                            self.txtState.text = placemark.addressDictionary!["State"] as? String
                        }
                    }
                }
            }
        }
    }
}
