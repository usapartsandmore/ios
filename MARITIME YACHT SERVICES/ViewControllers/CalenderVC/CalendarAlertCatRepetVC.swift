//
//  CalendarAlertCatRepetVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 01/02/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class CalendarDetailCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if isSelected == false {
            accessoryType = UITableViewCellAccessoryType.none
        }
        else {
            accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
}


class CalendarAlertCatRepetVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var lbl_header: UILabel!
 
    @IBOutlet weak var btnRepair: UIButton!
    @IBOutlet weak var btnMaintenance: UIButton!
    @IBOutlet weak var btnSafety: UIButton!
    @IBOutlet weak var btnMonitor: UIButton!
    @IBOutlet weak var btnManage: UIButton!
    @IBOutlet weak var btnCharter: UIButton!
    @IBOutlet weak var btnOwners_Trip: UIButton!
    @IBOutlet weak var btnOwner: UIButton!
    
    
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var txtOther: UITextField!
    @IBOutlet weak var btnOtherDone: UIButton!
    
    var calendarData = [String]()
    var isFrom = ""
    
    
    var block: (([String : String])->Void)?
    var selectedRow = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tblView.tableFooterView = UIView()
        
        if self.isFrom == "category"
        {
            self.tblView.isHidden = true
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            self.lbl_header.text = "Select Category"
         }else if self.isFrom == "alert"
        {
            self.tblView.isHidden = false
             self.lbl_header.text = "Alert"
            self.calendarData = ["None","At time of event","5 minutes before","15 minutes before","30 minutes before","1 hour before","2 hour before","1 day before","2 days before","1 week before"]
        }else
        {
            self.tblView.isHidden = false
            self.lbl_header.text = "Repeat Alert"
            self.calendarData = ["Never","Every Day","Every Week","Every 2 Weeks","Every Month","Every Year"]
        }
        
     
        if self.isFrom != "category"
        {
            //Set selected row
            var indexPaths = [IndexPath]()
            
            for (index, name) in self.calendarData.enumerated(){
                if selectedRow.contains(name){
                    indexPaths.append(IndexPath(row: index, section: 0))
                }
            }
            
            indexPaths.forEach { (indexPath) in
                self.tblView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }

     }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
       // getCalendarDetailWebService()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func BtnCategoryClicked(_ sender: UIButton) {
        
        switch sender {
        case btnRepair:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Repair"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break
            
        case btnMaintenance:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Maintenance"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break
         
        case btnSafety:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Safety"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break
            
        case btnMonitor:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Monitor"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break
            
        case btnManage:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Manage"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break
            
        case btnCharter:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Charter"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break

            
        case btnOwners_Trip:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Owners Trip"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break

        case btnOwner:
            self.txtOther.isHidden = true
            self.btnOtherDone.isHidden = true
            
            let dataDict = ["name":"Owner"]
            block?(dataDict)
            self.navigationController?.popViewController(animated: true)
            break
 
         case btnOther:
            self.txtOther.isHidden = false
            self.btnOtherDone.isHidden = false
            break
   
        default:
            break
        }
    }
    
    @IBAction func btnOtherDoneClicked(_ sender: UIButton) {
        if self.txtOther.text == ""{
        }else{
            let dataDict = ["name":self.txtOther.text]
            block?(dataDict as! [String : String])
            self.navigationController?.popViewController(animated: true)
        }
     }
    
    
        // MARK: - Table view data source
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.calendarData.count
        }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarDetailCell", for: indexPath) as! CalendarDetailCell
    
            cell.name.text = self.calendarData[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
        {
           let dataDict = ["name":self.calendarData[indexPath.row]]
    
            block?(dataDict)
    
            self.navigationController?.popViewController(animated: true)
        }

  
}
