//
//  AddImageVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 20/02/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

 class AddImageVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var selectedImage: UIImage? = nil
    var imageCollection = [UIImage]()
   // var imageURL = [UIImage: String]()
    var lastSelectedIndex: Int? = nil
    
    var block: (([String:String])->Void)?
    var selectedImgDict = [String]()
    
    var mainImgDict = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        if mainImgDict[0] is String{
//            
//        }
        
        if selectedImgDict.count > 0
        {
            if !selectedImgDict.contains("btn_add"){
                selectedImgDict.append("btn_add")
            }
            
        }else{
        }
        if !imageCollection.contains(#imageLiteral(resourceName: "btn_add")){
            imageCollection.append(#imageLiteral(resourceName: "btn_add"))
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
          _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btn_Continue(_ sender: Any){
        if imageCollection.count > 0{
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
    
        var imagesData = [String : UIImage]()
        
        _=imageCollection.removeLast()
        
        for (index, img) in imageCollection.enumerated(){
            imagesData["image\(index+1)"] = img
        }
        
        if imagesData.count == 0 {
            return
        }
        print("\(imagesData)")
        
        webService_obj.uploadMultipleImages(header: "add_image", withParameter: PostData, images: imagesData, compressRatio: 0.1, inVC: self, showHud: true) { (response, message, status) in
            if status{
                 //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    let data = response.value(forKey: "data") as! [String:String]
                    self.block?(data)
                    self.navigationController?.popViewController(animated: true)

                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
             }
        }
        }else
        {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

     //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedImgDict.count > 0
        {
            return selectedImgDict.count
        }else{
            return imageCollection.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventandTaskImgCell", for: indexPath) as! EventandTaskImgCell
        cell.contentView.layer.cornerRadius = 1.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        cell.contentView.layer.borderWidth = 0.5
        
        if selectedImgDict.count > 0
        {
            if indexPath.row == selectedImgDict.count-1{
                cell.btn_delete.isHidden = true
                cell.img_Event.image = UIImage(named: self.selectedImgDict[indexPath.row])
            }else{
                cell.btn_delete.isHidden = false
                let str = self.selectedImgDict[indexPath.row]
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cell.img_Event.af_setImage(withURL: url as URL)
            }
        }else{
            cell.img_Event.image = imageCollection[indexPath.row]
            
            if indexPath.row == imageCollection.count-1{
                cell.btn_delete.isHidden = true
            }else{
                cell.btn_delete.isHidden = false
            }

        }
        
        cell.btn_delete.tag = indexPath.row
        cell.btn_delete.addTarget(self, action: #selector(btnDeleteImgClicked(_:)), for: .touchUpInside)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        lastSelectedIndex = indexPath.row
        if selectedImgDict.count > 0
        {
            if indexPath.row == selectedImgDict.count-1{
                if imageCollection.count == 51{
                    self.AlertMessage("Maximum image upload limit of 50 has been reached.")
                }else{
                    self.imagePickerAlert()
                }
            }else{
                
            }
            
        }else{
            if indexPath.row == imageCollection.count-1{
                if imageCollection.count == 51{
                    self.AlertMessage("Maximum image upload limit of 50 has been reached.")
                }else{
                    self.imagePickerAlert()
                }
            }else{
                
            }
        }
        
        if indexPath.row == imageCollection.count-1
        {
            
        }else{
            let cell = collectionView.cellForItem(at: indexPath) as! EventandTaskImgCell
            
            let newImageView = UIImageView(image: cell.img_Event.image)
            let viewNew = collectionView.superview!.superview!.superview!.superview!.superview!
            
            newImageView.frame = viewNew.frame
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            viewNew.addSubview(newImageView)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = 80
        return CGSize(width: size, height: size)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
    
    
    @IBAction func btnDeleteImgClicked(_ sender: UIButton)
    {
        if selectedImgDict.count > 0
        {
            self.selectedImgDict.remove(at: sender.tag)
        }else{
            self.imageCollection.remove(at: sender.tag)
        }
        self.collectionView.reloadData()
    }
    
}

//ImagePicker extension
extension AddImageVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        
        if let pImage = (info[UIImagePickerControllerOriginalImage] as? UIImage)?.fixed{
            
            if let imageData = UIImageJPEGRepresentation(pImage, 0.1), let img = UIImage.init(data: imageData) {
                
                let image = imageCollection.removeLast()
                imageCollection.append(img)
                imageCollection.append(image)
                collectionView.reloadData()
                let indexPath = IndexPath(item: imageCollection.count-1, section: 0)
                self.collectionView.scrollToItem(at: indexPath, at: .right, animated: false)
                self.collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredVertically)
                let itemCount = CGFloat(imageCollection.count-1)
                let width = collectionView.bounds.height*0.75
                var size = width * itemCount
                size -= width/2
                selectedImage = imageCollection[Int(itemCount-1)]
             }
        }
        dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerAlert()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imagePickerCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imagePickerGallery()
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        // 4
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}


