//
//  ShippingAddressVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 11/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import CoreLocation

class ShippingAddressVC: UIViewController, UIScrollViewDelegate, UITextFieldDelegate{
    
    @IBOutlet var txt_phoneNo: UITextField!
    @IBOutlet var txt_postalCode: UITextField!
    @IBOutlet var txt_state: UITextField!
    @IBOutlet var txt_city: UITextField!
    @IBOutlet var txt_address2: UITextField!
    @IBOutlet var txt_address1: UITextField!
    @IBOutlet var txt_country: UITextField!
    @IBOutlet var txt_name: UITextField!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var btnCity: UIButton!
    @IBOutlet var btnState: UIButton!
    var geocoder = CLGeocoder()
    
    var citiesArr = [[String:AnyObject]]()
    var countriesArr = [[String:AnyObject]]()
    var statesArr = [[String:AnyObject]]()
    var arrCityName = [String]()
    var arrStateName = [String]()
    var arrCountryName = [String]()
    var selectedCity = ""
    var selectedState = ""
    var selectedCountry = ""
    var isfromServices = ""
    var stateId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isfromServices == "true"
        {
            lblHeader.text = "Address"
         }else{
            lblHeader.text = "Shipping Address"
        }
        
        //Fetch Country List Data
        if let path = Bundle.main.path(forResource: "countries", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    countriesArr = json as! [[String : AnyObject]]
                    self.arrCountryName = self.countriesArr.map{ ($0["name"]! as! String) }
                    //print("Country List Data:",countriesArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        //Fetch State List Data
        if let path = Bundle.main.path(forResource: "states", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    statesArr = json as! [[String : AnyObject]]
                    self.arrStateName = self.statesArr.map{ ($0["name"]! as! String) }
                    
                    //print("State List Data:",statesArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        //Fetch City List Data
        if let path = Bundle.main.path(forResource: "cities", ofType: "json"){
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                do{
                    let json =  try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    citiesArr = json as! [[String : AnyObject]]
                    self.arrCityName = self.citiesArr.map{ ($0["name"]! as! String) }
                    
                    //print("City List Data:",citiesArr)
                }catch let error{
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        
        
        // Do any additional setup after loading the view.
    }
    // MARK: State Button Click
    @IBAction func btnStateSelect(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select State", rows:self.arrStateName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txt_state.text = value as? String
            self.selectedState = self.statesArr[index]["id"]! as! String
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
        
    }
    
    // MARK: City Button Click
    @IBAction func btnCitySelect(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select City", rows:self.arrCityName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txt_city.text = value as? String
            self.selectedCity = self.citiesArr[index]["id"]! as! String
            
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
    }
    // MARK: Country Button Click
    @IBAction func btnCountrySelect(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select Country", rows:self.arrCountryName, initialSelection: 0, doneBlock: {
            picker,index,value in
            
            self.txt_country.text = value as? String
            self.selectedCountry = self.countriesArr[index]["id"]! as! String
            
            if self.txt_country.text != "United States"
            {
                self.txt_city.text = ""
                self.txt_state.text = ""
                
                self.btnCity.isHidden = true
                self.btnState.isHidden = true
            }else{
                self.btnCity.isHidden = false
                self.btnState.isHidden = false
            }

            
            return
        }, cancel: {
            
            ActionStringCancelBlock in return
            
        }, origin: sender)
        
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnSavePressed(_ sender: UIButton) {
        if txt_name.text! .isEmpty
        {
            self.AlertMessage("Please enter name.")
        }
        else if txt_country.text! .isEmpty
        {
            self.AlertMessage("Please select country.")
        }
        else if txt_address1.text! .isEmpty
        {
            self.AlertMessage("Please enter address line 1.")
        }
        else if txt_city.text! .isEmpty
        {
            self.AlertMessage("Please select city.")
        }
        else if txt_state.text! .isEmpty
        {
            self.AlertMessage("Please select state.")
        }
        else if txt_postalCode.text! .isEmpty
        {
            self.AlertMessage("Please enter postal code.")
        }
        else if txt_phoneNo.text! .isEmpty
        {
            self.AlertMessage("Please enter phone no.")
        }
        else
        {
//            if isfromServices == "true"
//            {
//                 self.dataPostWebservice()
//            }else{
//             }
            self.dataPostWebservice()
            
         }
    }
    
    func dataPostWebservice() {
        
        if selectedCity == ""
        {
            selectedCity = self.txt_city.text!
        }
        
        if selectedState == ""
        {
            selectedState = self.txt_state.text!
        }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "name":txt_name.text!,
                    "city":self.selectedCity,
                    "state":self.selectedState,
                    "country":self.selectedCountry,
                    "zipcode":txt_postalCode.text!,
                    "contact_number":txt_phoneNo.text!,
                    "address1":txt_address1.text!,
                    "address2":txt_address2.text!]
        
        webService_obj.fetchDataFromServer(header:"customer_shipping_address", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus {
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Address added successfully.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                     _ = self.navigationController?.popViewController(animated:true)
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Method Alert Controll
    func textField (_ textField :  UITextField, shouldChangeCharactersIn range:  NSRange, replacementString string:  String  )  ->  Bool {
        
        if textField == txt_postalCode
        {
            let maxLength = 9
            let currentString: NSString = txt_postalCode.text! as NSString
            if txt_postalCode.text?.characters.count != 6
            {
                let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
            }
        }
        if textField == txt_phoneNo
        {
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            if textField == txt_phoneNo{
                return checkEnglishPhoneNumberFormat(string: string, str: str)
            }else{
                return true
            }
        }
        return true;
    }
    
    func checkEnglishPhoneNumberFormat(string: String?, str: String?) -> Bool{
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.characters.count < 3{
            
            if str!.characters.count == 1{
                
                txt_phoneNo.text = "("
            }
            
        }else if str!.characters.count == 5{
            
            txt_phoneNo.text = txt_phoneNo.text! + ") "
            
        }else if str!.characters.count == 10{
            
            txt_phoneNo.text = txt_phoneNo.text! + "-"
            
        }else if str!.characters.count > 19{
            
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            if textField == self.txt_postalCode {
                self.geocoder.geocodeAddressString(self.txt_postalCode.text!) { (placemarks, error) in
                    if (error != nil){
                        
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:"Location not available for this zip code.", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                            self.txt_postalCode.text = ""
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                        
                    }else{
                        
                        let placemark = placemarks?[0] ?? CLPlacemark()
                        let cityname = placemark.locality
                        
                        if self.txt_country.text == "United States"
                        {
                            for (index,dict) in self.arrCityName.enumerated() {
                                if dict.contains(cityname ?? "") {
                                    self.selectedCity =  self.citiesArr[index]["id"]! as! String
                                    self.txt_city.text = self.citiesArr[index]["name"]! as? String
                                    self.stateId = (self.citiesArr[index]["state_id"]! as? String)!
                                }
                            }
                            
                            for (index,dict) in self.statesArr.enumerated() {
                                if (String(describing: dict["id"]!) == self.stateId) {
                                    self.selectedState =  self.statesArr[index]["id"]! as! String
                                    self.txt_state.text = self.statesArr[index]["name"]! as? String
                                }
                            }
                        }else
                        {
                            self.txt_city.text = placemark.addressDictionary!["SubAdministrativeArea"] as? String
                            self.txt_state.text = placemark.addressDictionary!["State"] as? String
                        }
                    }
                }
            }
        }
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        DispatchQueue.main.async {
//            if textField == self.txt_postalCode {
//                if self.txt_country.text == "United States"
//                {
//                     self.geocoder.geocodeAddressString(self.txt_postalCode.text!) { (placemarks, error) in
//                        if (error != nil){
//
//                            //Show alert Message
//                            let alertMessage = UIAlertController(title: "MYS", message:"Location not available for this zip code.", preferredStyle: .alert)
//                            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//                                self.txt_postalCode.text = ""
//                            }
//                            alertMessage.addAction(action)
//                            self.present(alertMessage, animated: true, completion: nil)
//
//                        }else{
//
//                            let placemark = placemarks?[0] ?? CLPlacemark()
//                            let cityname = placemark.locality
//                            for (index,dict) in self.arrCityName.enumerated() {
//                                if dict.contains(cityname ?? "") {
//                                    self.selectedCity =  self.citiesArr[index]["id"]! as! String
//                                    self.txt_city.text = self.citiesArr[index]["name"]! as? String
//                                    self.stateId = (self.citiesArr[index]["state_id"]! as? String)!
//                                }
//                            }
//
//                            for (index,dict) in self.statesArr.enumerated() {
//                                if (String(describing: dict["id"]!) == self.stateId) {
//                                    self.selectedState =  self.statesArr[index]["id"]! as! String
//                                    self.txt_state.text = self.statesArr[index]["name"]! as? String
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

    
    
}
