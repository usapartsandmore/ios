//
//  MyOrderVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 09/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class CustomeOrderCell: UITableViewCell {
    
    @IBOutlet var lbl_time: UILabel!
    @IBOutlet var lbl_date: UILabel!
    @IBOutlet var lbl_totalPrice: UILabel!
    @IBOutlet var lbl_item: UILabel!
    @IBOutlet var lbl_orderId: UILabel!
}
class MyOrderVC: UIViewController, UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    
    @IBOutlet var ordertableview: UITableView!
    @IBOutlet var verticalSpacefromTopview: NSLayoutConstraint!
    @IBOutlet var Btn_topSearch: UIButton!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var btn_complete: UIButton!
    @IBOutlet var btn_pending: UIButton!
    @IBOutlet var btn_allOrder: UIButton!
    @IBOutlet var viewSelector3: UIView!
    @IBOutlet var viewSelector2: UIView!
    @IBOutlet var viewSelector1: UIView!
    @IBOutlet var btnTopRight: UIButton!
  
    var filteredData = [[String:String]]()
    var webServiceData = [String:Any]()
    var allOrderData = [[String:String]]()
    var pendingData = [[String:String]]()
    var completedData = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.verticalSpacefromTopview.constant = 0.0
        self.searchBar.tag = 0
        self.searchBar.isHidden = true
        btn_allOrder.isSelected = true
        btn_pending.isSelected = false
        btn_complete.isSelected = false
        viewSelector2.backgroundColor = Whitecolor
        viewSelector3.backgroundColor = Whitecolor
        viewSelector1.backgroundColor = Greencolor
        btn_allOrder.setTitleColor(Greencolor, for: UIControlState.selected)
        self.fetchAllOrderListWebServices ()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: FETCH ORDER DATA WEB SERVICES
    @IBAction func btnFetchOrderListWebServices (_ sender:UIButton) {
         self.fetchAllOrderListWebServices ()
        filteredData.removeAll()
        if sender == btn_allOrder {
            btn_allOrder.isSelected = true
            btn_pending.isSelected = false
            btn_complete.isSelected = false
            viewSelector1.backgroundColor = Greencolor
            viewSelector2.backgroundColor = Whitecolor
            viewSelector3.backgroundColor = Whitecolor
            filteredData = allOrderData
        }
        else if sender == btn_pending{
            btn_allOrder.isSelected = false
            btn_pending.isSelected = true
            btn_complete.isSelected = false
            viewSelector1.backgroundColor = Whitecolor
            viewSelector2.backgroundColor = Greencolor
            viewSelector3.backgroundColor = Whitecolor
            filteredData = pendingData
        }
        else if sender == btn_complete {
            btn_allOrder.isSelected = false
            btn_pending.isSelected = false
            btn_complete.isSelected = true
            viewSelector1.backgroundColor = Whitecolor
            viewSelector2.backgroundColor = Whitecolor
            viewSelector3.backgroundColor = Greencolor
            filteredData = completedData
        }
        sender.setTitleColor(Greencolor, for: UIControlState.selected)
        sender.setTitleColor(Graycolor, for: UIControlState.normal)
        self.ordertableview.reloadData()
    }

    @IBAction func btnSearchClick (_ sender: UIButton) {
        if self.searchBar.tag == 0
        {
            self.verticalSpacefromTopview.constant = 55.0
            self.searchBar.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchBar.tag = 1
            self.Btn_topSearch.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
            self.verticalSpacefromTopview.constant = 0.0
            self.searchBar.text = ""
            self.searchBar.isHidden = true
            self.searchBar.endEditing(true)
            self.Btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchBar.tag = 0
            
            if btn_allOrder.isSelected {
                allOrderData = filteredData
            }
            else if btn_pending.isSelected {
                pendingData = filteredData
            }
            else {
                completedData = filteredData
            }
            ordertableview.reloadData()
        }
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
         sideMenuVC.toggleMenu()
     }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if btn_allOrder.isSelected {
            return  self.allOrderData.count
        }else if btn_pending.isSelected {
            return self.pendingData.count
        }else {
            return self.completedData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomeOrderCell", for: indexPath) as! CustomeOrderCell
        cell.selectionStyle = .none
        let dateFormatter = DateFormatter()
        
        if btn_allOrder.isSelected {
            cell.lbl_orderId.text =  self.allOrderData[indexPath.row]["id"]
            cell.lbl_item.text = self.allOrderData[indexPath.row]["total_item"]
            
            //Convert in date format
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: self.allOrderData[indexPath.row]["date"] ?? "")
            dateFormatter.dateFormat = "MMM dd, yyyy"
            cell.lbl_date.text =  dateFormatter.string(from: myDate!)
            
            cell.lbl_time.text = self.allOrderData[indexPath.row]["time"]
            cell.lbl_totalPrice.text = "$\(self.allOrderData[indexPath.row]["amount"] ?? "")"
        }
        else if btn_pending.isSelected {
            cell.lbl_orderId.text =  self.pendingData[indexPath.row]["id"]
            cell.lbl_item.text = self.pendingData[indexPath.row]["total_item"]
            
            //Convert in date format
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: self.pendingData[indexPath.row]["date"] ?? "")
            dateFormatter.dateFormat = "MMM dd, yyyy"
            cell.lbl_date.text =  dateFormatter.string(from: myDate!)
            
            cell.lbl_time.text = self.pendingData[indexPath.row]["time"]
            cell.lbl_totalPrice.text = "$\(self.pendingData[indexPath.row]["amount"] ?? "")"
        }
        else {
            cell.lbl_orderId.text =  self.completedData[indexPath.row]["id"]
            cell.lbl_item.text = self.completedData[indexPath.row]["total_item"]
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: self.completedData[indexPath.row]["date"] ?? "")
            dateFormatter.dateFormat = "MMM dd, yyyy"
            cell.lbl_date.text =  dateFormatter.string(from: myDate!)
            
            cell.lbl_time.text = self.completedData[indexPath.row]["time"]
            cell.lbl_totalPrice.text = "$\(self.completedData[indexPath.row]["amount"] ?? "")"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var orderaId = ""
        var orderDate = ""
        var orderTime = ""
        var isfromComplete = Bool()
        if btn_allOrder.isSelected {
            orderaId = (self.allOrderData[indexPath.row]["id"])!
            orderDate = (self.allOrderData[indexPath.row]["date"])!
            orderTime = (self.allOrderData[indexPath.row]["time"])!
            isfromComplete = false
        }
        else if btn_pending.isSelected {
            orderaId = (self.pendingData[indexPath.row]["id"])!
            orderDate = (self.pendingData[indexPath.row]["date"])!
            orderTime = (self.pendingData[indexPath.row]["time"])!
            isfromComplete = false
        }
        else{
            orderaId = (self.completedData[indexPath.row]["id"])!
            orderDate = (self.completedData[indexPath.row]["date"])!
            orderTime = (self.completedData[indexPath.row]["time"])!
            isfromComplete = true
        }
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"MyDetailOrderBoxVc") as! MyDetailOrderBoxVc
        controller.order_ID = orderaId
        controller.order_date = orderDate
        controller.order_time = orderTime
        controller.isfromComplete = isfromComplete
        controller.push()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            //searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            if btn_allOrder.isSelected {
                allOrderData = filteredData
            }
            else if btn_pending.isSelected {
                pendingData = filteredData
            }
            else {
                completedData = filteredData
            }
        }
        else
        {
            if btn_allOrder.isSelected {
                let arr = filteredData.filter({ (fruit) -> Bool in
                    return (fruit["id"] as AnyObject).lowercased.contains(searchText.lowercased())
                })
                
                self.allOrderData = arr
            }
            else if btn_pending.isSelected {
                let arr = filteredData.filter({ (fruit) -> Bool in
                    return (fruit["id"] as AnyObject).lowercased.contains(searchText.lowercased())
                })
                
                self.pendingData = arr
            }
            else {
                let arr = filteredData.filter({ (fruit) -> Bool in
                    return (fruit["id"] as AnyObject).lowercased.contains(searchText.lowercased())
                })
                
                self.completedData = arr
            }
        }
        ordertableview.reloadData()
    }
    
    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        filteredData.removeAll()
//        
//        if searchText.isEmpty {
//            //searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
//            self.filteredData = webServiceData
//        }
//        else
//        {
//            if searchText.length >= 3
//            {
//                let arr = webServiceData.filter({ (fruit) -> Bool in
//                    return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
//                })
//                filteredData = arr
//            }
//        }
//       ordertableview.reloadData()
//    }

    
    func fetchAllOrderListWebServices()  {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"customer_order", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.webServiceData = responce.value(forKey: "data") as! [String:Any]
                self.allOrderData = self.webServiceData["all"]  as! [[String : String]]
                self.filteredData = self.allOrderData
                self.pendingData = self.webServiceData["panding"]  as! [[String:String]]
                self.completedData = self.webServiceData["completed"]  as! [[String:String]]
                self.ordertableview.reloadData()
            }
        }
    }

}
