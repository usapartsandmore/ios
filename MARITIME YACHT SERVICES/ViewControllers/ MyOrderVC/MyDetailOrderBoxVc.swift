//
//  MyDetailOrderBoxVc.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 29/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

let Greencolor = #colorLiteral(red: 0, green: 0.669770658, blue: 0.7918640375, alpha: 1)
let Whitecolor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
let Graycolor = #colorLiteral(red: 0.4078193307, green: 0.4078193307, blue: 0.4078193307, alpha: 1)

class customeOrderProductBoxCell: UICollectionViewCell {
    @IBOutlet var img_logo: UIImageView!
    @IBOutlet var btnForCheckmarkOrder: UIButton!
    @IBOutlet weak var lbl_PName: UILabel!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet var rating_product: FloatRatingView!
    @IBOutlet var img_orderCheck: UIImageView!
    @IBOutlet var lbl_unitPrice: UILabel!
    @IBOutlet var lbl_totalPrice: UILabel!
    @IBOutlet var lbl_qty: UILabel!
    
    override func awakeFromNib() {
        btnForCheckmarkOrder.isHidden = true
    }
    
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            if newValue{
                btnForCheckmarkOrder.setImage(UIImage(named: "myorderconfirm"), for: .normal)
            }else{
                btnForCheckmarkOrder.setImage(UIImage(named: "myordernonconfirm"), for: .normal)
            }
        }
        get{
            return super.isSelected
        }
    }
}

class MyDetailOrderBoxVc: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet var verticalSpaceToCollectionView: NSLayoutConstraint!
    @IBOutlet var btn_markAsReceived: UIButton!
    @IBOutlet var btn_confirmItemReceived: UIButton!
    
    @IBOutlet var lbl_orderId: UILabel!
    @IBOutlet var lbl_orderTime: UILabel!
    @IBOutlet var lbl_orderDate: UILabel!
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchCollectionView: UICollectionView!
    @IBOutlet var btnTopRightSearch: UIButton!
    @IBOutlet var productBoxCollectionView: UICollectionView!
    var order_ID = ""
    var order_time = ""
    var order_date = ""
    var orderProduct_ID = ""
    var webServiceData = [[String:Any]]()
    var filteredData = [[String:Any]]()
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    var orderListData = [[String:String]]()
    var isfromComplete = Bool()
    var order_Status = ""
    var alreadyMarked = ""
    @IBOutlet weak var btnMarkHeightConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchOrderListWebServices ()
        productBoxCollectionView?.allowsMultipleSelection = true
        verticalSpaceToCollectionView.constant = 67.0
        btn_confirmItemReceived.tag = 0
        btn_confirmItemReceived.isHidden = false
        
        btn_markAsReceived.isHidden = true
        btnMarkHeightConst.constant = 0.0
        
        self.searchView.tag = 0
        self.searchView.isHidden = true
        
        //if isfromComplete == false{
        //self.btn_confirmItemReceived.isUserInteractionEnabled = false
        //}else{
        //self.btn_confirmItemReceived.isUserInteractionEnabled = true
        //}
    }
    
    //MARK: CONFIRM ITEM RECEIVED BUTTON CLICK
    @IBAction func btnConfirmItemReceivedClick(_ sender: UIButton) {
            btn_markAsReceived.isHidden = false
            btnMarkHeightConst.constant = 45.0
            verticalSpaceToCollectionView.constant = 10.0
            btn_confirmItemReceived.tag = 1
            productBoxCollectionView.reloadData()
     }
    
    //MARK: MARK AS RECEIVED BUTTON CLICK
    @IBAction func btnMarkAsReceivedClick(_ sender: UIButton) {
        self.markAsReceivedWebServiceCall ()
    }
    
    //MARK:SEARCH BUTTON CLICK
    @IBAction func btnSearchClick(_ sender: UIButton) {
        if self.searchView.tag == 0
        {
            self.searchView.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchView.tag = 1
            self.btnTopRightSearch.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
            self.searchBar.text = ""
            filteredData.removeAll()
            
            self.searchView.isHidden = true
            self.searchBar.endEditing(true)
            self.btnTopRightSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchView.tag = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func AddToCartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.orderListData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customeOrderProductBoxCell", for: indexPath) as! customeOrderProductBoxCell
        if btn_confirmItemReceived.tag == 1 {
            if self.orderListData[indexPath.row]["received"] ?? "" == "0"
            {
                cell.btnForCheckmarkOrder.isHidden = false
            }else{
                cell.btnForCheckmarkOrder.isHidden = true
            }
        }
        
        cell.contentView.layer.cornerRadius = 1.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        cell.contentView.layer.borderWidth = 0.5
        //cell.img_orderCheck.setImage(UIImage(named: ""), for: UIControlState.normal)
        cell.lbl_PName.text = self.orderListData[indexPath.row]["name"] ?? ""
        cell.rating_product.rating = (self.orderListData[indexPath.row]["rating"]! as NSString).floatValue
        cell.lbl_qty.text = "x\(self.orderListData[indexPath.row]["qty"] ?? "")"
        cell.lbl_totalPrice.text = "$\(self.orderListData[indexPath.row]["totalPrice"] ?? "")"
        cell.lbl_unitPrice.text =  "$\(self.orderListData[indexPath.row]["price"] ?? "")"
        let strImg = self.orderListData[indexPath.row]["image"]!
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: strImg))")!
        cell.img_Product.af_setImage(withURL: url as URL)
        let strLogo = self.orderListData[indexPath.row]["logo"]!
        let url1:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo))")!
        cell.img_logo.af_setImage(withURL: url1 as URL)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var itemType = Bool()
        if (self.orderListData[indexPath.row]["type"])! == "1"{
            itemType = false
        }else{
            itemType = true
        }
        if btn_confirmItemReceived.tag != 1 {
            
            self.orderProduct_ID = (self.orderListData[indexPath.row]["order_product_id"])!
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"OrderDetailVC") as! OrderDetailVC
            controller.orderProductId = self.orderProduct_ID
            controller.orderServiceId = (self.orderListData[indexPath.row]["user_services_id"])!
            controller.order_id = self.order_ID
            controller.order_Date = self.order_date
            controller.order_Time = self.order_time
            controller.isServiceItem = itemType
            controller.push()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.size.width - 26) / 2
        let height = width * 1.3
        return CGSize(width: width, height: height);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData.removeAll()
        
        if searchText.isEmpty {
            
        }else{
            
        }
    }
    
    //MARK: WEB SERVICE CALL
    func fetchOrderListWebServices() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "order_id":order_ID,
                    "page":-1]
        
        webService_obj.fetchDataFromServer(header:"order_product_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.orderListData = responce.value(forKey: "data") as! [[String:String]]
                self.order_Status = responce.value(forKey: "order_status") as! String
                self.alreadyMarked = responce.value(forKey: "allReceived") as! String
                
                
                
                if self.order_Status == "Completed" || self.order_Status == "Shipped" {
                    
                    if self.alreadyMarked == "1"{
                        self.btnMarkHeightConst.constant = -1.0
                        self.verticalSpaceToCollectionView.constant = 10.0
                    }else{
                        self.btnMarkHeightConst.constant = 45.0
                        self.verticalSpaceToCollectionView.constant = 67.0
                    }
                 }else {
                    self.btnMarkHeightConst.constant = -1.0
                    self.verticalSpaceToCollectionView.constant = 10.0
                }
                
                
                self.lbl_orderId.text = self.order_ID
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let myDate = dateFormatter.date(from: self.order_date)
                dateFormatter.dateFormat = "MMM dd, yyyy"
                self.lbl_orderDate.text =  dateFormatter.string(from: myDate!)
                
                self.lbl_orderTime.text = self.order_time
                self.productBoxCollectionView.reloadData()
            }
        }
    }
    
    func markAsReceivedWebServiceCall() {
        let item = self.productBoxCollectionView.indexPathsForSelectedItems
        
        var data = [String]()
        for indexPath in item!
        {
            let dict = self.orderListData[indexPath.row]
            let  orderProductID = dict["order_product_id"]
            data.append(orderProductID!)
        }
        if data.count > 0{
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "order_id":order_ID,
                        "order_product_id": data]
            
            webService_obj.fetchDataFromServer(header:"order_received", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    
                    let data = responce.value(forKey: "html") as? String ?? ""
                    
                    //Show alert Message
                    let alertController = UIAlertController(title: "Alert", message: "", preferredStyle: .alert)
                    do {
                        let str = try NSAttributedString(data:(data).data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                        
                        alertController.setValue(str,forKey: "attributedMessage")
                        let action1 = UIAlertAction(title: "OK", style: .default) { (action) in
                            print("\(String(describing: action.title))")
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(action1)
                        alertController.view.layer.cornerRadius = 40
                        self.present(alertController, animated: true, completion: nil)
                    } catch {
                        print(error)
                    }
                }
            }
        }else{
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Please select product.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
}
