//
//  TrackOrderVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 09/05/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class TrackOrderVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView:UIWebView!
    @IBOutlet weak var back_btn:UIButton!
    
    var track_URL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        self.webView.scalesPageToFit = true
        self.webView.contentMode = UIViewContentMode.scaleAspectFit
        
        let  url = URL (string:track_URL)
        self.webView.loadRequest(URLRequest(url: url!))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    func webViewDidStartLoad(_ webView : UIWebView) {
        load.show(views: self.view)
        
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        load.hide(delegate:self)
        load.hide(delegate:self)
        load.hide(delegate:self)
    }
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        load.hide(delegate: self)
        load.hide(delegate:self)
        load.hide(delegate:self)
    }
    
}

