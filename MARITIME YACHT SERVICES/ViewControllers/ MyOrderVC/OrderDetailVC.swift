//
//  OrderDetailVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 21/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class myOrderSliderCell: UICollectionViewCell {
    @IBOutlet weak var img_Product: UIImageView!
}

class OrderDetailVC: UIViewController , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , UICollectionViewDataSource, UIScrollViewDelegate,UITextViewDelegate {
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var imageCollectionView: UICollectionView!
    @IBOutlet var lbl_productName: UILabel!
    @IBOutlet var lbl_productNumber: UILabel!
    @IBOutlet var rating_vendor: FloatRatingView!
    @IBOutlet var img_vendorLogo: UIImageView!
    @IBOutlet var lbl_orderTotalAmount: UILabel!
    @IBOutlet var lbl_orderQty: UILabel!
    @IBOutlet var lbl_orderShippingMethod: UILabel!
    @IBOutlet var lbl_orderStatus: UILabel!
    @IBOutlet var lbl_orderId: UILabel!
    @IBOutlet var lbl_orderDate: UILabel!
    @IBOutlet weak var lblProductOrService: UILabel!
    @IBOutlet weak var btn_viewServiceDetail: UIButton!
    
    @IBOutlet var hidden_view_rating: UIView!
    @IBOutlet var rating_Product: FloatRatingView!
    @IBOutlet var rateCmnt_txtView: UITextView!
    
    @IBOutlet weak var blabk_background_view: UIView!
    @IBOutlet weak var refundPopUp: UIView!
    @IBOutlet var btn_Hide_PopUp: UIButton!
    @IBOutlet var btn_CancelReson: UIButton!
    @IBOutlet var txt_cancelReson: UITextView!
    
    @IBOutlet var btn_Track_Order: UIButton!
    @IBOutlet var btn_Cancel_Order: UIButton!
    @IBOutlet var btn_Cancel_Hide: UIButton!
    
    
    
    var orderDetailData = [String:Any]()
    var defaultImgData = [[String:String]]()
    var orderProductId = ""
    var orderServiceId = ""
    var order_id = ""
    var order_Date = ""
    var order_Time = ""
    var isServiceItem = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isServiceItem == true{
            self.lblProductOrService.text = "Service"
            self.btn_viewServiceDetail.isHidden = false
            self.lbl_orderShippingMethod.isHidden = true
        }else{
            self.lblProductOrService.text = "Product"
            self.btn_viewServiceDetail.isHidden = true
            self.lbl_orderShippingMethod.isHidden = false
        }
        
        self.blabk_background_view.isHidden = true
        self.refundPopUp.isHidden = true
        
        self.btn_Cancel_Hide.isHidden = true
        self.fetchOrderDetailWebService ()
        // Do any additional setup after loading the view.
        
        txt_cancelReson.addPaddingView(width: 10, onSide: 0)
        txt_cancelReson.text = "Why you want to cancel this order?"
        txt_cancelReson.textColor = UIColor.lightGray
        
        rateCmnt_txtView.addPaddingView(width: 10, onSide: 0)
        rateCmnt_txtView.text = "Please enter review"
        rateCmnt_txtView.textColor = UIColor.lightGray
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: WEB SERVICE METHOD
    func fetchOrderDetailWebService() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "order_product_id": orderProductId]
        
        webService_obj.fetchDataFromServer(header:"order_product_detail", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.orderDetailData = responce.value(forKey:"data") as! [String:Any]
                self.defaultImgData = self.orderDetailData["image"]  as? [[String:String]] ?? []
                
                self.lbl_productName.text = self.orderDetailData["name"] as? String
                
                if self.isServiceItem{
                    self.lbl_productNumber.text = "#\(self.orderServiceId)"
                }else{
                self.lbl_productNumber.text = "#\(self.orderDetailData["product_id"] as? String ?? "")"
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let myDate = dateFormatter.date(from: self.order_Date)
                dateFormatter.dateFormat = "MMM dd, yyyy"
                let mDate =  dateFormatter.string(from: myDate!)
                
                if self.orderDetailData["is_already_rating"] as? String == "1"
                {
                    self.hidden_view_rating.isHidden = true
                }else{
                    if self.orderDetailData["order_status"] as? String == "Cancelled" || self.orderDetailData["order_status"] as? String == "Needs to be returned"
                    {
                        self.hidden_view_rating.isHidden = true
                    }else{
                        self.hidden_view_rating.isHidden = false
                    }
                 }
 
                if self.orderDetailData["order_status"] as? String == "Completed" || self.orderDetailData["order_status"] as? String == "Shipped"
                {
                    if self.orderDetailData["tracking_url"] as? String != "" {
                        self.btn_Track_Order.isHidden = false
                        self.btn_Cancel_Order.isHidden = false
                        self.btn_Cancel_Order.setTitle("Return Request", for: .normal)
                    }else{
                        self.btn_Cancel_Order.setTitle("Return Request", for: .normal)
                        self.btn_Track_Order.isHidden = true
                        self.btn_Cancel_Order.isHidden = true
                        self.btn_Cancel_Hide.isHidden = false
                        self.btn_Cancel_Hide.setTitle("Return Request", for: .normal)
                    }
                  }
                else if self.orderDetailData["tracking_url"] as? String != "" {
                    
                    if self.orderDetailData["order_status"] as? String == "Cancelled" || self.orderDetailData["order_status"] as? String == "Needs to be returned"{
                         self.btn_Cancel_Order.isHidden = true
                        self.btn_Track_Order.isHidden = true
                     }else{
                        self.btn_Track_Order.isHidden = false
                        self.btn_Cancel_Order.isHidden = false
                        self.btn_Cancel_Order.setTitle("Cancel Request", for: .normal)
                    }
                 }else if self.orderDetailData["tracking_url"] as? String == "" {
                    self.btn_Track_Order.isHidden = true
                    
                    if self.orderDetailData["order_status"] as? String == "Cancelled" || self.orderDetailData["order_status"] as? String == "Needs to be returned"{
                        self.btn_Cancel_Order.isHidden = true
                        self.btn_Cancel_Hide.isHidden = true
                    }else{
                        self.btn_Cancel_Order.setTitle("Cancel Request", for: .normal)
                        self.btn_Cancel_Order.isHidden = true
                        self.btn_Cancel_Hide.isHidden = false
                        self.btn_Cancel_Hide.setTitle("Cancel Request", for: .normal)
                    }
                 }else{
                    
                }
 
                self.lbl_orderDate.text = "\(mDate)  \(self.order_Time)"
                self.lbl_orderId.text = self.order_id
                self.lbl_orderStatus.text = self.orderDetailData["order_status"] as? String
                self.lbl_orderShippingMethod.text = self.orderDetailData["shipping_method"] as? String
                self.lbl_orderQty.text = "x\(String(describing: self.orderDetailData["qty"] as! String))"
                self.lbl_orderTotalAmount.text = "$\(self.orderDetailData["totalPrice"] ?? "")"
                self.rating_vendor.rating =  (self.orderDetailData["rating"]! as! NSString).floatValue
                
                let strLogo = self.orderDetailData["logo"] as? String ?? ""
                let urlLogo:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo))")!
                self.img_vendorLogo.af_setImage(withURL: urlLogo as URL)
                
                self.pageControl.numberOfPages = self.defaultImgData.count
                self.pageControl.currentPage = 0
            }
            
            self.imageCollectionView.reloadData()
        }
    }
    
    @IBAction func btn_viewServiceDetailClicked(_ sender: Any) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
        controller.localOffer_id = orderServiceId
        controller.isfromMyOrders = true
        controller.push()
    }
    
    
    @IBAction func btnForProductDetail(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"MyDetailOrderBoxVc") as! MyDetailOrderBoxVc
        controller.push()
    }
    
    @IBAction func btnRateProductBtnClick(_ sender: UIButton) {
        if self.rating_Product.rating == 0.0
        {
            AlertMessage("Please select rating.")
        }else{
            if rateCmnt_txtView.textColor == UIColor.lightGray
            {self.rateCmnt_txtView.text = ""}
            
            if self.isServiceItem{
                PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                            "user_id": self.orderDetailData["user_id"] as? String ?? "",
                            "overall_grade":self.rating_Product.rating,
                            "service_performed": "",
                            "what_provider_do":"",
                            "how_it_go": rateCmnt_txtView.text ?? "",
                            "service_date": "",
                            "price": "",
                            "use_again":"",
                            "price_rate": "",
                            "quality_rate": "",
                            "responsive_rate": "",
                            "punctuality_rate":"",
                            "professionalism_rate": ""]
                
                webService_obj.fetchDataFromServer(header:"service_review", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                    if staus{
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                            self.rateCmnt_txtView.text = "Please enter review"
                            self.rateCmnt_txtView.textColor = UIColor.lightGray
                            self.rating_Product.rating = 0.0
                            self.hidden_view_rating.isHidden = true
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                }
            }else{
                PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                            "product_id":self.orderDetailData["product_id"] as? String ?? "",
                            "comment":rateCmnt_txtView.text ?? "",
                            "rating":self.rating_Product.rating]
                
                webService_obj.fetchDataFromServer(header:"customer_product_rating", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                    if staus{
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                            self.rateCmnt_txtView.text = "Please enter review"
                            self.rateCmnt_txtView.textColor = UIColor.lightGray
                            self.rating_Product.rating = 0.0
                            self.hidden_view_rating.isHidden = true
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    // MARK: - Track Order Methods
    @IBAction func btnTrackProductClick(_ sender: UIButton) {
        if self.orderDetailData["tracking_url"] as? String == ""
        {
            AlertMessage("Track facility is not available for this order.")
        }else{
            let st = UIStoryboard(name: "Main", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"TrackOrderVC") as! TrackOrderVC
            controller.track_URL = self.orderDetailData["tracking_url"] as! String
            controller.push()
        }
    }
    
    // MARK: - Cancel Order Methods
    @IBAction func btnCancelProductClick(_ sender: UIButton) {
        
        self.blabk_background_view.isHidden = false
        self.refundPopUp.isHidden = false
     }
    
    // MARK: - Pop Up Cancel Order Methods
    @IBAction func btnPopUPCancelClick(_ sender: UIButton) {
        self.txt_cancelReson.text = ""
        self.blabk_background_view.isHidden = true
        self.refundPopUp.isHidden = true
        self.view.endEditing(true)
    }
    
    // MARK: - Pop Up Confirm Order Methods
    @IBAction func btnPopUpConfirmClick(_ sender: UIButton) {
        if txt_cancelReson.textColor == UIColor.lightGray || txt_cancelReson.text == ""{
            AlertMessage("Please enter a reason for cancel order.")
        }else{
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "order_id":self.order_id,
                        "cancel_reason":self.txt_cancelReson.text!]
            
            var apitext = ""
            
            if btn_Cancel_Order.titleLabel?.text == "Return Request"{
                apitext = "order_need_to_return"
            }else{
                 apitext = "order_cancel"
            }
            
            webService_obj.fetchDataFromServer(header:apitext, withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//                        self.blabk_background_view.isHidden = true
//                        self.refundPopUp.isHidden = true
//                         self.view.endEditing(true)
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - TextView Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            if textView == rateCmnt_txtView {
            textView.text = "Please enter review"
            }else{
                textView.text = "Why you want to cancel this order?"
            }
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func AddToCartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    //MARK: UICollectionViewDataSource
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.defaultImgData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myOrderSliderCell", for: indexPath) as! myOrderSliderCell
        
        let iData = self.defaultImgData[indexPath.row]
        
        if iData["image"] == ""
        {
            cell.img_Product.image = UIImage(named:"noimage")
        }else{
            let str = iData["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_Product.af_setImage(withURL: url as URL)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.bounds.size
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
}
