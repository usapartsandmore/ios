//
//  ManageInventoryMainVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 10/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class ManageInventoryCell: UITableViewCell {
    @IBOutlet var imgviewCellbackground: UIImageView!
    @IBOutlet var imageviewCellDeck: UIImageView!
    @IBOutlet var lblDeckName: UILabel!
    
}
class ManageInventoryMainVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BtnAddClick(_ sender: UIButton) {
    }
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageInventoryCell", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
 
}
