//
//  FilterCategoryVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 27/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class FIlterCustomCell: UITableViewCell {
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if isSelected == false {
            accessoryType = UITableViewCellAccessoryType.none
        }
        else {
            accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
}

class FilterCategoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
 
    var block: (([[String : String]])->Void)?
    var selectedRows = [[String:String]]()
    var categoryData = [[String:String]]()
    var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getCategoriesWebServiceMethod()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
 }
        
        
    @IBAction func gotoBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true);
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "FIlterCustomCell", for: indexPath) as! FIlterCustomCell
        let data = self.categoryData[indexPath.row]
        
        cell.textLabel?.text=data["name"]        
        return cell
    }
    
    @IBAction func btnDoneClick(_ sender: Any) {
        if let rows = tableView.indexPathsForSelectedRows{
            var data = [[String:String]]()
            for indexPath in rows
            {
                let dict = self.categoryData[indexPath.row]
                data.append(dict)
            }
            block?(data)
            self.navigationController?.popViewController(animated: true)
        }else
        {
            block?([])
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    // MARK: - Call WebServices
    func getCategoriesWebServiceMethod () {
        webService_obj.fetchDataFromServer(header:"productCatWioutImage", withParameter: [:], inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.categoryData = responce.value(forKey: "data") as! [[String:String]]
                self.tableView.reloadData()
                
                let ids = self.selectedRows.map{ ($0["id"]! ) }
 
                var indexPaths = [IndexPath]()
                
                for (index, dict) in self.categoryData.enumerated(){
                    let id = dict["id"]!
                    if ids.contains(id){
                        indexPaths.append(IndexPath(row: index, section: 0))
                    }
                }
                
                indexPaths.forEach { (indexPath) in
                    self.tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }

                
             }
        }
    }

}
