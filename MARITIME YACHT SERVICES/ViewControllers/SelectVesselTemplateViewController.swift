//
//  SelectVesselTemplateViewController.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac114 on 26/10/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import SDWebImage

class SelectVesselTemplateViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableViewList: UITableView!
    
    //MARK: Constants
    let labelTagInHeader = 100
    let imageViewTagInHeader = 500
    
    let lableTagInCell = 200
    let imageViewTagInCell = 600
    
    
    //MARK: Properties
    var arrTemplatesSections : NSMutableArray? = NSMutableArray()
    
    //MARK: ViewController Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableViewList.dataSource = self
        tableViewList.delegate = self
        
        //get all templates from server
        getAllTemplates()
        
    }

    
    //MARK: Actions
    @IBAction func showSideMenu(_ sender: UIButton) {
        
        sideMenuVC.toggleMenu()
    }
    
    
    //MARK: TableviewDatasource and Delegate methods
    //sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return (arrTemplatesSections?.count)!
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "templateHeaderCell") as! UITableViewCell
        
        let headerLbl = headerCell.contentView.viewWithTag(labelTagInHeader) as! UILabel
        let headerImgView = headerCell.contentView.viewWithTag(imageViewTagInHeader) as! UIImageView
        
        let templateDict =  arrTemplatesSections?.object(at: section) as! NSDictionary
        headerLbl.text = templateDict.object(forKey: "name") as! String
        
        let imgSubUrl = templateDict.object(forKey: "image") as! String
        //using alomafire
        let imgUrl:NSURL = NSURL(string :"\(imageURL)\(imgSubUrl)")!
        
        headerImgView.sd_setImage(with: imgUrl as URL, placeholderImage: nil, options: SDWebImageOptions(rawValue: 0)) { (img, err, type, url) in
            
            headerImgView.image = img
            
        }
        
        return headerCell.contentView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    
    //cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRows = 0
        
        let templateDict = arrTemplatesSections?.object(at: section) as! NSDictionary
        let arrBoats = templateDict.object(forKey: "template_list") as! NSArray
        
        return arrBoats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "templateContentCell") as! UITableViewCell
        
        let cellLbl = cell.contentView.viewWithTag(lableTagInCell) as! UILabel
        let cellImgView = cell.contentView.viewWithTag(imageViewTagInCell) as! UIImageView
        
        cellImgView.isHidden = true
        
        let templateDict = arrTemplatesSections?.object(at: indexPath.section) as! NSDictionary
        let arrBoats = templateDict.object(forKey: "template_list") as! NSArray
        
        let cellDict = arrBoats.object(at: indexPath.row) as! NSDictionary
        let cellText = cellDict.object(forKey: "name") as! String
        
        let countStr = cellDict.object(forKey:"total_deck") as! String
        if let counts = Int(countStr) {
        
            if counts > 0 {
                //show detail arrow
                
                cellImgView.isHidden = false
            }
        
        }
        
        cellLbl.text = cellText
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //get count of deck for selected row
        let templateDict = arrTemplatesSections?.object(at: indexPath.section) as! NSDictionary
        let arrBoats = templateDict.object(forKey: "template_list") as! NSArray
        
        let cellDict = arrBoats.object(at: indexPath.row) as! NSDictionary
        let deckCount = Int(cellDict.object(forKey: "total_deck") as! String)!
        
        guard deckCount > 0 else {
            return
        }
        
        //open deck list view
        //Just navigate user to selectionview for deck
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"selectDecksViewController") as! selectDecksViewController
        controller.templateId = cellDict.object(forKey: "id") as? String
        controller.push()
        
    }
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         
    }
 
    
    //MARK: Utility methods
    func getAllTemplates() {
    
        let productionBaseUrl = "http://56.octallabs.com/mys/businessadmin/web_services/"
        let localBaseUrl = "http://192.168.1.181/mys/businessadmin/web_services"
        
        let baseUrl = productionBaseUrl
        let getTemplatesSubUrl = "deck_template_list"
        
        let completeUrl = baseUrl + getTemplatesSubUrl
        
        Config().fetchDataFromServerForBothUrl(completeUrl: completeUrl, withParameter: [:], inVC: self, showHud: true) { (responseDict, msg, status) in
            
            if status {
            
                //getting templates
               self.arrTemplatesSections = NSMutableArray(array: responseDict.object(forKey: "data") as! NSArray)
                
                DispatchQueue.main.async {
                    
                    self.tableViewList.reloadData()
                    
                }
                
            }
            
        }
    }
    
}



