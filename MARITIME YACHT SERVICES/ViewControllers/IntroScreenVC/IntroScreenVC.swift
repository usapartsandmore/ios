//
//  IntroScreenVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 03/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class IntroScreenVC: UIViewController {
    
    @IBOutlet var btnFacebookLogin: UIButton!
    @IBOutlet var btnEmailLogin: UIButton!
    @IBOutlet var btnSignIn: UIButton!
    
    var pickedImage: UIImage!
    
    var  fbData = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Button Corner Radius
        btnFacebookLogin.layer.cornerRadius = 5
        btnEmailLogin.layer.cornerRadius = 5
        btnSignIn.layer.cornerRadius = 5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: SignUp Button Method
    @IBAction func btnSignUpPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SignupVC", sender: sender)
    }
    
    // MARK: Login Button Method
    @IBAction func btnLoginPressed(_ sender: UIButton) {
        self.performSegue(withIdentifier: "LoginVC", sender: sender)
    }
    
    // MARK: Facebook SDK Method
    @IBAction func btnFbPressed(_ sender: Any)
    {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) -> Void in
            if error != nil {
                print(error?.localizedDescription ?? "")
                self.getFBUserInfos()
                self.dismiss(animated: true, completion: nil)
            } else if (result?.isCancelled)! {
                print("Cancelled")
                self.dismiss(animated: true, completion: nil)
            }
            else{
                self.getFBUserInfos()
            }
        }
    }
    
    // MARK: - Get User Information login By Facebook
    func getFBUserInfos()
    {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name,email,picture"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                self.loginbyFaceboobk(Details: fbDetails)
            }
        })
    }
    
    // MARK: - CallSocialLoginService
    func loginbyFaceboobk(Details:NSDictionary)
    {
        let pic_Url = ((Details.value(forKey: "picture") as AnyObject).value(forKey: "data") as AnyObject).value(forKey: "url") as! String
        print("pic urlllll",pic_Url)
        
        //Use image's path to create NSData
        let url:NSURL = NSURL(string : pic_Url)!
        //Now use image to create into NSData format
        let imageData:NSData = NSData.init(contentsOf: url as URL)!
        pickedImage = UIImage(data: imageData as Data)
        
        
        PostData = ["fb_id":(Details.value(forKey: "id") as? String)!,
                    "email":(Details.value(forKey: "email") as? String)!,
                    "type":"fb"]
        
        fbData =  ["fb_id":(Details.value(forKey: "id") as? String)!,
                   "email":(Details.value(forKey: "email") as? String)!,
                   "name":(Details.value(forKey: "first_name") as? String)!]
        
        webService_obj.fetchDataFromServer(header:"existEmail", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                let data = responce.value(forKey: "data") as! NSDictionary
                let isexist = data.value(forKey: "isExist")
                let isverified = data.value(forKey: "isFbEmailVerified")
                
                if (isexist! as AnyObject).doubleValue == 1.0
                {
                    if (isverified! as AnyObject).doubleValue == 1.0
                    {
                        
                        //Save User Data
                        defaults.set(true, forKey: kAPPLOGIN_IN)
                        let User_Id = data["customer_id"]
                        webService_obj.Save(User_Id as! String, keyname: "User_Id")
                        
                        let mainVcIntial = kConstantObj.SetIntialMainViewController("HomeVC")
                        appDelegate.window?.rootViewController = mainVcIntial
                    }else{
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:"Please verify your email first, then Login.", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                            
                        }
                        alertMessage.addAction(action)
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
                    vc.isfromFacebook = true
                    vc.facebookData = self.fbData as! [String : String]
                    vc.facebookUserImg = self.pickedImage
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
