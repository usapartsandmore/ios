//
//  ManageInventoryDetailProductVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 14/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import SDWebImage

extension Int {
    var boolValue: Bool {
        if self == 0 {
            return false
        } else {
            return true
        }
    }
}

class ManageInventoryDetailProductVC: UIViewController {
    
    //MARK: Enum define
    enum PinStatus: Float {
        
        case nothing = 0.0
        case havePins = 45.0
        case haveLockers = 95.0
        case haveBoxes = 140.0
        
    }
    
    //MARK:- Properties to be initialized by previous controller
    var dictProductDetail : NSDictionary? = nil
    var deck_id = ""
    var pin_id = ""
    var locker_id = ""
    var box_id = ""
    var isForPin = ""
    var isFromQuickView = ""
    var isFromsearchInventory = Bool()
    var productDetailMain = NSDictionary()
    
    
    //Properties to identify, view is used for add product or just to manageProduct
    var isViewToAddProduct: Bool = true
    
    var isProductAvailable: String? = ""
    //MARK: Enum property
    var currentStatus: PinStatus = .nothing{
        
        didSet {
            
            //manage UI according to currentStatus
            switch currentStatus {
            case .nothing:
                self.consHeightParentViewToMangePinsNChild.constant = CGFloat(currentStatus.rawValue)
                self.viewPins.isHidden = true
                self.viewPinsLockers.isHidden = true
                self.viewPinsBoxes.isHidden = true
                break
            case .havePins:
                self.consHeightParentViewToMangePinsNChild.constant = CGFloat(currentStatus.rawValue)
                self.viewPins.isHidden = false
                self.viewPinsLockers.isHidden = true
                self.viewPinsBoxes.isHidden = true
                break
            case .haveLockers:
                self.consHeightParentViewToMangePinsNChild.constant = CGFloat(currentStatus.rawValue)
                self.viewPins.isHidden = false
                self.viewPinsLockers.isHidden = false
                self.viewPinsBoxes.isHidden = true
                break
            case .haveBoxes:
                self.consHeightParentViewToMangePinsNChild.constant = CGFloat(currentStatus.rawValue)
                self.viewPins.isHidden = false
                self.viewPinsLockers.isHidden = false
                self.viewPinsBoxes.isHidden = false
                break
            }
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK:- Outlet connections
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productVendor: UILabel!
    @IBOutlet weak var productPricePerItem: UILabel!
    @IBOutlet weak var productCurrentStock: UILabel!
    @IBOutlet weak var productMinimumStock: UILabel!
    @IBOutlet weak var productMaximumStock: UILabel!
    @IBOutlet weak var productIDNumber: UILabel!
    @IBOutlet weak var productNotesCount: UILabel!
    @IBOutlet weak var btnRestockOrAdd: UIButton!
    @IBOutlet weak var btnUpdateItems: UIButton!
    
    //MARK:- Outlet- stockManageControls
    @IBOutlet weak var btnMInusStockControlCurrentStock: UIButton!
    @IBOutlet weak var btnPlusStockControlCurrentStock: UIButton!
    @IBOutlet weak var lblStockControlCurrentStock: UILabel!
    
    @IBOutlet weak var btnMinusStockControlMinimumStock: UIButton!
    @IBOutlet weak var btnPlusStockControlMinimumStock: UIButton!
    @IBOutlet weak var lblMinimumStockStockControl: UILabel!
    
    @IBOutlet weak var btnMinusStockControlMaximumStock: UIButton!
    @IBOutlet weak var btnPlusStockControlMaximumStock: UIButton!
    @IBOutlet weak var lblMaximumStockStockControl: UILabel!
    
    //MARK:- Outlet to manage Pins and child
    @IBOutlet weak var consHeightParentViewToMangePinsNChild: NSLayoutConstraint!
    @IBOutlet weak var viewPins: UIView!
    @IBOutlet weak var viewPinsLockers: UIView!
    @IBOutlet weak var viewPinsBoxes: UIView!
    @IBOutlet weak var lblPinTitle: UILabel!
    @IBOutlet weak var lblLockerTitle: UILabel!
    @IBOutlet weak var lblBoxTitle: UILabel!
    @IBOutlet weak var btnCellRestock: UIButton!
    
    var product_id = ""
    var product_varient_id = ""
    var product_name = ""
    
    var ischangePin = Bool()
    
    //MARK:- ViewController LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        ischangePin = false
        //Properties to identify, view is used for add product or just to manageProduct
        if isViewToAddProduct == true {
            self.btnUpdateItems.isHidden = true
        }else{
            self.btnUpdateItems.isHidden = false
        }
        
        // Do any additional setup after loading the view.
        print("\n\n Product Detail \n\n")
        print(dictProductDetail ?? "")
        print("\n\n Deck Id: \(deck_id)")
        print("\n\n Pin Id: \(pin_id)")
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // call webservice to get detail for view
        getProductDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnImageViewClicked(_ sender: Any) {
        let newImageView = UIImageView(image: self.productImage.image)
        newImageView.frame = self.view.frame
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
    
    @IBAction func btnPinclick(_ sender: Any) {
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier: "ChooseRoomVC") as? ChooseRoomVC
        controller?.isfromVC = "pin"
        controller?.deck_id = self.deck_id
        controller?.pin_id = self.pin_id
        controller?.block = { dict in
            if dict.count == 0
            {
             }else
            {
                self.pin_id = dict["id"]!
                self.lblPinTitle.text = dict["name"]!
                 self.lblLockerTitle.text = "Choose Locker"
                self.lblBoxTitle.text = "Choose Box"
                self.locker_id = ""
                self.box_id = ""
                self.ischangePin = true
            }
         }
        controller?.push()
    }
    
    @IBAction func btnLockerclick(_ sender: Any) {
        
        if self.lblPinTitle.text == ""
        {
            self.AlertMessage("Please first select location.")
        }else
        {
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier: "ChooseRoomVC") as? ChooseRoomVC
            controller?.isfromVC = "locker"
            controller?.deck_id = self.deck_id
            controller?.pin_id = self.pin_id
            controller?.locker_id = self.locker_id
            controller?.block = { dict in
                if dict.count == 0
                {
                    self.locker_id = ""
                    self.lblLockerTitle.text = "Choose Locker"
                    
                }else{
                    self.locker_id = dict["id"]!
                    self.lblLockerTitle.text = dict["name"]!
                    self.lblBoxTitle.text = "Choose Box"
                    self.box_id = ""
                    self.ischangePin = true
                }
            }
            controller?.push()
        }
    }
    
    @IBAction func btnBoxesClick(_ sender: Any) {
        if self.lblPinTitle.text == ""
        {
            self.AlertMessage("Please first select location.")
        }
        else if self.lblLockerTitle.text == ""
        {
            self.AlertMessage("Please first select locker.")
        }
        else{
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier: "ChooseRoomVC") as? ChooseRoomVC
            controller?.isfromVC = "boxes"
            controller?.deck_id = self.deck_id
            controller?.pin_id = self.pin_id
            controller?.locker_id = self.locker_id
            controller?.box_id = self.box_id
            controller?.block = { dict in
                
                if dict.count == 0
                {
                    self.box_id = ""
                    self.lblBoxTitle.text = "Choose Box"
                    
                }else{
                    self.box_id = dict["id"]!
                    self.lblBoxTitle.text = dict["name"]!
                    self.ischangePin = true
                }
             }
            controller?.push()
            
        }
    }
    
    
    @IBAction func btnNotesClick(_ sender: Any) {
        if self.lblPinTitle.text == ""
        {
            self.AlertMessage("Please first select location.")
        }
         else
        {
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier: "NotesVC") as? NotesVC
            controller?.deck_id = self.deck_id
            controller?.pin_id = self.pin_id
            controller?.locker_id = self.locker_id
            controller?.box_id = self.box_id
            controller?.product_id = self.product_id
            controller?.product_varient_id = self.product_varient_id
            controller?.product_name = self.product_name
            controller?.push()
        }
    }
    
    
    //MARK:- IBActions
    
    @IBAction func btnBackNavigationTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCurrentSPlusClick(_ sender: UIButton) {
        lblStockControlCurrentStock.text = self.pluseBtnClick(number: lblStockControlCurrentStock.text!, filterValue: 0)
    }
    
    @IBAction func btnCurrentSMinusClick(_ sender: UIButton) {
        lblStockControlCurrentStock.text = self.minusBtnClick(number: lblStockControlCurrentStock.text!, filterValue: 0)
    }
    
    @IBAction func btnMinLevlPlusClick(_ sender: UIButton) {
        lblMinimumStockStockControl.text = self.pluseBtnClick(number: lblMinimumStockStockControl.text!, filterValue: 1)
    }
    
    @IBAction func btnMinLevlMinusClick(_ sender: UIButton) {
        lblMinimumStockStockControl.text = self.minusBtnClick(number: lblMinimumStockStockControl.text!, filterValue: 0)
    }
    
    @IBAction func btnMaxLevlPlusClick(_ sender: UIButton) {
        lblMaximumStockStockControl.text = self.pluseBtnClick(number: lblMaximumStockStockControl.text!, filterValue: 0)
    }
    
    @IBAction func btnMaxLevlMinusClick(_ sender: UIButton) {
        lblMaximumStockStockControl.text = self.minusBtnClick(number: lblMaximumStockStockControl.text!, filterValue: 2)
    }
    
    
    //MARK:- Webservice methods
    func getProductDetails() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "blueprint_deck_id":deck_id,
                    "decks_pin_id":pin_id,
                    "pins_locker_id":self.locker_id,
                    "locker_box_id":self.box_id,
                    "product_id":dictProductDetail?.object(forKey: "product_id") as! String,
                    "id":dictProductDetail?.object(forKey: "id") as? String ?? "0",
                    "product_variant_id":dictProductDetail?.object(forKey: "product_variant_id") as! String]
        
        webService_obj.fetchDataFromServer(header:"inventory_product_detail", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            
            if staus {
                
                let statusCode = responce.object(forKey: "status") as! String
                if statusCode == "OK" {
                    
                    let requriedDetail = responce.object(forKey: "data") as! NSDictionary
                    self.productDetailMain = requriedDetail.object(forKey: "product_info") as! NSDictionary
                   
                    let productDetail = requriedDetail.object(forKey: "product_info") as! NSDictionary
                    let haveBox = requriedDetail.object(forKey: "boxes") as! String
                    let haveLocker = requriedDetail.object(forKey: "lockers") as! String
                    self.isProductAvailable = requriedDetail.object(forKey: "is_available") as? String
                    let notesCount = requriedDetail.object(forKey: "noteCount") as! String
                    let pinTitle = requriedDetail.object(forKey: "pin_name") as! String
                    
                    if requriedDetail.object(forKey: "locker_name") as? String == ""
                    {
                        self.lblLockerTitle.text = "Choose Locker"
                    }else
                    {
                        self.lblLockerTitle.text = requriedDetail.object(forKey: "locker_name") as? String
                    }
                    
                    if requriedDetail.object(forKey: "box_name") as? String == ""
                    {
                        self.lblBoxTitle.text = "Choose Box"
                    }else
                    {
                        self.lblBoxTitle.text = requriedDetail.object(forKey: "box_name") as? String
                    }
                    
                    
                    if self.isForPin == "true"
                    {
                        self.currentStatus = .havePins
                    }else{
                        self.currentStatus = .havePins
                        if Int(haveBox) != 0 {
                            self.currentStatus = .haveBoxes
                        }else if Int(haveLocker) != 0 {
                            self.currentStatus = .haveLockers
                        }
                    }
                    
                    if self.ischangePin == true
                    {
                    }else{
                        
                        //button title
                        if (Int(self.isProductAvailable!)?.boolValue)! {
                            self.btnRestockOrAdd.setTitle("RESTOCK", for: .normal)
                            self.btnUpdateItems.isHidden = false
                            self.btnCellRestock.isHidden = false
                        }
                        else{
                            self.btnRestockOrAdd.setTitle("ADD ITEM", for: .normal)
                            self.btnCellRestock.isHidden = true
                            self.btnUpdateItems.isHidden = true
                        }
                        
                        let maxStock = productDetail.object(forKey: "max_qty") as! String
                        let currentStock = productDetail.object(forKey: "current_qty") as! String
                        let minStock = productDetail.object(forKey: "min_qty") as! String
                        self.productCurrentStock.text = currentStock
                        self.lblStockControlCurrentStock.text = currentStock
                        
                        self.productMinimumStock.text = minStock
                        self.lblMinimumStockStockControl.text = minStock
                        
                        self.productMaximumStock.text = maxStock
                        self.lblMaximumStockStockControl.text = maxStock
                    }
                    
                    //product details
                    self.productTitle.text = productDetail.object(forKey: "name") as? String
                    self.productVendor.text = productDetail.object(forKey: "vender_name") as? String
                    self.productIDNumber.text = "#\(productDetail.object(forKey: "product_id") as! String)"
                    
                    if productDetail.object(forKey: "price") as! String == "N/A"
                    {
                        self.productPricePerItem.text = productDetail.object(forKey: "price") as? String
                    }
                    else
                    {
                        self.productPricePerItem.text = "$\(productDetail.object(forKey: "price") as! String)"
                    }
                    
                    
                    self.productNotesCount.text = notesCount
                    self.lblPinTitle.text = pinTitle
                    let imageUrl = productDetail.object(forKey: "image") as! String
                    self.product_id = (productDetail["product_id"] as? String)!
                    self.product_varient_id = (productDetail["product_variant_id"] as? String)!
                    self.product_name = (productDetail["name"] as? String)!
                    
 
                    //setting product thumbnail image
                    let url:NSURL = NSURL(string :"\(imageURL)\(imageUrl)")!
                    self.productImage.sd_setImage(with: url as URL, placeholderImage: nil, options: SDWebImageOptions(rawValue: 0)) { (img, err, type, url) in }
                 }
                
            }
        }
    }
    
    @IBAction func btnCellRestockTapped(_ sender: UIButton) {
        
        if dictProductDetail?.object(forKey: "product_id") as! String != "0"
        {
            self.RestockProductWebService()
        }else
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"MYS Product entered manually, find vendor under products", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnAddRestockItemTapped(_ sender: UIButton) {
        
        if (Int(self.isProductAvailable!)?.boolValue)!{
            //TODO: call restock webservice
            if dictProductDetail?.object(forKey: "product_id") as! String == "0"
            {
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"MYS Product entered manually, find vendor under products.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }else{
                self.RestockProductWebService()
            }
        }
        else{
            
            self.addPresentProductWebService()
        }
    }
    
    @IBAction func btnUpdateItemclick(_ sender: Any) {
        
            PostData = ["customer_id":self.userId,
                        "blueprint_deck_id":deck_id,
                        "decks_pin_id":pin_id,
                        "id":dictProductDetail?.object(forKey: "id") as? String ?? "0",
                        "pins_locker_id":locker_id,
                        "locker_box_id":box_id,
                        "qty":self.lblStockControlCurrentStock.text!,
                        "min_qty":self.lblMinimumStockStockControl.text ?? "",
                        "product_id":dictProductDetail?.object(forKey: "product_id") as! String,
                        "product_variant_id":dictProductDetail?.object(forKey: "product_variant_id") as! String,
                        "max_qty":self.lblMaximumStockStockControl.text ?? ""]

        
        webService_obj.fetchDataFromServer(header:"inventory_product_update", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
                let minimumStockCount = Int(self.lblMinimumStockStockControl.text!)!
                let currentStockCount = Int(self.lblStockControlCurrentStock.text!)!
                
                if currentStockCount < minimumStockCount
                {
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:"Hey, the current stock for this product has reached below Minimum Level.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        
                        if self.isFromQuickView == "true"
                        {
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: MnageInventListVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }else if self.isFromsearchInventory == true{
                            self.navigationController?.popViewController(animated: true)
                            
                        }else{
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: ViewPinsDataVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }else
                {
                    if self.isFromQuickView == "true"
                    {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: MnageInventListVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }else if self.isFromsearchInventory == true{
                        self.navigationController?.popViewController(animated: true)
                        
                    }else{
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: ViewPinsDataVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
              }
        }
    }
    
    
    func addPresentProductWebService() {
        //TODO: Add product using existing product
        
        if isSharingUser == true
        {
            PostData = ["customer_id":self.userId,
                        "blueprint_deck_id":deck_id,
                        "decks_pin_id":pin_id,
                        "pins_locker_id":locker_id,
                        "locker_box_id":box_id,
                        "qty":self.lblStockControlCurrentStock.text!,
                        "min_qty":self.lblMinimumStockStockControl.text ?? "",
                        "product_id":dictProductDetail?.object(forKey: "product_id") as! String,
                        "product_variant_id":dictProductDetail?.object(forKey: "product_variant_id") as! String,
                        "max_qty":self.lblMaximumStockStockControl.text ?? "",
                        "create_by":sharedUser_id]
        }else{
            PostData = ["customer_id":self.userId,
                        "blueprint_deck_id":deck_id,
                        "decks_pin_id":pin_id,
                        "pins_locker_id":locker_id,
                        "locker_box_id":box_id,
                        "qty":self.lblStockControlCurrentStock.text!,
                        "min_qty":self.lblMinimumStockStockControl.text ?? "",
                        "product_id":dictProductDetail?.object(forKey: "product_id") as! String,
                        "product_variant_id":dictProductDetail?.object(forKey: "product_variant_id") as! String,
                        "max_qty":self.lblMaximumStockStockControl.text ?? "",
                        "create_by":webService_obj.Retrive("User_Id") as! String]
        }
        
        webService_obj.fetchDataFromServer(header:"product_add_in_inventory", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
                let minimumStockCount = Int(self.lblMinimumStockStockControl.text!)!
                let currentStockCount = Int(self.lblStockControlCurrentStock.text!)!
                
                if currentStockCount < minimumStockCount
                {
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:"Hey, the current stock for this product has reached below Minimum Level.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        if self.isFromQuickView == "true"
                        {
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: MnageInventListVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }else{
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: ViewPinsDataVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }else
                {
                    if self.isFromQuickView == "true"
                    {
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: MnageInventListVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }else{
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: ViewPinsDataVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func RestockProductWebService() {
        //TODO: Add product using existing product
        
        let maximumStockCount = Int(lblMaximumStockStockControl.text!)!
        let currentStockCount = Int(lblStockControlCurrentStock.text!)!
        let minimumStockCount = Int(lblMinimumStockStockControl.text!)!
        
        var cartQty = maximumStockCount - currentStockCount
        
        if cartQty < 0{
            cartQty = 0
        }
        
        //only call webservice when cartQty > 0, else donothing
        guard cartQty > 0 else {
            
            print("No need to restock item")
            
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Restock quantity is zero please check the maximum and current quantity.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
            return
        }
        
        
        PostData = ["customer_id":self.userId,
                    "blueprint_deck_id":deck_id,
                    "decks_pin_id":pin_id,
                    "id":dictProductDetail?.object(forKey: "id") as? String ?? "0",
                    "cart_qty":"\(cartQty)",
            "pins_locker_id":locker_id,
            "locker_box_id":box_id,
            "qty":self.lblStockControlCurrentStock.text!,
            "min_qty":self.lblMinimumStockStockControl.text ?? "",
            "product_id":dictProductDetail?.object(forKey: "product_id") as! String,
            "product_variant_id":dictProductDetail?.object(forKey: "product_variant_id") as! String,
            "max_qty":self.lblMaximumStockStockControl.text ?? ""]
        
        webService_obj.fetchDataFromServer(header:"restock", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
                if currentStockCount < minimumStockCount
                {
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:"Hey, the current stock for this product has reached below Minimum Level.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        
                        //Show alert Message
                        let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction) in
                            if self.isFromQuickView == "true"
                            {
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: MnageInventListVC.self) {
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }else if self.isFromsearchInventory == true{
                                self.navigationController?.popViewController(animated: true)
                                
                            }else{
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: ViewPinsDataVC.self) {
                                        self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }
                        }
                        let actionCart = UIAlertAction(title: "Go To Cart", style: .default) { (action:UIAlertAction) in
                            let st = UIStoryboard.init(name: "Cart", bundle: nil)
                            let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
                            controller.push()
                        }
                        alertMessage.addAction(action)
                        alertMessage.addAction(actionCart)
                        self.present(alertMessage, animated: true, completion: nil)
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }
                else
                {
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction) in
                        if self.isFromQuickView == "true"
                        {
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: MnageInventListVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }else if self.isFromsearchInventory == true{
                            self.navigationController?.popViewController(animated: true)
                            
                        }else{
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: ViewPinsDataVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                    let actionCart = UIAlertAction(title: "Go To Cart", style: .default) { (action:UIAlertAction) in
                        let st = UIStoryboard.init(name: "Cart", bundle: nil)
                        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
                        controller.push()
                    }
                    alertMessage.addAction(action)
                    alertMessage.addAction(actionCart)
                    self.present(alertMessage, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    
    //MARK:- Utility methods
    
    //filterValue :
    //0 - No filter
    //1 - validation1
    //2 - validation2
    
    func pluseBtnClick(number:String,filterValue:Int) -> String {
        var temp = Int(number) ?? 0
        temp += 1
        
        if filterValue == 1 {
            guard validation1(valueToCheck: temp) else {
                temp -= 1
                return String(temp)
            }
        }
        
        
        return String(temp)
    }
    func minusBtnClick(number:String,filterValue: Int) -> String {
        var temp = Int(number) ?? 0
        if temp <= 0 {
            return "0"
        }
        else {
            temp -= 1
        }
        
        if filterValue == 2 {
            guard validation2(valueToCheck: temp) else {
                
                temp += 1
                return String(temp)
            }
        }
        
        return String(temp)
    }
    func validation1(valueToCheck: Int) -> Bool{
        
        let maximumVal = Int(lblMaximumStockStockControl.text!)!
        return valueToCheck <= maximumVal
    }
    func validation2(valueToCheck: Int) -> Bool{
        
        let minimumVal = Int(lblMinimumStockStockControl.text!)!
        return minimumVal <= valueToCheck
    }
}
