//
//  MangeInventoryHomeViewController.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac114 on 15/11/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class MangeInventoryHomeViewController: UIViewController {

    //MARK: - Viewcontroller Navigatio
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:- IBActions
    
    @IBAction func btnBackTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnManageInventroyTapped(_ sender: UIButton) {
        
        
    }
    
    @IBAction func btnAddToInventoryTapped(_ sender: UIButton) {
        
        
    }
    @IBAction func btnManageInventoryTapped(_ sender: UIButton) {
        
        let st = UIStoryboard(name: "Inventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"DeckLevelList") as! DeckLevelList
        controller.isToManageOrFirstTime = true
        controller.isforFirstTime = false
        controller.push()
    }
}
