//
//  ChooseLocationInventoryMngViewController.swift
//  MARITIME YACHT SERVICES
//
//  Created by Mac114 on 15/11/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class ChooseLocationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Text: UILabel!
    
    
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            if newValue{
                lbl_Text.textColor = UIColor.white
            }else{
                lbl_Text.textColor = UIColor(red: 92/255, green: 183/255, blue: 227/255, alpha: 1.0)
            }
        }
        get{
            return super.isSelected
        }
    }
}


class ChooseLocationListTableviewCell: UITableViewCell {
    
    typealias RadioButtonBlock = (_ cell: ChooseLocationListTableviewCell) -> Void
    
    @IBOutlet weak var lblPinName: UILabel!
    @IBOutlet weak var btnSelection: UIButton!
    
    var block: RadioButtonBlock?
    
    @IBAction func btnCellSelectionTapped(_ sender: UIButton) {
        
        block!(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if isSelected == false {
            //accessoryType = UITableViewCellAccessoryType.none
            self.btnSelection.isSelected = false
        }
        else {
            //accessoryType = UITableViewCellAccessoryType.checkmark
            self.btnSelection.isSelected = true
        }
    }

    
}

class ChooseLocationInventoryMngViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    let locationCellId = "ChooseLocationListCell"
    let emptyRecordCellId = "NoLocationCell"
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var tableviewChooseLocation: UITableView!
    var arrDecks: NSMutableArray = NSMutableArray()
    var arrSelection = [Bool]()
    var arrPinsForDeck: NSMutableArray = NSMutableArray()
    var selectedDeckDict:[String:Any]? = nil
    var selectedPinId: String? = nil
    var selectedPinName: String? = nil
    
     var block: (([String : String])->Void)?
    
    var selectedDeckIndex = 0 {
        
        didSet {
            
            //change offset
            //change color
            collectionView.reloadData()
            
            
            //refresh tableview to show data
            selectedDeckDict = arrDecks.object(at: selectedDeckIndex) as? [String:Any]
            
            //remove all elements from array, and put the new content to pins array and reload table
            arrPinsForDeck.removeAllObjects()
            selectedPinId = nil
            selectedPinName = nil
            
            arrPinsForDeck = NSMutableArray(array: selectedDeckDict?["pin"] as! NSArray)
            self.tableviewChooseLocation.reloadData()
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
         // Do any additional setup after loading the view.
        getAllDecks()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (arrDecks.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseLocationCell", for: indexPath) as! ChooseLocationCollectionViewCell
        
        cell.lbl_Text.text = (arrDecks.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as? String ?? "No Title"
        
        cell.isSelected = arrSelection[indexPath.row]
        
        //        cell.selectedBackgroundView = UIView(frame: cell.bounds)
        //        cell.selectedBackgroundView!.backgroundColor = .white
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("deck name selected")
        
        arrSelection = Array(repeating: false, count: self.arrDecks.count)
        arrSelection[indexPath.row] = true
        
        selectedDeckIndex = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let title = ((arrDecks.object(at: indexPath.row) as! NSDictionary).object(forKey: "name") as? String ?? "No Title") as NSString
        let width = title.size(attributes: [NSFontAttributeName: UIFont(name: "Poppins-Semibold", size: 17.0) ?? ""]).width
        let height = CGFloat(30)
        
        return CGSize(width: width+40, height: height)
    }
    
    //MARK:- Utility Methods
   
    //MARK:- IBActions
    @IBAction func btnBackTapped(sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTickTapped(sender: UIButton) {
    
        if selectedPinId != nil {
            //process the selected pinId and selectedDeckId
            print("DeckId: \(String(describing: selectedDeckDict?["id"])) \n\nPinId: \(selectedPinId!)")
            
//            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
//            let controller = st.instantiateViewController(withIdentifier:"AddItemVC") as! AddItemVC
//            controller.deck_id = selectedDeckDict?["id"]! as! String
//            controller.pin_id = selectedPinId!
//            self.navigationController?.pushViewController(controller, animated: true)
            
            let dataDict = ["name":selectedPinName!,
                            "id":selectedPinId!]
            
            block?(dataDict)
            
            self.navigationController?.popViewController(animated: true)
        }
        else{
            //show user error
            print("Error: Please select pin")
            AlertMessage("Please select location.")
        }
    }
    
    
    //MARK:- Tableview datasource and delegate methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // let cell = self.tableviewChooseLocation.cellForRow(at: indexPath)
       // cell?.accessoryType = UITableViewCellAccessoryType.checkmark
        
        if let indexPath = tableView.indexPathForSelectedRow{
             let index = indexPath.row
            if self.arrPinsForDeck.count != 0
            {
                let selectedDict = self.arrPinsForDeck[index] as! NSDictionary
                self.selectedPinId = selectedDict.object(forKey: "id") as? String
                self.selectedPinName = selectedDict.object(forKey: "title") as? String
            }
        }else{
             self.selectedPinId = nil
            self.selectedPinName = nil
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard arrPinsForDeck.count > 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: emptyRecordCellId)
            cell?.textLabel?.text = "No record found!"
            return cell!
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: locationCellId) as! ChooseLocationListTableviewCell
        
        let dictRecord = arrPinsForDeck[indexPath.row] as! NSDictionary
        
        cell.lblPinName.text = dictRecord["title"] as? String
        cell.btnSelection.isSelected = false
        cell.selectionStyle = .none
        
        cell.block = { selectedCell in
            
            //check if btnSelected, get the id -- else pass nil
            if selectedCell.btnSelection.isSelected {
                
                //getting selected index and getting pinId from that
                let indexpath = self.tableviewChooseLocation.indexPath(for: selectedCell as UITableViewCell)
                let index = indexpath?.row
                
                let selectedDict = self.arrPinsForDeck[index!] as! NSDictionary
                self.selectedPinId = selectedDict.object(forKey: "id") as? String
                self.selectedPinName = selectedDict.object(forKey: "title") as? String
              }
            else{
                self.selectedPinId = nil
                self.selectedPinName = nil
            }
        }
        
        
        return cell as UITableViewCell
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard arrPinsForDeck.count > 0 else {
            return 1
        }
        return arrPinsForDeck.count
    }

    //MARK:- Webservice methods
    func getAllDecks() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "shared_customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"getBluePrintList", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
                self.arrDecks = NSMutableArray(array: responce.object(forKey: "data") as! [Any])
                self.arrSelection = Array(repeating: false, count: self.arrDecks.count)
                self.arrSelection[0] = true //to hightlight first deck
                
                //get the pins for first deck and reload tableview
                self.selectedDeckIndex = 0
                
                //updating UI on main thread
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.tableviewChooseLocation.reloadData()
                }
            }
        }
    }

}
