//
//  PrivacyPolicyVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 05/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class PrivacyPolicyVC: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var webView:UIWebView!
    var selctButton = ""
    
    @IBOutlet var webview: UIWebView!
    @IBOutlet var lbl_title: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if self.selctButton == "Privacy Policy"
        {
            self.lbl_title.text = "Privacy Policy"
            let url = URL(string: "https://56.octallabs.com/mys/businessadmin/pages/display/privacy_policy")
            self.webview.loadRequest(URLRequest(url: url!))
        }
        else if self.selctButton == "Cancellation Policy"
        {
            self.lbl_title.text = "Cancellation Policy"
            let url = URL(string: "https://56.octallabs.com/mys/businessadmin/pages/display/cancellation_policy")
            self.webview.loadRequest(URLRequest(url: url!))
        }
        else if self.selctButton == "Term Of Services"
        {
            self.lbl_title.text = "Term Of Services"
            let url = URL(string: "https://56.octallabs.com/mys/businessadmin/pages/display/terms_and_conditions")
            self.webview.loadRequest(URLRequest(url: url!))
        }
    }
    
    func webViewDidStartLoad(_ webView : UIWebView) {
        load.show(views: self.view)
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        load.hide(delegate:self)
        load.hide(delegate:self)
        load.hide(delegate:self)
        
    }
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        load.hide(delegate: self)
        load.hide(delegate:self)
        load.hide(delegate:self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
}
