//  HelpVC.swift
//  MARITIME YACHT SERVICES
//  Created by mac112 on 11/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.


import UIKit

class HelpVC: UIViewController , UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var tblView: UITableView!

    var questionsArr = [[String:String]]()
    
    var newArray = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblView.estimatedRowHeight = 44.0
        tblView.rowHeight = UITableViewAutomaticDimension
        
        tblView.estimatedSectionHeaderHeight = 44
        tblView.sectionHeaderHeight = UITableViewAutomaticDimension

        
       self.helpListWebService()
        // Do any additional setup after loading the view.
        
    }
    
    
    func helpListWebService()
    {
        webService_obj.fetchDataFromServer(header:"faq_list", withParameter: [:], inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.questionsArr = responce.value(forKey: "data") as! [[String:String]]
            }
            
            self.newArray = self.questionsArr.map { dict in
                
                var newDict = dict
                newDict["open"] = "0"
                return newDict
            }
            
            
            print(self.newArray)
            
            self.tblView.reloadData()

        }
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    override func viewDidLayoutSubviews() {
        
    }
 
    
    
    func pressButton(button: UIButton) {
        
        tblView.numberOfRows(inSection: button.tag)
        
        var updateDict = newArray[button.tag]
        
        if (self.newArray [button.tag]["open"] == "1") {
            updateDict["open"] = "0"
        }else {
            updateDict["open"] = "1"
        }
        
        newArray[button.tag] = updateDict
        
        self.tblView.reloadData()
        
    }
    
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")
        
        
        let lblTitle  = headerView?.viewWithTag(10) as! UILabel
        lblTitle.text =  self.newArray [section]["question"]
        
        let lblSepBottom  = headerView?.viewWithTag(11) as! UILabel

        let btnDown  = headerView?.viewWithTag(12) as! UIButton
        btnDown.tag = section
        
        btnDown.addTarget(self, action: #selector(pressButton(button:)), for: .touchUpInside)
        
        
        if (self.newArray [section]["open"] == "0") {
            lblSepBottom.backgroundColor = UIColor.lightGray
            let image = UIImage(named: "arrowdown") as UIImage?
            btnDown.setImage(image, for: .normal)
        }else {
            lblSepBottom.backgroundColor = UIColor.white
            let image = UIImage(named: "arrowup") as UIImage?
            btnDown.setImage(image, for: .normal)
        }
        
        return headerView
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.newArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.newArray [section]["open"] == "1") {
            return 1
        }
        else {
            return 0
        }
        
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpCell", for: indexPath)
        
        let lblDesc  = cell.viewWithTag(10) as! UILabel
        lblDesc.text = self.newArray [indexPath.section]["answer"]
        
        do {
            let str = try NSAttributedString(data:(self.newArray [indexPath.section]["answer"]?.data(using: String.Encoding.unicode, allowLossyConversion: true)!)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                lblDesc.attributedText = str
        } catch {
            print(error)
        }

        return cell
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
