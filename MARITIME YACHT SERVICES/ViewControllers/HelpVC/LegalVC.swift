//
//  LegalVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 14/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit


class LegalVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView:UIWebView!
    @IBOutlet weak var back_btn:UIButton!
    @IBOutlet weak var header_Txt:UILabel!
    
    var isforPayment = Bool()
    var cartData = [[String:Any]]()
    
    var discountAmount = Double()
    
    var order_id = ""
    var admin_Paypal_id = ""
    var admin_commission = ""
    var cart_ids = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        
        if isforPayment == true{
            header_Txt.text = "Payment"
         
            self.back_btn.setImage(UIImage(named: "back"), for: .normal)
            
            self.webView.scalesPageToFit = true
            self.webView.contentMode = UIViewContentMode.scaleAspectFit
            
            var paydata = [[String:String]]()
            var admin_Amt = 0.0
            var shipping_charge = 0.00
            
            var array_admin_amt = [Double]()
            
            for (_, dict) in self.cartData.enumerated(){
                  var final_Amt = 0.0
                let total_Amt = dict["total_amount"] as! Double
                
                admin_Amt = (total_Amt/100)*Double(admin_commission)!
                array_admin_amt.append(admin_Amt)
                
                 shipping_charge = dict["totalShippingCharge"] as! Double
                
                final_Amt = total_Amt - admin_Amt
                final_Amt = final_Amt + shipping_charge
                
                paydata.append(["email": dict["vendor_email"] as! String,
                                "amount": String(format: "%.2f", final_Amt)])
            }
            
            //Admin calculation
            var amt = array_admin_amt.reduce(0, +)
            
            if shipping_charge > 0{
                if Double(self.discountAmount) > 0
                {
                    amt = amt - Double(self.discountAmount)
                    if amt < 0{
                        amt = 0.01
                    }
                }
            }
            paydata.append(["email":self.admin_Paypal_id,
                            "amount": String(format: "%.2f", amt)])
            
            
            let jsonobject = ["order_id":self.order_id,
                              "vendor":paydata] as [String : Any]
            
            if let data = try? JSONSerialization.data(withJSONObject: jsonobject, options: .prettyPrinted),
                let str = String(data: data, encoding: .utf8) {
                print(str)
                var request = URLRequest(url: URL(string: "https://56.octallabs.com/mys/businessadmin/orders/payPalPayment")!)
                request.httpMethod = "POST"
                request.httpBody = data
                self.webView.loadRequest(request)
            }
        }else{
            header_Txt.text = "Legal"
            self.back_btn.setImage(UIImage(named: "menu"), for: .normal)
            
            let  url = URL (string:"https://56.octallabs.com/mys/businessadmin/pages/display/legal")
            self.webView.loadRequest(URLRequest(url: url!))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        if isforPayment == true{
            _=self.navigationController?.popViewController(animated: true)
        }else{
            sideMenuVC.toggleMenu()
        }
    }
    
    
    func webViewDidStartLoad(_ webView : UIWebView) {
        load.show(views: self.view)
           let id  = "/2"
        if isforPayment == true{
            if let text = webView.request?.url?.absoluteString{
                print(text)
                
                if text.range(of:"payment_success") != nil {
                    print("**********************************************")
                   let  url = URL(string: "\(text)/\(self.cart_ids.joined(separator: ","))\(id)")!
                   let request = URLRequest(url: url)
                print("Success Reuset--->\(url)")
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        guard let data = data else {
                            
                            print("request failed \(String(describing: error))")
                            return
                        }
                        do {
                            if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any], let result = json["response"] as? [String: Any] {
                                // Parse JSON _
                                
                                if result["status"] as! String == "OK"{
                                    
                                    //  Show alert Message
                                    let alertMessage = UIAlertController(title: "MYS", message:result["success_message"] as? String, preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                        _=kConstantObj.SetMainViewController("MyOrderVC")
                                    }
                                    alertMessage.addAction(action)
                                    self.present(alertMessage, animated: true, completion: nil)
                                }else{
                                    //  Show alert Message
                                    let alertMessage = UIAlertController(title: "MYS", message:"There is some error in processing.", preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                        _=kConstantObj.SetMainViewController("ProductsVC")
                                    }
                                    alertMessage.addAction(action)
                                    self.present(alertMessage, animated: true, completion: nil)
                                }
                            }
                        } catch let parseError {
                            print("parsing error: \(parseError)")
                            let responseString = String(data: data, encoding: .utf8)
                            print("raw response: \(String(describing: responseString))")
                        }
                    }
                    task.resume()
                }
                else if text.range(of:"payment_cancel") != nil {
                    
                  let  url = URL(string: text)
                   let request = URLRequest(url: url!)
                    
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        guard let data = data else {
                            print("request failed \(String(describing: error))")
                            return
                        }
                        do {
                            if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any], let result = json["response"] as? [String: Any]  {
                                // Parse JSON _
                                //  Show alert Message
                                let alertMessage = UIAlertController(title: "MYS", message:result["error_message"] as? String, preferredStyle: .alert)
                                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                    _=self.navigationController?.popViewController(animated: true)
                                }
                                alertMessage.addAction(action)
                                self.present(alertMessage, animated: true, completion: nil)
                                
                            }
                        } catch let parseError {
                            print("parsing error: \(parseError)")
                            let responseString = String(data: data, encoding: .utf8)
                            print("raw response: \(String(describing: responseString))")
                        }
                    }
                    task.resume()
                }
             }
            
        }
    }
    
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        load.hide(delegate:self)
        load.hide(delegate:self)
        load.hide(delegate:self)
        let id = "/2"
        if let text = webView.request?.url?.absoluteString{
            print(text)
         
            if text.range(of:"payment_success") != nil {
                print("-----------------",self.cart_ids)
                
                let  url = URL(string: "\(text)/\(self.cart_ids.joined(separator: ","))\(id)")!
                let request = URLRequest(url: url)
                print("Success Reuset--->\(url)")
                
                
//                let html = webView.stringByEvaluatingJavaScript(from: "document.body.innerHTML")  //html?.contains("status_code\":200")
//                let data = html?.data(using: .utf8)!
//
//                do {
//                    if let jsonArray = try JSONSerialization.jsonObject(with: data!) as? [String: Any], let result = jsonArray["response"] as? [String: Any]
//                    {
//                        print("JSON  = \(jsonArray)")
//
//                        if result["status_code"] as! Int == 200 {
//
//                            //  Show alert Message
//                            let alertMessage = UIAlertController(title: "MYS", message:result["success_message"] as? String, preferredStyle: .alert)
//                            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//                                _=kConstantObj.SetMainViewController("MyOrderVC")
//                            }
//                            alertMessage.addAction(action)
//                            self.present(alertMessage, animated: true, completion: nil)
//                        }else{
//                            //  Show alert Message
//                            let alertMessage = UIAlertController(title: "MYS", message:"There is some error in processing.", preferredStyle: .alert)
//                            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//                                _=kConstantObj.SetMainViewController("ProductsVC")
//                            }
//                            alertMessage.addAction(action)
//                            self.present(alertMessage, animated: true, completion: nil)
//                        }
//
//                    } else {
//                        print("bad json")
//                    }
//                } catch let error as NSError {
//                    print(error)
//                }
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data else {
                        print("request failed \(String(describing: error))")
                        return
                    }
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any], let result = json["response"] as? [String: Any] {
                            // Parse JSON _

                            print("JSON  = \(json)")

                            if result["status_code"] as! Int == 200 {

                            //  Show alert Message
                            let alertMessage = UIAlertController(title: "MYS", message:result["success_message"] as? String, preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                  _=kConstantObj.SetMainViewController("MyOrderVC")
                            }
                            alertMessage.addAction(action)
                            self.present(alertMessage, animated: true, completion: nil)
                            }else{
                                //  Show alert Message
                                let alertMessage = UIAlertController(title: "MYS", message:"There is some error in processing.", preferredStyle: .alert)
                                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                    _=kConstantObj.SetMainViewController("ProductsVC")
                                }
                                alertMessage.addAction(action)
                                self.present(alertMessage, animated: true, completion: nil)
                            }
                         }
                    } catch let parseError {
                        print("parsing error: \(parseError)")
                        let responseString = String(data: data, encoding: .utf8)
                        print("raw response: \(String(describing: responseString))")
                    }
                }
                task.resume()
                
                
            }
            else if text.range(of:"payment_cancel") != nil {
                
                let  url = URL(string: text)
                let request = URLRequest(url: url!)
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                    guard let data = data else {
                        print("request failed \(String(describing: error))")
                        return
                    }
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data) as? [String: Any], let result = json["response"] as? [String: Any]  {
                            // Parse JSON _
                            
                            //  Show alert Message
                            let alertMessage = UIAlertController(title: "MYS", message:result["error_message"] as? String, preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                _=self.navigationController?.popViewController(animated: true)
                            }
                            alertMessage.addAction(action)
                            self.present(alertMessage, animated: true, completion: nil)
                            
                        }
                    } catch let parseError {
                        print("parsing error: \(parseError)")
                        let responseString = String(data: data, encoding: .utf8)
                        print("raw response: \(String(describing: responseString))")
                    }
                }
                task.resume()
            }
        }
    }
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        load.hide(delegate: self)
        load.hide(delegate:self)
        load.hide(delegate:self)
    }
     
}
