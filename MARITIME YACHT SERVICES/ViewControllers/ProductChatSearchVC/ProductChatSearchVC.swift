//
//  ProductChatSearchVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 08/01/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit
import Firebase

class ProductChatSearchVC: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource,UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet var collectionViewSearch: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet var searchTable: UITableView!
    
    var filteredData = [[String:String]]()
    var senderDisplayName: String?
    private var channels: [Channel] = []
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("channels")
    
    var customer_Name = ""
    var customer_Image = ""
    var isforService = Bool()
    var serviceCatID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.white
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.searchBar.text = ""
    
        if isforService == true{
            self.searchTable.isHidden = false
            self.collectionViewSearch.isHidden = true
           // self.searchBar.isHidden = true
            
            searchTable.tableFooterView = UIView()
            searchTable.estimatedRowHeight = 44.0
            searchTable.rowHeight = UITableViewAutomaticDimension
            
            self.lblHeader.text = "Select Vendor"
            
            self.fetchServiceCategoryListWebServiceCall(searchText:"")
        }else{
           // self.searchBar.isHidden = false
            
            self.searchTable.isHidden = true
            self.collectionViewSearch.isHidden = false
            
            self.lblHeader.text = "Search Products"
         }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    //MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             return filteredData.count
     }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCustomCell", for: indexPath) as! searchCustomCell
        
        let data = filteredData[indexPath.row]
        cell.lbl_vendor_Name.text = data["name"]
        cell.rating_vendor.rating = Float(data["rating"] ?? "") ?? 0.0
        let str = data["image"]!
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
        cell.img_vendor.af_setImage(withURL: url as URL)
        
        do {
            let str = try NSAttributedString(data:(data["description"] ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
            cell.lbl_detail.attributedText = str
        } catch {
            print(error)
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.view.endEditing(true)
        
        let searchData = self.filteredData[indexPath.row]
        
        let vendor_id = searchData["id"]!
        let vendor_name = searchData["name"]!
        let user_id = webService_obj.Retrive("User_Id") as! String
        let channel_id = user_id+"_"+vendor_id+"_"+vendor_id
        self.channels.removeAll()
        self.channels.append(Channel(id: vendor_id, name: vendor_name))
        
        self.checkChannelWebServiceCall(channel_Name: channel_id, vendor_id: vendor_id, product_id: vendor_id, product_name: vendor_name, type: "2")
        
        //        let channel = self.channels[0]
        //
        //        self.channelRef.child(channel_id)
        //
        //        let main = UIStoryboard(name: "MngInventory", bundle: nil)
        //        let controller = main.instantiateViewController(withIdentifier:"ChatViewController")as! ChatViewController
        //        controller.isfromMyTeam = false
        //        controller.channel = channel
        //        controller.senderDisplayName = self.customer_Name
        //        controller.channelRef = self.channelRef.child(channel_id)
        //        controller.customer_image = self.customer_Image
        //        controller.channel_name = channel_id
        //        controller.push()
    }

  
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return filteredData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cellD = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsMainCell", for: indexPath) as! ProductsMainCell
            
            let sData = self.filteredData[indexPath.row]
            
            cellD.lbl_PName.text = sData["name"] ?? ""
            
            let strLogo2 = sData["logo"]!
            let urlLogo2:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo2))")!
            cellD.img_Logo.af_setImage(withURL: urlLogo2 as URL)
            
            cellD.lbl_Price.text = "$\(sData["price"] ?? "")"
            
            cellD.view_Rating.rating = (sData["rating"]! as NSString).floatValue
            
            if sData["image"] == ""
            {
                cellD.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = sData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellD.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellD
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        self.view.endEditing(true)
        
        let searchData = self.filteredData[indexPath.row]
        
        let vendor_id = searchData["vendor_id"]
        let vendor_name = searchData["vendor_name"]
        let product_id = searchData["product_id"]
         let product_name = searchData["name"]
        
        let user_id = webService_obj.Retrive("User_Id") as! String
        let channel_id = user_id+"_"+vendor_id!+"_"+product_id!
        
        self.channels.removeAll()
        
        self.channels.append(Channel(id: vendor_id!, name: vendor_name!))
        
        self.checkChannelWebServiceCall(channel_Name: channel_id, vendor_id: vendor_id!, product_id: product_id!, product_name: product_name!, type: "1")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.size.width - 40) / 2
        let height = width * 1.2
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
        
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        filteredData.removeAll()
        
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            if isforService == true{
                self.fetchServiceCategoryListWebServiceCall(searchText:"")
            }
         }
        else
        {
            if isforService == true{
                self.fetchServiceCategoryListWebServiceCall(searchText:searchText)
            }else{
                self.fetchSearchdataWebServiceCall(searchText: searchText)
            }
        }
        collectionViewSearch.reloadData()
    }
    
    func fetchSearchdataWebServiceCall(searchText :String) {
   
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "search":searchText,
                        "type" : "home",
                        "page":-1]
 
        load.hide(delegate: self)
        
        webService_obj.fetchDataFromServer(alertMsg: true,header:"product_searching", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                
                self.customer_Name = responce.value(forKey: "customer_name") as! String
                self.customer_Image = responce.value(forKey: "customer_image") as! String
                
                let searchData = responce.value(forKey: "data") as! [String: Any]
                let product = searchData["product"] as! [[String:String]]
                
                    let arr = product.filter({ (fruit) -> Bool in
                        return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
                    })
                    self.filteredData = arr
                 
                 self.collectionViewSearch.reloadData()
            }
         }
    }
    
    func fetchServiceCategoryListWebServiceCall(searchText :String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "cat_id":self.serviceCatID,
                    "search":searchText]
      
        webService_obj.fetchDataFromServer(alertMsg: false, header:"service_cat_user_list", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                let searchData = responce.value(forKey: "data") as!  [[String:String]]
                
                self.customer_Name = responce.value(forKey: "customer_name") as! String
                self.customer_Image = responce.value(forKey: "customer_image") as! String
                
                self.filteredData.removeAll()
                self.filteredData = searchData
                self.searchTable.reloadData()
            }else
            {
                self.filteredData.removeAll()
                self.searchTable.reloadData()
            }
        }
    }
    
    
    func checkChannelWebServiceCall(channel_Name :String, vendor_id: String, product_id: String, product_name: String, type:String ) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "channel_name":channel_Name,
                    "user_id" :vendor_id,
                    "related_id":product_id,
                    "name":product_name,
                    "chat_type":type]
 
        webService_obj.fetchDataFromServer(alertMsg: true,header:"add_channel", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                
                let channel = self.channels[0]
                
                self.channelRef.child(channel_Name)
                
                let main = UIStoryboard(name: "MngInventory", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"ChatViewController")as! ChatViewController
                controller.isfromMyTeam = false
                
                if self.isforService == true{
                    controller.isforProduct = false
                }else{
                    controller.isforProduct = true
                    controller.productTitle = "\(product_name) - \(channel.name)"
                }
                
                controller.channel = channel
                controller.senderDisplayName = self.customer_Name
                controller.channelRef = self.channelRef.child(channel_Name)
                controller.customer_image = self.customer_Image
                controller.channel_name = channel_Name
                 controller.push()

            }
        }
    }
 }
