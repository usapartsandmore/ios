//
//  ExpensesMainVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 22/11/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class ExpensesMainVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }

    @IBAction func btnIndividualExpensesClick(_ sender: Any) {
        let st = UIStoryboard.init(name: "Expense", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ExpenseVC")
        controller.push()
    }

    @IBAction func btnTeamExpensesClicked(_ sender: Any) {
        let st = UIStoryboard.init(name: "Expense", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ExpenseTeamList")
        controller.push()
    }
    
    @IBAction func btnGroupExpensesClick(_ sender: Any) {
        let st = UIStoryboard.init(name: "Expense", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"GroupExpensesListVC")
        controller.push()
    }
}
