//
//  MessageVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit on 05/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class MessageCenterCell: UITableViewCell {
    
    @IBOutlet weak var lblVentorName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imgVendor: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class MessageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblview: UITableView!
    var messageCenterArr = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchMessageCenterListWebService()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func cartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageCenterArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCenterCell") as! MessageCenterCell
        
        let messagedict = self.messageCenterArr[indexPath.row]
        
        //Convert in date format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myDate = dateFormatter.date(from: (messagedict["created"] as? String)!)
        dateFormatter.dateFormat = "MMM dd, yyyy"
        cell.lblTime.text = dateFormatter.string(from: myDate!)
        
        
        let msgDict = messagedict["msg"] as! [String:String]
        cell.lblMessage.text = msgDict["msg"]
        
        let vendorDetailDict = messagedict["vendorInfo"] as! [String:String]
        cell.lblVentorName.text = vendorDetailDict["name"]
        
        if vendorDetailDict["image"] == ""
        {
            cell.imgVendor.image = UIImage(named:"noimage")
        }else{
            let str = vendorDetailDict["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.imgVendor.af_setImage(withURL: url as URL)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = tableView.separatorColor ?? .lightGray
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let messagedict = self.messageCenterArr[indexPath.row]
        let vendorDetailDict = messagedict["vendorInfo"] as! [String:String]
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        obj.vendor_id = messagedict["user_id"] as! String
        obj.vendor_name = vendorDetailDict["name"]!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
             return true
     }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
         let data = self.messageCenterArr[indexPath.row]
        
            let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Do you want to delete this conversation?", preferredStyle: .alert)
                let actionCancel = UIAlertAction(title: "NO", style: .default) { (action:UIAlertAction) in
                }
                let actionOk = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction) in
                    self.deleteMessagesWebService(user_id: data["user_id"]! as! String)
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
                
            }
            delete.backgroundColor = UIColor.red
            
            return [delete]
    }

    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        _=self.navigationController?.popViewController(animated: true)
    }
    
    func fetchMessageCenterListWebService()
    {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(alertMsg:false,header:"messageList", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.messageCenterArr = responce.value(forKey: "data") as! [[String:Any]]
                self.tblview.reloadData()
            }else
            {
                self.messageCenterArr.removeAll()
                self.tblview.reloadData()
            }
        }
    }
    
    func deleteMessagesWebService(user_id:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "user_id":user_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_message", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.fetchMessageCenterListWebService()
            }
        }
    }

}

