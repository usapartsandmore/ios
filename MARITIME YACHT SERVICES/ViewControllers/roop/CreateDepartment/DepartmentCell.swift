//
//  DepartmentCell.swift
//  MARITIME YACHT SERVICES
//
//  Created by Octal on 24/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class DepartmentCell: UITableViewCell {

    @IBOutlet weak var lblMemberName: UILabel!
    @IBOutlet weak var imgMember: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
