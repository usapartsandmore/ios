//
//  CreateDepartmentVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Octal on 24/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class CreateDepartmentVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var dataDict = [[String:String]]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tblViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var txtTeamName: UITextField!
    
     
    override func viewDidLoad() {
        super.viewDidLoad()
      }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.dataDict.count==0
        {
            self.tblViewHeightConst.constant = self.tableView.contentSize.height
        }else{
            self.tblViewHeightConst.constant = 184.5
        }
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Back Button Click
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
          return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Department", for: indexPath) as! DepartmentCell
        
             var dict = self.dataDict[indexPath.row]
            
            cell.lblMemberName.text = dict["name"]
            cell.imgMember.image = UIImage(named:"ancor")
          
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     }
    
    // MARK: AddMember Button Click
    @IBAction func btnAddMemberClick(_ sender: Any) {
        self.view.endEditing(true)
        let st = UIStoryboard.init(name: "MyTeam", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"MemberListing") as! MemberListing
        controller.selectedArr = self.dataDict
        controller.isfromMembers = "false"
        controller.selectedDictblock = { dict in
              NSLog("Selected Dict-->\(dict)")
            self.dataDict = dict
            self.tableView.reloadData()
        }
        controller.push()
    }
    
     // MARK: Crate Button Click
    @IBAction func btnCreateClick(_ sender: Any) {
        self.view.endEditing(true)
        if self.txtTeamName.text! .isEmpty
        {
            self.AlertMessage("Please enter team name.")
        }
        else if self.dataDict.count == 0
        {
            self.AlertMessage("Please add members.")
        }
        else{
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "name":self.txtTeamName.text!,
                        "members":self.dataDict]
            
            webService_obj.fetchDataFromServer(header:"customer_team", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    _=kConstantObj.SetIntialMainViewController("MyTeamListing")
                }
            }
        }
    }
  
    
}
