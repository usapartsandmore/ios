//
//  ViewPinsDataVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 19/12/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class ViewPinsDataVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblListing: UITableView!
    // KJ Tree instances -------------------------
    var arrayTree:[Parent] = []
    var kjtreeInstance: KJTree = KJTree()
    
    @IBOutlet weak var lblHeader: UILabel!
    
    let baseTagForAddLockerButton = 1000
    let baseTagForAddBoxbutton = 5000
    let baseTagForAddProductButton = 500
    
    var bluePrintDeckId:String = ""
    var pinID = ""
    var pinName = ""
    
    var dictPinRecord:NSDictionary = NSMutableDictionary()
    
    var completeData = [[String:Any]]()
    
    @IBOutlet weak var viewTransBack: UIView!
    @IBOutlet weak var viewAddDescriptionToPin: UIView!
    @IBOutlet weak var txtFldPinDescription: UITextField!
    
    @IBOutlet weak var btnRemovePin: UIButton!
    @IBOutlet weak var lblPopupTxt: UILabel!
    var editType = ""
    var selectedEditID = ""
    var seletedEditType = ""
    
    var pin_owner_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //New intializations
        // let treeDictionary = dictPinRecord.object(forKey: "data") as! NSDictionary
        // bluePrintDeckId = treeDictionary.object(forKey: "id") as! String
        // createTree(treeDictionary: treeDictionary)
        
        tblListing.delegate = self
        tblListing.dataSource = self
        tblListing.estimatedRowHeight = 15
        tblListing.rowHeight = UITableViewAutomaticDimension
        tblListing.tableFooterView = UIView()
        
        self.lblHeader.text = self.pinName
        
        let cellIdentifierParents = "ParentsTableViewCellIdentity"
        tblListing.register(UINib(nibName: "ParentsTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierParents)
        
        var cellIdentifierChilds = "Childs2ndStageTableViewCellIdentity"
        tblListing.register(UINib(nibName: "Childs2ndStageTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierChilds)
        
        cellIdentifierChilds = "Childs3rdStageTableViewCellIdentity"
        tblListing.register(UINib(nibName: "Childs3rdStageTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierChilds)
        
         
        cellIdentifierChilds = "SepLineTableViewCellIdentity"
        tblListing.register(UINib(nibName: "SepLineTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierChilds)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //adding current view as observer to notification
        
        //Hide the add description view
        viewTransBack.isHidden = true
        viewAddDescriptionToPin.isHidden = true
        txtFldPinDescription.text = ""
        
        
        getDeckRecord()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(notification:)), name: Notification.Name("refreshList"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("refreshList"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initTree(array:[[String:Any]]){
        let parents = array.map(getParent)
    
        var productFound = false
        var boxFound = false
        
        var newCollection = [Parent]()
        
        for parent in parents{
            
            if !boxFound && parent.key == "box"{
                boxFound = true
                newCollection.append(Parent.init(key: "sep_box", data: [:]))
            }
            
            if !productFound && parent.key == "product"{
                productFound = true
                newCollection.append(Parent.init(key: "sep", data: [:]))
            }
            
            newCollection.append(parent)
        }
        kjtreeInstance = KJTree(Parents: newCollection)
        kjtreeInstance.isInitiallyExpanded = false
        kjtreeInstance.animation = .fade
    }
    
    private func getParent(for dictionary: [String: Any])->Parent{
        let type = dictionary["type"] as! String
        
        if type == "product"{
            return Parent(key: type, data: dictionary)
        }
        
        if type == "box"{
            let products = dictionary["product"] as? [[String:Any]] ?? []
            return Parent(key: type, data: dictionary, childs: { () -> [Child] in
                return products.map{ data in
                    return Child(key: "product", data: data)
                }
            })
        }
        
        if type == "locker"{
            
            return Parent(key: "locker", data: dictionary, childs: { () -> [Child] in
                
                let products = dictionary["product"] as? [[String:Any]] ?? []
                
                return { ()->[Child] in
                        let boxs = dictionary["box"] as? [[String:Any]] ?? []
                        return boxs.map{ boxData in
                            let products = boxData["product"] as? [[String:Any]] ?? []
                            return Child(key: "box", data: boxData,  subChilds: { () -> [Child] in
                                return products.map{data in
                                    return Child(key: "product", data: data)
                                }
                            })
                        }
                    }() + { () -> [Child] in
                        
                        var prods =  products.map{data in
                            return Child(key: "product", data: data)
                        }
                        
                        if prods.count > 0{
                            prods.insert(Child(key: "sep_under_locker", data: [:]), at: 0)
                        }
                        
                        return prods
                }()
                
            })
        }
        return Parent(key: "unspecified", data: [:])
    }

    @IBAction func showBluePrint(_ sender: UISegmentedControl) {
        self.navigationController?.popViewController(animated:true)
    }
    
    func handleNotification(notification: Notification){
        //Take Action on Notification
        getDeckRecord()
    }
    
    //MARK:- API methods
    func getDeckRecord()
    {
        let dictParam = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                         "blueprint_deck_id":self.bluePrintDeckId,
                         "decks_pin_id":self.pinID]
        
        webService_obj.fetchDataFromServer(header: "getPinLockerBoxProduct", withParameter: dictParam as NSDictionary, inVC: self, showHud: true) { (responseDict, msg, status) in
            if status{
                //Create dictionary to make list
                self.completeData = responseDict.object(forKey: "data") as! [[String:Any]]
                
                self.pin_owner_id = responseDict.object(forKey: "pin_owner_id") as! String
                
                self.initTree(array: self.completeData)
                self.tblListing.reloadData()
            }
            else{
                //TODO: handle error or show user to alert
            }
        }
    }
    
      func deleteSelectedBlockWebService(selected_id:String,selected_type:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "type":selected_type,
                    "id":selected_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_inventory", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.getDeckRecord()
            }
        }
    }

}

extension ViewPinsDataVC{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let total = kjtreeInstance.tableView(tableView, numberOfRowsInSection: section)
        return total
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let node = kjtreeInstance.cellIdentifierUsingTableView(tableView, cellForRowAt: indexPath)
        let dataDictionary = node.dictionary
        
        //NSLog("Selected Node Dict-->\(dataDictionary)")
        let type = node.key
        
        if type == "sep_under_locker"
        {
            let cellIdentifierSep = "SepLineTableViewCellIdentity"
            let cellSep = tableView.dequeueReusableCell(withIdentifier: cellIdentifierSep) as! SepLineTableViewCell
            cellSep.labelSepText.text = "==========UNDER LOCKER=========="
             cellSep.selectionStyle = .none
            return cellSep

        }else if type == "sep_box"{
            let cellIdentifierSep = "SepLineTableViewCellIdentity"
            let cellSep = tableView.dequeueReusableCell(withIdentifier: cellIdentifierSep) as! SepLineTableViewCell
            cellSep.labelSepText.text = "=============================="
            cellSep.selectionStyle = .none
            return cellSep
        
        }else if type == "sep"{
         //   let cell = UITableViewCell(style: .default, reuseIdentifier: "12300")
         //   cell.backgroundColor = .blue
          //  return cell
            
            let cellIdentifierSep = "SepLineTableViewCellIdentity"
            let cellSep = tableView.dequeueReusableCell(withIdentifier: cellIdentifierSep) as! SepLineTableViewCell
            
            cellSep.labelSepText.text = "==========LOOSE PRODUCTS=========="
            cellSep.selectionStyle = .none
            return cellSep
            
        }else if type == "product"{
            let cellIdentifierChilds = "Childs3rdStageTableViewCellIdentity"
            let cellSubChild = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as! Childs3rdStageTableViewCell
            
            cellSubChild.cellFillUp(indexParam: node.index,dictRecord: dataDictionary as NSDictionary)
            cellSubChild.selectionStyle = .none
            
            cellSubChild.productclick = {
                let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"ManageInventoryDetailProductVC") as! ManageInventoryDetailProductVC
                
                let boxId = dataDictionary["locker_box_id"] as! String
                let lockerId = dataDictionary["pins_locker_id"] as! String
                
                controller.dictProductDetail = dataDictionary as NSDictionary
                controller.deck_id = self.bluePrintDeckId
                controller.pin_id = self.pinID
                controller.box_id = boxId
                controller.locker_id = lockerId
                controller.isViewToAddProduct = false
                controller.isFromQuickView = "false"
                controller.push()
            }
            
            cellSubChild.viewImageclick = {
                let newImageView = UIImageView(image: cellSubChild.imageviewProduct.image)
                let viewNew = self.tblListing.superview!.superview!.superview!.superview!.superview!
                
                newImageView.frame = viewNew.frame
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                viewNew.addSubview(newImageView)
            }
            
            cellSubChild.restockclick = {
                //TODO: Add product using existing product
                
                let boxId = dataDictionary["locker_box_id"] as! String
                let lockerId = dataDictionary["pins_locker_id"] as! String
                let maximumStockCount = Int((dataDictionary["max_qty"] as? String)!)!
                let currentStockCount = Int((dataDictionary["qty"] as? String)!)!
                
                var cartQty = maximumStockCount - currentStockCount
                
                if cartQty < 0{
                    cartQty = 0
                }
                
                //only call webservice when cartQty > 0, else donothing
                guard cartQty > 0 else {
                    
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:"Restock quantity is zero please check the maximum and current quantity.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                    
                    print("No need to restock item")
                    return
                }
                
                if dataDictionary["product_id"] as! String == "0"
                {
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:"MYS Product entered manually, find vendor under products.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }else{
                     
                    PostData = ["customer_id":self.userId,
                                "blueprint_deck_id":self.bluePrintDeckId,
                                "decks_pin_id":self.pinID,
                                "id":dataDictionary["id"] as? String ?? "0",
                                "cart_qty":"\(cartQty)",
                        "pins_locker_id":lockerId,
                        "locker_box_id":boxId,
                        "qty":dataDictionary["qty"] as? String ?? "",
                        "min_qty":dataDictionary["min_qty"] as? String ?? "",
                        "product_id":dataDictionary["product_id"] as! String,
                        "product_variant_id":dataDictionary["product_variant_id"] as! String,
                        "max_qty":dataDictionary["max_qty"] as? String ?? ""]
                    
                    webService_obj.fetchDataFromServer(header:"restock", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                        if staus{
                            
                            //Show alert Message
                            let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                            
                            let action = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction) in
                            }
                            let actionCart = UIAlertAction(title: "Go To Cart", style: .default) { (action:UIAlertAction) in
                                let st = UIStoryboard.init(name: "Cart", bundle: nil)
                                let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
                                controller.push()
                            }
                            alertMessage.addAction(action)
                            alertMessage.addAction(actionCart)
                            self.present(alertMessage, animated: true, completion: nil)
                        }
                    }
                }
            }
            return cellSubChild
            
        }else if type == "box"{
            //Box Section
            let cellIdentifierChilds = "Childs2ndStageTableViewCellIdentity"
            let cellChild = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as! Childs2ndStageTableViewCell
            
            cellChild.cellFillUp(indexParam: node.index,dictRecord: dataDictionary as NSDictionary)
            
            cellChild.selectionStyle = .none
            
            if node.state == .open {
                cellChild.buttonState.setImage(UIImage(named: "arrowup"), for: .normal)
            }else if node.state == .close {
                cellChild.buttonState.setImage(UIImage(named: "arrowdown"), for: .normal)
            }else{
                cellChild.buttonState.setImage(nil, for: .normal)
            }
            
            //cellChild.btnAddProduct.addTarget(self, action: #selector(self.addProductClicked(button:)), for: .touchUpInside)
            
            cellChild.productclick = {
                
                let boxId = dataDictionary["id"] as! String
                let lockerId = dataDictionary["pins_locker_id"] as! String
                
                let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"AddItemVC") as! AddItemVC
                controller.deck_id = self.bluePrintDeckId
                controller.pin_id = self.pinID
                controller.box_id = boxId
                controller.locker_id = lockerId
                controller.isForPin = "false"
                controller.isFromQuickView = "false"
                controller.push()
            }
            
            return cellChild
            
        }else{
            //Locker Section
            let cellIdentifierParents = "ParentsTableViewCellIdentity"
            let cellParents = tableView.dequeueReusableCell(withIdentifier: cellIdentifierParents) as! ParentsTableViewCell
            
            cellParents.cellFillUp(indexParam: node.index, tupleCount:0, dictRecord: dataDictionary as NSDictionary)
            
            cellParents.selectionStyle = .none
            
            if node.state == .open {
                cellParents.buttonState.setImage(UIImage(named: "arrowwhiteup"), for: .normal)
            }else if node.state == .close {
                cellParents.buttonState.setImage(UIImage(named: "arrowdownquantiy"), for: .normal)
            }else{
                cellParents.buttonState.setImage(nil, for: .normal)
            }
            
           // cellParents.btnAddBox.tag = baseTagForAddLockerButton + indexPath.row
           // cellParents.btnAddBox.addTarget(self, action: #selector(self.addBoxClicked(button:)), for: .touchUpInside)
            
            cellParents.boxclick = {
                // Create a custom view controller
                let saveLocker = SaveLocker(nibName: "SaveLocker", bundle: nil)
                saveLocker.isFrom = "2"
                // Create the dialog
                let popup = PopupDialog(viewController: saveLocker, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
                
                // Create first button
                _ = CancelButton(title: "CANCEL", height: 0) {
                    // self.label.text = "You canceled the rating dialog"
                }
                
                // Create second button
                _ = DefaultButton(title: "RATE", height: 0) {
                    // self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
                }
                
                saveLocker.bluePrintDeckId = self.bluePrintDeckId
                saveLocker.deckPinId = self.pinID
                
                let lockerId = dataDictionary["id"] as! String
                saveLocker.pinLockerId = lockerId
                
                 self.present(popup, animated:true, completion: nil)

            }
             //cellParents.btnAddProduct.tag = baseTagForAddProductButton + indexPath.row
          //  cellParents.btnAddProduct.addTarget(self, action: #selector(self.addProductClicked(button:)), for: .touchUpInside)
            
            cellParents.productclick = {
                
                let lockerId = dataDictionary["id"] as! String
                
                let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"AddItemVC") as! AddItemVC
                controller.deck_id = self.bluePrintDeckId
                controller.pin_id = self.pinID
                controller.locker_id = lockerId
                controller.isForPin = "false"
                controller.isFromQuickView = "false"
                controller.push()
            }

            
            return cellParents
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let node = kjtreeInstance.tableView(tableView, didSelectRowAt: indexPath)
        print(node.index)
        print(node.key)
        // if you've added any identifier or used indexing format
        print(node.givenIndex)
     }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let node = kjtreeInstance.cellIdentifierUsingTableView(tableView, cellForRowAt: indexPath)
        //let dataDictionary = node.dictionary
        
        //NSLog("Selected Node Dict-->\(dataDictionary)")
        let type = node.key
        
        if type == "sep_under_locker"
        {
            return false
        }else if type == "sep_box"
        {
            return false
        }
        else if type == "sep"{
            return false
        }
        else
        {
            return true
        }
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if (editingStyle == UITableViewCellEditingStyle.delete) {
//           
//        }
//    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let node = kjtreeInstance.cellIdentifierUsingTableView(tableView, cellForRowAt: indexPath)
        let type = node.key
        let dataDictionary = node.dictionary
        
        //Inventory Sharing
        let current_user_id = dataDictionary["owner_id"] as! String
        
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { action, index in
            
            self.selectedEditID = dataDictionary["id"] as! String
            self.seletedEditType = type
            
            let name = dataDictionary["name"] as! String
            
            self.editType = "table data"
            
            self.viewTransBack.isHidden = false
            self.viewAddDescriptionToPin.isHidden = false
            self.btnRemovePin.isHidden = false
            self.btnRemovePin.isHidden = true
            
            var description = ""
            description = name
            self.txtFldPinDescription.text = description
            
            print("edit button tapped")
        }
        edit.backgroundColor = UIColor.blue
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            NSLog("Selected Node Dict-->\(dataDictionary)")
            
            let selectedID = dataDictionary["id"] as! String
            let selectedType = type
            
            self.deleteSelectedBlockWebService(selected_id: selectedID, selected_type: selectedType)
            print("delete button tapped")
        }
        delete.backgroundColor = UIColor.red
        
        if isSharingUser == true
        {
            if self.userId == current_user_id{
                if type == "product"
                {
                    return [delete]
                }else
                {
                    return [edit,delete]
                }
            }else{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
                
                return nil
            }

        }else{
            if type == "product"
            {
                return [delete]
            }else
            {
                return [edit,delete]
            }

        }
    }

    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnMainProductClick(_ sender: Any) {
        //storyboard id : "AddItemVC"
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"AddItemVC") as! AddItemVC
        controller.deck_id = self.bluePrintDeckId
        controller.pin_id = self.pinID
        controller.isForPin = "true"
        controller.isFromQuickView = "false"
        controller.push()
    }
    
    @IBAction func btnMainLockerClick(_ sender: Any) {
        NSLog("Add Locker Clicked!")
        
        // Create a custom view controller
        let saveLocker = SaveLocker(nibName: "SaveLocker", bundle: nil)
        saveLocker.isFrom = "1"

        saveLocker.bluePrintDeckId = self.bluePrintDeckId
        saveLocker.deckPinId = self.pinID
        
        // Create the dialog
        let popup = PopupDialog(viewController: saveLocker, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        _ = CancelButton(title: "CANCEL", height: 0) {
            // self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        _ = DefaultButton(title: "RATE", height: 0) {
            // self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
        }
        
        // Present dialog
        present(popup, animated:true, completion: nil)
      }
    
    @IBAction func btnMainBoxClick(_ sender: Any) {
        // Create a custom view controller
        let saveLocker = SaveLocker(nibName: "SaveLocker", bundle: nil)
        saveLocker.isFrom = "2"
        // Create the dialog
        let popup = PopupDialog(viewController: saveLocker, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        _ = CancelButton(title: "CANCEL", height: 0) {
            // self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        _ = DefaultButton(title: "RATE", height: 0) {
            // self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
        }
        
        saveLocker.bluePrintDeckId = self.bluePrintDeckId
        saveLocker.deckPinId = self.pinID
        
        present(popup, animated:true, completion: nil)
    }
    
     @IBAction func editButtonOnCalloutTapped(_ sender: Any) {
        self.editType = "pin"
        viewTransBack.isHidden = false
        viewAddDescriptionToPin.isHidden = false
        btnRemovePin.isHidden = false
        var description = ""
        description = self.pinName
        txtFldPinDescription.text = description
    }
    
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        txtFldPinDescription.resignFirstResponder()
        
        viewAddDescriptionToPin.isHidden = true
        viewTransBack.isHidden = true
        
        
        //local validation
        guard txtFldPinDescription.text!.trimmingCharacters(in: .whitespacesAndNewlines).length != 0 else {
            
            //Show error
            let alertVC = UIAlertController(title: "Error", message: "Provide pin description", preferredStyle: .alert)
            
            let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                alertVC.dismiss(animated: true, completion: nil)
            })
            
            alertVC.addAction(alertActionOK)
            
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        
        //Add description to selected pin's description
            
            //Call API to Edit title of pin
            //Getting pin id
             let pinId = self.pinID
            
            //Getting pin new title
            let pinTitle = txtFldPinDescription.text!
        
        
            
            //Making editPin Param
            let editPinParamDict = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                                    "decks_pin_id":pinId,
                                    "title":pinTitle]
        
        
        let editOtherParamDict = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                                  "id":self.selectedEditID,
                                  "type":self.seletedEditType,
                                  "name":pinTitle]
        
        if self.editType == "pin"{
            webService_obj.fetchDataFromServer(alertMsg: false,header:editPinToDeck, withParameter: editPinParamDict as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
                if staus{
                    print("pin edited successfully with title")
                    
                    //Show alert of sucess and on completion , refresh all the pins on screen
                    let alertVC = UIAlertController(title: "Success", message: "Pin updated successfully!", preferredStyle: .alert)
                    
                    let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        //call code to refresh UI
                        // self.refershUIToUpdate()
                        
                        let data = responce.value(forKey: "data") as! [String:String]
                        self.lblHeader.text = data["title"]
                        self.pinName = data["title"]!
                        // self.getDeckRecord()
                        
                        //dismiss the alert
                        alertVC.dismiss(animated: true, completion: nil)
                    })
                    
                    alertVC.addAction(alertActionOK)
                    
                    self.present(alertVC, animated: true, completion: nil)
                }else
                {
                }
            }
        }else
        {
            webService_obj.fetchDataFromServer(alertMsg: false,header:"edit_inventory", withParameter: editOtherParamDict as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
                if staus{
                    print("pin edited successfully with title")
                    
                    //Show alert of sucess and on completion , refresh all the pins on screen
                    let alertVC = UIAlertController(title: "Success", message: message as String, preferredStyle: .alert)
                    
                    let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                       self.getDeckRecord()
                       alertVC.dismiss(animated: true, completion: nil)
                    })
                    
                    alertVC.addAction(alertActionOK)
                    
                    self.present(alertVC, animated: true, completion: nil)
                }else
                {
                }
            }
        }
        txtFldPinDescription.text = ""
    }
    
    @IBAction func btnRemovePinTapped(_ sender: UIButton) {
        
        if isSharingUser == true
        {
            if pin_owner_id == self.userId
            {
                
                txtFldPinDescription.resignFirstResponder()
                
                viewAddDescriptionToPin.isHidden = true
                viewTransBack.isHidden = true
                
                //Confirm from user to remove pinView
                let alert = UIAlertController(title: "Confirm", message: "Do you want to continue?", preferredStyle: .alert)
                
                let alertActionCance = UIAlertAction(title: "CANCEL", style: .cancel) { (action) in
                    alert.dismiss(animated: true, completion: nil)
                }
                
                let alertActionOk = UIAlertAction(title: "OK", style: .default) { (action) in
                    
                    alert.dismiss(animated: true, completion: {
                        
                    })
                    
                    //Call API to delet pin
                    //Making editPin Param
                    let editPinParamDict = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                                            "decks_pin_id":self.pinID]
                    
                    webService_obj.fetchDataFromServer(alertMsg: false,header:removeDeckPin, withParameter: editPinParamDict as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                        if staus{
                            print("pin removed successfully")
                            
                            //Show alert of sucess and on completion , refresh all the pins on screen
                            let alertVC = UIAlertController(title: "Success", message: "Pin removed successfully!", preferredStyle: .alert)
                            
                            let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                //call code to refresh UI
                                _=self.navigationController?.popViewController(animated: true)
                                
                                //dismiss the alert
                                alertVC.dismiss(animated: true, completion: nil)
                            })
                            
                            alertVC.addAction(alertActionOK)
                            
                            self.present(alertVC, animated: true, completion: nil)
                        }
                        else
                        {
                            //TODO: handle remove pin fail api response
                        }
                    }
                }
                
                alert.addAction(alertActionCance)
                alert.addAction(alertActionOk)
                
                self.present(alert, animated: true, completion: nil)
            }else{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }else{
            
            txtFldPinDescription.resignFirstResponder()
            
            viewAddDescriptionToPin.isHidden = true
            viewTransBack.isHidden = true
            
            //Confirm from user to remove pinView
            let alert = UIAlertController(title: "Confirm", message: "Do you want to continue?", preferredStyle: .alert)
            
            let alertActionCance = UIAlertAction(title: "CANCEL", style: .cancel) { (action) in
                alert.dismiss(animated: true, completion: nil)
            }
            
            let alertActionOk = UIAlertAction(title: "OK", style: .default) { (action) in
                
                alert.dismiss(animated: true, completion: {
                    
                })
                
                //Call API to delet pin
                //Making editPin Param
                let editPinParamDict = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                                        "decks_pin_id":self.pinID]
                
                webService_obj.fetchDataFromServer(alertMsg: false,header:removeDeckPin, withParameter: editPinParamDict as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                    if staus{
                        print("pin removed successfully")
                        
                        //Show alert of sucess and on completion , refresh all the pins on screen
                        let alertVC = UIAlertController(title: "Success", message: "Pin removed successfully!", preferredStyle: .alert)
                        
                        let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            //call code to refresh UI
                            _=self.navigationController?.popViewController(animated: true)
                            
                            //dismiss the alert
                            alertVC.dismiss(animated: true, completion: nil)
                        })
                        
                        alertVC.addAction(alertActionOK)
                        
                        self.present(alertVC, animated: true, completion: nil)
                    }
                    else
                    {
                        //TODO: handle remove pin fail api response
                    }
                }
            }
            
            alert.addAction(alertActionCance)
            alert.addAction(alertActionOk)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnCrossTapped(_ sender: UIButton) {
        //hide addlocation form
        txtFldPinDescription.resignFirstResponder()
        btnRemovePin.isHidden = true
        
        viewAddDescriptionToPin.isHidden = true
        viewTransBack.isHidden = true
        txtFldPinDescription.text = ""
    }

    
    @objc func addLockerClicked(button: UIButton)
    {
        NSLog("Add Box Clicked!")
        
        // Create a custom view controller
        let saveLocker = SaveLocker(nibName: "SaveLocker", bundle: nil)
        saveLocker.isFrom = "1"
        
        //pass deck_id and pin_id
        let dataDict = dictPinRecord.object(forKey: "data") as! NSDictionary
        let deckId = dataDict.object(forKey: "id") as! String
        
        let selectedPinIndex = button.tag - baseTagForAddLockerButton
        let pinArray = dataDict.object(forKey: "pin") as! NSArray
        let selectedPinRecord = pinArray.object(at: selectedPinIndex) as! NSDictionary
        let pinId = selectedPinRecord.object(forKey: "id") as! String
        
        saveLocker.bluePrintDeckId = deckId
        saveLocker.deckPinId = pinId
        
        // Create the dialog
        let popup = PopupDialog(viewController: saveLocker, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        _ = CancelButton(title: "CANCEL", height: 0) {
            // self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        _ = DefaultButton(title: "RATE", height: 0) {
            // self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
        }
        
        // Add buttons to dialog
        // popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated:true, completion: nil)
        
    }
    
    
    @objc func addBoxClicked(button: UIButton)
    {
        // Create a custom view controller
        let saveLocker = SaveLocker(nibName: "SaveLocker", bundle: nil)
        saveLocker.isFrom = "2"
        // Create the dialog
        let popup = PopupDialog(viewController: saveLocker, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        _ = CancelButton(title: "CANCEL", height: 0) {
            // self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        _ = DefaultButton(title: "RATE", height: 0) {
            // self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
        }
        
        // Add buttons to dialog
        // popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        
        //Getting Back pinIndex,lockerIndex from button.tag
        let buttonTag = button.tag - baseTagForAddBoxbutton
        let pinIndex = buttonTag / 100
        let lockerIndex = buttonTag % 100
        
        //passing needed data to instance
        //passing deckId
        let dataDict = dictPinRecord.object(forKey: "data") as! NSDictionary
        let deckId = dataDict.object(forKey: "id") as! String
        saveLocker.bluePrintDeckId = deckId
        
        //passing pinId
        let pinArray = dataDict.object(forKey: "pin") as! NSArray
        let selectedPinRecord = pinArray.object(at: pinIndex) as! NSDictionary
        let pinId = selectedPinRecord.object(forKey: "id") as! String
        saveLocker.deckPinId = pinId
        
        //passing lockerId
        let lockerArray = selectedPinRecord.object(forKey: "locker") as! NSArray
        let selectedLocker = lockerArray.object(at: lockerIndex) as! NSDictionary
        let lockerId = selectedLocker.object(forKey: "id") as! String
        saveLocker.pinLockerId = lockerId
        
        present(popup, animated:true, completion: nil)
    }
    

}
