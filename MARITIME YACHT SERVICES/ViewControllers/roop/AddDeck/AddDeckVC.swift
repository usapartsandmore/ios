//  AddDeckVC.swift
//  MARITIME YACHT SERVICES
//  Created by Octal on 23/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.


import UIKit


extension UINavigationController {
    func popViewController(animated: Bool, completion: @escaping () -> ()) {
        popViewController(animated: animated)
        
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}

class AddDeckVC: UIViewController, UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
   @IBOutlet weak var tblListing: UITableView!
    @IBOutlet weak var imgViewToShowPickedImage: UIImageView!
    @IBOutlet weak var txtFldDeckName:UITextField!
    
    var deck_name = [String]()
    
    
    //property to get image from previous controller
    var pickedImg :UIImage?

    //MARK: - ViewController Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFldDeckName.addPaddingView(width: 10, onSide: 0)
         deck_name = ["Storage","Sun Deck","Boat Deck","Upper Deck","Main Deck","Lower Deck"]

        tblListing.reloadData()
        imgViewToShowPickedImage.image = pickedImg
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return deck_name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath)
        
        cell.textLabel?.text = self.deck_name[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //TODO: need to change dummy deck to actual one
        txtFldDeckName.text = self.deck_name[indexPath.row]
        
        //data is selected, so hide the tableview
        tblListing.isHidden = true
    }
    
    //MARK: - Textfield delegate methods
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //show tableview
        tblListing.isHidden = false
        return false
    }
    
    //MARK: - IBActions
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        
        let deckNameStrLenght = txtFldDeckName.text!.trimmingCharacters(in: .whitespacesAndNewlines).length
        
        guard txtFldDeckName.text != nil , deckNameStrLenght != 0 else {
            //show error message
            
            showAlert(title: "Error", msg: "Provide the deck name.", isToNavigate: false)
            
            return
        }
        
        //API to save deck
        let imgDict = ["image":pickedImg!]
        let paramDict = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                         "name":txtFldDeckName.text!,
                         "deck_id":""]
        
        
        Config().callAPiFor(subApiName: addBluePrintUrl, parameter: paramDict, images: imgDict, view: self.view!, inVC: self) { (response, msg, status) in
            
            print(msg)
            
            if status {
                //Show user the success Response and on click of OK, navigate user back
                self.showAlert(title: "Success", msg: msg as String,isToNavigate: true)
            }
            else{
                //TODO: error alert to user
            }
        }
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
       // _ = self.navigationController?.popViewController(animated:true)
        self.navigationController?.popViewController(animated: true, completion: {
            NotificationCenter.default.post(name: Notification.Name("setupViewAccordingToOperation"), object: nil
                , userInfo: ["isCrop":true])
        })
    }
    
    func showAlert(title:String,msg:String,isToNavigate:Bool)
    {
        let alertVC = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let alertActionOK = UIAlertAction(title: "OK", style: .default) { (action) in
            alertVC.dismiss(animated: true, completion: {
                
            })
            
            if isToNavigate{
                //Navigate user to cropView
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true, completion: {
                        NotificationCenter.default.post(name: Notification.Name("setupViewAccordingToOperation"), object: nil
                            , userInfo: ["isCrop":true,"title":self.txtFldDeckName.text!,"img":self.imgViewToShowPickedImage.image!])
                    })
                }
            }
        }
        
        alertVC.addAction(alertActionOK)
        
        self.present(alertVC, animated: true, completion: nil)
    }
}
