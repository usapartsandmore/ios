//  ExpandableTable.swift
//  MARITIME YACHT SERVICES
//  Created by Octal on 23/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.

import UIKit

class ExpandableTable: UIViewController{//UITableViewDataSource,UITableViewDelegate {

//    @IBOutlet weak var tblListing: UITableView!
//    // KJ Tree instances -------------------------
//    var arrayTree:[Parent] = []
//    var kjtreeInstance: KJTree = KJTree()
//    
//    let baseTagForAddLockerButton = 1000
//    let baseTagForAddBoxbutton = 5000
//    var bluePrintDeckId:String = ""
//    
//    var dictPinRecord:NSDictionary = NSMutableDictionary()
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view, typically from a nib.
//        
//        let filepath: String? = Bundle.main.path(forResource: "Tree", ofType: "json")
//        let url = URL(fileURLWithPath: filepath ?? "")
//        
//        var jsonData: Data?
//        do {
//            jsonData = try Data(contentsOf: url)
//        }catch{
//            print("error")
//        }
//        
//        var jsonDictionary: NSDictionary?
//        do {
//            jsonDictionary = try JSONSerialization.jsonObject(with: jsonData!, options: .init(rawValue: 0)) as? NSDictionary
//        }catch{
//            print("error")
//        }
//        
//        
//        /* --- old code ---
//        if let treeDictionary = jsonDictionary?.object(forKey: "Tree") as? NSDictionary {
//            if let arrayOfParents = treeDictionary.object(forKey: "Parents") as? NSArray {
//                arrayParents = arrayOfParents
//            }
//        }
//        
//        if let arrayOfParents = arrayParents {
//            kjtreeInstance = KJTree(parents: arrayOfParents, childrenKey: "Children", expandableKey: "Expanded", key: "Id")
//        }
//        */
//        
//        //New intializations
//        let treeDictionary = dictPinRecord.object(forKey: "data") as! NSDictionary
//        
//        createTree(treeDictionary: treeDictionary)
//        
//        tblListing.delegate = self
//        tblListing.dataSource = self
//        tblListing.rowHeight = UITableViewAutomaticDimension
//        tblListing.estimatedRowHeight = 44
//    }
//    
//    
//    
//    func createTree(treeDictionary:NSDictionary)
//    {
//        var arrayParents: NSArray?
//        
//        arrayParents = treeDictionary.object(forKey: "pin") as? NSArray
//        bluePrintDeckId = treeDictionary.object(forKey: "id") as! String
//        
//        //kjtreeInstance = KJTree(parents: arrayParents!, childrenKey: "locker", expandableKey: nil, key: "id")
//        //kjtreeInstance = KJTree(indices: ["1.1.1","1.2","2"])
//        
//        //Creating codebase to create tree
//        let pinCounts = arrayParents!.count
//        var arrCodeIndexes = [String]()
//        for i in 0 ..< pinCounts
//        {
//            let str = String(i+1)
//            let pinDict = arrayParents!.object(at: i) as! NSDictionary
//            let lockersArray = pinDict.object(forKey: "locker") as! NSArray
//            
//            if lockersArray.count == 0 {
//                arrCodeIndexes.append(str)
//            }
//            for j in 0 ..< lockersArray.count
//            {
//                let str2 = str.appending(".\(j+1)")
//                let lockerDict = lockersArray.object(at: j) as! NSDictionary
//                let boxArray = lockerDict.object(forKey: "box") as! NSArray
//                
//                if boxArray.count == 0 {
//                    arrCodeIndexes.append(str2)
//                }
//                for k in 0 ..< boxArray.count
//                {
//                    let str3 = str2.appending(".\(k+1)")
//                    arrCodeIndexes.append(str3)
//                }
//                
//            }
//        }
//        kjtreeInstance = KJTree(indices: arrCodeIndexes)
//        kjtreeInstance.isInitiallyExpanded = true
//        kjtreeInstance.animation = .fade
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        //adding current view as observer to notification
//       // getDeckRecord()
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(notification:)), name: Notification.Name("refreshList"), object: nil)
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self, name: Notification.Name("refreshList"), object: nil)
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    @IBAction func showBluePrint(_ sender: UISegmentedControl) {
//        self.navigationController?.popViewController(animated:true)
//    }
//    
//    func handleNotification(notification: Notification){
//        //Take Action on Notification
//        getDeckRecord()
//    }
//    
//    //MARK:- API methods
//    func getDeckRecord()
//    {
//        let dictParam = ["customer_id":webService_obj.Retrive("User_Id") as! String,
//                         "blueprint_deck_id":bluePrintDeckId]
//        
//        webService_obj.fetchDataFromServer(header: getDeckRecordForId, withParameter: dictParam as NSDictionary, inVC: self, showHud: true) { (responseDict, msg, status) in
//            if status{
//                //fill the array and reload the list
//                
//                //Create dictionary to make list
//                
//                DispatchQueue.main.async {
//                    //reload table
//                    print("Updated data got")
//                    
//                    self.dictPinRecord = NSMutableDictionary()
//                    let singleRecord = responseDict.object(forKey: "data") as! NSArray
//                    let dict = singleRecord.object(at: 0) as! NSDictionary
//                    self.dictPinRecord.setValue(dict, forKey: "data")
//                    //dictPinRecord = responseDict.object(forKey: "data")
//                    
//                    let treeDictionary = self.dictPinRecord.object(forKey: "data") as! NSDictionary
//                    self.createTree(treeDictionary: treeDictionary)
//                    self.tblListing.reloadData()
//                }
//            }
//            else{
//                //TODO: handle error or show user to alert
//            }
//        }
//    }
//    
//}
//
//extension ExpandableTable{
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let total = kjtreeInstance.tableView(tableView, numberOfRowsInSection: section)
//        return total
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let node = kjtreeInstance.cellIdentifierUsingTableView(tableView, cellForRowAt: indexPath)
//        let indexTuples = node.index.components(separatedBy: ".")
//        
//        if indexTuples.count == 1  || indexTuples.count == 4 {
//            
//            // Parents
//            let cellIdentifierParents = "ParentsTableViewCellIdentity"
//            var cellParents: ParentsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifierParents) as? ParentsTableViewCell
//            if cellParents == nil {
//                tableView.register(UINib(nibName: "ParentsTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierParents)
//                cellParents = tableView.dequeueReusableCell(withIdentifier: cellIdentifierParents) as? ParentsTableViewCell
//            }
//            cellParents?.cellFillUp(indexParam: node.index, tupleCount: indexTuples.count, dictRecord: dictPinRecord)
//            cellParents?.selectionStyle = .none
//            
//            if node.state == .open {
//                
//                cellParents?.buttonState.setImage(UIImage(named: "arrowup"), for: .normal)
//            }else if node.state == .close {
//                cellParents?.buttonState.setImage(UIImage(named: "arrowdown"), for: .normal)
//            }else{
//                 cellParents?.buttonState.setImage(nil, for: .normal)
//            }
//            
////            if (indexPath.row % 2 == 0) {
////                
////                cellParents?.imageviewBackground.backgroundColor = UIColor.groupTableViewBackground
////            }
////            else
////            {
////                cellParents?.imageviewBackground.backgroundColor = UIColor.white
////            }
//            
//            cellParents?.btnAddLocker.tag = baseTagForAddLockerButton + indexPath.row
//            cellParents?.btnAddLocker.addTarget(self, action: #selector(self.addLockerClicked(button:)), for: .touchUpInside)
//            
//            
//            
//            return cellParents!
//            
//        }else if indexTuples.count == 2{
//            
//            // Parents
//            let cellIdentifierChilds = "Childs2ndStageTableViewCellIdentity"
//            var cellChild: Childs2ndStageTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? Childs2ndStageTableViewCell
//            if cellChild == nil {
//                tableView.register(UINib(nibName: "Childs2ndStageTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierChilds)
//                cellChild = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? Childs2ndStageTableViewCell
//            }
//            
//            cellChild?.cellFillUp(indexParam: node.index,dictRecord:dictPinRecord)
//            cellChild?.selectionStyle = .none
//            
//            if node.state == .open {
//                cellChild?.buttonState.setImage(UIImage(named: "whiteUp"), for: .normal)
//            }else if node.state == .close {
//                cellChild?.buttonState.setImage(UIImage(named: "whiteDown"), for: .normal)
//            }else{
//                cellChild?.buttonState.setImage(nil, for: .normal)
//            }
//            
//             cellChild?.btnAddBox.addTarget(self, action: #selector(self.addBoxClicked(button:)), for: .touchUpInside)
//            
//            //Getting pinIndex and LockerIndex to addTag to AddBox button
//            let infoStr = node.index
//            let arrIndex = infoStr.components(separatedBy: ".")
//            
//            let pinIndex = Int(arrIndex[0])!
//            let lockerIndex = Int(arrIndex[1])!
//            
//             cellChild?.btnAddBox.tag = baseTagForAddBoxbutton + ((pinIndex * 100) + lockerIndex)
//            
//            return cellChild!
//            
//        }else if indexTuples.count == 3{
//            
//            // Parents
//            let cellIdentifierChilds = "Childs3rdStageTableViewCellIdentity"
//            var cellChild: Childs3rdStageTableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? Childs3rdStageTableViewCell
//            if cellChild == nil {
//                tableView.register(UINib(nibName: "Childs3rdStageTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifierChilds)
//                cellChild = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? Childs3rdStageTableViewCell
//            }
//            cellChild?.cellFillUp(indexParam: node.index,dictRecord: dictPinRecord)
//            cellChild?.selectionStyle = .none
//            
//            if node.state == .open {
//                cellChild?.buttonState.setImage(UIImage(named: "arrowup"), for: .normal)
//            }else if node.state == .close {
//                cellChild?.buttonState.setImage(UIImage(named: "arrowdown"), for: .normal)
//            }else{
//                cellChild?.buttonState.setImage(nil, for: .normal)
//            }
//            
//            return cellChild!
//            
//        }else{
//            // Childs
//            // grab cell
//            var tableviewcell = tableView.dequeueReusableCell(withIdentifier: "cellidentity")
//            if tableviewcell == nil {
//                tableviewcell = UITableViewCell(style: .default, reuseIdentifier: "cellidentity")
//            }
//            tableviewcell?.textLabel?.text = node.index
//            tableviewcell?.backgroundColor = UIColor.yellow
//            tableviewcell?.selectionStyle = .none
//            return tableviewcell!
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let node = kjtreeInstance.tableView(tableView, didSelectRowAt: indexPath)
//        print(node.index)
//        print(node.key)
//        // if you've added any identifier or used indexing format
//        print(node.givenIndex)
//    }
//    
//    @IBAction func btnBackPressed(_ sender: UIButton) {
//        _ = self.navigationController?.popViewController(animated:true)
//    }
//    
//    @objc func addLockerClicked(button: UIButton)
//    {
//        NSLog("Add Locker Clicked!")
//        
//        // Create a custom view controller
//        let saveLocker = SaveLocker(nibName: "SaveLocker", bundle: nil)
//        saveLocker.isFrom = "1"
//        
//        //pass deck_id and pin_id
//        let dataDict = dictPinRecord.object(forKey: "data") as! NSDictionary
//        let deckId = dataDict.object(forKey: "id") as! String
//        
//        let selectedPinIndex = button.tag - baseTagForAddLockerButton
//        let pinArray = dataDict.object(forKey: "pin") as! NSArray
//        let selectedPinRecord = pinArray.object(at: selectedPinIndex) as! NSDictionary
//        let pinId = selectedPinRecord.object(forKey: "id") as! String
//        
//        saveLocker.bluePrintDeckId = deckId
//        saveLocker.deckPinId = pinId
//        
//        // Create the dialog
//        let popup = PopupDialog(viewController: saveLocker, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
//        
//        // Create first button
//        let buttonOne = CancelButton(title: "CANCEL", height: 0) {
//           // self.label.text = "You canceled the rating dialog"
//        }
//        
//        // Create second button
//        let buttonTwo = DefaultButton(title: "RATE", height: 0) {
//           // self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
//        }
//        
//        // Add buttons to dialog
//       // popup.addButtons([buttonOne, buttonTwo])
//        
//        // Present dialog
//        present(popup, animated:true, completion: nil)
//        
//    }
//    
//    
//    @objc func addBoxClicked(button: UIButton)
//    {
//        // Create a custom view controller
//        let saveLocker = SaveLocker(nibName: "SaveLocker", bundle: nil)
//         saveLocker.isFrom = "2"
//        // Create the dialog
//        let popup = PopupDialog(viewController: saveLocker, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
//        
//        // Create first button
//        let buttonOne = CancelButton(title: "CANCEL", height: 0) {
//            // self.label.text = "You canceled the rating dialog"
//        }
//        
//        // Create second button
//        let buttonTwo = DefaultButton(title: "RATE", height: 0) {
//            // self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
//        }
//        
//        // Add buttons to dialog
//        // popup.addButtons([buttonOne, buttonTwo])
//        
//        // Present dialog
//        
//        //Getting Back pinIndex,lockerIndex from button.tag
//        let buttonTag = button.tag - baseTagForAddBoxbutton
//        let pinIndex = buttonTag / 100
//        let lockerIndex = buttonTag % 100
//        
//        //passing needed data to instance
//        //passing deckId
//        let dataDict = dictPinRecord.object(forKey: "data") as! NSDictionary
//        let deckId = dataDict.object(forKey: "id") as! String
//        saveLocker.bluePrintDeckId = deckId
//        
//        //passing pinId
//        let pinArray = dataDict.object(forKey: "pin") as! NSArray
//        let selectedPinRecord = pinArray.object(at: pinIndex) as! NSDictionary
//        let pinId = selectedPinRecord.object(forKey: "id") as! String
//        saveLocker.deckPinId = pinId
//        
//        //passing lockerId
//        let lockerArray = selectedPinRecord.object(forKey: "locker") as! NSArray
//        let selectedLocker = lockerArray.object(at: lockerIndex) as! NSDictionary
//        let lockerId = selectedLocker.object(forKey: "id") as! String
//        saveLocker.pinLockerId = lockerId
//        
//        present(popup, animated:true, completion: nil)
//    }
}
