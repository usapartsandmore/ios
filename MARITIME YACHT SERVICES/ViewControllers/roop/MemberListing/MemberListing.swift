//  MemberListing.swift
//  MARITIME YACHT SERVICES
//  Created by Octal on 24/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.

import UIKit
import Contacts

class MemberCell : UITableViewCell{
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnSelect: UIButton!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected{
            btnSelect.setImage(UIImage(named: "rightsymbolcheck"), for: .normal)
        }else{
            btnSelect.setImage(UIImage(named: "rightsymbol"), for: .normal)
        }
    }
}


class MemberListing: UIViewController , UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate{
    
    @IBOutlet weak var memberSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var indexTitlesArray = NSMutableArray()
    var cnContacts = [CNContact]()
    
    var completedData = [[String:String]]()
    var filteredData = [[String:String]]()
    var selectedArr = [[String:Any]]()
    var selectedDictblock: (([[String : String]])->Void)?
    
    var selectedEmail = [String]()
    
    var isfromMembers = ""
    
    var team_id = ""
    var team_name = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        indexTitlesArray = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
        
        
        // MARK: - Fetch Contact List (CNContact)
        let contactStore = CNContactStore()
        
        contactStore.requestAccess(for: .contacts, completionHandler: {
            granted, error in
            
            guard granted else {
                let alert = UIAlertController(title: "Can't access contact", message: "Please go to Settings -> MYS to enable contact permission", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        })
        do {
            try contactStore.enumerateContacts(with: CNContactFetchRequest(keysToFetch: [CNContactGivenNameKey as CNKeyDescriptor, CNContactEmailAddressesKey as CNKeyDescriptor])) {
                (contact, cursor) -> Void in
                if (!contact.emailAddresses.isEmpty){
                    
                    self.cnContacts.append(contact)
                    //Add to your array
                }
            }
        }
        catch{
            print("Handle the error please")
        }
        
 
        for contact in self.cnContacts {
            //print(contact)
            let firstName = contact.givenName
           // nameArray.append(firstName)
            print("first:\(firstName)")
            
            let emailAddress = contact.emailAddresses[0].value(forKey: "value")
            //emailArray.append(emailAddress as! String)
            
            NSLog("\(contact.givenName): \(String(describing: emailAddress))")
            
            let dict = ["name":firstName,
                        "email":emailAddress as! String]
            filteredData.append(dict)
            completedData.append(dict)
         }
        
        self.selectedEmail = self.selectedArr.map{($0["email"] as! String)}
    
        var selectedindexPaths = [IndexPath]()
        for (index,dict) in filteredData.enumerated() {
            if selectedEmail.contains(dict["email"]!){
                selectedindexPaths.append(IndexPath(row: index, section: 0))
            }
        }
        
        selectedindexPaths.forEach { (indexPath) in
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addMemberPressed(_ sender: UIButton) {
        
        var seletedDict = [[String:String]]()
        
        for (index,dict) in self.completedData.enumerated() {
            for (_,email) in self.selectedEmail.enumerated() {
                if dict["email"] == email
                {
                    seletedDict.append(self.completedData[index])
                }
            }
        }

        if self.isfromMembers == "true"
        {
            if seletedDict.count != 0{
                // var arrMembers = [[String:String]]()
                // for indexPath in rows{
                //      arrMembers.append(filteredData[indexPath.row])
                // }
                self.updateMembersWebService(selectedMembers: seletedDict)
            }else
            {
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Please select members.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
            
        }else{
            //  if let rows = tableView.indexPathsForSelectedRows{
            //     var arr = [[String:String]]()
            //      for indexPath in rows{
            //          arr.append(filteredData[indexPath.row])
            //     }
            selectedDictblock?(seletedDict)
            // }
            self.navigationController?.popViewController(animated: true);
        }
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filteredData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as! MemberCell
        
        cell.lblName.text = filteredData[indexPath.row]["name"]
        cell.lblEmail.text = filteredData[indexPath.row]["email"]
        
        
        if self.selectedEmail.contains(self.filteredData[indexPath.row]["email"]!)
        {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
        else{
            tableView.deselectRow(at: indexPath, animated: false)
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView,willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//       
//        if let indexPathForSelectedRow = tableView.indexPathsForSelectedRows {
//            if (indexPathForSelectedRow.contains(indexPath)) {
//                tableView.deselectRow(at: indexPath as IndexPath, animated: false)
//                if self.selectedEmail.contains(self.filteredData[indexPath.row]["email"]!)
//                {
//                    if let index = self.selectedEmail.index(of: self.filteredData[indexPath.row]["email"]!) {
//                        self.selectedEmail.remove(at: index)
//                    }
//                }
//
//                return nil
//            }
//        }
//        
//         return indexPath
//     }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.selectedEmail.contains(self.filteredData[indexPath.row]["email"]!){
             let index = self.selectedEmail.index(of: self.filteredData[indexPath.row]["email"]!)
             self.selectedEmail.remove(at: index!)
         }else{
             self.selectedEmail.append(self.filteredData[indexPath.row]["email"]!)
         }
         self.tableView.reloadData()
     }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if self.selectedEmail.contains(self.filteredData[indexPath.row]["email"]!)
//        {
//            if let index = self.selectedEmail.index(of: self.filteredData[indexPath.row]["email"]!) {
//                self.selectedEmail.remove(at: index)
//            }
//         }
     }
    
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return nil//indexTitlesArray as? [String]
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
          filteredData = searchText.isEmpty ? completedData : completedData.filter({(dataString) -> Bool in
             return dataString["name"]!.range(of: searchText, options: .caseInsensitive) != nil
        })
         tableView.reloadData()
    }
    
    
    // MARK: - Web-Service Methods
    func updateMembersWebService(selectedMembers:[[String:String]]) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":self.team_id,
                    "members":selectedMembers,
                    "name":self.team_name]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "update_myteam", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                 _ = self.navigationController?.popViewController(animated:true)
            }
        }
    }

    
 }
