//  DeckLevelCell.swift
//  MARITIME YACHT SERVICES
//  Created by Octal on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.

import UIKit
import SDWebImage

class DeckLevelCell: UITableViewCell {
    
    @IBOutlet weak var lblDeckName: UILabel!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var imgDeck: UIImageView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    var deckImg:UIImage? = UIImage()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupCellFor(_ dictRecord:NSDictionary,tableview:UITableView)
    {
        
        //self.loadingView.isHidden = true
        self.lblDeckName.text = dictRecord.object(forKey: "name") as? String
        
        self.imgDeck.image = nil
        self.loadingView.startAnimating()
        
        //using alomafire
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: String(describing: dictRecord["image"]!)))")!
        
        //self.imgDeck.af_setImage(withURL: url as URL)
        
        self.imgDeck.sd_setImage(with: url as URL, placeholderImage: nil, options: SDWebImageOptions(rawValue: 0)) { (img, err, type, url) in
            self.imgDeck.image = img
            self.deckImg = img
            self.loadingView.stopAnimating()
        }
    }

}
