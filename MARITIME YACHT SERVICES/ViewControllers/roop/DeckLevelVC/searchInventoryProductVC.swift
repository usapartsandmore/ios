//
//  searchInventoryProductVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 27/04/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class InventorySearchProductCell: UITableViewCell{
    
    @IBOutlet weak var imageviewBackground: UIImageView!
    //@IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet var imageviewProduct : UIImageView!
    @IBOutlet var lblProductTitle: UILabel!
    @IBOutlet var lblServiceProviderName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblStockQuantity: UILabel!
    @IBOutlet var lblMinimum: UILabel!
    @IBOutlet var lblMaximum: UILabel!
    @IBOutlet var lblProductId: UILabel!
    @IBOutlet var lblNotesCount: UILabel!
    
    @IBOutlet var lblDeckName: UILabel!
    @IBOutlet var lblPinName: UILabel!
    @IBOutlet var lblLockerName: UILabel!
    @IBOutlet var lblBoxName: UILabel!
    
    @IBOutlet weak var buttonState: UIButton!
    @IBOutlet weak var btnViewImage: UIButton!
    
}

class searchInventoryProductVC: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var vandorListTableview: UITableView!
    
    var filteredData = [[String:String]]()
    var webServiceData = [[String:String]]()
    var sharedCustomer_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.vandorListTableview.tableFooterView = UIView()
        self.searchBar.text = ""
        self.webServiceData.removeAll()
        self.vandorListTableview.reloadData()
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func backBtnPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.webServiceData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"InventorySearchProductCell") as! InventorySearchProductCell
        cell.selectionStyle = .none
        
        let dictRecord = self.webServiceData[indexPath.row]
 
        cell.lblProductTitle.text = dictRecord["name"]
        cell.lblServiceProviderName.text = dictRecord["vender_name"]
        
        if dictRecord["price"] == "N/A"{
            cell.lblPrice.text = dictRecord["price"]
        }else{
            cell.lblPrice.text = "$\(String(describing: dictRecord["price"]!))"
        }
 
        cell.lblNotesCount.text =  dictRecord["noteCount"]
        cell.lblStockQuantity.text =  dictRecord["qty"]
        cell.lblMinimum.text = dictRecord["min_qty"]
        cell.lblMaximum.text = dictRecord["max_qty"]
        cell.lblProductId.text = "# \(String(describing: dictRecord["id"]!))"
        
        if dictRecord["deck_name"] == "" {
             cell.lblDeckName.text =  "N/A"
        }else{
             cell.lblDeckName.text =  dictRecord["deck_name"]
        }
        
        if dictRecord["deck_pin_name"] == "" {
            cell.lblPinName.text =  "N/A"
        }else{
            cell.lblPinName.text =  dictRecord["deck_pin_name"]
        }
        
        if dictRecord["pins_locker_name"] == "" {
            cell.lblLockerName.text =  "N/A"
        }else{
            cell.lblLockerName.text =  dictRecord["pins_locker_name"]
        }
        
        if dictRecord["locker_box_name"] == "" {
            cell.lblBoxName.text =  "N/A"
        }else{
            cell.lblBoxName.text =  dictRecord["locker_box_name"]
        }
 
        //imageview
        let imgUrl = dictRecord["image"]
        if imgUrl == ""
        {
            cell.imageviewProduct.image = UIImage(named:"noimage")
        }else{
            let url:NSURL = NSURL(string :"\(imageURL)\(imgUrl ?? "")")!
            cell.imageviewProduct.af_setImage(withURL: url as URL)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        
        let dataDictionary = self.webServiceData[indexPath.row]
        
        let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ManageInventoryDetailProductVC") as! ManageInventoryDetailProductVC
        
        let boxId = dataDictionary["locker_box_id"]
        let lockerId = dataDictionary["pins_locker_id"]
        
        controller.dictProductDetail = dataDictionary as NSDictionary
        controller.deck_id = dataDictionary["blueprint_deck_id"]!
        controller.pin_id = dataDictionary["decks_pin_id"]!
        controller.box_id = boxId!
        controller.locker_id = lockerId!
        controller.isViewToAddProduct = false
        controller.isFromQuickView = "false"
        controller.isFromsearchInventory = true
        controller.push()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: SearchBar Delegate Method
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            webServiceData.removeAll()
        }else{
            self.searchInventoryProductListWebServices(searchtxt: searchText)
        }
        self.vandorListTableview.reloadData()
    }
  
    
    func searchInventoryProductListWebServices(searchtxt: String)  {
 
        if isSharingUser == true
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "shared_customer_id":self.sharedCustomer_id,
                        "search":searchtxt,
                        "page":"-1"]
        }else{
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "shared_customer_id":webService_obj.Retrive("User_Id") as! String,
                        "search":searchtxt,
                        "page":"-1"]
        }
        
        webService_obj.fetchDataFromServer(alertMsg:false, header:"search_inventory_product", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                self.webServiceData.removeAll()
                self.webServiceData = responce.value(forKey: "data") as! [[String:String]]
                self.vandorListTableview.reloadData()
            }else{
                self.webServiceData.removeAll()
                self.vandorListTableview.reloadData()
            }
        }
    }
  }

