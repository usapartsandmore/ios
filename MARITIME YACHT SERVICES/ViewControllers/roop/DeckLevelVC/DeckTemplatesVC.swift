//
//  DeckTemplatesVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 09/03/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class DeckTempCell : UITableViewCell{
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgBack: UIImageView!
    @IBOutlet var imgDeck: UIImageView!
    @IBOutlet var btnSelect: UIButton!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected{
            btnSelect.setImage(UIImage(named: "rightsymbolcheck"), for: .normal)
        }else{
            btnSelect.setImage(UIImage(named: "rightsymbol"), for: .normal)
        }
    }
}

class DeckTemplatesVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var tblview: UITableView!
    
    var decktempArr = [String]()
    var selectedRows = [String]()
    
    var isforFirstTime = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        decktempArr = ["Storage","Sun Deck","Boat Deck","Main Deck","Lower Deck"]
     }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tblview.reloadData()
        
//        var indexPaths = [IndexPath]()
//        if selectedDeckNames.count > 0{
//            self.selectedRows = selectedDeckNames
//        }
//        
//        for (index, selectedName) in self.decktempArr.enumerated(){
//            if self.selectedRows.contains(selectedName){
//                indexPaths.append(IndexPath(row: index, section: 0))
//            }
//        }
//        
//        indexPaths.forEach { (indexPath) in
//            self.tblview.selectRow(at: indexPath, animated: false, scrollPosition: .none)
//        }
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        if let rows = tblview.indexPathsForSelectedRows{
            //var data = [[String:String]]()
            for indexPath in rows
            {
                let maindict = self.decktempArr[indexPath.row]
                
                if self.selectedRows.count > 0{
                    if self.selectedRows.contains(maindict){
                    }else{
                        selectedRows.append(maindict)
                    }
                }else{
                    selectedRows.append(maindict)
                }
            }
            self.updateDecksWebService()
        }else{
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Please select deck level. ", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return decktempArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeckTempCell", for: indexPath) as! DeckTempCell
        
        cell.lblName.text = decktempArr[indexPath.row]
   
        
        switch indexPath.row {
        case 0: cell.imgBack.image = UIImage(named: "storage")
                cell.imgDeck.image = UIImage(named: "storageDesk")
            break
        case 1: cell.imgBack.image = UIImage(named: "sun_deck")
                cell.imgDeck.image = UIImage(named: "sundesk")
            break
        case 2: cell.imgBack.image = UIImage(named: "boat_deck")
                cell.imgDeck.image = UIImage(named: "boatdesk")
            break
        case 3: cell.imgBack.image = UIImage(named: "main_deck")
                cell.imgDeck.image = UIImage(named: "maindesk")
            break
        case 4:cell.imgBack.image = UIImage(named: "lower_deck")
               cell.imgDeck.image = UIImage(named: "lowerdesk")
            break
        default: print("default")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if let indexPathForSelectedRow = tableView.indexPathsForSelectedRows {
            if (indexPathForSelectedRow.contains(indexPath)) {
                
                tableView.deselectRow(at: indexPath as IndexPath, animated: false)
                
                let deckName = self.decktempArr[indexPath.row]
                
                for (index, selectedName) in self.selectedRows.enumerated(){
                    if deckName == selectedName{
                        self.selectedRows.remove(at: index)
                        break
                    }
                }
                 return nil
            }
        }
        return indexPath
    }

  
    // MARK: - Web-Service Methods
    func updateDecksWebService() {
        
        var dict = [[String:String]]()
        
        for (_,deck_name) in self.selectedRows.enumerated() {
            
            if deck_name == "Storage"{
                let data = ["name":deck_name,
                            "view_order":"0"]
                dict.append(data)
            }
            else if deck_name == "Sun Deck"{
                let data = ["name":deck_name,
                            "view_order":"1"]
                dict.append(data)
            }
            else if deck_name == "Boat Deck"{
                let data = ["name":deck_name,
                            "view_order":"2"]
                dict.append(data)
            }
            else if deck_name == "Main Deck"{
                let data = ["name":deck_name,
                            "view_order":"3"]
                dict.append(data)
            }
            else{
                let data = ["name":deck_name,
                            "view_order":"4"]
                dict.append(data)
            }
            //print("Final Data-->\(dict)")
        }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "deck":dict]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "add_deck", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                
                if self.isforFirstTime == true{
                    let st = UIStoryboard(name: "Inventory", bundle: nil)
                    let controller = st.instantiateViewController(withIdentifier:"DeckLevelList") as! DeckLevelList
                    controller.isToManageOrFirstTime = true
                    controller.isforFirstTime = true
                    controller.push()
                }else{
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: DeckLevelList.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
            }
        }
    }


}
