//
//  DeckLevelList.swift
//  MARITIME YACHT SERVICES
//
//  Created by Octal on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import SDWebImage

class DeckListViewCell: UITableViewCell{
    
    @IBOutlet weak var imgViewDeck: UIImageView!
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    @IBOutlet weak var lblDeckName: UILabel!
    @IBOutlet weak var btnAddBulePrint: UIButton!
    
    var deckImg:UIImage? = UIImage()
    
    func setupCell(_ dictRecord:NSDictionary) -> Void {
        
        let arrImage:[UIImage] = [#imageLiteral(resourceName: "storage"),#imageLiteral(resourceName: "main_deck"),#imageLiteral(resourceName: "sun_deck"),#imageLiteral(resourceName: "lower_deck"),#imageLiteral(resourceName: "boat_deck")]
        let arrImgIcons:[UIImage] = [#imageLiteral(resourceName: "storageDesk"),#imageLiteral(resourceName: "maindesk"),#imageLiteral(resourceName: "sundesk"),#imageLiteral(resourceName: "lowerdesk"),#imageLiteral(resourceName: "boatdesk")]
        let arrImageNames = ["Storage","Main Deck","Sun Deck","Lower Deck","Boat Deck"]
        
        let name = dictRecord.object(forKey: "name") as? String ?? " "
        self.lblDeckName.text = name
        
        let index = arrImageNames.index(of: name) ?? 0
        
        let image = arrImage[index]
        let iconImage = arrImgIcons[index]
        
        self.imgViewDeck.image = nil
        //self.loadingView.startAnimating()
        
        //using alomafire
        let url:NSURL = NSURL(string :"\(imageURL)\(dictRecord.object(forKey: "image") as! String)")!
        
        //images according to deck name
        self.imgViewIcon.image = iconImage
        self.imgViewDeck.image = image
        
        SDWebImageDownloader.shared().downloadImage(with: url as? URL, options: .continueInBackground, progress: nil) { (imageToUse, data, error, status) in
            self.deckImg = imageToUse
        }
    }
}

class DeckLevelList: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    var arrDeckRecords=[Any]()
    @IBOutlet weak var tableViewDeckRecords: UITableView!
    @IBOutlet weak var btnToggleDeckList: UIButton!
    @IBOutlet weak var btnAddInventory: UIButton!
    
    var isToManageOrFirstTime: Bool = true
    var isToShowDeckViewOrListView : Bool = false
    
    var sharedCustomer_id = ""
    var isforFirstTime = Bool()
    
    //MARK:- ViewController Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewDeckRecords.estimatedRowHeight = 167.5
        tableViewDeckRecords.rowHeight = UITableViewAutomaticDimension
        
        isToShowDeckViewOrListView = !isToManageOrFirstTime
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getAllDeckRecords()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IBActions
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _=kConstantObj.SetMainViewController("HomeVC")
    }
    
    //MARK:-Share Inventory Button Click
    //    @IBAction func btnShareInventoryClicked(_ sender: UIButton) {
    //        if myTeamAvailable == "1"
    //        {
    //            let st = UIStoryboard.init(name: "MyTeam", bundle: nil)
    //            let controller = st.instantiateViewController(withIdentifier:"MyTeamListing") as! MyTeamListing
    //           // controller.isFromInventory = "true"
    //            controller.push()
    //        }else{
    //            _=kConstantObj.SetIntialMainViewController("MyTeamVC")
    //        }
    //     }
    
    @IBAction func addBtnClicked(_ sender: Any) {
        //Inventory Sharing
        if isSharingUser == true
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            //selectedDeckNames.removeAll()
            //  selectedDeckNames = self.arrDeckRecords.map{ (($0 as! NSDictionary)["name"] as? String)! }
            
            if self.isforFirstTime == false{
                let st = UIStoryboard(name: "Inventory", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"DeckTemplatesVC") as! DeckTemplatesVC
                controller.isforFirstTime = true
                controller.push()
            }else{
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DeckTemplatesVC.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
            }
        }
    }
    
    @IBAction func searchInventoryProductsBtnClicked(_ sender: Any) {
        let st = UIStoryboard(name: "Main", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"searchInventoryProductVC") as! searchInventoryProductVC
        controller.sharedCustomer_id = self.sharedCustomer_id
         controller.push()
     }
    
    @IBAction func toggleList(_ sender: UIButton) {
        
        isToShowDeckViewOrListView = !isToShowDeckViewOrListView
        sender.isSelected = !sender.isSelected
        self.tableViewDeckRecords.reloadData()
    }
    
    @IBAction func btnTestRefershTapped(_ sender: UIButton) {
        getAllDeckRecords()
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrDeckRecords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isToShowDeckViewOrListView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeckLevelCell", for: indexPath) as! DeckLevelCell
            cell.setupCellFor(arrDeckRecords[indexPath.row] as! NSDictionary, tableview: tableView)
            //let imageView = cell.contentView.viewWithTag(100)?.viewWithTag(500) as! UIImageView
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeckListViewCell", for: indexPath) as! DeckListViewCell
            
            let dict = arrDeckRecords[indexPath.row] as! NSDictionary
            
            // let isPinsAvailable = dict.object(forKey: "pin") as! NSArray
            // if isPinsAvailable.count > 0 {
            // MARK: - Add Blue Print Button Event
            cell.btnAddBulePrint.isHidden = false
            cell.btnAddBulePrint.tag = indexPath.row
            cell.btnAddBulePrint.addTarget(self, action: #selector(btnAddorViewBluePrintClicked(_:)), for: .touchUpInside)
            //  }else{
            //      cell.btnAddBulePrint.isHidden = true
            // }
            
            cell.setupCell(dict)
            return cell as UITableViewCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isToManageOrFirstTime && !isToShowDeckViewOrListView {
            
            let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"MnageInventListVC") as! MnageInventListVC
            let dictForSelectedDeck = arrDeckRecords[indexPath.row] as! NSDictionary
            controller.bluePrintDeckId = dictForSelectedDeck.object(forKey: "id") as! String
            controller.deck_name = dictForSelectedDeck.object(forKey: "name") as! String
            //Check if deck contains any pin or not
            //yes:  Pass the first pinId
            //no :   Show alert message to drop pin first
            let pinStatus = findTheFirstPin(dictDeck: dictForSelectedDeck)
            
            if pinStatus.status {
                controller.pinID = pinStatus.pinId!
                controller.push()
            }
            else
            {
                controller.isPinAvailable = false
                controller.push()
            }
        }else
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"LowerDeckVC") as! LowerDeckVC
            // let cell = tableView.cellForRow(at: indexPath) as! DeckLevelCell
            let data = arrDeckRecords[indexPath.row] as! NSDictionary
            let img = data.object(forKey: "image") as! String
            controller.dictRecord = data
            controller.imgString = img
            let name = data.object(forKey: "name") as! String
            if  name == "Storage" {
                controller.isToRotat = false
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            let data = arrDeckRecords[indexPath.row] as! NSDictionary
            
            if data.object(forKey: "customer_id") as? String == webService_obj.Retrive("User_Id") as? String
            {
                self.deleteExpenseWebService(deck_id:(data.object(forKey: "id") as? String)!)
            }else{
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    func deleteExpenseWebService(deck_id:String) {
        
         PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "blueprint_deck_id":deck_id]
        
         webService_obj.fetchDataFromServer(alertMsg: false, header: "deleteBluePrint", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
   
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Do you want to delete this deck level?", preferredStyle: .alert)
                let actionCancel = UIAlertAction(title: "NO", style: .default) { (action:UIAlertAction) in
                }
                let actionOk = UIAlertAction(title: "YES", style: .default) { (action:UIAlertAction) in
                    self.getAllDeckRecords()
                }
                alertMessage.addAction(actionCancel)
                alertMessage.addAction(actionOk)
                self.present(alertMessage, animated: true, completion: nil)
              }
        }
    }
    
    
    // MARK: - Add Blue Print Button Event
    @IBAction func btnAddorViewBluePrintClicked(_ sender: UIButton)
    {
        let data = arrDeckRecords[sender.tag] as! NSDictionary
        
        //let isPinsAvailable = data.object(forKey: "pin") as! NSArray
        // if isPinsAvailable.count > 0 {
        
         if data["is_blueprint"] as! String == "1"
        {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"LowerDeckVC") as! LowerDeckVC
            let img = data.object(forKey: "image") as! String
            controller.dictRecord = data
            controller.imgString = img
            let name = controller.dictRecord.object(forKey: "name") as! String
            if  name == "Storage" {
                controller.isToRotat = false
            }
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            if isSharingUser == true
            {
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"You are not authorized for this action. ", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
                
            }else{
                let st = UIStoryboard.init(name: "Inventory", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"InventoryHomeVC") as! InventoryHomeVC
                controller.isfirstTime = "false"
                controller.deck_name = data.object(forKey: "name") as! String
                controller.bluePrintDeckId = data.object(forKey: "id") as! String
                controller.push()
            }
        }
    }
    //        }else{
    //            //Show alert Message
    //            let alertMessage = UIAlertController(title: "MYS", message:"Please first add the location.", preferredStyle: .alert)
    //            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
    //                let st = UIStoryboard.init(name: "MngInventory", bundle: nil)
    //                let controller = st.instantiateViewController(withIdentifier:"MnageInventListVC") as! MnageInventListVC
    //                let dictForSelectedDeck = self.arrDeckRecords[sender.tag] as! NSDictionary
    //                controller.bluePrintDeckId = dictForSelectedDeck.object(forKey: "id") as! String
    //                //Check if deck contains any pin or not
    //                //yes:  Pass the first pinId
    //                //no :   Show alert message to drop pin first
    //                let pinStatus = self.findTheFirstPin(dictDeck: dictForSelectedDeck)
    //
    //                if pinStatus.status {
    //                    controller.pinID = pinStatus.pinId!
    //                    controller.push()
    //                }else{
    //                    controller.isPinAvailable = false
    //                    controller.push()
    //                }
    //            }
    //
    //            let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
    //            }
    //            alertMessage.addAction(action)
    //            alertMessage.addAction(actionCancel)
    //            self.present(alertMessage, animated: true, completion: nil)
    //        }
    // }
    
    
    //MARK:- Utility Methods
    func findTheFirstPin(dictDeck: NSDictionary) -> (status:Bool,pinId:String?){
        
        let arrPins = dictDeck.object(forKey: "pin") as! NSArray
        if arrPins.count > 0 {
            let dict = arrPins.object(at: 0) as! NSDictionary
            return (true,dict.object(forKey: "id") as? String)
        }
        
        return (false,nil)
    }
    
    func getAllDeckRecords()
    {
        if isSharingUser == true
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "shared_customer_id":self.sharedCustomer_id]
        }else{
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "shared_customer_id":webService_obj.Retrive("User_Id") as! String]
        }
        
        webService_obj.fetchDataFromServer(header: getAllDeckRecordUrl, withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (responseDict, msg, status) in
            if status{
                //fill the array and reload the list
                
                let data = responseDict.object(forKey: "data") as? NSArray
                self.arrDeckRecords.removeAll()
                
                var defaultDict = [Any]()
                var otherDict = [Any]()
                
                for (_, dict) in (data?.enumerated())!{
                    let id = (dict as AnyObject).value(forKey: "is_default")
                    if id as! String == "1"{
                        // defaultDict.append(IndexPath(row: index, section: 0))
                        defaultDict.append(dict)
                    }else
                    {
                        //otherDict.append(IndexPath(row: index, section: 0))
                        otherDict.append(dict)
                    }
                }
                
                self.arrDeckRecords.append(contentsOf: defaultDict)
                self.arrDeckRecords.append(contentsOf: otherDict)
                
                
                DispatchQueue.main.async {
                    self.tableViewDeckRecords.reloadData()
                }
            }
            else{
                //TODO: handle error or show user to alert
                self.arrDeckRecords.removeAll()
                self.tableViewDeckRecords.reloadData()
            }
        }
    }
}
