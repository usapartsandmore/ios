//
//  ExpenseListVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Octal on 24/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class CustomeSelectExpenseCell: UITableViewCell {
    
    @IBOutlet weak var selectImg: UIImageView!
    @IBOutlet weak var expenseTitle: UILabel!
    @IBOutlet weak var expenseDate: UILabel!
    @IBOutlet weak var expenseAddress: UILabel!
    @IBOutlet weak var expensesCategory: UILabel!
    @IBOutlet weak var expensesPrice: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        if isSelected {
            self.selectImg.image = UIImage(named: "radiobtnselect")
        }
        else {
           self.selectImg.image = UIImage(named: "radiobtnunselect")
        }
    }

}

class ExpenseListVC: UIViewController , UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var tblExpense: UITableView!
    @IBOutlet weak var btnSelectAll: UIButton!
    @IBOutlet weak var btnDoneCheck: UIButton!
    
    var expensesData = [[String:String]]()
    var selectedexpensesId = [String]()
    
    var isFromGroup = ""
    var group_id = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        getIndividualExpensesWebService()
    }


    @IBAction func btnBackClick(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }

    
    @IBAction func showGroupExpense(_ sender: UIButton) {
        if let rows = tblExpense.indexPathsForSelectedRows{
            var data = [[String:String]]()
            for indexPath in rows
            {
                let dict = self.expensesData[indexPath.row]
                data.append(dict)
            }
            
            self.selectedexpensesId = data.map{ ($0["id"]! ) }
            print("Selected Expenses-->\(self.selectedexpensesId)")
        }
        
        if isFromGroup == "true"
        {
            addExpensesGroupWebService()
        }else{
            if self.selectedexpensesId.count == 0
            {
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:"Please select expenses.", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }else
            {
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"GroupExpensesVC") as! GroupExpensesVC
            controller.selectedExpenses = self.selectedexpensesId
            controller.isFromGroup = "false"
            controller.push()
            }
        }
    }
    
    // MARK: - Table view Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
     func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.expensesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomeSelectExpenseCell", for: indexPath) as! CustomeSelectExpenseCell
        
        let data = self.expensesData[indexPath.row]
        
        cell.expenseTitle.text = data["sub_expense_type"]
        cell.expenseDate.text = data["date"]
        cell.expensesCategory.text = data["vendor"]
        cell.expenseAddress.text = data["address"]
        
        let amt = (data["amount"]! as NSString).doubleValue
        cell.expensesPrice.text = String(format: "$%.2f", amt)
        
        if cell.isSelected {
            cell.selectImg.image = UIImage(named: "radiobtnselect")
        }
        else{
            cell.selectImg.image = UIImage(named: "radiobtnunselect")
        }
         
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
    
    @IBAction func btnSelectAllClick(_ sender: UIButton)
    {
        let totalRows = expensesData.count
        if  sender.tag ==  0{
            for row in 0..<totalRows {
                tblExpense.selectRow(at: NSIndexPath(row: row, section: 0) as IndexPath, animated: false, scrollPosition: .none)
            }
            self.btnSelectAll.tag = 1
        }
        else{
            for row in 0..<totalRows {
                tblExpense.deselectRow(at: NSIndexPath(row: row, section: 0) as IndexPath, animated: false)
            }
            self.btnSelectAll.tag = 0
        }
    }
    
    
    // MARK: - Web-Service Methods
    func getIndividualExpensesWebService() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "page":"-1"]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "expenses_list_not_in_group", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.expensesData = response.value(forKey: "data") as! [[String: String]]
                if self.expensesData.count == 0
                {
                    self.btnSelectAll.isHidden = true
                    self.btnDoneCheck.isHidden = true
                }else{
                    self.btnSelectAll.isHidden = false
                    self.btnDoneCheck.isHidden = false
                }
                 self.tblExpense.reloadData()
            }else{
                 self.btnSelectAll.isHidden = true
                 self.btnDoneCheck.isHidden = true
                 self.tblExpense.reloadData()
             }
        }
    }
    
    func addExpensesGroupWebService() {
        if self.selectedexpensesId.count == 0
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Please select expenses.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "group_id":self.group_id,
                        "expenses":self.selectedexpensesId]
            
            webService_obj.fetchDataFromServer(alertMsg: false, header: "add_expenses_in_group", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
                if status{
                    _ = self.navigationController?.popViewController(animated:true)
                }
            }
        }
    }
}
