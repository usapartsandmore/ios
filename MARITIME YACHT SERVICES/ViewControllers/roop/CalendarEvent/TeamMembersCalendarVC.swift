//
//  TeamMembersCalendarVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 27/02/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class TeamMembersCalendarCell: UITableViewCell {
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        if isSelected == false {
            accessoryType = UITableViewCellAccessoryType.none
        }
        else {
            accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
 }

class TeamMembersCalendarVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableview: UITableView!
    @IBOutlet weak var lbl_header: UILabel!
    
    var block: (([[String:String]])->Void)?
    var membersListData = [[String:String]]()
    var selectedRows = [[String:String]]()
    var team_id = ""
    var team_name = ""
    
    override func viewDidLoad() {
        self.tableview.tableFooterView = UIView()
        super.viewDidLoad()
        
        self.lbl_header.text = self.team_name
        
        self.getTeamMemberslistWebServiceMethod()
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: MENU BUTTON CLICK
    @IBAction func btnbackClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return membersListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamMembersCalendarCell", for: indexPath) as! TeamMembersCalendarCell
        cell.textLabel?.text = self.membersListData[indexPath.row]["name"]!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
//    func tableView(_ tableView: UITableView,willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        if let indexPathForSelectedRow = tableView.indexPathsForSelectedRows {
//            if (indexPathForSelectedRow.contains(indexPath)) {
//             tableView.deselectRow(at: indexPath as IndexPath, animated: false)
// 
//                return nil
//            }
//        }
//        return indexPath
//    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let ids = self.membersListData[indexPath.row]["customer_id"]
        
        for (index, dict) in self.selectedRows.enumerated(){
            let id = dict["customer_id"]!
            if ids == id{
                self.selectedRows.remove(at: index)
                break
            }
        }
    }
 
    
    @IBAction func btnDoneClick(_ sender: Any)
    {
        if let rows = tableview.indexPathsForSelectedRows{
            //var data = [[String:String]]()
            for indexPath in rows
            {
                let maindict = self.membersListData[indexPath.row]
                let ids = self.membersListData[indexPath.row]["customer_id"]
                if self.selectedRows.count > 0{
                    let selectedids = self.selectedRows.map{ ($0["customer_id"]! ) }
                    if selectedids.contains(ids!){
                    }else{
                        selectedRows.append(maindict)
                    }
                }else{
                    selectedRows.append(maindict)
                }
            }
            block?(self.selectedRows)
            self.navigationController?.popViewController(animated: true)
        }else{
            block?(self.selectedRows)
            self.navigationController?.popViewController(animated: true)
        }
    }

    
    
    // MARK: - Call WebServices
    func getTeamMemberslistWebServiceMethod () {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "team_id":self.team_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "get_team_member_list_temp", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
            if status{
                self.membersListData.removeAll()
                self.membersListData = response.value(forKey: "data") as! [[String: String]]
                 
                self.tableview.reloadData()
                
                let ids = self.selectedRows.map{ ($0["customer_id"]! ) }
                
                var indexPaths = [IndexPath]()
                
                for (index, dict) in self.membersListData.enumerated(){
                    let id = dict["customer_id"]!
                    if ids.contains(id){
                        indexPaths.append(IndexPath(row: index, section: 0))
                    }
                }
                
                indexPaths.forEach { (indexPath) in
                    self.tableview.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }

            }
        }
    }
}
