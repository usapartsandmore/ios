//
//  SepLineTableViewCell.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 22/12/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class SepLineTableViewCell: UITableViewCell {
     @IBOutlet weak var labelSepText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
