//
//  SaveLocker.swift
//  MARITIME YACHT SERVICES
//
//  Created by Octal on 23/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class SaveLocker: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var viewForCrossButton: UIView!
    @IBOutlet weak var cosmosStarRating: CosmosView!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    var isFrom: String = ""
    var bluePrintDeckId = ""
    var deckPinId = ""
    var pinLockerId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // commentTextField.delegate = self
       // view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        
        
       viewForCrossButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        
        if isFrom == "1"{
            
            lblTitle.text = "Add Locker"
        }
        else{
             lblTitle.text = "Add Box"
         }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func endEditing() {
        self.dismiss(animated: true, completion: nil)
        view.endEditing(true)
    }
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        var msg = ""
        
        if isFrom == "1" {
            msg = "Provide Locker name"
        }
        else{
            msg = "Provide box name"
        }
        
        guard commentTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines).length != 0 else {
            
            //Show error

            let alertVC = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
            
            let alertActionOK = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                alertVC.dismiss(animated: true, completion: nil)
            })
            
            alertVC.addAction(alertActionOK)
            
            self.present(alertVC, animated: true, completion: nil)
            
            return
        }
        
        if isFrom == "1" {
            //Add locker API
            
            
            //Add Locker dict
            if isSharingUser == true
            {
                PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                            "blueprint_deck_id":bluePrintDeckId,
                            "decks_pin_id":deckPinId,
                            "name":commentTextField.text!,
                            "create_by":sharedUser_id]
            }else{
                PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                            "blueprint_deck_id":bluePrintDeckId,
                            "decks_pin_id":deckPinId,
                            "name":commentTextField.text!,
                            "create_by":webService_obj.Retrive("User_Id") as! String]
            }

            
            
            webService_obj.fetchDataFromServer(alertMsg: false,header:addLockerApi, withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
                if staus{
                    print("locker added successfully")
                    self.showAlertFor(title: "Success", msg: "Locker added successfully.")
                }
                else
                {
                    //TODO: handle fail api response
                    self.dismiss(animated: true, completion: nil)
                }
            }

        }
        else{
            //Add box API
            
            //Add box params dict
             if isSharingUser == true
            {
                PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                            "blueprint_deck_id":bluePrintDeckId,
                            "decks_pin_id":deckPinId,
                            "name":commentTextField.text!,
                            "pins_locker_id":pinLockerId,
                            "create_by":sharedUser_id]
            }else{
                PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                            "blueprint_deck_id":bluePrintDeckId,
                            "decks_pin_id":deckPinId,
                            "name":commentTextField.text!,
                            "pins_locker_id":pinLockerId,
                            "create_by":webService_obj.Retrive("User_Id") as! String]
            }

            
            webService_obj.fetchDataFromServer(alertMsg: false,header:addBoxApi, withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
                if staus{
                    print("box added successfully")
                    self.showAlertFor(title: "Success", msg: "Box added successfully")
                }
                else
                {
                    //TODO: handle fail api response
                    //showAlertFor(title: "Error", msg: "Something went wrong")
                    self.dismiss(animated: true, completion: nil)
                }
            }
         }
     }
    
    func showAlertFor(title:String,msg:String){
        
        let alertVC = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let alertActionOK = UIAlertAction(title: "OK", style: .default) { (action) in
            alertVC.dismiss(animated: true, completion: nil)
            
            //Fire notification to list view to refresh the data
            NotificationCenter.default.post(name: Notification.Name("refreshList"), object: nil
                , userInfo: ["isCrop":true])
            
            self.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(alertActionOK)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func btnCrossTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension SaveLocker {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}
