//
//  ParentsTableViewCell.swift
//  Expandable3
//
//  Created by MAC241 on 11/05/17.
//  Copyright © 2017 KiranJasvanee. All rights reserved.
//

import UIKit

class ParentsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageviewBackground: UIImageView!
    @IBOutlet weak var constraintLeadingLabelParent: NSLayoutConstraint!
    @IBOutlet weak var labelParentCell: UILabel!
    @IBOutlet weak var btnAddBox: UIButton!
    @IBOutlet weak var btnAddProduct: UIButton!
    
    @IBOutlet weak var buttonState: UIButton!
    
    
    var productclick: (()->Void)? = nil
    var boxclick: (()->Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnAddBox.addTarget(self, action: #selector(onBoxlick), for: .touchUpInside)
        self.btnAddProduct.addTarget(self, action: #selector(onProductclick), for: .touchUpInside)
    }
    
    @objc func onProductclick(){
        productclick?()
    }
    
    @objc func onBoxlick(){
        boxclick?()
    }

    
    func cellFillUp(indexParam: String, tupleCount: NSInteger,dictRecord:NSDictionary)
    {
        let pinTitle = dictRecord.object(forKey: "name") as! String
        
        if tupleCount == 1 {
            labelParentCell.text = pinTitle
        }else{
            labelParentCell.text = pinTitle
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
