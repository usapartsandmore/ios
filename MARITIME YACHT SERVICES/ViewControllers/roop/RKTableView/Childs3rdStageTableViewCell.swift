//
//  Childs3rdStageTableViewCell.swift
//  Expandable3
//
//  Created by MAC241 on 11/05/17.
//  Copyright © 2017 KiranJasvanee. All rights reserved.
//

import UIKit

class Childs3rdStageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageviewBackground: UIImageView!
    //@IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet var imageviewProduct : UIImageView!
    @IBOutlet var imageType : UIImageView!
    @IBOutlet var lblProductTitle: UILabel!
    @IBOutlet var lblServiceProviderName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblStockQuantity: UILabel!
    @IBOutlet var lblMinimum: UILabel!
    @IBOutlet var lblMaximum: UILabel!
    @IBOutlet var lblProductId: UILabel!
    @IBOutlet var lblNotesCount: UILabel!
    @IBOutlet weak var buttonState: UIButton!
    @IBOutlet weak var btnShowProduct: UIButton!
    @IBOutlet weak var btnRestockProduct: UIButton!
    @IBOutlet weak var btnViewImage: UIButton!
    
    var productclick: (()->Void)? = nil
    
    var restockclick: (()->Void)? = nil
    
    var viewImageclick: (()->Void)? = nil
    
    @objc func onProductclick(){
        productclick?()
    }
    
    @objc func onRestockclick(){
        restockclick?()
    }
    
    @objc func onViewImageclick(){
        viewImageclick?()
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.btnShowProduct.addTarget(self, action: #selector(onProductclick), for: .touchUpInside)
        self.btnRestockProduct.addTarget(self, action: #selector(onRestockclick), for: .touchUpInside)
        
        self.btnViewImage.addTarget(self, action: #selector(onViewImageclick), for: .touchUpInside)
    }
    
    func cellFillUp(indexParam: String,dictRecord:NSDictionary)
    {
        lblProductTitle.text = dictRecord.object(forKey: "name") as? String
        lblServiceProviderName.text = dictRecord.object(forKey: "vender_name") as? String
        
        let imgType = dictRecord.object(forKey: "type") as? String
        
        if imgType == "product"
        {
            imageType.image = UIImage(named:"locationicon")
        }
        else if imgType == "box"
        {
             imageType.image = UIImage(named:"product")
        }else
        {
             imageType.image = UIImage(named:"addlocker")
        }
        
        if dictRecord.object(forKey: "price") as! String == "N/A"
        {
            lblPrice.text = dictRecord.object(forKey: "price") as? String
        }
        else
        {
            lblPrice.text = "$\(dictRecord.object(forKey: "price") as! String)"
        }
        
        lblNotesCount.text =  dictRecord.object(forKey: "noteCount") as? String
        
        lblStockQuantity.text =  dictRecord.object(forKey: "qty") as? String
        lblMinimum.text = dictRecord.object(forKey: "min_qty") as? String
        lblMaximum.text = dictRecord.object(forKey: "max_qty") as? String
        lblProductId.text = "# \(dictRecord.object(forKey: "product_id") as! String)"
        
        //imageview
        let imgUrl = dictRecord.object(forKey: "image") as! String
        
        if imgUrl == ""
        {
            imageviewProduct.image = UIImage(named:"noimage")
        }else{
            
            let url:NSURL = NSURL(string :"\(imageURL)\(imgUrl)")!
            imageviewProduct.af_setImage(withURL: url as URL)
        }
        
       // self.btnAddProduct.addTarget(self, action: #selector(self.addProductClicked(button:)), for: .touchUpInside)
    }
    
    func addProductClicked(button: UIButton,dictRecord:NSDictionary)
    {
        
    }
    
  //  {
    // labelTitle.textColor = UIColor.gray
    // labelSubTitle.textColor = UIColor.white
    // labelIndex.textColor = UIColor.white
    
    //  labelTitle.text = "Red Box"
    // labelSubTitle.text = "Index of:"
    // labelIndex.text = indexParam
    
    //Getting Box title
    //  let infoStr = indexParam //"0.1.0"
    //First part : index of pin
    //Second part: index of locker inside selected pin
    //Third part : index of box for selected locker and selected pin
    
    //        let arrIndex = infoStr.components(separatedBy: ".")
    //
    //
    //        let pinIndex = Int(arrIndex[0])!
    //        let lockerIndex = Int(arrIndex[1])!
    //        let boxIndex = Int(arrIndex[2])!
    //
    //        let mainDict = dictRecord.object(forKey: "data") as! NSDictionary
    //        let pinArray = mainDict.object(forKey: "pin") as! NSArray
    //
    //        let pinRecord = pinArray.object(at: pinIndex) as! NSDictionary
    //        let lockersArray = pinRecord.object(forKey: "locker") as! NSArray
    //
    //        let lockerRecord = lockersArray.object(at: lockerIndex) as! NSDictionary
    //        let boxArray = lockerRecord.object(forKey: "box") as! NSArray
    //
    //        let boxRecord = boxArray.object(at: boxIndex) as! NSDictionary
    //        let boxTitle = boxRecord.object(forKey: "name") as! String
    //
    //        labelTitle.text = boxTitle
 //   }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
