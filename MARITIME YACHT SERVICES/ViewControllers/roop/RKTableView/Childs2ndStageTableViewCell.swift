//  Childs2ndStageTableViewCell.swift
//  Expandable3
//  Created by MAC241 on 11/05/17.
//  Copyright © 2017 KiranJasvanee. All rights reserved.


import UIKit

class Childs2ndStageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageviewBackground: UIImageView!
    @IBOutlet weak var labelChildAtIndex: UILabel!
    
    @IBOutlet weak var buttonState: UIButton!
    @IBOutlet weak var btnAddProduct: UIButton!
    
    var productclick: (()->Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnAddProduct.addTarget(self, action: #selector(onProductclick), for: .touchUpInside)
    }
    
    @objc func onProductclick(){
        productclick?()
    }
    
    func cellFillUp(indexParam: String,dictRecord:NSDictionary) {
        
        let boxTitle = dictRecord.object(forKey: "name") as! String
        labelChildAtIndex.text = boxTitle
        
        //getting pin
        //get correct locker index by seperating indexParam by '.' and use second part of that
        //After seperating indexParam by '.' will get two parts:
        //at index 0 : pin array index
        //at index 1 : locker array index
        //        let mainDict = dictRecord.object(forKey: "data") as! NSDictionary
        //        let pinArray = mainDict.object(forKey: "pin") as! NSArray
        //
        //        let indexArrays = indexParam.components(separatedBy: ".")
        //        let pinIndex = Int(indexArrays[0])!
        //        let lockerIndex = Int(indexArrays[1])!
        //
        //        let pinRecord = pinArray.object(at: pinIndex) as! NSDictionary
        //        let lockersArray = pinRecord.object(forKey: "locker") as! NSArray
        //
        //        let lockerRecord = lockersArray.object(at: lockerIndex) as! NSDictionary
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
