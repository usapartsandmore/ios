//
//  SettingsVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 10/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import PWSwitch

class NotificationCustomCell: UITableViewCell {
    @IBOutlet var notifySwitch: PWSwitch!
}

class VersionCustomeCell: UITableViewCell {
    @IBOutlet var lblVersion: UILabel!
}
class BuildCustomCell: UITableViewCell {
    @IBOutlet var lblBuild :UILabel!
}

class SettingsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var push_noti = ""
    // var push_noti : Bool = true
    
    @IBOutlet var tableView :UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
           self.tableView.tableFooterView = UIView()
        self.push_noti = UserDefaults.standard.value(forKey: "notification") as? String ?? ""
        
        self.getNotificationStatus()
        // Do any additional setup after loading the view.
    }

    func getNotificationStatus() {
        
        //Notification Status Webservice Call
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"get_customer_setting", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                guard let cell = self.tableView.cellForRow(at:IndexPath(row:0, section:0)) as? NotificationCustomCell else {
                    return
                }
                
                let data = responce.value(forKey: "data") as! NSDictionary
                
                if data["notification"] as! String == "1"  {
                    self.push_noti = "1"
                    
                    UserDefaults.standard.setValue("1", forKey: "notification")
                    
                }else{
                    self.push_noti = "0"
                    UserDefaults.standard.setValue("0", forKey: "notification")
                    
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PushNotificationCustomeCell", for: indexPath) as! NotificationCustomCell
            cell.selectionStyle = .none
            
            if self.push_noti == "1"
            {
                cell.notifySwitch.setOn(true, animated:false)
            }else{
                cell.notifySwitch.setOn(false, animated:false)
            }
            cell.notifySwitch.addTarget(self, action:#selector(onSwitchClickEvent(sender:)), for: .valueChanged)
            
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacyPolicyCustomeCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RateAppCustomeCell", for: indexPath)
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppVersionCustomeCell", for: indexPath) as!VersionCustomeCell
            cell.selectionStyle = .none
            let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String
            cell.lblVersion.text = text
            return cell
        }
//        else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "BuildCustomeCell", for: indexPath) as!BuildCustomCell
//            cell.selectionStyle = .none
//            let text = Bundle.main.infoDictionary?["CFBundleVersion"]  as? String
//            cell.lblBuild.text = text
//            return cell
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as! PrivacyPolicyVC
            vc.selctButton = "Privacy Policy"
            vc.push()
        }else if  indexPath.row == 2{
            let urlStr = "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=\(ITUNES_APP_ID)"
            if let url = URL(string: urlStr){
                if UIApplication.shared.canOpenURL(url){
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
            }
        }
    }
    
    func onSwitchClickEvent(sender: PWSwitch){

        guard let cell = self.tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? NotificationCustomCell else {
            return
        }
        
        if cell.notifySwitch.on
        {
            self.push_noti = "1"
        }else{
            self.push_noti = "0"  
        }
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "notification":self.push_noti]
        webService_obj.fetchDataFromServer(header:"customer_setting", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                UserDefaults.standard.setValue(self.push_noti, forKey: "notification")
            }
        }
    }
}
