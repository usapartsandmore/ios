//
//  ChooseLocationVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 14/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class ChooseLocationListCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Text: UILabel!
    
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            if newValue{
                lbl_Text.textColor = UIColor.white
            }else{
                lbl_Text.textColor = UIColor(red: 92/255, green: 183/255, blue: 227/255, alpha: 1.0)
            }
        }
        get{
            return super.isSelected
        }
    }
}
class ChooseLocationVC: UIViewController,UITableViewDataSource,UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource{
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet var collectionView: UICollectionView!
     var arrNames = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrNames = ["Chart Table","Chart Table Drawers","Cockpit Locker Port","Cockpit Locker Starboard","Forepeak Cupboard Port","Forepeak Cupboard Standard","Forepeak Shelf Port","Forepeak Shelf Starboard","Forepeak Underberth Port","Forepeak Underberth Starboard"]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChooseLocationListCell", for: indexPath)
        let lblNames : UILabel = cell.viewWithTag(1) as! UILabel
        lblNames.font.withSize(30)
        lblNames.text = arrNames[indexPath.row] as? String
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChooseLocationListCell", for: indexPath) as! ChooseLocationListCell
        
        cell.lbl_Text.text = "Sun Deck"
        
         // cell.selectedBackgroundView = UIView(frame: cell.bounds)
        // cell.selectedBackgroundView!.backgroundColor = .white
         
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //let cell = collectionView.cellForItem(at: indexPath) as! MngeInventListCell
        
        // cell.lbl_Text.textColor = UIColor.white
        
        // collectionView.reloadItems(at: [indexPath])
    }
  
}
