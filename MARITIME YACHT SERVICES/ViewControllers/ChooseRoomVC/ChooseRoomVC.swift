//
//  ChooseRoomVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 11/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class RoomCustomCell: UITableViewCell {
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if isSelected == false {
            accessoryType = UITableViewCellAccessoryType.none
        }
        else {
            accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
}


class ChooseRoomVC: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var lbl_NoData: UILabel!
    @IBOutlet var tblView: UITableView!
    var allData = [[String:String]]()
    
    var deck_id = ""
    var pin_id = ""
    var locker_id = ""
    var box_id = ""
    
    var isfromVC = ""
    
    var block: (([String : String])->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbl_NoData.isHidden = true
        
        self.tblView.tableFooterView = UIView()
        
        if self.isfromVC == "pin"
        {
            self.fetchAllPinsWebServiceCall()
        }else if self.isfromVC == "locker"
        {
            self.fetchAllLockersWebServiceCall()
        }else
        {
            self.fetchAllBoxesWebServiceCall()
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Method Button click
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.allData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "ChooseRoomCustomeCell", for: indexPath)
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "RoomCustomCell", for: indexPath) as! RoomCustomCell
        cell.selectionStyle = .none
        
        let data = self.allData[indexPath.row]
        let lblNames : UILabel = cell.viewWithTag(1) as! UILabel
        lblNames.text = data["name"]
        //cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
     
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?{
        if self.isfromVC == "pin"
        {
            return indexPath
        }else{
            if (tableView.indexPathForSelectedRow == indexPath){
                self.tblView.deselectRow(at: indexPath, animated: false)
                return nil
            }else{
                return indexPath
            }
        }
    }

    
    @IBAction func btnDoneClick(_ sender: Any) {
        if let indexPath = tblView.indexPathForSelectedRow{
            let data = self.allData[indexPath.row]
            block?(data)
            self.navigationController?.popViewController(animated: true)
        }else
        {
            block?([:])
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - Web-Service Methods
    func fetchAllPinsWebServiceCall() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "blueprint_deck_id":deck_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"deck_pins", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.allData.removeAll()
                self.allData = responce["data"] as! [[String:String]]
                 self.tblView.reloadData()
                
                let pinid = self.pin_id
                var indexPaths = [IndexPath]()
                
                for (index, dict) in self.allData.enumerated(){
                    let id = dict["id"]!
                    if pinid.contains(id){
                        indexPaths.append(IndexPath(row: index, section: 0))
                    }
                }
                
                indexPaths.forEach { (indexPath) in
                    self.tblView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
               
            }
            if self.allData.count == 0
            {
                self.lbl_NoData.isHidden = false
            }
        }
    }
    
    func fetchAllLockersWebServiceCall() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "blueprint_deck_id":deck_id,
                    "decks_pin_id":pin_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"deck_lockers", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.allData.removeAll()
                self.allData = responce["data"] as! [[String:String]]
                self.tblView.reloadData()
                
                let lockerid = self.locker_id
                var indexPaths = [IndexPath]()
                
                for (index, dict) in self.allData.enumerated(){
                    let id = dict["id"]!
                    if lockerid.contains(id){
                        indexPaths.append(IndexPath(row: index, section: 0))
                    }
                }
                
                indexPaths.forEach { (indexPath) in
                    self.tblView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }
             }
            if self.allData.count == 0
            {
                self.lbl_NoData.isHidden = false
            }
        }
    }
    
    func fetchAllBoxesWebServiceCall() {
        
        if locker_id == ""
        {
            locker_id = "0"
        }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "blueprint_deck_id":deck_id,
                    "decks_pin_id":pin_id,
                    "pins_locker_id":locker_id ]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"deck_boxes", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.allData.removeAll()
                self.allData = responce["data"] as! [[String:String]]
                self.tblView.reloadData()
                
                let boxid = self.box_id
                var indexPaths = [IndexPath]()
                
                for (index, dict) in self.allData.enumerated(){
                    let id = dict["id"]!
                    if boxid.contains(id){
                        indexPaths.append(IndexPath(row: index, section: 0))
                    }
                }
                
                indexPaths.forEach { (indexPath) in
                    self.tblView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                }

            }
            if self.allData.count == 0
            {
                self.lbl_NoData.isHidden = false
            }
        }
    }
    
    
    
}
