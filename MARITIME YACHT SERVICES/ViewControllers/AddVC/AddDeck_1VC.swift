//
//  AddDeck_1VC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 28/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class AddDeck_1VC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK: - Properties
    public var pickedImage: UIImage!
    @IBOutlet weak var vesselSizeTextField: UITextField!
    @IBOutlet weak var vesselSizeLabel: UILabel!
    
    
    //MARK: - ViewController LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Fetch vessel size
        getVesselSize()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBActions
    @IBAction func uploadIndividualDeckClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"AddDeck_2VC")
        controller?.push()
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func uploadCompleteDeckBluePrint(_ sender: UIButton) {
        
        /* old implemenation
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"CropDeckLevelVC")
        controller?.push()
        */
        
        //new implementation as follows:
        //1. Give user choices to pick image 
        //2. Get image from source
        //3. push to cropViewController with picked image
        
        //alert controller to show choices to user
        let imagePickChoiceAlertController:UIAlertController = UIAlertController(title: "Select Image Source", message: "", preferredStyle: .actionSheet)
        
        //adding choices to UIAlertController
        let btnCameraChoice:UIAlertAction = UIAlertAction(title: "Camera", style: .default) { action -> Void in
            self.showImageSourceWithCamera(isCamera: true)
        }
        let btnImageGalleryChoice:UIAlertAction = UIAlertAction(title: "Photo Gallery", style: .default) { action -> Void in
            self.showImageSourceWithCamera(isCamera: false)
        }
        let btnCancel:UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            imagePickChoiceAlertController.dismiss(animated: true, completion: nil)
        }
        
        imagePickChoiceAlertController.addAction(btnCameraChoice)
        imagePickChoiceAlertController.addAction(btnImageGalleryChoice)
        imagePickChoiceAlertController.addAction(btnCancel)
        
        //present alert controller to user
        self.present(imagePickChoiceAlertController, animated: true, completion: nil)
    }
    
    @IBAction func addUpdateVesselSize(_ sender: UIButton) {
    
//        // local validation
//        guard let vesselSizeEntered = vesselSizeTextField.text else {
//            
//            showError(title: "Error", message: "Please provide vessel size.")
//            return
//        }
//        
//        guard vesselSizeEntered.trimmingCharacters(in: .whitespacesAndNewlines).length > 0 else {
//            
//            showError(title: "Error", message: "Please provide vessel size.")
//            return
//        }
//        
//        // webService call
//        callApiToAddUpdateVesselSize(vesselSizeEntered)
    
    }
    
    //MARK: - Imagepickercontroller delegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            picker.dismiss(animated: true, completion: { 
                //get image and push to viewController
                self.pickedImage = img
                
                //push to cropViewController with pickedImage
                
                DispatchQueue.main.async {
                    let controller = self.storyboard?.instantiateViewController(withIdentifier:"CropDeckLevelVC") as! CropDeckLevelVC
                    controller.imagePicked = self.pickedImage.fixed
                    controller.isForIndividual = "false"
                    controller.push()
                }
            })
        }
    }

    //MARK: - Utility methods
    func showImageSourceWithCamera(isCamera:Bool) -> Void {
        
        //flag is to map whether to present imagepickercontroller or not
        var flag = true
        
        //imagepicker controller setup
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.delegate = self
        if isCamera {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerVC.sourceType = .camera
            }
            else{
                imagePickerVC.sourceType = .photoLibrary
                flag = false    //when camera choice is made and no camera available, make flag 'false'
            }
            
        }else{
            imagePickerVC.sourceType = .photoLibrary
        }
        
        //check using flag, whether to present imagepickercontroller or alert to show error
        if flag {
            present(imagePickerVC, animated: true, completion: nil)
        }
        else{
            showError(title: "Error", message: "Sorry, Camera is not available.")
        }
    }
    
    func showError(title:String,message:String) -> Void {
        let errAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (alertVC) in
            errAlert.dismiss(animated: true, completion: nil)
        }
        
        errAlert.addAction(okAction)
        self.present(errAlert, animated: true, completion: nil)
    }

    
    //MARK:- API calls
    func getVesselSize() {
    
        //payload
        let dictParam = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        //Webservice call 
        webService_obj.fetchDataFromServer(header: getVesselSizeUrl, withParameter: dictParam as NSDictionary, inVC: self, showHud: true) { (responseDict, msg, status) in
            if status{
                //fill the array and reload the list

                DispatchQueue.main.async {

                    //update textfield value
                    let responseDataDict = responseDict.value(forKey: "data") as! NSDictionary
                    let vesselSize = responseDataDict.value(forKey: "vessel_size") as! String
                    
                    guard vesselSize.trimmingCharacters(in: .whitespacesAndNewlines).length > 0 else {
                    
                        self.vesselSizeLabel.text = "Please enter Vessels size (Feet)"
                        return
                    }
                    
                    
                    //get the unit of size
                    let unitOfSize = responseDataDict.value(forKey: "vessel_size_type") as! String
                    
                    // setting upto UI
                    self.vesselSizeTextField.text = vesselSize
                    self.vesselSizeLabel.text = "Please enter Vessels size (\(unitOfSize))"
                    
                }
            }
            else{
                //TODO: handle error or show user to alert
            }
        }
    
    }
    
    func callApiToAddUpdateVesselSize(_ size:String ) {
        
        //payload
        let dictParam = ["customer_id":webService_obj.Retrive("User_Id") as! String, "vessel_size":size]
        
        //Webservice call
        webService_obj.fetchDataFromServer(header: addUpdateVesselSizeUrl, withParameter: dictParam as NSDictionary, inVC: self, showHud: true) { (responseDict, msg, status) in
            
            if status{
                
                //fill the array and reload the list
                DispatchQueue.main.async {
                    
                    // vessel size updated, successfully.
                    
                }
            }
            else{
                //TODO: handle error or show user to alert
            }
        }
    }
}
