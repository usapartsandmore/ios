//
//  AddNewPartVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 17/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import Firebase
extension AddNewPartVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        img_Profile.image = pickedImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}


class AddNewPartVC: UIViewController ,UIScrollViewDelegate,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet var img_Profile: UIImageView!
    @IBOutlet weak var txtPartName: UITextField!
    @IBOutlet weak var txtPartDescription: UITextView!
    @IBOutlet weak var txtUnitCost: UITextField!
    @IBOutlet weak var txtBarcode: UITextField!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var txtMininumQty: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    
    var partData = [String:Any]()
    let imagePicker = UIImagePickerController()
    var pickedImage: UIImage!
    var block: (([String:Any])->Void)?
    var isfromchat = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtPartDescription.text = "Part Description"
        self.txtPartDescription.textColor = UIColor.lightGray
        
        if isfromchat == true{
            self.partData.removeAll()
            self.partData = partDataMain
        }
        
        if self.partData.count > 0 {
            
            if isfromchat == true{
                let str = self.partData["photoURL"] as? String
                //let url:NSURL = NSURL(string :"\(String(describing: str))")!
                if str != "NOTSET"{
                    let storageRef = Storage.storage().reference(forURL: str!)
                    storageRef.getData(maxSize: INT64_MAX){ (data, error) in
                        self.img_Profile.image = UIImage(data: data!)
                        self.pickedImage = UIImage(data: data!)
                    }
                }
            }else{
                
                if partData["part_image"] is UIImage{
                    self.img_Profile.image = partData["part_image"] as? UIImage ?? UIImage(named: "camera")
                    self.pickedImage = partData["part_image"] as? UIImage
                }else{
                    let str = partData["part_image"] as! String
                    let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                    self.img_Profile.af_setImage(withURL: url as URL)
                }
            }
            
            if partData["part_unit_cost"] as! String != ""{
                let amt = (partData["part_unit_cost"]! as! NSString).doubleValue
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value:amt))
                self.txtUnitCost.text =  ("$") +  (formattedNumber!)
            }
            
            
            if partData["part_desc"] as! String == ""{
                self.txtPartDescription.text = "Part Description"
                self.txtPartDescription.textColor = UIColor.lightGray
            }else{
                self.txtPartDescription.text = partData["part_desc"] as? String
                self.txtPartDescription.textColor = UIColor.darkGray
            }
            
            
            self.txtPartName.text = partData["part_name"] as? String
            self.txtBarcode.text = partData["part_bar_code"] as? String
            self.txtQuantity.text = partData["part_quantity"] as? String
            self.txtMininumQty.text = partData["part_minimum_quantity"] as? String
            self.txtArea.text = partData["part_area"] as? String
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.partData.count > 0 {
        }else{
            if self.isfromchat == true{
                self.txtBarcode.text = ""
            }else{
                self.txtBarcode.text = scan_Code
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         self.txtBarcode.text = ""
        scan_Code = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnSaveClicked(_ sender: Any) {
        if img_Profile.image == UIImage.init(named:"camera")
        {
            self.AlertMessage("Please select image.")
        }
        else if txtPartName.text! .isEmpty
        {
            self.AlertMessage("Please enter part name.")
        }
            //        else if txtPartDescription.text! .isEmpty
            //        {
            //            self.AlertMessage("Please enter part description.")
            //        }
            //        else if txtUnitCost.text! .isEmpty
            //        {
            //            self.AlertMessage("Please enter unit cost.")
            //        }
            //        else if txtBarcode.text! .isEmpty
            //        {
            //            self.AlertMessage("Please scan barcode.")
            //        }
            //        else if txtQuantity.text! .isEmpty
            //        {
            //            self.AlertMessage("Please enter quantity.")
            //        }
            //        else if txtMininumQty.text! .isEmpty
            //        {
            //            self.AlertMessage("Please enter minimum quantity.")
            //        }
            //        else if txtArea.text! .isEmpty
            //        {
            //            self.AlertMessage("Please enter area.")
            //        }
        else{
            
            var discriptionTxt = ""
            if self.txtPartDescription.text == "Part Description"{
                discriptionTxt = ""
            }else{
                discriptionTxt = self.txtPartDescription.text
            }
            
            
            var data = ["part_name":txtPartName.text!,
                        "part_desc":discriptionTxt,
                        "part_bar_code":txtBarcode.text!,
                        "part_quantity":txtQuantity.text!,
                        "part_minimum_quantity":txtMininumQty.text!,
                        "part_area":txtArea.text!] as [String : Any]
            
            if self.txtUnitCost.text != ""{
                var str = self.txtUnitCost.text?.replacingOccurrences(of: ",", with: "")
                str = str?.replacingOccurrences(of: "$", with: "")
                let amt = String(format: "%.2f", Double(str!)!)
                data["part_unit_cost"] = amt
            }else{
                data["part_unit_cost"] = ""
            }
            
            
            if self.pickedImage != nil{
                data["part_image"] = self.pickedImage
            }else{
                data["part_image"] = self.partData["part_image"] as! String
            }
            
            if self.partData.count > 0 {
                data["id"] = partData["id"] as? String
            }
            
            self.block?(data)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnBarcodeClick(_ sender: Any) {
        let st = UIStoryboard.init(name: "Product", bundle: nil)
        let vc3 = st.instantiateViewController(withIdentifier: "ProductScanVC") as? ProductScanVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController!.present(vc3!, animated: true, completion: nil)
    }
    
    @IBAction func btnAddImageClicked(_ sender: Any) {
        self.AlertController()
    }
    
    // MARK: - Method Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        // 2
        let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Camera")
            self.imageCamera()
        })
        let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Gallery")
            self.imageGallery()
        })
        
        //
        let viewImgAction = UIAlertAction(title: "View Image", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if self.img_Profile.image != UIImage.init(named:"camera")
            {
                let newImageView = UIImageView(image: self.img_Profile.image)
                newImageView.frame = self.view.frame
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
            }
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        
        if self.img_Profile.image != UIImage.init(named:"camera")
        {
            // 4
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(viewImgAction)
            optionMenu.addAction(cancelAction)
        }else{
            // 4
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
        }
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let viewNew = sender.view!
        viewNew.removeFromSuperview()
    }
    
    
    // MARK: UITextFieldDelegate Method
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtUnitCost{
            if let amt = Double(txtUnitCost.text!){
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                let formattedNumber = numberFormatter.string(from: NSNumber(value:amt))
                
                self.txtUnitCost.text =  ("$") +  (formattedNumber!)
            }
        }
        return true
    }
    
    
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        //One Decimal and only two value after decimal
        if range.length == 1{
            return true
        }
        
        if textField == txtUnitCost{
            if let text = textField.text{
                let array = text.components(separatedBy: ".")
                if array.count > 1 && string == "."{
                    return false
                }
                
                if array.count > 1{
                    return array[1].length < 2
                }
            }
        }
        return true
    }
    
    // MARK: - TextView Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Part Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
}
