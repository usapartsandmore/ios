//
//  AddDeck_2VC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 28/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class AddDeck_2VC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    
    //MARK: - UIElement outlets
    @IBOutlet weak var imgViewCamera: UIImageView!
    @IBOutlet weak var txtFldDeckName: UILabel!
    var imageTaken:UIImage? = nil
    
    var deck_name = ""
    var bluePrintDeckId:String = ""
    
    //MARK: - Viewcontroller Lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
          // Do any additional setup after loading the view.
        
        self.txtFldDeckName.text = deck_name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UIButton Actions
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
  
    @IBAction func SavebtnClicked(_ sender: UIButton) {
        
        //call api to submit deck to server
        //TODO: Local validations
        
        guard txtFldDeckName.text!.trimmingCharacters(in: .whitespacesAndNewlines).length != 0 else {
                showAlert(title: "Error", msg: "Please provide deck name.")
            return
        }
        guard imageTaken != nil else {
            showAlert(title: "Error", msg: "Please select deck image.")
            return
        }
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"CropDeckLevelVC") as! CropDeckLevelVC
        controller.imagePicked = self.imgViewCamera.image?.fixed
        controller.isForIndividual = "true"
        controller.deck_name = self.txtFldDeckName.text!
        controller.bluePrintDeckId = self.bluePrintDeckId
        controller.push()
     }
    
    @IBAction func btnUploadFileTapped(_ sender: UIButton) {
        //Give user choice to pick the image source, 1. Camera 2. Image Gallery
        //Choice 1 : open camera, if not available show error
        //Choice 2 : open image Gallery
        
        //alert controller to show choices to user
        let imagePickChoiceAlertController:UIAlertController = UIAlertController(title: "Select Image Source", message: "", preferredStyle: .actionSheet)
        
        //adding choices to UIAlertController
        let btnCameraChoice:UIAlertAction = UIAlertAction(title: "Camera", style: .default) { action -> Void in
            self.showImageSourceWithCamera(isCamera: true)
        }
        let btnImageGalleryChoice:UIAlertAction = UIAlertAction(title: "Photo Gallery", style: .default) { action -> Void in
            self.showImageSourceWithCamera(isCamera: false)
        }
        let btnCancel:UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            imagePickChoiceAlertController.dismiss(animated: true, completion: nil)
        }
        
        imagePickChoiceAlertController.addAction(btnCameraChoice)
        imagePickChoiceAlertController.addAction(btnImageGalleryChoice)
        imagePickChoiceAlertController.addAction(btnCancel)
        
        //present alert controller to user
        self.present(imagePickChoiceAlertController, animated: true, completion: nil)
     }

    //MARK: - Imagepickercontroller delegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage{
            imgViewCamera.image = img.fixed
            imageTaken = img.fixed
            picker.dismiss(animated: true, completion:nil)
        }
    }

    //MARK: - Utility methods
    func showAlert(title:String,msg:String)
    {
        let alertVC = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        let alertActionOK = UIAlertAction(title: "OK", style: .default) { (action) in
                alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(alertActionOK)
        present(alertVC, animated: true, completion: nil)
    }
    
    func showImageSourceWithCamera(isCamera:Bool) -> Void {
        
        //flag is to map whether to present imagepickercontroller or not
        var flag = true
        
        //imagepicker controller setup
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.delegate = self
        if isCamera {
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerVC.sourceType = .camera
            }
            else{
                imagePickerVC.sourceType = .photoLibrary
                flag = false    //when camera choice is made and no camera available, make flag 'false'
            }
            
        }else{
            imagePickerVC.sourceType = .photoLibrary
        }
        
        //check using flag, whether to present imagepickercontroller or alert to show error
        if flag {
            present(imagePickerVC, animated: true, completion: nil)
        }
        else{
            showError(title: "Error", message: "Sorry, Camera is not available.")
        }
    }
    
    func showError(title:String,message:String) -> Void {
        let errAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (alertVC) in
            errAlert.dismiss(animated: true, completion: nil)
        }
        
        errAlert.addAction(okAction)
        self.present(errAlert, animated: true, completion: nil)
    }
}
