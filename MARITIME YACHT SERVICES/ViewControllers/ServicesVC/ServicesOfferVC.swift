//
//  ServicesOfferVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 16/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class ServiceOfferSearchCustomCell: UITableViewCell {
    
    @IBOutlet var img_vendorlogo: UIImageView!
    @IBOutlet var rating_vendor: FloatRatingView!
    @IBOutlet var lbl_vendorDetail: UILabel!
    @IBOutlet var lbl_vendorName: UILabel!
}

class ShopLocalOfferCustomeCell: UITableViewCell {
    @IBOutlet var imgCell: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblServiceName: UILabel!
    @IBOutlet var lblOfferPrice: UILabel!
    @IBOutlet var lblOriginalPrice: UILabel!
    @IBOutlet var lblTotalReviews: UILabel!
    @IBOutlet var vendorRating: FloatRatingView!
}

class ServicesOfferVC: UIViewController, UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate {
    @IBOutlet var OfferTableView: UITableView!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var lblResults: UILabel!
    var servicesComplteDate = [String:String]()
    
    @IBOutlet var btn_topRight: UIButton!
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchTableview: UITableView!
     var filteredData = [[Any]]()
    var webserviceData = [[String:String]]()
    var selectedCategory_id = ""
    var headername = ""
    var location = ""
    var selectedlat = Double()
    var selectedlong = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.localOffersWebServices ()
        searchTableview.tableFooterView = UIView()
        self.searchTableview.estimatedRowHeight = 44.0
        self.searchTableview.rowHeight = UITableViewAutomaticDimension
        OfferTableView.tableFooterView = UIView()
        self.searchView.tag = 0
        self.searchView.isHidden = true
        self.btn_topRight.isHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    func localOffersWebServices() {

        PostData = ["customer_id": webService_obj.Retrive("User_Id") as! String,
                    "lat": self.selectedlat,
                    "lng": self.selectedlong,
                    "cat_id": self.selectedCategory_id]
        
        webService_obj.fetchDataFromServer(header:"service_local_offer_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.webserviceData = responce.value(forKey: "data") as! [[String:String]]
                 self.OfferTableView.reloadData()
                self.lblResults.text = "\(self.webserviceData.count) Results for '\(self.headername)' in your area"
                self.lblAddress.text = self.location
            }
            else{
                let alertMessage = UIAlertController(title: "MYS", message:"No data found", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    _ = self.navigationController?.popViewController(animated:true)
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
             }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchTableview {
            return filteredData.count
        }
        else {
            return self.webserviceData.count
        }
  
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == searchTableview {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceOfferSearchCustomCell", for: indexPath) as! ServiceOfferSearchCustomCell
            
            let data = filteredData[indexPath.row]
            cell.lbl_vendorName.text = data[0] as? String
            
            do {
                let str = try NSAttributedString(data:(data[1] as? String ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                cell.lbl_vendorDetail.attributedText = str
            } catch {
                print(error)
            }

            cell.img_vendorlogo.image = data[2] as? UIImage
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShopLocalOfferCustomeCell", for: indexPath) as! ShopLocalOfferCustomeCell
            
            let data = self.webserviceData[indexPath.row]
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.imgCell.af_setImage(withURL: url as URL)
            cell.lblTitle.text = data["name"]!
            cell.lblServiceName.text = data["vendor_name"]!
            cell.lblServiceName.sizeToFit()
            cell.vendorRating.rating = Float(data["rating"] ?? "") ?? 0.0
            cell.lblTotalReviews.text = "\(data["review_count"] ?? "") Reviews"
            
            if data["discount_offer"] != "0"{
                 cell.lblOriginalPrice.isHidden = false
                cell.lblOfferPrice.text = "$\(data["offered_price"] ?? "")"
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(data["retail_price"] ?? "")")
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                cell.lblOriginalPrice.attributedText = attributeString
                
            }else{
                cell.lblOfferPrice.text = "$\(data["retail_price"] ?? "")"
                 cell.lblOriginalPrice.isHidden = true
             }
             return cell
         }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == searchTableview {
            return
        }
        else {
                let main = UIStoryboard(name: "Main", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
                controller.ServiceData = self.webserviceData[indexPath.row]
            controller.isfromMyOrders = false
                controller.push()
          
        }
    }
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnSearchClick(_ sender: UIButton) {
        if self.searchView.tag == 0
        {
            self.searchView.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchView.tag = 1
            self.btn_topRight.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
            self.searchBar.text = ""
            filteredData.removeAll()
            
            self.searchView.isHidden = true
            self.searchBar.endEditing(true)
            self.btn_topRight.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchView.tag = 0
            
            searchTableview.reloadData()
        }
    }
    
    @IBAction func btnAddressClick(_ sender: UIButton) {
    }
    
    @IBAction func Goback(_ sender: UIButton) {
       // _ = self.navigationController?.popViewController(animated:true)
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SearchNearCurrentAddressVC {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData.removeAll()
        
        if searchText.isEmpty {
            //searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
        }
        else
        {
            let arr = paintersArray.filter({ (fruit) -> Bool in
                return (fruit[0] as AnyObject).lowercased.contains(searchText.lowercased())
            })
            filteredData = arr
        }
        searchTableview.reloadData()
    }

    
}
