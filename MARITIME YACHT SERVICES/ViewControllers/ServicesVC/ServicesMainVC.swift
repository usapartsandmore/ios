//
//  ServicesMainVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 31/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class ServicesMainVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAddToCartClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
     @IBAction func btnBackClick(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)

    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
         sideMenuVC.toggleMenu()
     }
    // MARK: Search The List
    @IBAction func searchTheListClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"SearchNearCurrentAddressVC") as! SearchNearCurrentAddressVC
        controller.isFromOffers = "List"
        controller.push()
    }
    //   MARK: Shop Local Offer
    @IBAction func shopLocalOfferClicked(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"SearchNearCurrentAddressVC") as! SearchNearCurrentAddressVC
        controller.isFromOffers = "Offers"
        controller.push()
        
    }
//         Show alert Message
//        let alertMessage = UIAlertController(title: "MYS", message:"This functionality is under development.", preferredStyle: .alert)
//        let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//
//        }
//        alertMessage.addAction(action)
//        self.present(alertMessage, animated: true, completion: nil)
}
