//  SearchVendorVC.swift
//  MARITIME YACHT SERVICES
//  Created by mac112 on 23/08/17.
//  Copyright © 2017 OctalSoftware.All rights reserved.


import UIKit
import AlamofireImage

class ServicesCustomeCell: UITableViewCell {
    @IBOutlet var category_image: UIImageView!
    @IBOutlet var category_name: UILabel!
    
}
//class cell: ScalingCarouselCell {}
class searchCustomCell: UITableViewCell {
    @IBOutlet var lbl_detail: UILabel!
    @IBOutlet var rating_vendor: FloatRatingView!
    @IBOutlet var lbl_vendor_Name: UILabel!
    @IBOutlet var img_vendor: UIImageView!
}

class SearchVendorVC: UIViewController, UITableViewDelegate, UITableViewDataSource ,UICollectionViewDataSource,UICollectionViewDelegate, UISearchBarDelegate{
    @IBOutlet var lbl_noAds: UILabel!
    @IBOutlet var btn_topRight: UIButton!
    @IBOutlet var categoryTable: UITableView!
    @IBOutlet var searchTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchView: UIView!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet weak var lblheader: UILabel!
    
    var categoryData = [[String:String]] ()
    var bannerData = [[String:String]] ()
    
    var lat = Double ()
    var long = Double ()
    var address = ""
    var isFromOffers = ""
    //  var filteredData = [[Any]]()
    var filteredData = [[String:String]]()
    
    @IBOutlet weak var carousel: ScalingCarouselView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_noAds.isHidden = true
        if isFromOffers == "Offers"
        {
            self.searchTable.estimatedRowHeight = 125
            self.lblheader.text = "Shop Local Offers"
        }
        else{
            self.lblheader.text = "Search Vendor"
            searchTable.estimatedRowHeight = 44.0
            searchTable.rowHeight = UITableViewAutomaticDimension
        }
 
        searchTable.tableFooterView = UIView()
        categoryTable.tableFooterView = UIView()
         lblAddress.text = address
        self.getAddsWebServiceMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.searchBar.text = ""
        filteredData.removeAll()
        self.searchView.isHidden = true
        self.searchBar.endEditing(true)
        self.btn_topRight.setImage(UIImage(named: "search"), for: UIControlState.normal)
        self.searchView.tag = 0
        
        searchTable.reloadData()
    }
    
    @IBAction func btnAddressClick(_ sender: UIButton) {
    }
    
    @IBAction func btnSearchClick(_ sender: UIButton) {
        if self.searchView.tag == 0
        {
            self.searchView.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchView.tag = 1
            self.btn_topRight.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
            self.searchBar.text = ""
            filteredData.removeAll()
            
            self.searchView.isHidden = true
            self.searchBar.endEditing(true)
            self.btn_topRight.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchView.tag = 0
            
            searchTable.reloadData()
        }
    }
    
 
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let touchView = touch.view{
            if touchView.tag == 1004{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == categoryTable {
            return self.categoryData.count
        }
        else {
            return filteredData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == categoryTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCustomeCell", for: indexPath) as! ServicesCustomeCell
            let sData = self.categoryData[indexPath.row]
            
            if sData["image"] == ""
            {
                cell.category_image.image = UIImage(named:"noimage")
            }else{
                let str = sData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cell.category_image.af_setImage(withURL: url as URL)
            //    cell.category_image.contentMode = .scaleAspectFit
                cell.category_name.text = sData["name"]!
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        else {
            if isFromOffers == "Offers" {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ShopLocalOfferCustomeCell", for: indexPath) as! ShopLocalOfferCustomeCell
                
                let data = filteredData[indexPath.row]
                let str = data["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cell.imgCell.af_setImage(withURL: url as URL)
                cell.lblTitle.text = data["name"]!
                cell.lblServiceName.text = data["vendor_name"]!
                cell.lblServiceName.sizeToFit()
                cell.vendorRating.rating = Float(data["rating"] ?? "") ?? 0.0
                cell.lblOfferPrice.text = "$\(data["offered_price"] ?? "")"
                cell.lblTotalReviews.text = "\(data["review_count"] ?? "") Reviews"
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(data["retail_price"] ?? "")")
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
                cell.lblOriginalPrice.attributedText = attributeString
                
                //                cell.lbl_vendor_Name.text = data["name"]
                //                cell.rating_vendor.rating = Float(data["rating"] ?? "") ?? 0.0
                //                let str = data["image"]!
                //                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                //                cell.img_vendor.af_setImage(withURL: url as URL)
                //
                //                do {
                //                    let str = try NSAttributedString(data:(data["offer_details"] ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                //                    cell.lbl_detail.attributedText = str
                //                } catch {
                //                    print(error)
                //                }
                
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "searchCustomCell", for: indexPath) as! searchCustomCell
                let data = filteredData[indexPath.row]
                cell.lbl_vendor_Name.text = data["company_name"]
                cell.rating_vendor.rating = Float(data["rating"] ?? "") ?? 0.0
                let str = data["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cell.img_vendor.af_setImage(withURL: url as URL)
                cell.img_vendor.contentMode = .scaleAspectFit
                
                do {
                    let str = try NSAttributedString(data:(data["description"] ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                    cell.lbl_detail.attributedText = str
                } catch {
                    print(error)
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == categoryTable {
            let categoryId = self.categoryData[indexPath.row]["id"]
            let categoryName = self.categoryData[indexPath.row]["name"]
            if isFromOffers == "Offers" {
                let st = UIStoryboard.init(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"ServicesOfferVC") as! ServicesOfferVC
                controller.selectedCategory_id = categoryId!
                controller.selectedlat = lat
                controller.selectedlong = long
                controller.headername = categoryName!
                controller.location = address
                controller.push()
            }else{
                
                let st = UIStoryboard.init(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"BusinessListVC") as! BusinessListVC
                controller.selectedCategory_id = categoryId!
                controller.selectedlat = lat
                controller.selectedlong = long
                controller.headername = categoryName!
                controller.push()
            }
            
        }
        else {
             self.view.endEditing(true)
            
            if isFromOffers == "Offers" {
                let main = UIStoryboard(name: "Main", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
                controller.ServiceData = filteredData[indexPath.row]
                controller.isfromMyOrders = false
                controller.push()
                
            }
            else {
                let vendorId = filteredData[indexPath.row]["id"]
                let st = UIStoryboard.init(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
                controller.selectedVendorId = vendorId!
                controller.push()
            }
            
        }
    }
    
    // CROUSEL VIEW
    typealias CarouselDatasource = SearchVendorVC
    // extension CarouselDatasource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bannerData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        if isFromOffers == "Offers" {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
//            let data = self.bannerData[indexPath.row]
//            let imageView = cell.viewWithTag(1) as! UIImageView
//            let title = cell.viewWithTag(2) as! UILabel
//            let description = cell.viewWithTag(3) as! UILabel
//            let str = data["image"]!
//            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
//            imageView.af_setImage(withURL: url as URL)
//            title.text = data["banner_title"]
//            title.textAlignment = .left
//            description.text = data["banner_sub_title"]
//            
//            if let scalingCell = cell as? ScalingCarouselCell {
//                scalingCell.layer.cornerRadius = 0
//                scalingCell.mainView.backgroundColor = UIColor.white
//                scalingCell.backgroundColor = UIColor.clear
//            }
//            return cell
//        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let data = self.bannerData[indexPath.row]
        let imageView = cell.viewWithTag(1) as! UIImageView
        let title = cell.viewWithTag(2) as! UILabel
        let description = cell.viewWithTag(3) as! UILabel
        let str = data["image"]!
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
        imageView.af_setImage(withURL: url as URL)
        title.text = data["banner_title"]
        title.textAlignment = .left
         description.text = data["banner_sub_title"]
        
        if let scalingCell = cell as? ScalingCarouselCell {
            scalingCell.layer.cornerRadius = 0
            scalingCell.mainView.backgroundColor = UIColor.white
            scalingCell.backgroundColor = UIColor.clear
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dataArr = self.bannerData[indexPath.row]
        
        self.bannerCountWebService(banner_Id: dataArr["id"]!)
        //1=Link to Third Domain , 2 = Link to Product , 3 = Link to Service , 4 = Link to local offer
        
        let adType = dataArr["link_type"]!
        
        switch adType {
        case "1" :
            let urlstr = dataArr["link"]!
            let url = NSURL(string: urlstr)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url! as URL)
            }
        case "2" :
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
            controller.product_ID = dataArr["product_id"]!
            controller.product_variant_ID = dataArr["product_variant_id"]!
            controller.push()
        case "3" :
            let vendorId = dataArr["link"]!
            let st = UIStoryboard.init(name: "Services", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
            controller.selectedVendorId = vendorId
            controller.push()
        case "4" :
            let offerId = dataArr["link"]!
            let main = UIStoryboard(name: "Main", bundle: nil)
            let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
            controller.localOffer_id = offerId
            controller.isfromMyOrders = false
            controller.push()
        default:
            print("F. You failed")//Any number less than 0 or greater than 99
            
        }        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 3) - 20), height: CGFloat(100))
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
             filteredData.removeAll()
            self.searchTable.reloadData()
        }
        else{
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "search":searchText,
                        "lat":String(self.lat),
                        "lng":String (self.long)]
            
            var Str = ""
            if isFromOffers == "Offers" {
                
                Str = "service_local_offer_search_list"
            }
            else {
                
                Str = "service_user_search_list"
                
            }
            webService_obj.fetchDataFromServer(alertMsg: false, header:Str, withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
                if staus{
                    let searchData = responce.value(forKey: "data") as!  [[String:String]]
                    self.filteredData.removeAll()
                    self.filteredData = searchData
                    self.searchTable.reloadData()
                }else
                {
                    self.filteredData.removeAll()
                    self.searchTable.reloadData()
                }
            }
         }
      }
    
    
    
    func getAddsWebServiceMethod () {
        if isFromOffers == "Offers" {
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "lat":String(lat),
                        "lng":String (long),
                        "search":""]
            
            webService_obj.fetchDataFromServer(header:"service_local_offer_ad_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    let webServiceData = responce.value(forKey: "data") as! [String:[[String:String]]]
                    self.bannerData = (webServiceData["banner"])!
                    self.categoryData = (webServiceData["category"])!
                    self.categoryTable.reloadData()
                    self.carousel.reloadData()
                    
                    if self.bannerData.count == 0 {
                        self.lbl_noAds.isHidden = false
                    }
                    else {
                        self.lbl_noAds.isHidden = true
                    }
                }
            }
        }
        else {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "lat":String(self.lat),
                        "lng":String (self.long)]
            
            webService_obj.fetchDataFromServer(header:"service_ad", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    let webServiceData = responce.value(forKey: "data") as! [String:[[String:String]]]
                    self.bannerData = (webServiceData["banner"])!
                    self.categoryData = (webServiceData["category"])!
                    self.categoryTable.reloadData()
                    self.carousel.reloadData()
                    if self.bannerData.count == 0 {
                        self.lbl_noAds.isHidden = false
                    }
                    else {
                        self.lbl_noAds.isHidden = true
                    }
                }
            }
        }
    }
    
    func bannerCountWebService(banner_Id:String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "ad_id":banner_Id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"banerClick", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
            }
        }
    }
}
