//
//  ServicesCatListVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 27/03/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class ServicesCatListVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var categoryTable: UITableView!
    @IBOutlet weak var lblheader: UILabel!
     @IBOutlet var searchBar: UISearchBar!
    var categoryData = [[String:String]] ()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryTable.tableFooterView = UIView()
        self.getAddsWebServiceMethod(searchText:"")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
 
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
     
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.categoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCustomeCell", for: indexPath) as! ServicesCustomeCell
        let sData = self.categoryData[indexPath.row]
        
        if sData["image"] == ""
        {
            cell.category_image.image = UIImage(named:"noimage")
        }else{
            let str = sData["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.category_image.af_setImage(withURL: url as URL)
            cell.category_name.text = sData["name"]!
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let sData = self.categoryData[indexPath.row]
        
        let main = UIStoryboard(name: "Main", bundle: nil)
        let controller = main.instantiateViewController(withIdentifier:"ProductChatSearchVC")as! ProductChatSearchVC
        controller.isforService = true
        controller.serviceCatID = sData["id"]!
        controller.push()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        categoryData.removeAll()
        
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            self.getAddsWebServiceMethod(searchText:"")
        }
        else
        {
            self.getAddsWebServiceMethod(searchText:searchText)
        }
        categoryTable.reloadData()
    }

    
    func getAddsWebServiceMethod(searchText :String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "lat":"",
                    "lng":"",
                    "search":searchText]
        
        webService_obj.fetchDataFromServer(alertMsg: true, header:"service_local_offer_ad_list", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                let webServiceData = responce.value(forKey: "data") as! [String:[[String:String]]]
               
                self.categoryData.removeAll()
                self.categoryData = (webServiceData["category"])!
                self.categoryTable.reloadData()
            }
            else
            {
                self.categoryData.removeAll()
                self.categoryTable.reloadData()
            }
        }
    }
}
