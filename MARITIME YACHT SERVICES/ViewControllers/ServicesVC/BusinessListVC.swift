//  BusinessListVC.swift
//  MARITIME YACHT SERVICES
//  Created by mac112 on 21/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.


import UIKit
import MapKit

class BusinessListVC: UIViewController, UITableViewDataSource, UITableViewDelegate,MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet var vandorListTableview: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()

    @IBOutlet var lbl_header: UILabel!
    
    var latLongData = [[String:String]]()
    var vandorListData = [[String:String]]()
    var selectedCategory_id = ""
    var headername = ""
    var selectedlat = Double()
    var selectedlong = Double()

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
         self.vandorListTableview.tableFooterView = UIView()
        self.lbl_header.text = self.headername
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways) || (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse) {
            print("Authorized")
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
            
            mapView.delegate = self
            mapView.mapType = .standard
            mapView.isZoomEnabled = true
            mapView.isScrollEnabled = true
            
            if let coor = mapView.userLocation.location?.coordinate{
                mapView.setCenter(coor, animated: true)
            }
        } else {
            print("Application is not authorized to use location services")
          
        }
        self.vendorlistForSelectedCategoryWebServiceMethod()
        vandorListTableview.estimatedRowHeight = 44.0
        vandorListTableview.rowHeight = UITableViewAutomaticDimension
     }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        mapView.mapType = MKMapType.standard
        
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        //centerMap(locValue)
    }
    // Search all vendore web service method
    func vendorlistForSelectedCategoryWebServiceMethod () {
 
        
        let lat = String(format:"%.7f", Double(self.selectedlat))
        let long = String(format:"%.7f", Double(self.selectedlong))

        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "cat_id": self.selectedCategory_id,
                    "lat":lat,
                    "lng":long]
        webService_obj.fetchDataFromServer(header:"service_user_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                let webserviceData = responce.value(forKey: "data") as! [String:[[String:String]]]
                self.vandorListData = (webserviceData["user"])!
                self.latLongData = (webserviceData["latlong"])!
                self.vandorListTableview.reloadData()
                for maindata in self.latLongData {
                    
                    let latitude = Double(maindata["lat"]!) ?? 0.0
                    let longitude = Double(maindata["lng"]!) ?? 0.0
                    
//                    let center = CLLocationCoordinate2D(latitude: latitude, longitude:longitude)
//                    let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))
//                    self.mapView.setRegion(region, animated: true)
                    let cordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = cordinate
                    annotation.title = maindata["company_name"]
                    self.mapView.addAnnotation(annotation)
              
                }
               //   self.mapView.showAnnotations(self.mapView.annotations, animated: true)
             }
            else{
              
                let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                     _ = self.navigationController?.popViewController(animated:true)
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)

               
            }
        }
     }

    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
          return self.vandorListData.count
       // return paintersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceProviderListCell", for: indexPath)
        cell.selectionStyle = .none
        let imageView = cell.viewWithTag(1) as! UIImageView
        let title = cell.viewWithTag(2) as! UILabel
        let description = cell.viewWithTag(3) as! UILabel
        let ratingView = cell.viewWithTag(1005) as! FloatRatingView
        
                let data = self.vandorListData[indexPath.row]
                let str = data["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                imageView.af_setImage(withURL: url as URL)
                title.text = data["company_name"]!
                description.text = data["description"]!
                ratingView.rating = Float(data["rating"] ?? "") ?? 0.0
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
            let vendorId = self.vandorListData[indexPath.row]["id"]
            let st = UIStoryboard.init(name: "Services", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
            controller.selectedVendorId = vendorId!
            controller.push()
            
        
    }
    // MARK: 
   func mapView(_ map: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        let reuseId = "AnnotationIdentifier"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.image = UIImage(named:"locationicon")
            anView?.canShowCallout = true
        }
        else {
            //we are re-using a view, update its annotation reference...
            anView?.annotation = annotation
        }
        return anView

    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
}
 
