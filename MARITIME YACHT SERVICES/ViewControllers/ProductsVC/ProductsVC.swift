//
//  ProductsVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 25/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductMenuItemsCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Text: UILabel!
    @IBOutlet weak var img_Text: UIImageView!
    
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            if newValue{
                lbl_Text.textColor = UIColor.darkGray
            }else{
                lbl_Text.textColor = UIColor.black
            }
        }
        get{
            return super.isSelected
        }
    }
}

class SearchKeywordCell: UICollectionViewCell {
    @IBOutlet weak var lbl_Keywords: UILabel!
    @IBOutlet weak var btn_Close: UIButton!
 }

class ProductsMainCell: UICollectionViewCell {
    @IBOutlet weak var lbl_PName: UILabel!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var img_Logo: UIImageView!
    @IBOutlet weak var view_Rating: FloatRatingView!
    @IBOutlet weak var lbl_Price: UILabel!
}



class ProductsVC: UIViewController,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var collectionViewHeader: UICollectionView!
    @IBOutlet var collectionViewFProducts: UICollectionView!
    @IBOutlet var collectionViewRViewed: UICollectionView!
    @IBOutlet var collectionViewSearch: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var productImg: UIImageView!
    
    @IBOutlet var btn_camera: UIButton!
    
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var scrollView: UIScrollView!
    var filteredData = [[String:String]]()
    var imagesbanner = [String]()
    var productData = [String:[[String:String]]]()
    
    var featuredData = [[String:String]]()
    var recentData = [[String:String]]()
    var homeBannerData = [[String:String]]()
    
    var img_Txt:[String] = ["cateogries.jpg","freshdeal.jpg","newdeal","bestseller-1"]
    var menuItems:[String] = ["Categories","Flash Deals","New Arrivals", "Best Sellers"]
    
    var subcat_id = ""
    var timer_Ride: Timer?
    var type = ""
    
    let imagePicker = UIImagePickerController()
    var pickedImage: UIImage!
    
    var visionImgSearchArray = [String]()
    
    var googleAPIKey = "AIzaSyBBxfwGLfhDccha-7or8FTxn4rOp0mRx8Y"
    var googleURL: URL {
        return URL(string: "https://vision.googleapis.com/v1/images:annotate?key=\(googleAPIKey)")!
    }
    let session = URLSession.shared
    @IBOutlet var btn_ImgCamera: UIButton!
    var imagesearchOn = ""
    @IBOutlet var view_Search: UIView!
    @IBOutlet var collViewSearched_keywords: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collViewSearched_keywords.isHidden = true
        self.btn_ImgCamera.tag = 0
         self.view_Search.isHidden = true
        self.btn_ImgCamera.isHidden = true
        
      //  UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor.white
        
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        timer_Ride = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
        self.fetchFeaturedRecentWebServiceCall()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        
        if imagesearchOn == "Y"
        {
            
        }else{
            
            if scan_Code.length != 0
            {
                self.btn_camera.setImage(UIImage(named: "cross"), for: UIControlState.normal)
                type = "barcode"
                self.view_Search.isHidden = false
                self.fetchSearchdataWebServiceCall(searchText: scan_Code)
                self.btn_camera.tag = 1
                 self.btn_ImgCamera.tag = 1
                
            }else
            {
                self.btn_camera.setImage(UIImage(named: "chatcamera"), for: UIControlState.normal)
                self.btn_camera.tag = 0
                self.view_Search.isHidden = true
                type = "home"
                searchBar.text = ""
            }
        }
    }
    
    @objc func scrollToNextCell(){
        
        var offset = scrollView.contentOffset
        offset.x += scrollView.bounds.width
        if offset.x >= scrollView.contentSize.width{
            offset.x = 0
        }
        UIView.animate(withDuration: 0.25) {
            self.scrollView.contentOffset = offset
        }
        let pageNumber = round(offset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        pageControl.updateCurrentPageDisplay()
    }
    
    
    @IBAction func btnBannerClick(_ sender: UIButton) {
        let bIndex = pageControl.currentPage
        if homeBannerData.count > 0
        {
            let dataArr = homeBannerData[bIndex]
            
            // self.bannerCountWebService(banner_Id: data["id"]!)
            //1=Link to Third Domain , 2 = Link to Product , 3 = Link to Service , 4 = Link to local offer
            
            let adType = dataArr["link_type"]!
            
            switch adType {
            case "1" :
                let urlstr = dataArr["link"]!
                let url = NSURL(string: urlstr)
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url! as URL)
                }
            case "2" :
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
                controller.product_ID = dataArr["product_id"]!
                controller.product_variant_ID = dataArr["product_variant_id"]!
                controller.push()
            case "3" :
                let vendorId = dataArr["link"]!
                let st = UIStoryboard.init(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
                controller.selectedVendorId = vendorId
                controller.push()
            case "4" :
                let offerId = dataArr["link"]!
                let main = UIStoryboard(name: "Main", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
                controller.localOffer_id = offerId
                controller.isfromMyOrders = false
                controller.push()
            default:
                print("F. You failed")//Any number less than 0 or greater than 99
                
            }
            
            self.bannerCountWebService(banner_Id: dataArr["id"]!)
        }
    }
    
 
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    // MARK: ScrollView Methods
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
        }
    }
    
 
    
    @IBAction func btnCameraClick(_ sender: UIButton) {
         if self.btn_ImgCamera.tag == 1
        {
            collViewSearched_keywords.isHidden = true
            //self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
             self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            type = "home"
            self.imagesearchOn = "N"
            
            self.btn_camera.setImage(UIImage(named: "chatcamera"), for: UIControlState.normal)
            self.btn_camera.tag = 0
            scan_Code = ""
        }else
        {
            collViewSearched_keywords.isHidden = false
            
            filteredData.removeAll()
            visionImgSearchArray.removeAll()
            collViewSearched_keywords.reloadData()
            collectionViewSearch.reloadData()
            
            //self.btn_ImgCamera.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = false
            type = "image_search"
            self.imagesearchOn = "Y"
            self.view_Search.isHidden = false
            self.AlertController()
            self.btn_ImgCamera.tag = 1
        }
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func cartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    @IBAction func viewMoreBtnClick(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"NewArrivalsVC") as! NewArrivalsVC
        controller.type = "Featured Products"
        controller.push()
    }
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionViewHeader
        {
            return menuItems.count
        }
        else if collectionView == self.collectionViewSearch
        {
            return filteredData.count
        }
        else if collectionView == self.collectionViewFProducts
        {
            return featuredData.count
        }
        else if collectionView == self.collViewSearched_keywords
        {
            return visionImgSearchArray.count
        }else
        {
            return recentData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionViewHeader {
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductMenuItemsCell", for: indexPath) as! ProductMenuItemsCell
            
            cellA.lbl_Text.text = self.menuItems[indexPath.row]
            cellA.img_Text.image = UIImage(named:self.img_Txt[indexPath.row])
            return cellA
        }
        else if collectionView == self.collectionViewFProducts
        {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsMainCell", for: indexPath) as! ProductsMainCell
            
            let fData = self.featuredData[indexPath.row]
            cellB.lbl_PName.text = fData["name"] ?? ""
            
            let strLogo = fData["logo"]!
            let urlLogo:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo))")!
            cellB.img_Logo.af_setImage(withURL: urlLogo as URL)
            cellB.img_Logo.contentMode = .scaleAspectFit
            cellB.img_Logo.clipsToBounds = true
            cellB.lbl_Price.text = "$\(fData["price"] ?? "")"
            cellB.view_Rating.rating = (fData["rating"]! as NSString).floatValue
            
            if fData["image"] == ""
            {
                cellB.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = fData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellB.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellB
        }
        else if collectionView == self.collectionViewRViewed
        {
            let cellC = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsMainCell", for: indexPath) as! ProductsMainCell
            
            let rData = self.recentData[indexPath.row]
            
            cellC.lbl_PName.text = rData["name"] ?? ""
            
            let strLogo1 = rData["logo"]!
            let urlLogo1:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo1))")!
            cellC.img_Logo.af_setImage(withURL: urlLogo1 as URL)
             cellC.img_Logo.contentMode = .scaleAspectFit
            cellC.lbl_Price.text = "$\(rData["price"] ?? "")"
            cellC.view_Rating.rating = (rData["rating"]! as NSString).floatValue
            
            if rData["image"] == ""
            {
                cellC.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = rData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellC.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellC
        }
        else if collectionView == self.collViewSearched_keywords
        {
            let cellS = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchKeywordCell", for: indexPath) as! SearchKeywordCell
            
            cellS.lbl_Keywords.text = visionImgSearchArray[indexPath.row]
            
            cellS.btn_Close.tag = indexPath.row
            cellS.btn_Close.addTarget(self, action: #selector(deleteSearchedKeyWord(_:)), for: .touchUpInside)
            
            return cellS
        }
        else
        {
            let cellD = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsMainCell", for: indexPath) as! ProductsMainCell
            
            let sData = self.filteredData[indexPath.row]
            
            cellD.lbl_PName.text = sData["name"] ?? ""
            
            let strLogo2 = sData["logo"]!
            let urlLogo2:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo2))")!
            cellD.img_Logo.af_setImage(withURL: urlLogo2 as URL)
             cellD.img_Logo.contentMode = .scaleAspectFit
            cellD.lbl_Price.text = "$\(sData["price"] ?? "")"
            
            cellD.view_Rating.rating = (sData["rating"]! as NSString).floatValue
            
            if sData["image"] == ""
            {
                cellD.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = sData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellD.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellD
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        self.view.endEditing(true)
        if collectionView == self.collectionViewHeader {
            
            if indexPath.row == 0 {
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"CategoriesVC")
                controller?.push()
            }
            else if indexPath.row == 1 {
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"FlashDealsVC")
                controller?.push()
            }
            else if indexPath.row == 2{
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"NewArrivalsVC") as! NewArrivalsVC
                controller.type = "New Arrivals"
                controller.push()
            }else
            {
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"NewArrivalsVC") as! NewArrivalsVC
                controller.type = "Best Sellers"
                controller.push()
            }
        }
        else if collectionView == self.collectionViewFProducts
        {
            let fData = self.featuredData[indexPath.row]
            let p_id = fData["product_id"]
            let variant_id = fData["product_variant_id"]
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
            controller.product_ID = p_id!
            controller.product_variant_ID = variant_id!
            controller.push()
        }
        else if collectionView == self.collectionViewRViewed
        {
            let rData = self.recentData[indexPath.row]
            
            let p_id = rData["product_id"]
            let variant_id = rData["product_variant_id"]
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
            controller.product_ID = p_id!
            controller.product_variant_ID = variant_id!
            controller.push()
        }
        else if collectionView == self.collViewSearched_keywords
        {
        }
        else{
            let sData = self.filteredData[indexPath.row]
            
            let p_id = sData["product_id"]
            let variant_id = sData["product_variant_id"]
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
            controller.product_ID = p_id!
            controller.product_variant_ID = variant_id!
            controller.push()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionViewHeader
        {
            let width = (self.view.frame.size.width) / 3.5
            let height = CGFloat(40)
            return CGSize(width: width, height: height)
        }
        else if collectionView == collectionViewSearch
        {
            let width = (self.view.frame.size.width - 40) / 2
            let height = width * 1.2
            return CGSize(width: width, height: height)
        }
        else if collectionView == collViewSearched_keywords
        {
            let width = (visionImgSearchArray[indexPath.row] as NSString).size(attributes: nil)
            let height = CGFloat(30)
            return CGSize(width: width.width+40, height: height)
        }
        else
        {
         //   let width = (self.view.frame.size.width - 40) / 2.2
            let height = CGFloat(170)
            return CGSize(width: 170, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.collectionViewHeader
        {
            return 2.0
        }else
        {
        return 10.0
        }
    }
    
    // MARK: - Delete Searched KeyWord Method
    @IBAction func deleteSearchedKeyWord(_ sender: UIButton)
    {
        if self.visionImgSearchArray.count == 1
        {
            self.collViewSearched_keywords.isHidden = true
           // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            self.type = "home"
            self.imagesearchOn = "N"
        }else{
            self.visionImgSearchArray.remove(at: sender.tag)
            print("Updated Keyword Array----> \(self.visionImgSearchArray)")
            self.collViewSearched_keywords.reloadData()
            self.fetchSearchdataWebServiceCall(searchText: "")
        }
     }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.view_Search.isHidden = false
        filteredData.removeAll()
        
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
             self.view_Search.isHidden = true
        }
        else
        {
            if searchText.length >= 3
            {
                type = "home"
                self.fetchSearchdataWebServiceCall(searchText: searchText)
    
            }
            //            if searchText.length >= 3
            //            {
            //                let arr = featuredData.filter({ (fruit) -> Bool in
            //                    return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
            //                })
            //                filteredData = arr
            //            }
        }
        collectionViewSearch.reloadData()
    }
    
    
    // MARK: - Call WebServices
    func fetchFeaturedRecentWebServiceCall() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "subcat_id":subcat_id]
        
        webService_obj.fetchDataFromServer(header:"productHome", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.productData = responce.value(forKey: "data") as! [String:[[String:String]]]
                
                self.featuredData = self.productData["feature_product"]!
                self.recentData = self.productData["recent_view"]!
                
                //Banner Data 
                self.homeBannerData = self.productData["baner"]!
                self.imagesbanner =  self.homeBannerData.map{ $0["image"]!}
                self.scrollView.subviews.forEach { $0.removeFromSuperview() }
                var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
                
                self.pageControl.numberOfPages = self.imagesbanner.count
                self.pageControl.currentPage = 0
                
                for index in 0..<self.imagesbanner.count {
                    frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
                    frame.size = self.scrollView.frame.size
                    self.productImg  = UIImageView(frame: frame)
                    let btn = UIButton(frame: frame)
                    let str = self.imagesbanner[index]
                    let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                    self.productImg.af_setImage(withURL: url as URL)
                    self.scrollView.addSubview(self.productImg)
                    self.scrollView.addSubview(btn)
                    btn.tag = index
                    btn.addTarget(self, action: #selector(self.btnBannerClick(_:)), for: .touchUpInside)
                 }
                self.scrollView.contentSize = CGSize(width:CGFloat(self.imagesbanner.count)*self.scrollView.bounds.width, height: self.scrollView.bounds.height)
                 
                self.collectionViewFProducts.reloadData()
                self.collectionViewRViewed.reloadData()
            }
        }
    }
    
    
    func bannerCountWebService(banner_Id:String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "ad_id":banner_Id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"banerClick", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
            }
        }
    }
    
    
    func fetchSearchdataWebServiceCall(searchText :String) {
        if searchText == ""
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "search":self.visionImgSearchArray,
                        "type" : "image_search",
                        "page":-1]
        }else
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "search":searchText,
                        "type" : type,
                        "page":-1]
        }
        load.hide(delegate: self)
        
        webService_obj.fetchDataFromServer(alertMsg: true,header:"product_searching", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                let searchData = responce.value(forKey: "data") as! [String: Any]
                let product = searchData["product"] as! [[String:String]]
                
                if self.type == "home"
                {
                    let arr = product.filter({ (fruit) -> Bool in
                        return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
                    })
                    self.filteredData = arr
                }else
                {
                    self.filteredData = product
                 }
                
                self.collViewSearched_keywords.reloadData()
                self.collectionViewSearch.reloadData()
            }else
            {
                  if self.imagesearchOn == "Y"
                 {
                    self.collViewSearched_keywords.isHidden = true
                    //self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
                    self.btn_ImgCamera.isHidden = true
                    self.btn_ImgCamera.tag = 0
                    self.view_Search.isHidden = true
                    self.type = "home"
                    self.imagesearchOn = "N"
                  }
            }
        }
    }
    
    @IBAction func btnCameraImageClick(_ sender: UIButton) {
        
        if self.btn_ImgCamera.tag == 1
        {
            collViewSearched_keywords.isHidden = true
           // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            type = "home"
            self.imagesearchOn = "N"
        }else
        {
             collViewSearched_keywords.isHidden = false
            
            filteredData.removeAll()
            visionImgSearchArray.removeAll()
            collViewSearched_keywords.reloadData()
            collectionViewSearch.reloadData()
            
            // self.btn_ImgCamera.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = false
            type = "image_search"
            self.imagesearchOn = "Y"
            self.view_Search.isHidden = false
            self.AlertController()
            self.btn_ImgCamera.tag = 1
        }
    }
    
    // MARK: - Method Alert Controller
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
        
        let ScanAction = UIAlertAction(title: "Bar Code Scan", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Bar Code Scan")
            
            self.collViewSearched_keywords.isHidden = true
            // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            self.type = "home"
            self.imagesearchOn = "N"
            
            if self.btn_camera.tag == 1
            {
                self.btn_camera.setImage(UIImage(named: "chatcamera"), for: UIControlState.normal)
                self.btn_camera.tag = 0
                self.view_Search.isHidden = true
                self.type = "home"
                scan_Code = ""
            }else
            {
                let st = UIStoryboard.init(name: "Product", bundle: nil)
                
                let vc3 = st.instantiateViewController(withIdentifier: "ProductScanVC") as? ProductScanVC
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController!.present(vc3!, animated: true, completion: nil)
                self.type = "barcode"
            }

        })
        
        let imageAction = UIAlertAction(title: "Image Recognition", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let optionMenu = UIAlertController(title: "", message: "Choose Option", preferredStyle: .actionSheet)
            
            let deleteAction = UIAlertAction(title: "Camera", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                print("Camera")
                self.imageCamera()
            })
            
            let saveAction = UIAlertAction(title: "Gallery", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                print("Gallery")
                self.imageGallery()
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                print("Cancelled")
                
                self.collViewSearched_keywords.isHidden = true
              //  self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
                self.btn_ImgCamera.isHidden = true
                self.btn_ImgCamera.tag = 0
                self.view_Search.isHidden = true
                self.type = "home"
                self.imagesearchOn = "N"
            })
            
            optionMenu.addAction(deleteAction)
            optionMenu.addAction(saveAction)
            optionMenu.addAction(cancelAction)
            
            // 5
            self.present(optionMenu, animated: true, completion: nil)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
            
            self.collViewSearched_keywords.isHidden = true
           // self.btn_ImgCamera.setImage(UIImage(named: "search_image"), for: UIControlState.normal)
            self.btn_ImgCamera.isHidden = true
            self.btn_ImgCamera.tag = 0
            self.view_Search.isHidden = true
            self.type = "home"
            self.imagesearchOn = "N"
        })
        
        // 4
        optionMenu.addAction(ScanAction)
        optionMenu.addAction(imageAction)
        optionMenu.addAction(cancelAction)
 
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
}

extension ProductsVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imageGallery()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imageCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedURL = info[UIImagePickerControllerReferenceURL] as? NSURL {
            print(pickedURL)
        }
        pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        //img_Profile.image = pickedImage
        
        // Base64 encode the image and create the request
        let binaryImageData = base64EncodeImage(pickedImage)
        createRequest(with: binaryImageData)
        load.show(views: self.view)
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(_ imageSize: CGSize, image: UIImage) -> Data {
        UIGraphicsBeginImageContext(imageSize)
        image.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        let resizedImage = UIImagePNGRepresentation(newImage!)
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    func analyzeResults(_ dataToParse: Data) {
        
        // Update UI on the main thread
        DispatchQueue.main.async(execute: {
            
            // Use SwiftyJSON to parse results
            let json = JSON(data: dataToParse)
            let errorObj: JSON = json["error"]
            
            // Check for errors
            if (errorObj.dictionaryValue != [:]) {
                // self.lblLableAnotation.text = "Error code \(errorObj["code"]): \(errorObj["message"])"
            } else {
                // Parse the response
                print(json)
                let responses: JSON = json["responses"][0]
                
                self.visionImgSearchArray.removeAll()
                
                // Get logoAnnotations
                let logoAnnotations: JSON = responses["logoAnnotations"]
                let numLogoLabels: Int = logoAnnotations.count
                if numLogoLabels > 0 {
                    //var labelResultsText:String = "Logo Annotations Found: "
                    for index in 0..<numLogoLabels {
                        let label = logoAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                }
                
                // Get textAnnotations
                let txtAnnotations: JSON = responses["textAnnotations"]
                let numtxtLabels: Int = txtAnnotations.count
                if numtxtLabels > 0 {
                    //  var labelResultsText:String = "Text Annotations Found: "
                    for index in 0..<numtxtLabels {
                        let label = txtAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                }
                
                // Get label annotations
                let labelAnnotations: JSON = responses["labelAnnotations"]
                let numLabels: Int = labelAnnotations.count
                if numLabels > 0 {
                    for index in 0..<numLabels {
                        let label = labelAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                }
                
                // Get webDetection
                let webAnnotations: JSON = responses["webDetection"]["webEntities"]
                let numWebLabels: Int = webAnnotations.count
                if numWebLabels > 0 {
                    for index in 0..<numWebLabels {
                        let label = webAnnotations[index]["description"].stringValue
                        if label.length >= 3
                        {
                            self.visionImgSearchArray.append(label.capitalized)
                        }
                    }
                    print("Complete Array----> \(self.visionImgSearchArray.count)")
                    
                    let uniqueStrings = self.uniqueElementsFrom(array:self.visionImgSearchArray)
                    
                    self.visionImgSearchArray = uniqueStrings
                    
                    print("Complete Array uniqueStrings----> \(self.visionImgSearchArray.count)")
                    
                    self.type = "image_search"
                    self.view_Search.isHidden = false
                    self.fetchSearchdataWebServiceCall(searchText: "")
                }
                
             }
        })
    }
    
    func uniqueElementsFrom(array: [String]) -> [String] {
        //Create an empty Set to track unique items
        var set = Set<String>()
        let result = array.filter {
            guard !set.contains($0) else {
                //If the set already contains this object, return false
                //so we skip it
                return false
            }
            //Add this item to the set since it will now be in the array
            set.insert($0)
            //Return true so that filtered array will contain this item.
            return true
        }
        return result
    }
}


extension ProductsVC {
    func base64EncodeImage(_ image: UIImage) -> String {
        var imagedata = UIImagePNGRepresentation(image)
        
        // Resize the image if it exceeds the 2MB API limit
        if (imagedata?.count > 2097152) {
            let oldSize: CGSize = image.size
            let newSize: CGSize = CGSize(width: 800, height: oldSize.height / oldSize.width * 800)
            imagedata = resizeImage(newSize, image: image)
        }
        
        return imagedata!.base64EncodedString(options: .endLineWithCarriageReturn)
    }
    
    func createRequest(with imageBase64: String) {
        // Create our request URL
        
        var request = URLRequest(url: googleURL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(Bundle.main.bundleIdentifier ?? "", forHTTPHeaderField: "X-Ios-Bundle-Identifier")
        
        // Build our API request
        let jsonRequest = [
            "requests": [
                "image": [
                    "content": imageBase64
                ],
                "features": [
                    [
                        "type": "LABEL_DETECTION",
                        "maxResults": 10
                    ],
                    [
                        "type": "TEXT_DETECTION",
                        "maxResults": 10
                    ],
                    [
                        "type": "LOGO_DETECTION",
                        "maxResults": 10
                    ],
                    [
                        "type": "WEB_DETECTION",
                        "maxResults": 10
                    ]
                    
                ]
            ]
        ]
        let jsonObject = JSON(jsonDictionary: jsonRequest)
        
        // Serialize the JSON
        guard let data = try? jsonObject.rawData() else {
            return
        }
        
        request.httpBody = data
        
        // Run the request on a background thread
        DispatchQueue.global().async { self.runRequestOnBackgroundThread(request) }
    }
    
    func runRequestOnBackgroundThread(_ request: URLRequest) {
        // run the request
        
        let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }
            
            
            self.analyzeResults(data)
        }
        
        task.resume()
    }
}


// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


