//
//  AllReviewsVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 22/11/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class VendorsListCell: UITableViewCell {
    @IBOutlet var lblVendor_name: UILabel!
    @IBOutlet var lblVendor_Description: UILabel!
    @IBOutlet var lblVendor_date: UILabel!
    @IBOutlet var vendor_rating: FloatRatingView!
    
}

class AllReviewsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
   var Vendor_Id = ""
    var allreviewData = [[String:String]]()
    @IBOutlet var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.AllReviewsWebServiceMethod()
        self.tableview.tableFooterView = UIView()
        tableview.estimatedRowHeight = 44.0
        tableview.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    //MARK: UITableView
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allreviewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VendorsListCell", for: indexPath) as! VendorsListCell
            cell.lblVendor_name.text = self.allreviewData[indexPath.row]["what_provider_do"]!
            cell.lblVendor_Description.text = self.allreviewData[indexPath.row]["how_it_go"]!
            cell.lblVendor_date.text =  self.allreviewData[indexPath.row]["date"]!
            cell.vendor_rating.rating = (self.allreviewData[indexPath.row]["overall_grade"]! as NSString).floatValue
            cell.selectionStyle = .none
            return cell
        
    }
    func AllReviewsWebServiceMethod() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "user_id": self.Vendor_Id,
                    "page":"-1"]
        
        webService_obj.fetchDataFromServer(header:"get_service_review", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.allreviewData = responce.value(forKey: "data") as! [[String:String]]
                self.tableview.reloadData()
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
