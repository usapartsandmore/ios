//
//  VendorsVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 28/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import MapKit
import AlamofireImage
import MessageUI

class VendorOffersCell: UICollectionViewCell {
    
    @IBOutlet var lbl_EndingDate: UILabel!
    @IBOutlet var lbl_totalSaving: UILabel!
    @IBOutlet var lbl_price: UILabel!
    @IBOutlet var lbl_offerPrice: UILabel!
    @IBOutlet var lbl_offerTitle: UILabel!
    @IBOutlet var promotionalOfferImagview: UIImageView!
}

internal class VendorTblCell: UITableViewCell {
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_detail: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet var vendorRating: FloatRatingView!
    
    
}

class VendorsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout,MKMapViewDelegate, MFMailComposeViewControllerDelegate,CLLocationManagerDelegate
{
    @IBOutlet var collectionviewHeight: NSLayoutConstraint!
    @IBOutlet var btn_Fav: UIButton!
    @IBOutlet var btn_viewAllReviews: UIButton!
    @IBOutlet var lbl_noReviewAvailable: UILabel!
    @IBOutlet weak var reviewTblConst: NSLayoutConstraint!
    
    @IBOutlet var img_fav: UIImageView!
    @IBOutlet var vendorMapview: MKMapView!
    @IBOutlet var collectionViewOffers: UICollectionView!
    @IBOutlet weak var tableViewOffers: UITableView!
    @IBOutlet var lbl_vendorDescription1: UILabel!
    @IBOutlet var lbl_vendorName: UILabel!
    @IBOutlet var lbl_noOffer: UILabel!
    @IBOutlet var img_vendorLogo: UIImageView!
    @IBOutlet var vendor_rating: FloatRatingView!
    @IBOutlet weak var lblReturnPolicy: UILabel!
    var isFavSelected = Bool()
    var vendorDetailData = [String:Any]()
    var PromotionalOfferData = [[String:String]]()
    var reviewData = [[String:String]]()
    var selectedVendorId = ""
    
    var vendorNumbers = ""
    var vendorWebLink = ""
    var vendorEmail = ""
    let locationManager = CLLocationManager()
    var refrralMSG = ""
    
    @IBOutlet weak var lbl_ServiceOffere: UILabel!
     @IBOutlet weak var lbl_keeps: UILabel!
      @IBOutlet var top_Space: NSLayoutConstraint!
    
    
    @IBOutlet weak var lbl_Warrenty: UILabel!
    @IBOutlet weak var lbl_RecentView: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.tableViewOffers.tableFooterView = UIView()
        tableViewOffers.estimatedRowHeight = 44.0
        tableViewOffers.rowHeight = UITableViewAutomaticDimension
        self.lbl_noOffer.isHidden = true
        vendorMapview.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        vendorMapview.delegate = self
        vendorMapview.mapType = .standard
        vendorMapview.isZoomEnabled = true
        vendorMapview.isScrollEnabled = true
        
        if let coor = vendorMapview.userLocation.location?.coordinate{
            vendorMapview.setCenter(coor, animated: true)
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.VendorDetailWebServiceMethod()
    }
 
    // MARK: View All Review button
    @IBAction func btnViewAllReviewClick(_ sender: UIButton) {
        let main = UIStoryboard(name: "Services", bundle: nil)
        let controller = main.instantiateViewController(withIdentifier:"AllReviewsVC")as! AllReviewsVC
        controller.Vendor_Id = self.selectedVendorId
        controller.push()
     }
    
    @IBAction func btnCallToVendorClick(_ sender: UIButton) {
        
        var number = self.vendorNumbers
        
        number = number.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "")
        
        if let url = URL(string: "telprompt://"+number){
            if UIApplication.shared.canOpenURL(url){
                 if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
    }
    
    @IBAction func btnMsgToVendorClick(_ sender: UIButton) {
         if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            
            mail.setToRecipients([self.vendorEmail])
            mail.title = "MARITIME YACHT SERVICES"
            mail.setSubject("Contact Vendor")
            
            present(mail, animated: true)
        } else {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Simulator Error", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
             }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }
     }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func webLink(_ sender: UIButton)
    {
          let url = NSURL(string: self.vendorWebLink)
        if #available(iOS 10.0, *)
        {
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url! as URL)
        }
    }
    
    @IBAction func btnRefferralClick(_ sender: UIButton) {
             // set up activity view controller
            let textToShare = [ refrralMSG ]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that
            self.present(activityViewController, animated: true, completion: nil)
       }
    
    @IBAction func moreBtnClick(_ sender: UIButton) {
        let main = UIStoryboard(name: "Services", bundle: nil)
        let controller = main.instantiateViewController(withIdentifier:"CompanyInfoVC")as! CompanyInfoVC
        controller.vendorCompanyInfo = self.vendorDetailData
        controller.push()
    }
    
    @IBAction func btnFavoriteClick(_ sender: UIButton) {
        if self.isFavSelected == true
        {
            self.isFavSelected = false
            self.img_fav.image = UIImage(named:"servicefavorit")as UIImage?
        }
        else{
            self.isFavSelected = true
             self.img_fav.image = UIImage(named:"favorites")as UIImage?
        }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "user_id":self.selectedVendorId,
                    "is_favorite":self.isFavSelected ? 1 : 0]
        
        webService_obj.fetchDataFromServer(header:"favorites_users", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
            }
        }
        
        
    }
    // MARK: Write review button
    @IBAction func messageButtonClick(_ sender: UIButton) {
        let main = UIStoryboard(name: "Services", bundle: nil)
        let controller = main.instantiateViewController(withIdentifier:"WriteReviewVC") as! WriteReviewVC
        controller.vendorId = self.selectedVendorId
        controller.push()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    
    //MARK: UITableView
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.reviewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VendorTblCell", for: indexPath) as! VendorTblCell
        cell.lbl_Title.text = self.reviewData[indexPath.row]["what_provider_do"]!
        cell.lbl_detail.text = self.reviewData[indexPath.row]["how_it_go"]!
        cell.lbl_date.text =  self.reviewData[indexPath.row]["date"]!
        cell.vendorRating.rating = (self.reviewData[indexPath.row]["overall_grade"]! as! NSString).floatValue
        return cell
    }
    
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.PromotionalOfferData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "VendorOffersCell", for: indexPath) as! VendorOffersCell
        let promotionalData = self.PromotionalOfferData[indexPath.row]
        cellA.lbl_offerPrice.text = "$\(promotionalData["offered_price"] ?? "")"
        cellA.lbl_offerTitle.text = promotionalData["name"] ?? ""
        if promotionalData["image"] == ""
        {
            cellA.promotionalOfferImagview.image = UIImage(named:"noimage")
        }else{
            let str = promotionalData["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cellA.promotionalOfferImagview.af_setImage(withURL: url as URL)
        }
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "$\(promotionalData["retail_price"] ?? "")")
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
        cellA.lbl_price.attributedText = attributeString
        
        let str = "You Save \(promotionalData["discount"] ?? "")%"
        let range = (str as NSString).range(of: "\(promotionalData["discount"] ?? "")%")
        let attrString = NSMutableAttributedString(string: str)
        attrString.addAttribute(NSForegroundColorAttributeName, value: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) , range: NSRange(location: 0, length: str.length))
        attrString.addAttribute(NSForegroundColorAttributeName, value: #colorLiteral(red: 0.2862745098, green: 0.6274509804, blue: 0.8941176471, alpha: 1) , range: range)
        cellA.lbl_totalSaving.attributedText = attrString
        
        let DateStr = "Ending: \(promotionalData["offer_end_date"] ?? "")"
        let dateRange = (DateStr as NSString).range(of: "\(promotionalData["offer_end_date"] ?? "")")
        let dateAttrString = NSMutableAttributedString(string: DateStr)
        dateAttrString.addAttribute(NSForegroundColorAttributeName, value: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) , range: NSRange(location: 0, length: DateStr.length))
        dateAttrString.addAttribute(NSForegroundColorAttributeName, value: #colorLiteral(red: 0.2862745098, green: 0.6274509804, blue: 0.8941176471, alpha: 1) , range: dateRange)
        cellA.lbl_EndingDate.attributedText = dateAttrString
        
        return cellA
     }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
 
        let promotionalData = self.PromotionalOfferData[indexPath.row]
         let service_id = promotionalData["id"]
        //let variant_id = promotionalData["product_variant_id"]
        
        //let offerId = dataArr["link"]!
        let main = UIStoryboard(name: "Main", bundle: nil)
        let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
        controller.localOffer_id = service_id!
        controller.isfromMyOrders = false
        controller.push()
     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    // MARK: vendorMapview
     func mapView(_ map: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        let reuseId = "AnnotationIdentifier"
        
        var anView = vendorMapview.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.image = UIImage(named:"locationicon")
            anView?.canShowCallout = true
        }
        else {
            //we are re-using a view, update its annotation reference...
            anView?.annotation = annotation
        }
         return anView
     }
    
    
     // MARK: Web-Service Methods
    func VendorDetailWebServiceMethod () {
        // Search all vendore web service method
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "user_id": self.selectedVendorId]
        
        webService_obj.fetchDataFromServer(header:"service_user_details", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
                let vendorData = responce.value(forKey: "data") as! [String:Any]
                self.PromotionalOfferData = vendorData["promotionalOffers"] as! [[String:String]]
                self.vendorDetailData = vendorData["userInfo"] as! [String:Any]
                self.lbl_vendorName.text = self.vendorDetailData["company_name"] as? String ?? ""
                
                self.refrralMSG = "Hi, Please checkout Vendor \(self.vendorDetailData["company_name"] as? String ?? "") on app MYS: \(vendorData["appstore"] as? String ?? "") for their awesome services."
                
                self.vendorNumbers = self.vendorDetailData["contact_number"] as? String ?? ""
                self.vendorWebLink = self.vendorDetailData["website_link"] as? String ?? ""
                self.vendorEmail = self.vendorDetailData["email"] as? String ?? ""
                
                if self.vendorDetailData["return_policies"] as? String ?? "" == ""
                {
                    self.lblReturnPolicy.text = ""
                    self.lbl_Warrenty.text = ""
                }
                else
                
                {
                    self.lbl_Warrenty.text = "Warranty & Return Policies"
                    self.lblReturnPolicy.text = self.vendorDetailData["return_policies"] as? String ?? ""
                }
                
                
                let str = self.vendorDetailData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                self.img_vendorLogo.af_setImage(withURL: url as URL)
                self.vendor_rating.rating = (self.vendorDetailData["rating"]! as! NSString).floatValue
                self.collectionViewOffers.reloadData()
                self.lbl_vendorDescription1.text = self.vendorDetailData["description"]! as? String
              
                
                if self.PromotionalOfferData.count == 0 {
                    self.collectionViewOffers.isHidden = false
                    self.collectionviewHeight.constant = 0.0
                    self.lbl_noOffer.isHidden = true
                    self.lbl_ServiceOffere.text = ""
                    self.lbl_keeps.text = ""
                    self.top_Space.constant = 0.0
                    
                }
                else{
                      self.collectionviewHeight.constant = 181.0
                    self.top_Space.constant = 15.0
                    self.lbl_noOffer.isHidden = true
                    self.lbl_ServiceOffere.text = "Services Offered"
                    self.lbl_keeps.text = "MYS KEEPS TRACK OF WARRANTIES"
                }
                if self.vendorDetailData["is_favorite"] as! String == "1" {
                    self.img_fav.image = UIImage(named:"favorites")as UIImage?
                    self.isFavSelected = true
                }
                self.view.layoutIfNeeded()
                let latitude = Double(self.vendorDetailData["lat"] as! String) ?? 0.0
                let longitude = Double(self.vendorDetailData["lng"] as! String) ?? 0.0
                let cordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let annotation = MKPointAnnotation()
                annotation.coordinate = cordinate
                annotation.title = self.vendorDetailData["company_name"] as? String ?? ""
                self.vendorMapview.addAnnotation(annotation)
                self.vendorMapview.showAnnotations(self.vendorMapview.annotations, animated: true)
                self.AllReviewsWebServiceMethod()
                
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        vendorMapview.mapType = MKMapType.standard
        
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        vendorMapview.setRegion(region, animated: true)
        //centerMap(locValue)
    }
    func AllReviewsWebServiceMethod() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "user_id": self.selectedVendorId,
                    "page":"3"]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"get_service_review", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                self.reviewData = responce.value(forKey: "data") as! [[String:String]]
                
                if self.reviewData.count < 0{
                    self.reviewTblConst.constant = 0
                    self.btn_viewAllReviews.isHidden = true
                    self.lbl_noReviewAvailable.isHidden = false
                    self.lbl_keeps.text = ""
                }else{
                    self.reviewTblConst.constant = 250
                    self.btn_viewAllReviews.isHidden = false
                    self.lbl_noReviewAvailable.isHidden = true
                    self.lbl_keeps.text = "Recent Reviews"
                }
                
                self.tableViewOffers.reloadData()
                
            }else{
                self.reviewTblConst.constant = 0
                self.btn_viewAllReviews.isHidden = true
                self.lbl_noReviewAvailable.isHidden = false
                
            }
        }
    }
}
