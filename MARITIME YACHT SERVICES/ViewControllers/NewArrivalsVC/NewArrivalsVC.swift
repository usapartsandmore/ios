//
//  NewArrivalsVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 23/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class NewArrivalsCell: UICollectionViewCell {
    @IBOutlet weak var lbl_PName: UILabel!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var img_Logo: UIImageView!
    @IBOutlet weak var view_Rating: FloatRatingView!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_EndingDate: UILabel!
}

class NewArrivalsVC: UIViewController , UIScrollViewDelegate, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var collectionViewArrivals: UICollectionView!
    @IBOutlet var productImg: UIImageView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var collectionViewSearch: UICollectionView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var searchView: UIView!
    @IBOutlet var btn_topSearch: UIButton!
    
    @IBOutlet var lbl_sortBy: UILabel!
    @IBOutlet var lbl_filterCategory: UILabel!
    @IBOutlet var img_filterDropDown: UIImageView!
    @IBOutlet var img_sortDropDown: UIImageView!
    @IBOutlet var lbl_title: UILabel!
    @IBOutlet var lbl_noData: UILabel!
    
    var filteredData = [[String:String]]()
    var arrSortBy = [String]()
    var selectedCategoriesDict = [[String:String]]()
    var type = ""
    var search_type = ""
    
    var homeBannerData = [[String:String]]()
    var imagesbanner = [String]()
    var productData = [[String:String]]()
    var str_SortBy = ""
    
    var isPageRefresing = Bool()
    var currentPageNumber = 1
    
    var subcat_id = ""
    var subcat_name = ""
    var timer_Ride: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         currentPageNumber = 1
        
        if type == "Sub Category Product"
        {
             lbl_title.text = subcat_name
        }else{
             lbl_title.text = type
        }
        
        lbl_noData.isHidden = true
        
        pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        
        self.searchView.tag = 0
        self.searchView.isHidden = true
        arrSortBy = ["All","Price","Rating"]
        str_SortBy = "All"
        timer_Ride = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
     
        self.fetchDealsWebServiceCall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        
        if scan_Code.length != 0
        {
            search_type = "barcode"
            self.collectionViewSearch.isHidden = false
            self.fetchSearchdataWebServiceCall(searchText: scan_Code)
            
        }else
        {
            self.collectionViewSearch.isHidden = true
            search_type = type
        }

    }
    
    @objc func scrollToNextCell(){
        
        var offset = scrollView.contentOffset
        offset.x += scrollView.bounds.width
        if offset.x >= scrollView.contentSize.width{
            offset.x = 0
        }
        UIView.animate(withDuration: 0.25) {
            self.scrollView.contentOffset = offset
        }
        let pageNumber = round(offset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        pageControl.updateCurrentPageDisplay()
     }
    
    
   @IBAction func btnBannerClick(_ sender: UIButton) {
        let bIndex = pageControl.currentPage
        if homeBannerData.count > 0
        {
            let dataArr = homeBannerData[bIndex]
            
            // self.bannerCountWebService(banner_Id: data["id"]!)
            //1=Link to Third Domain , 2 = Link to Product , 3 = Link to Service , 4 = Link to local offer
            
            let adType = dataArr["link_type"]!
            
            switch adType {
            case "1" :
                let urlstr = dataArr["link"]!
                let url = NSURL(string: urlstr)
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url! as URL)
                }
            case "2" :
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
                controller.product_ID = dataArr["product_id"]!
                controller.product_variant_ID = dataArr["product_variant_id"]!
                controller.push()
            case "3" :
                let vendorId = dataArr["link"]!
                let st = UIStoryboard.init(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
                controller.selectedVendorId = vendorId
                controller.push()
            case "4" :
                let offerId = dataArr["link"]!
                let main = UIStoryboard(name: "Main", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
                controller.localOffer_id = offerId
                controller.isfromMyOrders = false
                controller.push()
            default:
                print("F. You failed")//Any number less than 0 or greater than 99
                
            }
            self.bannerCountWebService(banner_Id: dataArr["id"]!)
        }
    }

    
    @IBAction func btnCameraClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Product", bundle: nil)
        let vc3 = st.instantiateViewController(withIdentifier: "ProductScanVC") as? ProductScanVC
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController!.present(vc3!, animated: true, completion: nil)
        search_type = "barcode"
    }

    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func GoToCartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    // MARK: ScrollView Methods
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            pageControl.currentPage = Int(pageNumber)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != self.scrollView {
            //            if (collectionViewSearch != nil)
            //            {
            //                if collectionViewSearch.contentOffset.y >= (collectionViewSearch.contentSize.height - collectionViewSearch.bounds.size.height) {
            //                    if isPageRefresing == false {
            //                        isPageRefresing = true
            //                        currentPageNumber = currentPageNumber + 1
            //
            //                        self.fetchSearchdataWebServiceCall()
            //                    }
            //                }
            //            }else{
            if collectionViewArrivals.contentOffset.y >= (collectionViewArrivals.contentSize.height - collectionViewArrivals.bounds.size.height) {
                if isPageRefresing == false {
                    isPageRefresing = true
                    currentPageNumber = currentPageNumber + 1
                    
                    self.fetchDealsWebServiceCall()
                }
            }
        }
    }
        
 
    
    @IBAction func btnSearchClick(_ sender: UIButton) {
        if self.searchView.tag == 0
        {
            self.searchView.isHidden = false
            self.searchBar.becomeFirstResponder()
            self.searchView.tag = 1
            self.btn_topSearch.setImage(UIImage(named: "cross-2"), for: UIControlState.normal)
        }
        else {
            self.searchBar.text = ""
            filteredData.removeAll()
            scan_Code = ""
            
            self.searchView.isHidden = true
            self.searchBar.endEditing(true)
            self.btn_topSearch.setImage(UIImage(named: "search"), for: UIControlState.normal)
            self.searchView.tag = 0
            collectionViewSearch.reloadData()
            
            if currentPageNumber == 1
            {
                self.fetchDealsWebServiceCall()
            }
        }
    }
     
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionViewSearch
        {
            return filteredData.count
        }else{
            return productData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionViewArrivals
        {
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "NewArrivalsCell", for: indexPath) as! NewArrivalsCell
            
            let rData = self.productData[indexPath.row]
            
            cellA.lbl_PName.text = rData["name"] ?? ""
            cellA.lbl_Price.text = "$\(rData["price"] ?? "")"
            cellA.view_Rating.rating = (rData["rating"]! as NSString).floatValue
            
            if type == "New Arrivals"
            {
                cellA.lbl_EndingDate.text = "Added:\(rData["created"] ?? "")"
            }else if type == "Best Sellers"
            {
                cellA.lbl_EndingDate.text = rData["totalSold"] ?? ""
            }else
            {
                cellA.lbl_EndingDate.text = rData["productViewStatus"] ?? ""
            }
            
            let strLogo1 = rData["logo"]!
            let urlLogo1:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo1))")!
            cellA.img_Logo.af_setImage(withURL: urlLogo1 as URL)
            cellA.img_Logo.contentMode = .scaleAspectFit
            if rData["image"] == ""
            {
                cellA.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = rData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellA.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellA
        }
        else
        {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "NewArrivalsCell", for: indexPath) as! NewArrivalsCell
            
            let sData = self.filteredData[indexPath.row]
            
            cellB.lbl_PName.text = sData["name"] ?? ""
            cellB.lbl_Price.text = "$\(sData["price"] ?? "")"
            cellB.view_Rating.rating = (sData["rating"]! as NSString).floatValue
            cellB.img_Logo.contentMode = .scaleAspectFit
            if type == "New Arrivals"
            {
                cellB.lbl_EndingDate.text = "Added:\(sData["created"] ?? "")"
            }else if type == "Best Sellers"
            {
                cellB.lbl_EndingDate.text = sData["totalSold"] ?? ""
            }else
            {
                cellB.lbl_EndingDate.text = sData["productViewStatus"] ?? ""
            }
            
            let strLogo1 = sData["logo"]!
            let urlLogo1:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo1))")!
            cellB.img_Logo.af_setImage(withURL: urlLogo1 as URL)
            
            if sData["image"] == ""
            {
                cellB.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = sData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellB.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellB
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == self.collectionViewSearch
        {
            let sData = self.filteredData[indexPath.row]
            
            let p_id = sData["product_id"]
            let variant_id = sData["product_variant_id"]
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
            controller.product_ID = p_id!
            controller.product_variant_ID = variant_id!
            controller.push()
        }else
        {
            let fData = self.productData[indexPath.row]
            let p_id = fData["product_id"]
            let variant_id = fData["product_variant_id"]
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
            controller.product_ID = p_id!
            controller.product_variant_ID = variant_id!
            controller.push()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionViewSearch
        {
            let width = (self.view.frame.size.width - 40) / 2
            let height = width * 1.2
            return CGSize(width: width, height: height)
        }else
        {
            let width = (self.view.frame.size.width - 30) / 2
            let height = width * 1.1
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    
    // MARK: OPEN FILTER CATEGORY
    @IBAction func btnFilterCategoryClick(_ sender: UIButton) {
        if type == "Sub Category Product"
        {
            _ = kConstantObj.SetIntialMainViewController("CategoriesVC")
        }else{
            let st = UIStoryboard.init(name: "Product", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"FilterCategoryVC") as!FilterCategoryVC
            controller.block = { dict in
                print(dict)
                if dict.count != 0 {
                    self.selectedCategoriesDict = dict
                    self.lbl_filterCategory.text = "\(self.selectedCategoriesDict.count) Categories Selected"
                    self.img_filterDropDown.image = UIImage(named:"cross")
                }
                else{
                    self.selectedCategoriesDict.removeAll()
                    self.img_filterDropDown.image = UIImage(named:"arrowdown")
                    self.lbl_filterCategory.text = "Filter Categories"
                }
                self.currentPageNumber = 1
                self.fetchDealsWebServiceCall()
            }
            controller.selectedRows = self.selectedCategoriesDict
            controller.type = "Flash Deals"
            controller.push()
        }
    }
    
    // MARK: OPEN SORT BY
    @IBAction func btnSortByClick(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Sort By", rows:arrSortBy, initialSelection: 0, doneBlock: {
            picker,index,value in
            if value as! String == "All"
            {
                self.lbl_sortBy.text = "Sort by"
                self.str_SortBy = "All"
            }else{
                self.lbl_sortBy.text = "Sort by \(value as! String)"
                self.str_SortBy = value as! String
            }
            self.currentPageNumber = 1
            self.fetchDealsWebServiceCall()
            return
        }, cancel: {
            ActionStringCancelBlock in return
        }, origin: sender)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: SearchBar Delegate Method
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.collectionViewSearch.isHidden = false
        filteredData.removeAll()
        
        if searchText.isEmpty {
            searchBar.perform(#selector(self.resignFirstResponder), with: nil, afterDelay: 0.1)
            self.collectionViewSearch.isHidden = true
        }
        else
        {
            if searchText.length >= 3
            {
                 search_type = type
                self.currentPageNumber = 1
                self.fetchSearchdataWebServiceCall(searchText: searchText)
            }
        }
        collectionViewSearch.reloadData()
    }
    
    
    // MARK: - Call WebServices
    
    func fetchDealsWebServiceCall() {
        
        let filter_cat = self.selectedCategoriesDict.flatMap{($0["id"]) }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "filter_cat":filter_cat,
                    "sort_by":self.str_SortBy,
                    "type":type,
                    "page":currentPageNumber,
                    "subcat_id":subcat_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"allProductList", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                let data = responce.value(forKey: "data") as! [String: Any]
                
                self.homeBannerData = data["baner"] as! [[String : String]]
                self.imagesbanner =  self.homeBannerData.map{ $0["image"]!}
                self.scrollView.subviews.forEach { $0.removeFromSuperview() }
                var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
                
                self.pageControl.numberOfPages = self.imagesbanner.count
                self.pageControl.currentPage = 0
                
                for index in 0..<self.imagesbanner.count {
                    frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
                    frame.size = self.scrollView.frame.size
                    self.productImg  = UIImageView(frame: frame)
                    let str = self.imagesbanner[index]
                    let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                    self.productImg.af_setImage(withURL: url as URL)
                    let btn = UIButton(frame: frame)
                    self.scrollView.addSubview(self.productImg)
                    self.scrollView.addSubview(btn)
                    btn.tag = index
                    btn.addTarget(self, action: #selector(self.btnBannerClick(_:)), for: .touchUpInside)
                }
                self.scrollView.contentSize = CGSize(width:CGFloat(self.imagesbanner.count)*self.scrollView.bounds.width, height: self.scrollView.bounds.height)
                
                if self.currentPageNumber == 1
                {
                    self.productData = data["product"]  as! [[String:String]]
                }
                else
                {
                    self.productData.append(contentsOf: data["product"] as! [[String : String]])
                }
                self.isPageRefresing = false
                
                if self.productData.count == 0
                {
                    self.lbl_noData.isHidden = false
                }else
                {
                    self.lbl_noData.isHidden = true
                }
                
            }else
            {
                if (filter_cat.count != 0) || (self.str_SortBy != "All")
                {
                    if self.currentPageNumber == 1
                    {
                        self.productData.removeAll()
                        self.lbl_noData.isHidden = false
                    }
                }
                self.currentPageNumber -= 1
                self.isPageRefresing = false
                
                if self.productData.count == 0
                {
                    self.lbl_noData.isHidden = false
                }else
                {
                    self.lbl_noData.isHidden = true
                }
            }
            self.collectionViewArrivals.reloadData()
        }
    }
    
    
    func fetchSearchdataWebServiceCall(searchText :String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "search":searchText,
                    "type":search_type,
                    "page":currentPageNumber,
                    "subcat_id":subcat_id]
        
        webService_obj.fetchDataFromServer(alertMsg: true,header:"product_searching", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                let searchData = responce.value(forKey: "data") as! [String: Any]
                
                var product = [[String:String]]()
                
                if self.currentPageNumber == 1
                {
                    
                    if self.search_type != "barcode"
                    {
                        product = searchData["product"]  as! [[String:String]]
                        let arr = product.filter({ (fruit) -> Bool in
                            return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
                        })
                        self.filteredData = arr
                    }else
                    {
                        product = searchData["product"]  as! [[String:String]]
                        self.filteredData = product
                    }
                  }
                else
                {
                    if self.search_type != "barcode"
                    {
                        product.append(contentsOf: searchData["product"] as! [[String : String]])
                        
                        let arr = product.filter({ (fruit) -> Bool in
                            return (fruit["name"] as AnyObject).lowercased.contains(searchText.lowercased())
                        })
                        self.filteredData.append(contentsOf:arr)
                    }else
                    {
                        product.append(contentsOf: searchData["product"] as! [[String : String]])
                        self.filteredData.append(contentsOf:product)
                    }
                    
                }
                self.isPageRefresing = false
                
            }else
            {
                self.currentPageNumber -= 1
                self.isPageRefresing = false
            }
            self.collectionViewSearch.reloadData()
        }
    }
    
  
    func bannerCountWebService(banner_Id:String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "ad_id":banner_Id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"banerClick", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
            }
        }
    }
}
