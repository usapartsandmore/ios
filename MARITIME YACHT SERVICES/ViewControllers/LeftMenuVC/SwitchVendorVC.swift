//
//  SwitchVendorVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 10/10/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class VendorCustomCell: UITableViewCell {
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        if isSelected == false {
            accessoryType = UITableViewCellAccessoryType.none
        }
        else {
            accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
}

class SwitchVendorVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var vendorListData = [[String:String]] ()
    @IBOutlet var tableview: UITableView!
    
    override func viewDidLoad() {
        self.tableview.tableFooterView = UIView()
        super.viewDidLoad()
        self.getVendorlistWebServiceMethod()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: MENU BUTTON CLICK
    @IBAction func btnMenuClick(_ sender: UIButton) {
       _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vendorListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VendorCustomCell", for: indexPath) as! VendorCustomCell
        cell.textLabel?.text = self.vendorListData[indexPath.row]["user_name"]!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let indexPath = tableview.indexPathForSelectedRow{
            
            let newVendorID = vendorListData[(indexPath.row)]["user_id"]
            if newVendorID == "0"{
                PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                            "user_id":""]
            }else{
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "user_id":newVendorID!]
            }
            
            webService_obj.fetchDataFromServer(header:"set_default_customer_vendor", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    print(message)
                }
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath?{
//        if (tableView.indexPathForSelectedRow == indexPath){
//            self.tableview.deselectRow(at: indexPath, animated: false)
//    
//            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
//                        "user_id":""]
//            
//            webService_obj.fetchDataFromServer(header:"set_default_customer_vendor", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
//                if staus{
//                    print(message)
//                }
//            }
//
//            return nil
//        }else{
//            return indexPath
//        }
//    }
    
    
    // MARK: - Call WebServices
    func getVendorlistWebServiceMethod () {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"customer_vendor_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.vendorListData = responce.value(forKey: "data") as! [[String:String]]
                
                let allData = ["user_name":"All Vendors",
                               "user_id":"0",
                               "is_default":"0",
                               "account_number":"000000"]
                
                self.vendorListData.insert(allData, at: 0)
                
                self.tableview.reloadData()
                let defaultVendorID = "1"
                var indexPaths = [IndexPath]()
                
                var isallvendor = true

                for (index, dict) in self.vendorListData.enumerated(){
                    let id = dict["is_default"]!
                     if  defaultVendorID == id{
                        indexPaths.append(IndexPath(row: index, section: 0))
                        isallvendor = false
                    }
                }
                
                if isallvendor == true{
                    indexPaths.append(IndexPath(row: 0, section: 0))
                }
                
                indexPaths.forEach { (indexPath) in
                    self.tableview.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
     }
   }
 }
