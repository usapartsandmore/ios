//
//  LeftMenuVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 03/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKCoreKit
import FBSDKLoginKit

class LeftMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var viewBlurBlackView: UIView!
    
    @IBOutlet var viewLogoutPopover: UIView!
    @IBOutlet weak var tblLeftMenu: UITableView!
    
    var arrNames = NSArray()
    var arrImgs  = NSArray()
    var arrImgsSelected = NSArray()
    var kConstantObj = kConstant()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        self.tblLeftMenu.separatorColor = UIColor.clear
        
        arrNames = ["Home","Profile","My Orders","Events/Tasks","Expense Records","My Team","Favorites","Notification","Legal","Help","Refer a Vessel","Logout"]
        arrImgs = ["home","profile","myorder","calender","expresrecord","myteam","favorit","notification","legal","help","refer","logout"]
        arrImgsSelected = ["home","profile","myorder","calender","expresrecord","myteam","favorit","notification","legal","help","refer","logout"]
        
        //        arrNames = ["Home","Profile","Calendar/Tasks","Expense Records","My Team","Legal","Help","Logout"]
        //        arrImgs = ["home","profile","calender","expresrecord","myteam","legal","help","logout"]
        //        arrImgsSelected = ["home","profile","calender","expresrecord","myteam","legal","help","logout"]
        
        var strisLogin = ""
        if (defaults.object(forKey: "ISLOGIN") != nil){
            strisLogin = defaults.object(forKey: "ISLOGIN") as! String
        }
        if strisLogin == "YES" {
            // getProfileInfoFromServer()
        }
    }
    
    // MARK: - Table view data source & delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath)
        // 31 32
        
        let imgMenu : UIImageView = cell .viewWithTag(31) as! UIImageView
        
        let lblNames : UILabel = cell.viewWithTag(32) as! UILabel
        lblNames.font.withSize(30)
        lblNames.text = arrNames[indexPath.row] as? String
        
        if indexPath.row == selectedIndex {
            //cell.contentView.backgroundColor = imgUser.backgroundColor
            imgMenu.image = UIImage(named: arrImgsSelected[indexPath.row] as! String)
        } else {
            cell.contentView.backgroundColor = UIColor.clear
            imgMenu.image = UIImage(named: arrImgs[indexPath.row] as! String)
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
        
    }
    
    
    
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        selectedIndex = indexPath.row
    //        tableView.reloadData()
    //        let _ : UITableViewCell = tableView.cellForRow(at: indexPath)!
    //        //cell.contentView.backgroundColor = imgUser.backgroundColor
    //        if indexPath.row == 0 {
    //            _=kConstantObj.SetMainViewController("HomeVC")
    //        }
    //        else if indexPath.row == 1 {
    //            _=kConstantObj.SetMainViewController("ProfileVC")
    //        }
    //        else if indexPath.row == 2 {
    //            _=kConstantObj.SetMainViewController("CalendarAndTaskVC")
    //        }
    //        else if indexPath.row == 3 {
    //            _=kConstantObj.SetMainViewController("ExpensesMainVC")
    //        }
    //        else if indexPath.row == 4 {
    //            if myTeamAvailable == "1"
    //            {
    //                _=kConstantObj.SetIntialMainViewController("MyTeamListing")
    //            }else{
    //                _=kConstantObj.SetIntialMainViewController("MyTeamVC")
    //            }
    //        }
    //        else if indexPath.row == 5 {
    //            _=kConstantObj.SetIntialMainViewController("LegalVC")
    //        }
    //        else if indexPath.row == 6 {
    //            _=kConstantObj.SetIntialMainViewController("HelpVC")
    //        }
    //        else if indexPath.row == 7 {
    //
    //            if let container = sideMenuVC.view{
    //                container.addSubview(viewBlurBlackView)
    //                viewBlurBlackView.frame = container.bounds
    //                container.bringSubview(toFront: viewBlurBlackView)
    //            }
    //        }
    //
    //    }
    
    //  "Home","Profile","My Orders","Calendar/Tasks","Expense Records","My Team","Favourites","Notification","Legal","Help","Refer a Vessel","Logout"
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
        let _ : UITableViewCell = tableView.cellForRow(at: indexPath)!
        // cell.contentView.backgroundColor = imgUser.backgroundColor
        if indexPath.row == 0 {
            _=kConstantObj.SetMainViewController("HomeVC")
        }
        else if indexPath.row == 1 {
            _=kConstantObj.SetMainViewController("ProfileVC")
        }
        else if indexPath.row == 2 {
            _=kConstantObj.SetMainViewController("MyOrderVC")
            //Show alert Message
            // let alertMessage = UIAlertController(title: "MYS", message:"Working on it.", preferredStyle: .alert)
            // let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
             // }
            // alertMessage.addAction(action)
            // self.present(alertMessage, animated: true, completion: nil)
        }
        else if indexPath.row == 3 {
            _=kConstantObj.SetMainViewController("CalendarAndTaskVC")
        }
        else if indexPath.row == 4 {
            _=kConstantObj.SetMainViewController("ExpensesMainVC")
        }
        else if indexPath.row == 5 {
            if myTeamAvailable == "1"
            {
                _=kConstantObj.SetIntialMainViewController("MyTeamListing")
            }else{
                _=kConstantObj.SetIntialMainViewController("MyTeamVC")
            }
        }
        else if indexPath.row == 6 {
            _=kConstantObj.SetIntialMainViewController("BestFavoritesVC")
        }
        else if indexPath.row == 7 {
            _=kConstantObj.SetIntialMainViewController("NotificationVC")
        }
        else if indexPath.row == 8 {
            _=kConstantObj.SetIntialMainViewController("LegalVC")
        }
        else if indexPath.row == 9 {
            _=kConstantObj.SetIntialMainViewController("HelpVC")
        }
        else if indexPath.row == 10 {
            _=kConstantObj.SetIntialMainViewController("ReferralViewVC")
        }
        else if indexPath.row == 11 {
            if let container = sideMenuVC.view{
                container.addSubview(viewBlurBlackView)
                viewBlurBlackView.frame = container.bounds
                container.bringSubview(toFront: viewBlurBlackView)
            }
        }
    }
    
    @IBAction func logoutYes(_ sender: UIButton) {
        viewBlurBlackView.removeFromSuperview()
        
        //Logout Webservice Call
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "device_type":"iOS",
                    "device_token":webService_obj.Retrive("Device_token") as! String]
        
        webService_obj.fetchDataFromServer(header:"logout", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                defaults.removeObject(forKey: kAPPLOGIN_IN)
                self.kConstantObj = kConstant()
                
                let loginManager = FBSDKLoginManager()
                loginManager.logOut()
                
                let mainVcIntial = self.kConstantObj.SetIntialMainViewController("IntroScreenVC")
                UIApplication.shared.delegate?.window??.rootViewController = mainVcIntial
            }
        }
    }
    
    @IBAction func logoutNo(_ sender: UIButton) {
        viewBlurBlackView.removeFromSuperview()
    }
    
    @IBAction func settingPanelOpen(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"SettingsVC")
        controller?.push()
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
        //        cell.contentView.backgroundColor = UIColor.clear
    }
    
    @IBAction func btnLogoutPressed(_ sender: Any) {
        
        //        let alertController = UIAlertController(title: kAPPName, message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.alert)
        //        let CancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) {
        //            (result : UIAlertAction) -> Void in
        //            print("Destructive")
        //        }
        //        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        //        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
        //            (result : UIAlertAction) -> Void in
        //            print("OK")
        //            var strisLogin = ""
        //            if (defaults.object(forKey: "ISLOGIN") != nil){
        //                strisLogin = defaults.object(forKey: "ISLOGIN") as! String
        //            }
        //            if strisLogin == "YES" {
        ////                let aNv: UINavigationController = (self.storyboard!.instantiateViewController(withIdentifier: "NavLogin") as? UINavigationController)!
        ////                self.appDelegate.window!.rootViewController! = aNv
        //            }else{
        //                // send to login
        //
        //            }
        //        }
        //        alertController.addAction(CancelAction)
        //        alertController.addAction(okAction)
        //        self.present(alertController, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
