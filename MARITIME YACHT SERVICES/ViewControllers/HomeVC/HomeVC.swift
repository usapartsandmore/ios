//
//  HomeVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 02/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import MessageUI

class PagerCell: UICollectionViewCell {
    
    @IBOutlet weak var img_Product: UIImageView!
}

class BannerCell: UITableViewCell {
    
    @IBOutlet weak var img_Banner: UIImageView!
}

class HomeVC: UIViewController,UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,MFMailComposeViewControllerDelegate {
    let kConstantObj = kConstant()
    
    @IBOutlet var newPageControl: CSPageControl!
    @IBOutlet var btnMenu: UIButton!
    @IBOutlet var btnSearch: UIButton!
    @IBOutlet var btnMessage: UIButton!
    @IBOutlet var btnNotification: UIButton!
    @IBOutlet var scrollview: UIScrollView!
    @IBOutlet var bottomImageView: UIImageView!
    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var chatView: UIView!
    
    @IBOutlet weak var tblViewChat: UITableView!
    @IBOutlet weak var tblViewBanner: UITableView!
    @IBOutlet weak var tblHeightConst: NSLayoutConstraint!
    
    @IBOutlet var btnContactUs: UIButton!
    var arrNames = NSArray()
    var arrImgs  = NSArray()
    
    var homeBannerData = [[String:String]] ()
    var imagesbanner = [String]()
    var bottomBannerData = [[String:String]] ()
    var timer_Ride: Timer?
    var ifUserHaveDecks = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrNames = ["Product","Services","Technical Support","App Feedback"]
        arrImgs = ["product","services","technicalsupport","message"]
        view.sendSubview(toBack: chatView)
        chatView.isHidden = true
      }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
         timer_Ride?.invalidate()
         timer_Ride = nil
    }

    
    @objc func scrollToNextCell(){
        self.myCollectionView.reloadData()
        
        var offset = myCollectionView.contentOffset
        offset.x += myCollectionView.bounds.width
        if offset.x >= myCollectionView.contentSize.width{
            offset.x = 0
        }
        
        if homeBannerData.count > 0 {
            let pageNumber = round(offset.x / myCollectionView.frame.size.width)
            let indexPath = IndexPath(item: Int(pageNumber), section: 0)
            self.myCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
             print(indexPath.row)
            newPageControl.currentPage = Int(pageNumber)
            newPageControl.updateCurrentPageDisplay()
        }
      }
    
    override func viewWillAppear(_ animated: Bool) { 
        
        //Remove all data of deck names
        //selectedDeckNames.removeAll()
        
        chatView.isHidden = true
        
        //Inventory Sharing
        isSharingUser = false
        sharedUser_id = ""
        
         self.fetchHomeBannerWebService ()
        
        newPageControl.center = self.newPageControl.center
        newPageControl.activeImage = #imageLiteral(resourceName: "pagecantrol")
        newPageControl.inactiveImage = #imageLiteral(resourceName: "pagecantrolunfill")
        newPageControl.dotSize = 9
        newPageControl.activeStyle = CSPageControlStyle.image
        newPageControl.inactiveStyle = CSPageControlStyle.image
        newPageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
        timer_Ride = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
      }
    
    // MARK: ScrollView Methods
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.myCollectionView {
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            newPageControl.currentPage = Int(pageNumber)
            newPageControl.updateCurrentPageDisplay()
        }
    }
    
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        var visibleRect = CGRect()
//
//        visibleRect.origin = collectionView.contentOffset
//        visibleRect.size = collectionView.bounds.size
//
//        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//
//        guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
//
//        print(indexPath)
//    }
//    .
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(newPageControl.currentPage) * myCollectionView.frame.size.width
        myCollectionView.setContentOffset(CGPoint(x:x, y:0), animated: true)
    }
    
    func configurePageControl() {
        newPageControl.numberOfPages = self.imagesbanner.count
        newPageControl.currentPage = 0
     }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func onShowChatViewClicked(){
        
        chatView.transform = CGAffineTransform(translationX: 0, y: chatView.bounds.height)
        chatView.isHidden = false
        view.bringSubview(toFront: chatView)
        UIView.animate(withDuration: 0.25, animations: {
            self.chatView.transform = CGAffineTransform(translationX: 0, y: 0)
        })
                //Show alert Message
//                let alertMessage = UIAlertController(title: "MYS", message:"This functionality is under development.", preferredStyle: .alert)
//                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//
//                }
//                alertMessage.addAction(action)
//                self.present(alertMessage, animated: true, completion: nil)
    }
    
    @IBAction func onCloseChatViewClicked(){
        
        UIView.animate(withDuration: 0.25, animations: {
            self.chatView.transform = CGAffineTransform(translationX: 0, y: self.chatView.bounds.height)
        }) { (finish) in
            self.chatView.isHidden = true
            self.chatView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.view.sendSubview(toBack: self.chatView)
        }
    }
    
    @IBAction func btnInventorySelected(_ sender: UIButton) {
        //Show alert Message
//         let alertMessage = UIAlertController(title: "MYS", message:"Working on it.", preferredStyle: .alert)
//         let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
//         }
//         alertMessage.addAction(action)
//         self.present(alertMessage, animated: true, completion: nil)
        
        if ifUserHaveDecks == "1"{
            //Already have decks, so now let the user manage the decks
            let st = UIStoryboard(name: "Inventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"DeckLevelList") as! DeckLevelList
            controller.isToManageOrFirstTime = true
            controller.push()
        }
        else {
            //Don't have decks yet, so let the user add decks
            let st = UIStoryboard(name: "Inventory", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"DeckTemplatesVC") as! DeckTemplatesVC
            controller.isforFirstTime = true
            controller.push()
        }
    }
    
    @IBAction func btnServicesSelected(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ServicesMainVC")
        controller?.push()
    }
    
    @IBAction func btnNotificationClick(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"NotificationVC")
        controller?.push()
    }
    
    @IBAction func btnproductsSelected(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ProductsVC")
        controller?.push()
    }
    
    // MARK: - Table view data source & delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tblViewChat{
            return arrNames.count
        }else{
            self.tblHeightConst.constant = tableView.rowHeight * CGFloat(self.bottomBannerData.count)
            return self.bottomBannerData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblViewChat{
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "HomeViewCustomeCell", for: indexPath)
            let imgMenu : UIImageView = cell.viewWithTag(1) as! UIImageView
            let lblNames : UILabel = cell.viewWithTag(2) as! UILabel
            lblNames.text = arrNames[indexPath.row] as? String
            imgMenu.image = UIImage(named: arrImgs[indexPath.row] as! String)
            cell.contentView.backgroundColor = UIColor.clear
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as! BannerCell
            
           let dataArr = self.bottomBannerData[indexPath.row]
            
                    if dataArr["image"] == ""
                     {
                          cell.img_Banner.image = UIImage(named:"noimage")
                    }else{
                        let str = dataArr["image"]!
                        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                        cell.img_Banner.af_setImage(withURL: url as URL)
                    }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tblViewBanner{
            //1=Link to Third Domain , 2 = Link to Product , 3 = Link to Service , 4 = Link to local offer
            let dataArr = self.bottomBannerData[indexPath.row]
            let adType = dataArr["link_type"]!
            
            switch adType {
            case "1" :
                let urlstr = dataArr["link"]!
                let url = NSURL(string: urlstr)
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url! as URL)
                }
            case "2" :
                 let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
                controller.product_ID = dataArr["product_id"]!
                controller.product_variant_ID = dataArr["product_variant_id"]!
                controller.push()
            case "3" :
                let vendorId = dataArr["link"]!
                let st = UIStoryboard.init(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
                controller.selectedVendorId = vendorId
                controller.push()
            case "4" :
                let offerId = dataArr["link"]!
                let main = UIStoryboard(name: "Main", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
                controller.localOffer_id = offerId
                controller.isfromMyOrders = false
                controller.push()
             default:
                print("F. You failed")//Any number less than 0 or greater than 99 
                
            }
              self.bannerCountWebService(banner_Id: dataArr["id"]!)
        }else
        {
            //"Product","Services","Technical Support","App Feedback"
            
            if indexPath.row == 0
            {
                let main = UIStoryboard(name: "Main", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"ProductChatSearchVC")as! ProductChatSearchVC
                controller.isforService = false
                 controller.push()
            }
            else if indexPath.row == 1
            {
                 let main = UIStoryboard(name: "Main", bundle: nil)
                let controller = main.instantiateViewController(withIdentifier:"ServicesCatListVC")as! ServicesCatListVC
                controller.push()
             }
            else if indexPath.row == 3
            {
                self.openMailForHelp(title: "MYS Customer Service", recipient: "info@maritimeyachtservices.com")
            }else
            {
                self.openMailForHelp(title: "MYS Technical Support", recipient: "support@maritimeyachtservices.com")
            }
         }
    }
    
    private func openMailForHelp(title: String,recipient:String) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([recipient])
            mail.title = title
            mail.setSubject(String(title))
            
            let mailMessageStr = String("Title: </br>" +
                "Description: </br>")
            
            mail.setMessageBody(mailMessageStr!, isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    
    //MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imagesbanner.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PagerCell", for: indexPath) as! PagerCell
        let str = self.imagesbanner[indexPath.row]
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
        cell.img_Product.af_setImage(withURL: url as URL)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
       // newPageControl.currentPage = indexPath.row
    }
    
    
    //MARK: UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let dataArr = homeBannerData[indexPath.row]
        
         self.bannerCountWebService(banner_Id: dataArr["id"]!)
         //1=Link to Third Domain , 2 = Link to Product , 3 = Link to Service , 4 = Link to local offer
       
        let adType = dataArr["link_type"]!
        
        switch adType {
        case "1" :
            let urlstr = dataArr["link"]!
            let url = NSURL(string: urlstr)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url! as URL)
            }
        case "2" :
            let controller = self.storyboard?.instantiateViewController(withIdentifier:"MProductDetailsVC") as! MProductDetailsVC
            controller.product_ID = dataArr["product_id"]!
            controller.product_variant_ID = dataArr["product_variant_id"]!
            controller.push()
        case "3" :
            let vendorId = dataArr["link"]!
            let st = UIStoryboard.init(name: "Services", bundle: nil)
            let controller = st.instantiateViewController(withIdentifier:"VendorsVC") as! VendorsVC
            controller.selectedVendorId = vendorId
            controller.push()
        case "4" :
             let offerId = dataArr["link"]!
            let main = UIStoryboard(name: "Main", bundle: nil)
            let controller = main.instantiateViewController(withIdentifier:"CleaningVC")as! CleaningVC
            controller.localOffer_id = offerId
             controller.isfromMyOrders = false
            controller.push()
        default:
            print("F. You failed")//Any number less than 0 or greater than 99
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // return CGSize(width: self.view.frame.size.width/2, height:185);
        return collectionView.bounds.size
    }
 
    func fetchHomeBannerWebService () {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(header:"homeBaner", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
                 let completeData = responce.value(forKey: "data") as! [String:Any]
                 self.homeBannerData = completeData["premium"] as! [[String:String]]
                 self.bottomBannerData = completeData["general"] as! [[String:String]]
                
                
               // self.homeBannerData = responce.value(forKey: "data") as! [[String:String]]
                
                self.imagesbanner = self.homeBannerData.map{ $0["image"]!}
                self.ifUserHaveDecks = responce.value(forKey: "inventory_available") as! String
                
                if self.ifUserHaveDecks == "1"
                {
                    isInventoryAvailable = true
                }else{
                    isInventoryAvailable = false
                }
                
                
                myTeamAvailable = responce.value(forKey: "team_available") as! String
                
                
                self.configurePageControl()
                self.myCollectionView .reloadData()
                self.tblViewBanner.reloadData()
              
            }
        }
    }
    
    func bannerCountWebService(banner_Id:String) {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "ad_id":banner_Id]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"banerClick", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
            }
        }
    }
    
    
    
}
