//
//  MapVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 16/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController, MKMapViewDelegate{
    
    @IBOutlet weak var mapView: MKMapView!
    
    var mapDataDict = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        var arrPointsOnMap = [MKAnnotation]()
        for maindata in self.mapDataDict {
            
            let latitude = Double(maindata["latitude"] as! String) ?? 0.0
            let longitude = Double(maindata["longitude"] as! String) ?? 0.0
            
            let cordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let annotation = MKPointAnnotation()
            annotation.coordinate = cordinate
            annotation.title = maindata["title"] ?? ""
            arrPointsOnMap.append(annotation)
            self.mapView.addAnnotation(annotation)
        }
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)

        
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    func zoomToRegion() {
//        let location = CLLocationCoordinate2D(latitude: 28.0484584, longitude: -81.9053783)
//        let region = MKCoordinateRegionMakeWithDistance(location, 1000.0, 1000.0)
//        mapView.setRegion(region, animated: true)
//    }

    
    // MARK:
    func mapView(_ map: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if (annotation is MKUserLocation) {
            //if annotation is not an MKPointAnnotation (eg. MKUserLocation),
            //return nil so map draws default view for it (eg. blue dot)...
            return nil
        }
        
        let reuseId = "AnnotationIdentifier"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.image = UIImage(named:"locationicon")
            anView?.canShowCallout = true
        }
        else {
            //we are re-using a view, update its annotation reference...
            anView?.annotation = annotation
        }
        return anView
        
    }

    @IBAction func BtnCrossClick(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
}
