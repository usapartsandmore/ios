//
//  NotificationVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 16/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblnotificationText: UILabel!
    @IBOutlet weak var lbltimeStamp: UILabel!
    @IBOutlet weak var lblBoatName: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

class NotificationVC: UIViewController, UITableViewDataSource,UITableViewDelegate  {
    
    @IBOutlet weak var tblview: UITableView!
    var notificationsArr: [[String:Any]]!
    var textFont: UIFont!
    
    @IBOutlet weak var lbl_noDataMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_noDataMessage.isHidden = true
        notificationsArr = []
        self.recivedNotificationWebService()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        
        let notificationdict = notificationsArr[indexPath.row]
        
        cell.lblnotificationText.text = notificationdict["message"] as! String?
        cell.lblnotificationText.font = textFont
        cell.lbltimeStamp.text = notificationdict["created"] as! String?
        return cell
    }
    
    // func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    // let notificationdict = notificationsArr[indexPath.row]
    
    // return getHeight(text: (notificationdict["message"] as! String?)!)
    // }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let notificationdict = notificationsArr[indexPath.row]
            self.deleteNotificationWebService(notification_id: notificationdict["id"] as! String)
        }
    }
    
    func getHeight(text: String) -> CGFloat {
        
        let constraints = CGSize(width: view.bounds.width-40, height: CGFloat.greatestFiniteMagnitude)
        let context = NSStringDrawingContext()
        let bound: CGRect = text.boundingRect(with: constraints, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName:textFont], context: context)
        
        return ceil(bound.height)+49;
    }
    
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        sideMenuVC.toggleMenu()
    }
    
    
    func recivedNotificationWebService()
    {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(alertMsg:false,header:"get_notification_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                let dataMsg = responce.value(forKey: "data") as! [[String:Any]]
                
                if dataMsg.count == 0
                {
                    //self.AlertMessage(responce.value(forKey: "msg") as! String)
                    self.lbl_noDataMessage.isHidden = false
                    
                }else{
                    self.lbl_noDataMessage.isHidden = true
                    self.notificationsArr = dataMsg
                    self.tblview.reloadData()
                }
            }else{
                self.lbl_noDataMessage.isHidden = false
            }
            
        }
    }
    
    func deleteNotificationWebService(notification_id:String) {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "notification_id":notification_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header: "delete_notification", withParameter: PostData as NSDictionary, inVC: self, showHud: false) { (response, message, status) in
            if status{
                self.recivedNotificationWebService()
            }
        }
    }
    
}
