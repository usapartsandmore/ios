//  ProductOptionVC.swift
//  MARITIME YACHT SERVICES
//  Created by Octal on 22/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.

import UIKit
class SizeCell: UICollectionViewCell {
    var selectedSize_variant_ID = ""
    @IBOutlet weak var lbl_size: UILabel!
    var selectBorder: UIColor?
    var deselectBorder: UIColor?
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            lbl_size.borderColor = newValue ? selectBorder : deselectBorder
            lbl_size.borderWidth = 1.5
        }
        get{
            return super.isSelected
        }
    }
}

class ColorCell: UICollectionViewCell {
    var colorName = ""
    var selectedColour_variant_ID = ""
    @IBOutlet weak var itemImage: UIImageView!
    var selectBorder: UIColor?
    var deselectBorder: UIColor?
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            self.contentView.borderColor = newValue ? selectBorder : deselectBorder
            self.contentView.borderWidth = 1.5
        }
        get{
            return super.isSelected
        }
    }
}

class ProductOptionVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    
    @IBOutlet var vsSizeToTotalview: NSLayoutConstraint!
    @IBOutlet var vsSizeToColour: NSLayoutConstraint!
    @IBOutlet var vsQtyToTotalview: NSLayoutConstraint!
    @IBOutlet var vsQtyToSize: NSLayoutConstraint!
    @IBOutlet var vsQtyToColour: NSLayoutConstraint!
    @IBOutlet var lblQty: UILabel!
    @IBOutlet var lblColour: UILabel!
    @IBOutlet var lblSelectSize: UILabel!
    @IBOutlet var img_productImg: UIImageView!
    @IBOutlet var lbl_selectedSize: UILabel!
    @IBOutlet var lbl_selectedColour: UILabel!
    @IBOutlet var lbl_totalPrice: UILabel!
    @IBOutlet var rating_product: FloatRatingView!
    @IBOutlet var lbl_productPrice: UILabel!
    @IBOutlet var lbl_vendorName: UILabel!
    @IBOutlet var lbl_productname: UILabel!
    @IBOutlet var lbl_quantity: UILabel!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    @IBOutlet weak var sizeCollectionView: UICollectionView!
    
    var isFromCart = ""
    var product_ID = ""
    var product_variant_ID = ""
    var productOptionData = [String : Any]()
    var defaultData = [String : Any]()
    var sizeData = [[String:String]]()
    var colorData = [[String:String]]()
    var selectedDictblock: (([String : String])->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isFromCart = "false"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.fetchProductOptionWebServiceCall ()
        
        if isFromCart == "true"
        {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers
            for aViewController in viewControllers {
                if aViewController is ProductsVC {
                    self.navigationController!.popToViewController(aViewController, animated: true)
                }
            }
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnPlusClick(_ sender: UIButton) {
        print(self.defaultData["qty"]!)
        let  stockQty = self.defaultData["qty"] as! Int
        var temp = Int(lbl_quantity.text!) ?? 1
        if temp >= stockQty {
            return
        }
        else {
            temp += 1
        }
        lbl_quantity.text = String(temp)
        let basicAmt = (self.defaultData["price"] as? NSString)?.floatValue
        let qty = (self.lbl_quantity.text! as NSString).floatValue
        let Calulation = basicAmt! * qty
        self.lbl_totalPrice.text = "Total: $\(Calulation)"
    }
    
    @IBAction func btnMinusClick(_ sender: UIButton) {
        var temp = Int(lbl_quantity.text!) ?? 1
        if temp <= 1 {
            return
        }
        else {
            temp -= 1
        }
        lbl_quantity.text = String(temp)
        let basicAmt = (self.defaultData["price"] as? NSString)?.floatValue
        let qty = (self.lbl_quantity.text! as NSString).floatValue
        let Calulation = basicAmt! * qty
        self.lbl_totalPrice.text = "Total: $\(Calulation)"
        
        
    }
    @IBAction func btnBackPressed(_ sender: UIButton) {
        
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    
    @IBAction func crossBtnClick(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func continueBtnCLick(_ sender: UIButton) {
        
        if self.defaultData["is_variant_size"] as! String == "1" {
            if lbl_selectedSize.text! .isEmpty
            {
                self.AlertMessage("Please select size.")
            }
            else {
                let newString = self.lbl_totalPrice.text?.replacingOccurrences(of: "Total: $", with: "", options: .literal, range: nil)
                
                let dict = ["FinalPrice":newString,
                            "qty":self.lbl_quantity.text,
                            "selected_size":lbl_selectedSize.text,
                            "selected_color":lbl_selectedColour.text,
                            "product_variant_id":product_variant_ID ]
                
                selectedDictblock?(dict as! [String : String])
                _ = self.navigationController?.popViewController(animated:true)
                
                
            }
            
        }
        else {
            let newString = self.lbl_totalPrice.text?.replacingOccurrences(of: "Total: $", with: "", options: .literal, range: nil)
            
            let dict = ["FinalPrice":newString,
                        "qty":self.lbl_quantity.text,
                        "selected_size":lbl_selectedSize.text,
                        "selected_color":lbl_selectedColour.text,
                        "product_variant_id":product_variant_ID ]
            
            selectedDictblock?(dict as! [String : String])
            _ = self.navigationController?.popViewController(animated:true)
            
            
        }
        
        
    }
    //MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView==colorCollectionView {
            return self.colorData.count
        }
        else{
            return self.sizeData.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView==colorCollectionView {
            
            let cellColor = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as! ColorCell
            cellColor.selectBorder = UIColor(red: 53/255, green: 186/255, blue: 224/255, alpha: 1.0)
            cellColor.deselectBorder = UIColor(red: 220/255, green: 219/255, blue: 220/255, alpha: 1.0)
            
            let sData = self.colorData[indexPath.row]
            if sData["image"] == ""
            {
                cellColor.itemImage.image = UIImage(named:"noimage")
            }else{
                let str = sData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellColor.itemImage.af_setImage(withURL: url as URL)
                cellColor.selectedColour_variant_ID = sData["product_variant_id"]!
            }
            let defaultColor = self.defaultData["variant_name_color"] as! String
            if defaultColor == self.colorData[indexPath.row]["variant_name_color"] {
                cellColor.isSelected = true
            }
            else{
                cellColor.isSelected = false
            }
            return cellColor
        }
        else
        {
            let cellSize = collectionView.dequeueReusableCell(withReuseIdentifier: "SizeCell", for: indexPath) as! SizeCell
            cellSize.selectBorder = UIColor(red: 53/255, green: 186/255, blue: 224/255, alpha: 1.0)
            cellSize.deselectBorder = UIColor(red: 220/255, green: 219/255, blue: 220/255, alpha: 1.0)
            let sData = self.sizeData[indexPath.row]
            cellSize.lbl_size.text = sData["size"] ?? "0"
            cellSize.selectedSize_variant_ID = sData["product_variant_id"]!
            let defaultIds = self.defaultData["product_variant_id"] as! String
            if defaultIds == self.sizeData[indexPath.row]["product_variant_id"]{
                cellSize.isSelected = true
            } else {
                cellSize.isSelected = false
            }
            return cellSize
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView==colorCollectionView {
            let width = (self.view.frame.size.width - 70) / 3
            let height = CGFloat(91)
            return CGSize(width: width, height: height)
        }else{
            let width = (self.view.frame.size.width - 60) / 4
            let height = width * 0.5
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    
    //MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        if collectionView == colorCollectionView {
            let colorCell = collectionView.cellForItem(at: indexPath) as! ColorCell
            lbl_selectedColour.text = colorCell.colorName
            product_variant_ID = colorCell.selectedColour_variant_ID
            self.fetchProductOptionWebServiceCall()
            
        }
        else {
            let cellSize = collectionView.cellForItem(at: indexPath) as! SizeCell
            lbl_selectedSize.text = cellSize.lbl_size.text
            product_variant_ID = cellSize.selectedSize_variant_ID
            self.fetchProductOptionWebServiceCall()
            
        }
        
    }
    
    //MARK: WEB SERVICE METHOD
    func fetchProductOptionWebServiceCall() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "product_id":product_ID,
                    "product_variant_id":product_variant_ID]
        
        webService_obj.fetchDataFromServer(alertMsg: false,header:"productOption", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                self.productOptionData = responce.value(forKey: "data") as! [String : Any]
                self.defaultData = self.productOptionData["default"]  as! [String:Any]
                let imgData = self.defaultData["image"]  as? [String:String] ?? [:]
                
                if imgData.count > 0{
                let strImg = imgData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: strImg))")!
                self.img_productImg.af_setImage(withURL: url as URL)
                }
                
                self.lbl_productname.text = self.defaultData["name"] as? String ?? ""
                self.rating_product.rating = (self.defaultData["rating"] as! NSString).floatValue
                self.lbl_productPrice.text = "$\(self.defaultData["price"] as? String ?? "")"
                self.lbl_vendorName.text = self.defaultData["company_name"] as? String ?? ""
                let qty = (self.lbl_quantity.text! as NSString).doubleValue
                let calculatePrice = (self.defaultData["price"] as! NSString).doubleValue * qty
                self.lbl_totalPrice.text = "Total: $\(calculatePrice)"
                self.lbl_selectedColour.text = self.defaultData["variant_name_color"] as? String
                self.lbl_selectedSize.text = self.defaultData["variant_name_size"] as? String
                if self.defaultData["is_variant_color"] as! String == "1" {
                    if self.defaultData["is_variant_size"] as! String == "1" {
                        self.vsSizeToTotalview.constant = 148
                        self.vsQtyToTotalview.constant = 240
                    }
                    else {
                        self.vsQtyToColour.constant = 10
                        self.lblSelectSize.isHidden = true
                        self.sizeCollectionView.isHidden = true
                    }
                }
                else {
                    self.lblColour.isHidden = true
                    self.colorCollectionView.isHidden = true
                    if self.defaultData["is_variant_size"] as! String == "1" {
                        self.lblColour.isHidden = true
                        self.colorCollectionView.isHidden = true
                        self.vsSizeToColour.isActive = false
                        self.vsSizeToTotalview.constant = 10
                        self.vsQtyToSize.constant = 10
                        self.vsQtyToColour.isActive = false
                        self.vsQtyToTotalview.isActive = false
                    }
                    else {
                        self.lblQty.isHidden = false
                        self.lblSelectSize.isHidden = true
                        self.vsQtyToTotalview.constant = 10
                        self.vsQtyToSize.isActive = false
                        self.vsQtyToColour.isActive = false
                    }
                }
                
                self.sizeData = self.defaultData["size"] as? [[String:String]] ?? []
                self.colorData =  self.productOptionData["image"]  as? [[String:String]] ?? []
                self.colorCollectionView.reloadData()
                self.sizeCollectionView.reloadData()
                
            }
        }
    }
    
}
