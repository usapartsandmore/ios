//
//  WishListVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 05/02/18.
//  Copyright © 2018 OctalSoftware. All rights reserved.
//

import UIKit

class WishListCell : UITableViewCell {
    
    @IBOutlet var img_product: UIImageView!
    @IBOutlet var lbl_quantity: UILabel!
    @IBOutlet var btn_AddToCart: UIButton!
    @IBOutlet var lbl_deliveryDate: UILabel!
    @IBOutlet var lbl_startingPrice: UILabel!
    @IBOutlet var lbl_sellerName: UILabel!
    @IBOutlet var lbl_productName: UILabel!
    @IBOutlet var lbl_ShippingPrice: UILabel!
    
}

class WishListVC: UIViewController , UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var view_NoData: UIView!
    var completeShoppingData = [[String:Any]]()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.view_NoData.isHidden = true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchMyWishList ()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completeShoppingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.completeShoppingData[indexPath.row]
        
        if data["type"] as! String == "1"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WishListCell", for: indexPath) as! WishListCell
            cell.selectionStyle = .none
            
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_product.af_setImage(withURL: url as URL)
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if data["estimated_date"] as? String != "-"{
                let myDate = dateFormatter.date(from: data["estimated_date"] as? String ?? "")
                dateFormatter.dateFormat = "MMM dd, yyyy"
                cell.lbl_deliveryDate.text =  dateFormatter.string(from: myDate!)
            }else{
                cell.lbl_deliveryDate.text = "N/A"
            }
            
            cell.lbl_sellerName.text = data["vendor_name"] as? String ?? ""
            cell.lbl_productName.text = data["name"] as? String ?? ""
            cell.lbl_startingPrice.text = "$\(data["base_amount"] ?? "")"
            cell.lbl_ShippingPrice.text = "$\(data["extra_charges"] ?? "")"  
            cell.lbl_quantity.text = data["quantity"] as? String
            
            cell.btn_AddToCart.tag = indexPath.row
            cell.btn_AddToCart.addTarget(self, action: #selector(addToCartClicked(_:)), for: .touchUpInside)
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceWishListCell", for: indexPath) as! WishListCell
            cell.selectionStyle = .none

            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_product.af_setImage(withURL: url as URL)
            
            cell.lbl_sellerName.text = data["vendor_name"] as? String ?? ""
            cell.lbl_productName.text = data["name"] as? String ?? ""
            cell.lbl_startingPrice.text = "$\(data["base_amount"] ?? "")"
            
            cell.btn_AddToCart.tag = indexPath.row
            cell.btn_AddToCart.addTarget(self, action: #selector(addToCartClicked(_:)), for: .touchUpInside)

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
   
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let data = self.completeShoppingData[indexPath.row]
        
        if data["type"] as! String == "1"
        {
            return 165
        }else{
            return 130
        }
    }

    
    @IBAction func addToCartClicked(_ sender: UIButton)
    {
        let data = self.completeShoppingData[sender.tag]
        
        let id = data["product_cart_id"]
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "later_id":id ?? ""]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"move_to_cart", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus
            {
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    _=self.navigationController?.popViewController(animated: false)
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    func fetchMyWishList()
    {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"save_for_later_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            self.completeShoppingData.removeAll()
            if staus
            {
                self.completeShoppingData = responce.value(forKey: "data") as! [[String:Any]]
               
                self.tableView.reloadData()
                if self.completeShoppingData.count == 0
                {
                    self.view_NoData.isHidden = false
                }
            }
            
            if self.completeShoppingData.count == 0
            {
                self.tableView.reloadData()
                self.view_NoData.isHidden = false
            }
        }
    }
    
    
    
}
