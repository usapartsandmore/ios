//
//  ServicesShoppingListVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 16/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit

class ServiceShoppinglistCustomeCell : UITableViewCell {
    
    @IBOutlet var btn_cellSelection: UIButton!
    @IBOutlet var img_product: UIImageView!
    @IBOutlet var lbl_quantity: UILabel!
    @IBOutlet var btn_plus: UIButton!
    @IBOutlet var btn_minus: UIButton!
    @IBOutlet var btn_saveForLater: UIButton!
    @IBOutlet var lbl_deliveryDate: UILabel!
    @IBOutlet var lbl_startingPrice: UILabel!
    @IBOutlet var lbl_ShippingPrice: UILabel!
    @IBOutlet var lbl_sellerName: UILabel!
    @IBOutlet var lbl_productName: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if isSelected {
            self.btn_cellSelection.setImage(UIImage(named: "shoppingcartcheck"), for: UIControlState.normal)
        }
        else {
            self.btn_cellSelection.setImage(UIImage(named: "shoppingcartuncheck"), for: UIControlState.normal)
        }
    }
}

class ServicesShoppingListVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btn_check_uncheckAll: UIButton!
    @IBOutlet var lbl_totalAmount: UILabel!
    @IBOutlet var view_NoData: UIView!
    @IBOutlet var btn_Delete: UIButton!
    @IBOutlet var btn_Sort: UIButton!
    
    var completeShoppingData = [[String:Any]]()
    var selected_cartId = [String]()
    var sort_by = ""
    var user_ID = ""
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.sort_by = "0"
        
        self.btn_check_uncheckAll.addTarget(self, action: #selector(btnCheckUncheckAll(_:)), for: UIControlEvents.touchUpInside)
        
        self.view_NoData.isHidden = true
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchMyShoppingList ()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnSortClick(_ sender: UIButton) {
        
        if self.sort_by == "1"
        {
            self.sort_by = "0"
        }else{
            self.sort_by = "1"
        }
        self.fetchMyShoppingList()
    }
    
    @IBAction func btnCheckUncheckAll(_ sender: UIButton)
    {
        let totalRows = completeShoppingData.count
        if  sender.tag ==  0{
            for row in 0..<totalRows {
                tableView.selectRow(at: NSIndexPath(row: row, section: 0) as IndexPath, animated: false, scrollPosition: .none)
                self.btn_check_uncheckAll.setImage(UIImage(named: "shoppingcartcheck"), for: UIControlState.normal)
            }
            
            self.btn_check_uncheckAll.tag = 1
            
           // let finalAmt = self.getTotalPrice()
           // self.lbl_totalAmount.text =  String(format: "USD $%.2f",finalAmt)
        }
        else{
            for row in 0..<totalRows {
                tableView.deselectRow(at: NSIndexPath(row: row, section: 0) as IndexPath, animated: false)
                self.btn_check_uncheckAll.setImage(UIImage(named: "shoppingcartuncheck"), for: UIControlState.normal)
            }
            self.btn_check_uncheckAll.tag = 0
           // self.lbl_totalAmount.text =  "USD $0.00"
         }
    }
    

    
    @IBAction func btnMinusClicked(_ sender: UIButton)
    {
        guard let cell = tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? ServiceShoppinglistCustomeCell else {
            return
        }
        let data = self.completeShoppingData[sender.tag]
        
        var temp = Int(cell.lbl_quantity.text!) ?? 0
        if temp <= 1 {
            return
        }
        else {
            temp -= 1
        }
        
      //  if cell.isSelected
      //  {
            cell.lbl_quantity.text = String(temp)
            
            self.checkShippingOptionWebServiceCall(quantity:cell.lbl_quantity.text!, isfrom: "minus", cartData: data, cartIndex:sender.tag)
      //  }
      //  else{
       // }
    }
    
    @IBAction func btnPlusClicked(_ sender: UIButton)
    {
        guard let cell = tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? ServiceShoppinglistCustomeCell else {
            return
        }
        let data = self.completeShoppingData[sender.tag]
        var temp = Int(cell.lbl_quantity.text!) ?? 1
        
        if temp < data["stock_available"] as! Int
        {
            temp += 1
        }else
        {
            return
        }
        
       // if cell.isSelected
       // {
            cell.lbl_quantity.text = String(temp)
            
            self.checkShippingOptionWebServiceCall(quantity:cell.lbl_quantity.text!, isfrom: "pluse", cartData: data, cartIndex:sender.tag)
//        }
//        else{
//        }
    }

    
    @IBAction func BuyButtonClicked(_ sender: UIButton) {
        // let rows = tableView.indexPathsForSelectedRows
        //   if ((rows?.count) != nil) {
        self.productCheckBeforeBuy()
        //    }else{
        //Show alert Message
        //         let alertMessage = UIAlertController(title: "MYS", message:"Please select product to buy.", preferredStyle: .alert)
        //         let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        
        //        }
        //      alertMessage.addAction(action)
        //       self.present(alertMessage, animated: true, completion: nil)
        //    }
    }
    
    
    @IBAction func btnWishListClicked(_ sender: UIButton)
    {
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"WishListVC") as! WishListVC
        controller.push()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completeShoppingData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.completeShoppingData[indexPath.row]
        
        if data["type"] as! String == "1"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceShoppinglistCustomeCell", for: indexPath) as! ServiceShoppinglistCustomeCell
            cell.selectionStyle = .none
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: data["estimated_date"] as? String ?? "")
            dateFormatter.dateFormat = "MMM dd, yyyy"
            if((myDate) != nil){
                cell.lbl_deliveryDate.text =  dateFormatter.string(from: myDate!)
            }else{
                cell.lbl_deliveryDate.text = "N/A"
            }
            
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_product.af_setImage(withURL: url as URL)
            
            cell.lbl_sellerName.text = data["vendor_name"] as? String ?? ""
            cell.lbl_productName.text = data["name"] as? String ?? ""
            cell.lbl_ShippingPrice.text = "$\(data["extra_charges"] ?? "")"
            cell.lbl_startingPrice.text = "$\(data["base_amount"] ?? "")"
 
            cell.lbl_quantity.text = data["quantity"] as? String
            cell.btn_minus.tag = indexPath.row
            cell.btn_plus.tag = indexPath.row
            cell.btn_saveForLater.tag = indexPath.row
            cell.btn_plus.addTarget(self, action: #selector(btnPlusClicked(_:)), for: .touchUpInside)
            cell.btn_minus.addTarget(self, action: #selector(btnMinusClicked(_:)), for: .touchUpInside)
            cell.btn_saveForLater.addTarget(self, action: #selector(saveforLaterClicked(_:)), for: .touchUpInside)
            
            if cell.isSelected {
                cell.btn_cellSelection.setImage(UIImage(named: "shoppingcartcheck"), for: UIControlState.normal)
            }
            else {
                cell.btn_cellSelection.setImage(UIImage(named: "shoppingcartuncheck"), for: UIControlState.normal)
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCartListCell", for: indexPath) as! ServiceShoppinglistCustomeCell
            cell.selectionStyle = .none
            
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_product.af_setImage(withURL: url as URL)
            
            cell.lbl_sellerName.text = data["vendor_name"] as? String ?? ""
            cell.lbl_productName.text = data["name"] as? String ?? ""
            cell.lbl_startingPrice.text = "$\(data["base_amount"] ?? "")"
            cell.btn_saveForLater.tag = indexPath.row
            cell.btn_saveForLater.addTarget(self, action: #selector(saveforLaterClicked(_:)), for: .touchUpInside)
            
            if cell.isSelected {
                cell.btn_cellSelection.setImage(UIImage(named: "shoppingcartcheck"), for: UIControlState.normal)
            }
            else {
                cell.btn_cellSelection.setImage(UIImage(named: "shoppingcartuncheck"), for: UIControlState.normal)
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let finalAmt = self.getTotalPrice()
       // self.lbl_totalAmount.text =  String(format: "USD $%.2f",finalAmt)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
      //  let finalAmt = self.getTotalPrice()
     //   self.lbl_totalAmount.text =  String(format: "USD $%.2f",finalAmt)
              self.btn_check_uncheckAll.setImage(UIImage(named: "shoppingcartuncheck"), for: UIControlState.normal)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let data = self.completeShoppingData[indexPath.row]
        
        if data["type"] as! String == "1"
        {
            return 175
        }else{
            return 130
        }
    }
    
    
    @IBAction func saveforLaterClicked(_ sender: UIButton)
    {
        let data = self.completeShoppingData[sender.tag]
        let id = data["product_cart_id"]
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "cart_id":id ?? ""]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"save_product_for_later", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus
            {
                //Show alert Message
                let alertMessage = UIAlertController(title: "MYS", message:responce.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                    self.fetchMyShoppingList()
                }
                alertMessage.addAction(action)
                self.present(alertMessage, animated: true, completion: nil)
            }
        }
    }
    
    func getTotalPrice() -> Double {
        
        var totalAmt : Double = 0.0
        for (_, dict) in self.completeShoppingData.enumerated(){
            
            // tableView.indexPathsForSelectedRows?.forEach({ (indexPath) in
             totalAmt +=  Double(dict["amount_with_extra_charge_and_quantity"] as! String)!
            //  })
            
        }
        return totalAmt
    }
    
    
    @IBAction func btnDeleteClick(_ sender: UIButton) {
        
        if let rows = tableView.indexPathsForSelectedRows{
            var arr = [String]()
            
            for indexPath in rows
            {
                let dict = self.completeShoppingData[indexPath.row]
                
                arr.append(dict["product_cart_id"]! as! String)
            }
            self.selected_cartId = arr
        }
        
        if self.selected_cartId.count == 0
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Please select product to delete.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Do you want to delete selected products from cart?", preferredStyle: .alert)
            let actionYes = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction) in
                PostData = ["product_cart_ids":self.selected_cartId]
                
                webService_obj.fetchDataFromServer(header:"product_cart_delete", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                    if staus
                    {
                        self.selected_cartId.removeAll()
                        self.fetchMyShoppingList()
                    }
                }
            }
            let actionNo = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(actionYes)
            alertMessage.addAction(actionNo)
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
    func checkShippingOptionWebServiceCall(quantity: String, isfrom: String, cartData:[String:Any], cartIndex:Int)
    {
        
        let dict = ["product_id":cartData["product_id"] as! String,
                    "product_variant_id":cartData["product_variant_id"] as! String,
                    "cart_id":cartData["product_cart_id"] as! String,
                    "quantity":quantity,
                    "shipping_id":cartData["shipping_id"] as! String] as [String : Any]
        
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"qty_change_in_cart_list", withParameter: dict as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus
            {
                let arr = responce.value(forKey: "data") as! [String:Any]
                var dict = self.completeShoppingData[cartIndex]
                
                dict["amount_with_extra_charge_and_quantity"] = arr["amount_with_extra_charge_and_quantity"] as! String
                dict["quantity"] = quantity
                 dict["amount"] = arr["amount"] as! String
                dict["extra_charges"] = arr["extra_charges"] as! String
                self.completeShoppingData[cartIndex] = dict
                
                let finalAmt = self.getTotalPrice()
                self.lbl_totalAmount.text =  String(format: "USD $%.2f",finalAmt)
                
                self.fetchMyShoppingList()
            }
        }
    }
    
    
    func fetchMyShoppingList()
    {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "sort_by":self.sort_by]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"product_cart_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            self.completeShoppingData.removeAll()
            if staus
            {
                self.completeShoppingData = responce.value(forKey: "data") as! [[String:Any]]
                self.user_ID = responce.value(forKey: "user_id") as! String
                
                let finalAmt = self.getTotalPrice()
                self.lbl_totalAmount.text =  String(format: "USD $%.2f",finalAmt)
                
                self.tableView.reloadData()
                if self.completeShoppingData.count == 0
                {
                    self.view_NoData.isHidden = false
                }else{
                    self.btn_check_uncheckAll.tag = 1
                   self.btnCheckUncheckAll(self.btn_check_uncheckAll)
                    self.view_NoData.isHidden = true
                }
            }
            
            if self.completeShoppingData.count == 0
            {
                self.tableView.reloadData()
                self.view_NoData.isHidden = false
                self.btn_Delete.isHidden = true
                self.btn_Sort.isHidden = true
            }else
            {
                self.view_NoData.isHidden = true
                self.btn_Delete.isHidden = false
                self.btn_Sort.isHidden = false
            }
        }
    }
    
    func productCheckBeforeBuy()
    {
        var selectedCart = [[String:Any]]()
        var confirmCartData = [[String:Any]]()
        
       // tableView.indexPathsForSelectedRows?.forEach({ (indexPath) in
        for (_, data) in self.completeShoppingData.enumerated(){
           // let data = self.completeShoppingData[indexPath.row]
            //  let cell = tableView.cellForRow(at:IndexPath(row:indexPath.row, section:0)) as! ServiceShoppinglistCustomeCell
            
            let dict = ["product_id":data["product_id"] as! String,
                        "product_variant_id":data["product_variant_id"] as! String,
                        "product_name":data["name"] as! String,
                        "product_quantity":data["quantity"] as? String ?? "",
                        "product_amount":data["base_amount"] as! String ,
                        "amount":data["amount"] as! String ,
                        "product_shipping_charges":data["extra_charges"] as! String,
                        "shipping_id":data["shipping_id"] as! String ,
                        "user_id":data["vendor_id"] as! String,
                        "type":data["type"] as! String,
                        "user_services_id":data["user_services_id"] as! String] as [String : Any]
            
            selectedCart.append(dict)
             confirmCartData.append(data)
         }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "cart_list_to_order":selectedCart]
        
        webService_obj.fetchDataFromServer(alertMsg: true, header:"product_check_before_buy", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus
            {
                let arr = responce.value(forKey: "data") as! [String:String]
                
                let controller = self.storyboard?.instantiateViewController(withIdentifier:"OrderConfirmationVC") as! OrderConfirmationVC
                controller.completeCartData = confirmCartData
                controller.address = arr["customer_address"]!
                controller.c_Name = arr["customer_name"]!
                controller.total_CartPrice = self.lbl_totalAmount.text ?? ""
                //controller.qtyCartData = selectedCart
                controller.user_ID = self.user_ID
                controller.push()
                
            }else{
                self.fetchMyShoppingList()
            }
            //self.tableView.reloadData()
        }
    }
    
    
}
