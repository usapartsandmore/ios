 //
//  OrderConfirmationVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 17/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
class ProductCustomeCell : UITableViewCell {
    @IBOutlet var img_product: UIImageView!
    @IBOutlet var lbl_quantity: UILabel!
    @IBOutlet var btn_plus: UIButton!
    @IBOutlet var btn_minus: UIButton!
    // @IBOutlet var btn_edit: UIButton!
    @IBOutlet var lbl_ShippingPrice: UILabel!
    @IBOutlet var lbl_deliveryDate: UILabel!
    @IBOutlet var lbl_startingPrice: UILabel!
    @IBOutlet var lbl_sellerName: UILabel!
    @IBOutlet var lbl_productName: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if isSelected {
            //self.btn_cellSelection.setImage(UIImage(named: "shoppingcartcheck"), for: UIControlState.normal)
        }
        else {
            // self.btn_cellSelection.setImage(UIImage(named: "shoppingcartuncheck"), for: UIControlState.normal)
        }
    }
    
}

class OrderConfirmationVC: UIViewController, UITableViewDataSource, UITableViewDelegate, PayPalPaymentDelegate  {
    
    @IBOutlet var lbl_customerName: UILabel!
    @IBOutlet var lbl_address: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lbl_Total_Price: UILabel!
    @IBOutlet var txt_ReferralCode: UITextField!
    @IBOutlet var viewHideOrderInfo: UIView!
 
    @IBOutlet weak var BtnPaypal: UIButton!
    var strTransectionId: String  = ""
    var resultText: String = ""
    var strTotalAmount: String  = ""
    
    @IBOutlet weak var lbl_referralCode: UILabel!
    @IBOutlet weak var lbl_ReferralAmount: UILabel!
    @IBOutlet weak var lbl_ShippingCharges: UILabel!
    @IBOutlet weak var lbl_BoxTotalAmount: UILabel!
    
     
    var completeCartData = [[String:Any]]()
    var address = ""
    var c_Name = ""
    var total_CartPrice = ""
    var user_ID = ""
 
    var payPalConfig = PayPalConfiguration() // default
    
     var promo_Amount : Double = 0
    var promo_code = ""
    var promo_code_type = ""
    
    
    // You need to change environment as per your requirement. For e.g. If you have sandbox user registered on PayPal then you need to change like this
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    // if you try with any other user rather than registered sandbox user it will give you error. This will work for only sandbox user.
    // var environment:String = PayPalEnvironmentSandbox
    // {
    // willSet(newEnvironment) {
    // if (newEnvironment != environment)
    // {
    // PayPalMobile.preconnectWithEnvironment(newEnvironment)
    // }
    // }
    // }
    
    // There is one more environment called Production which is for live purpose that you need to ask credentials from your client.
    // PayPalEnvironmentProduction
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewHideOrderInfo.isHidden = true
        self.lbl_Total_Price.text = total_CartPrice
        
        let stringArray = self.completeCartData.map{ ($0["extra_charges"] as! String)}
        let doubleArray = stringArray.flatMap{ Double($0) }
        let sum = doubleArray.reduce(0, +)
 
        self.lbl_referralCode.text = "N/A"
        self.lbl_ReferralAmount.text = "N/A"
        self.lbl_ShippingCharges.text = "$\(String(describing: sum))"
        self.lbl_BoxTotalAmount.text = total_CartPrice
        
        self.lbl_customerName.text = c_Name
        self.lbl_address.text = address
        
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "MYS"
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
 
        payPalConfig.payPalShippingAddressOption = .payPal;
        self.environment = PayPalEnvironmentSandbox
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    @IBAction func btnOrderInformationClicked(_ sender: UIButton) {
        self.viewHideOrderInfo.isHidden = false
    }
    
    @IBAction func btnHideOrderInformationClicked(_ sender: UIButton) {
        self.viewHideOrderInfo.isHidden = true
    }
    
    @IBAction func btnApplyReferralCodeClicked(_ sender: UIButton) {
         if self.txt_ReferralCode.text == ""
        {
            AlertMessage("Please enter the referral code.")
        }else{
            
           // AlertMessage("Working on it.")
            
            let cart_data = self.completeCartData.flatMap{ ($0["product_cart_id"] as? String) ?? "0" }
            
           // {"promocode":"A9G3J0I441d","customer_id":"18","cart_id":"256,257,258"}
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "promocode":self.txt_ReferralCode.text ?? "",
                        "cart_id":cart_data]
            
            webService_obj.fetchDataFromServer(alertMsg: false, header: "apply_promocode", withParameter: PostData as NSDictionary, inVC: self, showHud: true) { (response, message, status) in
                if status{
                    
                    let data = response.value(forKey: "data") as! [String:String]
                    self.promo_Amount = Double(data["amount"]!)!
                    self.promo_code = self.txt_ReferralCode.text!
                    self.promo_code_type = data["promocode_type"]!
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:response.value(forKey: "success_message") as! String?, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        self.txt_ReferralCode.text = ""
 
                        let finalAmt = self.getOerderTotalPrice()
                       // finalAmt = finalAmt - Double(self.promo_Amount)
                        
                        self.lbl_referralCode.text = self.promo_code
                        self.lbl_ReferralAmount.text = "Up to $\(self.promo_Amount)"
                        self.lbl_BoxTotalAmount.text = String(format: "USD $%.2f",finalAmt)
                        self.lbl_Total_Price.text =  String(format: "USD $%.2f",finalAmt)
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)

                }else{
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:message as String, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completeCartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let data = self.completeCartData[indexPath.row]
 
        if data["type"] as! String == "1"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCustomeCell", for: indexPath) as! ProductCustomeCell
            
            cell.selectionStyle = .none
            
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: data["estimated_date"] as? String ?? "")
            dateFormatter.dateFormat = "MMM dd, yyyy"
            
            if((myDate) != nil){
                cell.lbl_deliveryDate.text =  dateFormatter.string(from: myDate!)
            }else{
                cell.lbl_deliveryDate.text = "N/A"
            }
            
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_product.af_setImage(withURL: url as URL)
            
            cell.lbl_sellerName.text = data["vendor_name"] as? String ?? ""
            cell.lbl_productName.text = data["name"] as? String ?? ""
            cell.lbl_ShippingPrice.text = "$\(data["extra_charges"] ?? "")"  
            cell.lbl_startingPrice.text = "$\(data["base_amount"] ?? "")"
            cell.lbl_quantity.text = data["quantity"] as? String ?? ""
            
            
            cell.btn_minus.tag = indexPath.row
            cell.btn_plus.tag = indexPath.row
            cell.btn_plus.addTarget(self, action: #selector(btnPlusClicked(_:)), for: .touchUpInside)
            cell.btn_minus.addTarget(self, action: #selector(btnMinusClicked(_:)), for: .touchUpInside)
            
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceOrderListCell", for: indexPath) as! ProductCustomeCell
            cell.selectionStyle = .none
            
            let str = data["image"]!
            let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
            cell.img_product.af_setImage(withURL: url as URL)
            
            cell.lbl_sellerName.text = data["vendor_name"] as? String ?? ""
            cell.lbl_productName.text = data["name"] as? String ?? ""
            cell.lbl_startingPrice.text = "$\(data["base_amount"] ?? "")"
   
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let data = self.completeCartData[indexPath.row]
        
        if data["type"] as! String == "1"
        {
            return 165
        }else{
            return 130
        }
    }
    
    @IBAction func btnMinusClicked(_ sender: UIButton)
    {
        guard let cell = tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? ProductCustomeCell else {
            return
        }
        let data = self.completeCartData[sender.tag]
        
        var temp = Int(cell.lbl_quantity.text!) ?? 0
        if temp <= 1 {
            return
        }
        else {
            temp -= 1
        }
        
        cell.lbl_quantity.text = String(temp)
        
        self.checkShippingOptionWebServiceCall(quantity:cell.lbl_quantity.text!, isfrom: "minus", cartData: data, cartIndex:sender.tag)
    }
    
    @IBAction func btnPlusClicked(_ sender: UIButton)
    {
        guard let cell = tableView.cellForRow(at:IndexPath(row:sender.tag, section:0)) as? ProductCustomeCell else {
            return
        }
        
        let data = self.completeCartData[sender.tag]
        
        var temp = Int(cell.lbl_quantity.text!) ?? 1
        
        if temp < data["stock_available"] as! Int
        {
            temp += 1
        }else
        {
            return
        }
        
        cell.lbl_quantity.text = String(temp)
        
        self.checkShippingOptionWebServiceCall(quantity:cell.lbl_quantity.text!, isfrom: "pluse", cartData: data, cartIndex:sender.tag)
        
    }
    
    
    @IBAction func placeOrderConfirmationClicked(_ sender: UIButton) {
        self.CallPaypalPayment()
    
 //        resultText = ""
//        
//        //       both payment details (subtotal, shipping, tax) and multiple items.
//        //       You would only specify these if appropriate to your situation.
//        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
//        //       and simply set payment.amount to your total charge.
//        
//        // Optional: include multiple items
//        let currencyFormatter = NumberFormatter()
//        currencyFormatter.formatterBehavior = .behavior10_4
//        currencyFormatter.generatesDecimalNumbers = true
//        currencyFormatter.numberStyle = .decimal
//
//        
//        var itemsData = [PayPalItem]()
//        for (_,dict) in self.completeCartData.enumerated() {
//            
//            let number = currencyFormatter.number(from: dict["amount_with_extra_charge_and_quantity"] as! String)
//            let dNumber = NSDecimalNumber(decimal: (number?.decimalValue)!)
//            
//            let itemdata = PayPalItem(name: dict["name"] as! String, withQuantity: UInt(dict["quantity"] as! String)!, withPrice: dNumber, withCurrency: "USD", withSku: "MYS")
//            
//            itemsData.append(itemdata)
//        }
//        
//        
//        // let item1 = PayPalItem(name: "MYS", withQuantity: 1, withPrice: decimalNumber, withCurrency: "USD", withSku: "MYS")
//        
//        let items = itemsData
//        
//        let subtotal = PayPalItem.totalPrice(forItems: items)
//        // Optional: include payment details
//        let shipping = NSDecimalNumber(string: "0.00")
//        let tax = NSDecimalNumber(string: "0.00")
//        
//        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
//        
//         let total = subtotal.adding(shipping).adding(tax)
//        
//        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "MYS", intent: .sale)
//        
//        payment.items = items
//        payment.paymentDetails = paymentDetails
//        
//        if (payment.processable)
//        {
//            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
//            present(paymentViewController!, animated: true, completion: nil)
//        }
    }
    
    
    func checkShippingOptionWebServiceCall(quantity: String, isfrom: String, cartData:[String:Any], cartIndex:Int)
    {
        
        let dict = ["product_id":cartData["product_id"] as! String,
                    "product_variant_id":cartData["product_variant_id"] as! String,
                    "cart_id":cartData["product_cart_id"] as! String,
                    "quantity":quantity,
                    "shipping_id":cartData["shipping_id"] as! String] as [String : Any]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"qty_change_in_cart_list", withParameter: dict as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus
            {
                let arr = responce.value(forKey: "data") as! [String:Any]
                var dict = self.completeCartData[cartIndex]
                
                dict["amount_with_extra_charge_and_quantity"] = arr["amount_with_extra_charge_and_quantity"] as! String
                dict["quantity"] = quantity
                dict["amount"] = arr["amount"] as! String
                dict["extra_charges"] = arr["extra_charges"] as! String
                self.completeCartData[cartIndex] = dict
                
                
                let finalAmt = self.getOerderTotalPrice()
                self.lbl_Total_Price.text =  String(format: "USD $%.2f",finalAmt)
                
                let stringArray = self.completeCartData.map{ ($0["extra_charges"] as! String)}
                let doubleArray = stringArray.flatMap{ Double($0) }
                let sum = doubleArray.reduce(0, +)
                
                self.lbl_referralCode.text = "N/A"
                self.lbl_ReferralAmount.text = "N/A"
                self.lbl_ShippingCharges.text = "$\(String(describing: sum))"
                self.lbl_BoxTotalAmount.text = String(format: "USD $%.2f",finalAmt)
                
                self.tableView.reloadData()
            }
        }
    }
    
    func getOerderTotalPrice() -> Double {
        var totalAmt : Double = 0.0
        let totalRows = completeCartData.count
        
        for row in 0..<totalRows {
            let data = self.completeCartData[row]
            totalAmt +=  Double(data["amount_with_extra_charge_and_quantity"] as! String)!
        }
        return totalAmt
    }
    
    
    @IBAction func btnEditClicked(_ sender: UIButton){
        let st = UIStoryboard.init(name: "Profile", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ProfileAddressVC") as! ProfileAddressVC
        controller.block = { dict in
            print(dict)
            if dict != "" {
                self.lbl_address.text = dict
                self.address = dict
            }else{
               
            }
        }
        controller.isforCart = true
        controller.push()
    }
    
    
    
    //Paypal
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        //  successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("\(String(describing: completedPayment.softDescriptor))")
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            let dict_details  =  completedPayment.confirmation as NSDictionary
            
            self.strTransectionId = (dict_details.value(forKey: "response") as AnyObject).value(forKey: "id") as! String
            
            self.resultText = completedPayment.description
            // self.showSuccess()
            self.CallPaypalPayment()
        })
    }
    
    func CallPaypalPayment()
    {
        // --------------------------------------------------
        //For Calculation
        var calculationDict = [[String:Any]]()
        var calculation_collection = [String:[[String:Any]]]()
        // -------------------------------------------------
        
        var vendor = [[String:Any]]()
        let totalRows = tableView.numberOfRows(inSection: 0)
        var collection = [String:[[String:Any]]]()
        
        for row in 0..<totalRows {
            
            //Total vendor they have shipping option
            //referal amount divided for each present vendor
            //minus the divided referal amount from the perticular shipping amount of that vendor
            
            let data = self.completeCartData[row]
            let id = data["vendor_id"] as! String
            var __arr = collection[id] ?? []
            
            let dict = ["product_id":data["product_id"] as! String,
                        "product_variant_id":data["product_variant_id"] as! String,
                        "product_name":data["name"] as! String,
                        "product_quantity":data["quantity"] as? String ?? "",
                        "product_amount":data["base_amount"] as! String,
                        "amount":data["amount"] as! String,
                        "product_shipping_charges":data["extra_charges"] as! String,
                        "shipping_id":data["shipping_id"] as! String,
                        "cart_id":data["product_cart_id"] as! String,
                        "user_id":data["vendor_id"] as! String,
                        "type":data["type"] as! String,
                        "user_services_id":data["user_services_id"] as! String] as [String : Any]
            
            __arr.append(dict)
            collection[id] = __arr
            
            // --------------------------------------------------
            //For Calculation
            let name = data["vendor_email"] as! String
            var __arr1 = calculation_collection[name] ?? []
            
            let dict1 = ["product_id":data["product_id"] as! String,
                         "product_variant_id":data["product_variant_id"] as! String,
                         "product_name":data["name"] as! String,
                         "product_quantity":data["quantity"] as? String ?? "",
                         "product_amount":data["base_amount"] as! String,
                         "amount":data["amount"] as! String,
                         "product_shipping_charges":data["extra_charges"] as! String,
                         "shipping_id":data["shipping_id"] as! String,
                         "cart_id":data["product_cart_id"] as! String,
                         "user_id":data["vendor_id"] as! String,
                         "vendor_email":data["vendor_email"] as! String] as [String : Any]
            
            __arr1.append(dict1)
            calculation_collection [name] = __arr1
            // --------------------------------------------------
        }
        
        for (id, list) in collection{
            
            let arrayShipCharg: [Double] = list.flatMap{ Double(($0["product_shipping_charges"] as? String) ?? "0") }
            let totalCharge = arrayShipCharg.reduce(0, +)
           
            let arrayTotalAmt: [Double] = list.flatMap{ Double(($0["amount"] as? String) ?? "0") }
            let totalAmt = arrayTotalAmt.reduce(0, +)
            
            vendor.append(
                ["user_id": id,
                 "totalShippingCharge":totalCharge,
                 "total_amount":totalAmt,
                 "products_list": list]
            )
        }
        
        // --------------------------------------------------
        //For Calculation
        for (name, list) in calculation_collection{
            
            let arrayShipCharg: [Double] = list.flatMap{ Double(($0["product_shipping_charges"] as? String) ?? "0") }
            let totalCharge = arrayShipCharg.reduce(0, +)
            
//            if totalCharge > 0{
//                if self.promo_Amount > 0{
//                    totalCharge = totalCharge - self.promo_Amount
//                }
//            }
            
            let arrayTotalAmt: [Double] = list.flatMap{ Double(($0["amount"] as? String) ?? "0") }
            let totalAmt = arrayTotalAmt.reduce(0, +)
            
            
            //For Calculation
            calculationDict.append(
                ["vendor_email": name,
                 "totalShippingCharge":totalCharge,
                 "total_amount":totalAmt,
                 "products_list": list]
            )
        }
        // --------------------------------------------------
        
        let finalResult = ["vendor": vendor]
        
        print("Final Result-->\(finalResult.toJson())")
        
        //let strprice = self.lbl_Total_Price.text?.replacingOccurrences(of: "USD $", with: "", options: .literal, range: nil)
        
        let finalAmt = self.getOerderTotalPrice()
        let strprice =  String(format: "%.2f",finalAmt)
        
        let array: [Double] = self.completeCartData.flatMap{ Double(($0["extra_charges"] as? String) ?? "0") }
        let value = array.reduce(0, +)
        
        //Cart_ids
        
        let cart_data = self.completeCartData.flatMap{ ($0["product_cart_id"] as? String) ?? "0" }

        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "address":address,
                    "total_amount":strprice,
                    "totalShippingCharge":value,
                    "vendor":vendor,
                    "promocode":self.promo_code,
                    "promocode_type":self.promo_code_type,
                    "promocode_amount":self.promo_Amount]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"order_payment_success", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus
            {
                let orderId = responce.value(forKey: "customer_order_id") as! String
                let pay_id = responce.value(forKey: "admin_payple_id") as! String
                let commission = responce.value(forKey: "admin_commission") as! String
 
                // _ = kConstantObj.SetIntialMainViewController("ProductsVC")
                let st = UIStoryboard(name: "Main", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"LegalVC") as! LegalVC
                controller.isforPayment = true
                controller.cartData = calculationDict
                controller.admin_commission = commission
                controller.admin_Paypal_id = pay_id
                controller.order_id = orderId
                controller.cart_ids = cart_data
                controller.discountAmount = self.promo_Amount
                 controller.push()
                
            }
        }
    }
    
    
}
