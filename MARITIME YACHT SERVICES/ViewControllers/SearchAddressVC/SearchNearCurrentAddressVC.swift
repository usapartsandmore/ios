//
//  SearchNearCurrentAddressVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by mac112 on 16/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import CoreLocation

class SearchNearTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imgDefault: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}

class SearchbyZipCell: UITableViewCell {
    
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeTitleLabel: UILabel!
    @IBOutlet weak var placeSubtitleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    private static let backColor = UIColor(white: 241.0/255, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 2.5
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.5)
        containerView.layer.shadowRadius = 1.0
        containerView.layer.shadowOpacity = 0.75
        containerView.backgroundColor = UIColor.white
        contentView.backgroundColor = SearchbyZipCell.backColor
        //selectedBackgroundView = UIView()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

class SearchNearCurrentAddressVC: UIViewController, UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate{
    
    var isFromOffers = ""
    var locationManger: CLLocationManager?
    var coreLocationController:LocationGet?
    
    var currentlat = Double ()
    var currentlong = Double ()
    var currentAddress = ""
    
    var addresslat = Double ()
    var addresslong = Double ()
    var myTblAddress = ""
    var deleted_id = ""
    var default_Id = ""
    
    var lastselected = IndexPath()
    
    var addressCompleteData = [[String:String]]()
    var geocoder = CLGeocoder()
    
    @IBOutlet var btnCurrentLocation: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var txt_Search: UITextField!
    @IBOutlet weak var tblView_Search: UITableView!
    
    var search_PostData = [[String: String]]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView_Search.isHidden = true
        self.tableView.tableFooterView = UIView()
        self.tblView_Search.tableFooterView = UIView()
        self.coreLocationController = LocationGet()
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse: break
        default:
            locationManger = CLLocationManager()
            locationManger?.delegate = self
            locationManger?.startUpdatingLocation()
            locationManger?.requestWhenInUseAuthorization()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(SearchNearCurrentAddressVC.locationAvailable(_:)), name: NSNotification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil)
        self.fetchshippingList()
    }
    
    // MARK: BUTTON CLICK METHOD
    
    // MARK: back Button Click
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: User Current Location Button Click
    @IBAction func currentLocationBtnClick(_ sender: UIButton) {
        if currentAddress == ""
        {
            //Show alert Message
            let alertMessage = UIAlertController(title: "Alert", message:"Location access is required to get current location. Please navigate to Settings and select While Using The App to allow location access.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                self.txt_Search.text = ""
                self.tblView_Search.isHidden = true
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
            
        }else{
            if self.isFromOffers == "Offers"
            {
                let st = UIStoryboard(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"SearchVendorVC") as! SearchVendorVC
                controller.lat = self.currentlat
                controller.long = self.currentlong
                controller.address = self.currentAddress
                controller.isFromOffers = "Offers"
                controller.push()
            }else{
                let st = UIStoryboard(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"SearchVendorVC") as! SearchVendorVC
                controller.lat = self.currentlat
                controller.long = self.currentlong
                controller.address = self.currentAddress
                controller.isFromOffers = "List"
                controller.push()
            }
        }
        
    }
    // MARK: Cross Button Click
    @IBAction func btnCrossClick(_ sender: UIButton) {
        txt_Search.text = ""
        self.tblView_Search.isHidden = true
    }
    
    // MARK: Add New Address button click
    @IBAction func btnAddNewAddressClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Profile", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ShippingAddressVC") as! ShippingAddressVC
        controller.isfromServices = "true"
        controller.push()
    }
    
    // MARK: Map button click
    @IBAction func btnMapViewClick(_ sender: UIButton) {
        
        var mapData = [[String:String]]()
        for maindata in self.addressCompleteData {
            
            let dict = ["latitude":maindata["lat"]! ,
                        "longitude":maindata["lng"]!,
                        "title":"\(maindata["address1"] ?? "") \(maindata["address2"] ?? "")"]
            mapData.append(dict)
        }
        //let st = UIStoryboard.init(name: "Services", bundle: nil)
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"MapVC") as! MapVC
        controller.mapDataDict = mapData
        controller.push()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if tableView == self.tblView_Search
        {
            return search_PostData.count
        }else{
            return addressCompleteData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblView_Search
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchbyZipCell", for: indexPath) as! SearchbyZipCell
            
            let data = self.search_PostData[indexPath.row]
            cell.placeTitleLabel?.text = data["place_name"]
            cell.placeSubtitleLabel?.text = data["address"]
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchNearTableViewCell", for: indexPath) as! SearchNearTableViewCell
            
            let maindata = self.addressCompleteData[indexPath.row]
            
            let add1 = maindata["address1"]!
            let add2 = maindata["address2"]!
            let cityname = maindata["city_name"]!
            let statename = maindata["state_name"]!
            let countryname = maindata["country_name"]!
            let zipcode = maindata["zipcode"]!
            let mainaddress = "\(add1) \(add2) \(",") \(cityname) \(",") \(statename) \("-") \(zipcode) \(countryname)"
            let isdefault = maindata["is_default"]
            
            cell.lblAddress.text = mainaddress
            
            
            if isdefault == "1"
            {
                cell.imgDefault.image = UIImage(named:"radiobtnselect")
                
            }else{
                cell.imgDefault.image = UIImage(named:"radiobtnunselect")
            }
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // search table data
        if tableView == self.tblView_Search
        {
            self.txt_Search.text = ""
            self.tblView_Search.isHidden = true
            
            tableView.deselectRow(at: indexPath as IndexPath, animated: true)
            let data = self.search_PostData[indexPath.row]
            
            if self.isFromOffers == "Offers"
            {
                let st = UIStoryboard(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"SearchVendorVC") as! SearchVendorVC
                controller.lat = (data["lat"]! as NSString).doubleValue
                controller.long = (data["long"]! as NSString).doubleValue
                controller.address = data["address"]!
                controller.isFromOffers = "Offers"
                controller.push()
            }else{
                let st = UIStoryboard(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"SearchVendorVC") as! SearchVendorVC
                controller.lat = (data["lat"]! as NSString).doubleValue
                controller.long = (data["long"]! as NSString).doubleValue
                controller.address = data["address"]!
                controller.isFromOffers = "List"
                controller.push()
            }
        }
        else{
            
            // Select a address from address table
            let maindata = self.addressCompleteData[indexPath.row]
            self.default_Id = maindata["id"]!
            self.setDefaultAddressWebServiceCall()
            let add1 = maindata["address1"]!
            let add2 = maindata["address2"]!
            let cityname = maindata["city_name"]!
            let statename = maindata["state_name"]!
            let countryname = maindata["country_name"]!
            let zipcode = maindata["zipcode"]!
            self.myTblAddress = "\(add1) \(add2) \(",") \(cityname) \(",") \(statename) \("-") \(zipcode) \(countryname)"
            print("default Selected ID-->\(self.default_Id)")
            self.addresslat =  Double(maindata["lat"]!) ?? 0.0
            self.addresslong = Double(maindata["lng"]!) ?? 0.0
            
            if self.isFromOffers == "Offers"
            {
                let st = UIStoryboard(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"SearchVendorVC") as! SearchVendorVC
                controller.lat = self.addresslat
                controller.long = self.addresslong
                controller.address = self.myTblAddress
                controller.isFromOffers = "Offers"
                controller.push()
            }else{
                let st = UIStoryboard(name: "Services", bundle: nil)
                let controller = st.instantiateViewController(withIdentifier:"SearchVendorVC") as! SearchVendorVC
                controller.lat = self.addresslat
                controller.long = self.addresslong
                controller.address = self.myTblAddress
                controller.isFromOffers = "List"
                controller.push()
            }
        }
    }
    
    // MARK: Fetch Address List WebService
    func fetchshippingList() {
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String]
        webService_obj.fetchDataFromServer(header:"customer_shipping_list", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.addressCompleteData = responce.value(forKey: "data") as! [[String:String]]
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: Set Default Address WebService
    func setDefaultAddressWebServiceCall() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "shipping_id":self.default_Id]
        
        webService_obj.fetchDataFromServer(header:"default_shipping", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.fetchshippingList()
            }
        }
    }
    
    // MARK: Location Available Method
    func locationAvailable(_ notification:Notification) -> Void
    {
        let userInfo = notification.userInfo as! Dictionary<String,String>
        load.hide(delegate: self)
        let  Currentlats = userInfo["latitude"]!
        let  Currentlots = userInfo["longitude"]!
        
        currentlat =    Double(Currentlats)!
        currentlong =    Double(Currentlots)!
        _ = NSMutableString(string: userInfo["postalCode"]!)
        //str_ZipCode = postalCode as String
        let str = "\(userInfo["name"]!),\(userInfo["Area"]!), \(userInfo["city"]!), \(userInfo["postalCode"]!), \(userInfo["state"]!)"
        
        currentAddress = "\(userInfo["Area"]!), \(userInfo["city"]!), \(userInfo["postalCode"]!), \(userInfo["state"]!)"
        //currentCity = userInfo["city"]!
        
        NSLog("Location Data -->\(str) Lat--> \(currentlat)  Long--> \(currentlong)")
        
        // NSNotification.removeObserver("")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil);
    }
    
    // MARK: UITextFieldDelegate Method
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField.text != ""
        {
            self.tblView_Search.isHidden = false
            geocoder.geocodeAddressString(txt_Search.text!) { (placemarks, error) in
                if (error != nil){
                    
                    //Show alert Message
                    let alertMessage = UIAlertController(title: "MYS", message:"Location not available for this zip code.", preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        self.txt_Search.text = ""
                        self.tblView_Search.isHidden = true
                    }
                    alertMessage.addAction(action)
                    self.present(alertMessage, animated: true, completion: nil)
                    
                }else{
                    
                    let placemark = placemarks?[0] ?? CLPlacemark()
                    let location: CLLocation? = placemark.location
                    let coordinate: CLLocationCoordinate2D? = location?.coordinate
                    print("Latitude \(String(describing: coordinate?.latitude))")
                    print("Longitude \(String(describing: coordinate?.longitude))")
                    
                    
                    let arraddress = placemark.addressDictionary?["FormattedAddressLines"] as! [String]
                    let str = arraddress.flatMap({$0}).joined(separator: ",")
                    
                    
                    var newPostData = [[String: String]]()
                    let dict = ["place_name":placemark.locality ?? "" ,
                                "address":str,
                                "lat": "\(placemark.location?.coordinate.latitude ?? 0)",
                        "long": "\(placemark.location?.coordinate.longitude ?? 0)"]
                    
                    newPostData.append(dict)
                    self.search_PostData = newPostData
                    
                    self.tblView_Search.reloadData()
                }
            }
        }
        else{
            self.txt_Search.text = ""
            self.tblView_Search.isHidden = true
        }
        return true
    }
}


