//
//  MProductDetailsVC.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 11/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

// MARK: Product Image Slider CollectionViewCell
class productSliderCell: UICollectionViewCell {
    @IBOutlet weak var img_Product: UIImageView!
}

// MARK: Product Color Slider CollectionViewCell
class productColorsCell: UICollectionViewCell {
    @IBOutlet weak var img_Product: UIImageView!
    var selectBorder: UIColor?
    var deselectBorder: UIColor?
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            self.contentView.borderColor = newValue ? selectBorder : deselectBorder
            self.contentView.borderWidth = 2
        }
        
        get{
            return super.isSelected
        }
    }
}

// MARK: Similar Product CollectionViewCell
class similarProductCell: UICollectionViewCell {
    
    @IBOutlet var rating_product: FloatRatingView!
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet weak var img_logo: UIImageView!
    @IBOutlet var lbl_product_price: UILabel!
    @IBOutlet var lbl_product_title: UILabel!
}

// MARK: Shipping Options CollectionViewCell
class shippingOptionsCell: UICollectionViewCell {
    @IBOutlet weak var img_Product: UIImageView!
    @IBOutlet var lbl_transit_time: UILabel!
    
    var selectBorder: UIColor?
    var deselectBorder: UIColor?
    override var isSelected: Bool{
        set{
            super.isSelected = newValue
            self.img_Product.borderColor = newValue ? selectBorder : deselectBorder
            self.img_Product.borderWidth = 2
        }
         get{
            return super.isSelected
        }
    }
}

class MProductDetailsVC: UIViewController, UIScrollViewDelegate,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionViewPColor: UICollectionView!
    @IBOutlet var collectionViewSOption: UICollectionView!
    @IBOutlet var collectionViewSProduct: UICollectionView!
    @IBOutlet var collectionViewProductImg: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var mailScrollView: UIScrollView!
    @IBOutlet var lbl_ProductName: UILabel!
    @IBOutlet var lblProductId: UILabel!
    @IBOutlet var viewRating: FloatRatingView!
    @IBOutlet var lblStock: UILabel!
    @IBOutlet var lblMimimumQty: UILabel!
    @IBOutlet var lblMaximumQty: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblSelectQty: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblSpecifications: UILabel!
    @IBOutlet var lbl_colorsName: UILabel!
    @IBOutlet weak var btn_FlashDeal: UIButton!
    @IBOutlet weak var btn_Fav: UIButton!
    @IBOutlet weak var viewBlur: UIView!
    @IBOutlet weak var view_PopUp: UIView!
    @IBOutlet var lblcart_Count: UILabel!
    @IBOutlet var lblColor: UILabel!
    @IBOutlet var btnSize: UIButton!
    @IBOutlet weak var lbl_ReturnPolicy: UILabel!
    @IBOutlet var lbl_NoSimilarProduct: UILabel!
    @IBOutlet weak var constantViewStock: NSLayoutConstraint!
    @IBOutlet var colorViewHeight: NSLayoutConstraint!
    @IBOutlet var sizeHeight: NSLayoutConstraint!
    @IBOutlet var similarViewHeight: NSLayoutConstraint!
    
    var img_OptionsNonSelected:[UIImage] = [#imageLiteral(resourceName: "twoweekgrey"), #imageLiteral(resourceName: "productgrey"), #imageLiteral(resourceName: "homegrey")]
    var img_OptionsSelected:[UIImage] = [#imageLiteral(resourceName: "twoweek"), #imageLiteral(resourceName: "product-1"), #imageLiteral(resourceName: "home-1")]
    var isFromProductOption = ""
    var shippingID = ""
    var product_ID = ""
    var product_variant_ID = ""
    var originalPrice = ""
    var productCompleteData = [String:Any]()
    var colourImgData = [[String:String]]()
    var similarProductData = [[String:String]]()
    var defaultImgData = [[String:String]]()
    var shippingOptionsData = [[String:Any]]()
    var isFavSelected = Bool()
    var productOptionData = [String:String]()
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isFromProductOption = "false"
        lbl_NoSimilarProduct.isHidden = true
        self.constantViewStock.constant = 0.0
        self.viewBlur.isHidden = true
        self.view_PopUp.isHidden = true
        
        // Fetch Complete Product Data Web-Service Call
        self.fetchFeaturedRecentWebServiceCall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
    }
    
    // MARK: Button Click Methods
    @IBAction func btnSearchClick(_ sender: UIButton) {
    }
    
    @IBAction func btnFlashDealClick(_ sender: UIButton) {
    }
    
    @IBAction func btnSelectQtyClick(_ sender: UIButton) {
        self.collectionViewSOption.reloadData()
        let defaultData = self.productCompleteData["default"]  as! [String:Any]
        let max = (defaultData["qty"] as? String)?.integerValue() ?? 1
        
        if max != 0{
            let arr = 1...max
            let qtyArray = arr.map{String($0)}
            
            ActionSheetStringPicker.show(withTitle: "Select Quantity", rows:qtyArray, initialSelection: 0, doneBlock: {
                picker,index,value in
                
                self.lblSelectQty.text = "QTY \(String(describing: value as! String))"
               // let calculatePrice = (self.originalPrice as NSString).doubleValue * (value as! NSString).doubleValue
               // self.lblPrice.text = "$\(calculatePrice)"
               
                return
            }, cancel: {
                
                ActionStringCancelBlock in return
                
            }, origin: sender)
        }
    }
    
    @IBAction func btnFavouritiesClick(_ sender: UIButton) {
        if self.isFavSelected == true
        {
            self.isFavSelected = false
            self.btn_Fav.setImage(UIImage(named: "favirot"), for: .normal)
        }
        else{
            self.isFavSelected = true
            self.btn_Fav.setImage(UIImage(named: "favorites"), for: .normal)
        }
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "product_id":product_ID,
                    "product_variant_id":product_variant_ID,
                    "is_favorite":self.isFavSelected ? 1 : 0]
        
        webService_obj.fetchDataFromServer(header:"favoritesProduct", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
            }
        }
    }
    
    @IBAction func BuyNowClick(_ sender: UIButton) {
        if shippingID == "" {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Please select shipping option", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
        }
        else {
            
            let defaultData = self.productCompleteData["default"]  as! [String:Any]
            
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "product_id":product_ID,
                        "product_variant_id":product_variant_ID,
                        "quantity":self.lblSelectQty.text?.replacingOccurrences(of: "QTY", with: "", options: .literal, range: nil) ?? 1 ,
                        "amount":defaultData["price"] as! String,
                        "shipping_id":shippingID,
                        "user_id":defaultData["user_id"] as! String]
            
            webService_obj.fetchDataFromServer(header:"product_add_to_cart", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    let st = UIStoryboard.init(name: "Cart", bundle: nil)
                    let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
                    controller.push()
                }
            }
        }
    }
    
    @IBAction func addCartBtnClick(_ sender: UIButton) {
        if shippingID == "" {
            //Show alert Message
            let alertMessage = UIAlertController(title: "MYS", message:"Please select shipping option", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                
            }
            alertMessage.addAction(action)
            self.present(alertMessage, animated: true, completion: nil)
        }
        else {
            
            let defaultData = self.productCompleteData["default"]  as! [String:Any]
            
            
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "product_id":product_ID,
                        "product_variant_id":product_variant_ID,
                        "quantity":self.lblSelectQty.text?.replacingOccurrences(of: "QTY", with: "", options: .literal, range: nil) ?? 1 ,
                        "amount":defaultData["price"] as! String,
                        "shipping_id":shippingID,
                        "user_id":defaultData["user_id"] as! String]
            
            webService_obj.fetchDataFromServer(header:"product_add_to_cart", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
                if staus{
                    self.viewBlur.isHidden = false
                    self.view_PopUp.isHidden = false
                    
                    self.lblcart_Count.text = "You have \(responce["cart_data_count"] as! String) item in your cart."
                    
                }
            }
        }
    }
    
    @IBAction func btnContinueShoppingClick(_ sender: Any) {
        self.viewBlur.isHidden = true
        self.view_PopUp.isHidden = true
        
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func btnGoToCartClick(_ sender: Any) {
        self.viewBlur.isHidden = true
        self.view_PopUp.isHidden = true
        
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func topCartBtnClick(_ sender: UIButton) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    @IBAction func sizeBtnClick(_ sender: UIButton) {
        let defaultData = self.productCompleteData["default"]  as! [String:Any]
        let controller = self.storyboard?.instantiateViewController(withIdentifier:"ProductOptionVC") as! ProductOptionVC
        controller.product_ID = self.product_ID
        controller.product_variant_ID = defaultData["product_variant_id"] as? String ?? ""
        controller.selectedDictblock = { dict in
            NSLog("Selected Dict-->\(dict)")
            self.productOptionData = dict
            self.isFromProductOption = "true"
            self.product_variant_ID = self.productOptionData["product_variant_id"] ?? ""
            self.fetchFeaturedRecentWebServiceCall()
        }
        controller.push()
    }
    
    // MARK: ScrollView Methods
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    //MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionViewProductImg
        {
            return self.defaultImgData.count
        }
        else if collectionView == self.collectionViewPColor
        {
            return colourImgData.count
        }
        else if collectionView == self.collectionViewSOption
        {
            return self.shippingOptionsData.count
        }
        else
        {
            return similarProductData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionViewProductImg
        {
            let cellD = collectionView.dequeueReusableCell(withReuseIdentifier: "productSliderCell", for: indexPath) as! productSliderCell
            
            let iData = self.defaultImgData[indexPath.row]
            
            if iData["image"] == ""
            {
                cellD.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = iData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellD.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellD
        }
        else if collectionView == self.collectionViewPColor
        {
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "productColorsCell", for: indexPath) as! productColorsCell
            
            cellA.selectBorder = UIColor(red: 53/255, green: 186/255, blue: 224/255, alpha: 1.0)
            cellA.deselectBorder = UIColor(red: 220/255, green: 219/255, blue: 220/255, alpha: 1.0)
            let defaultData = self.productCompleteData["default"]  as! [String:Any]
            let color = defaultData["variant_name_color"] as? String ?? ""
            let iData = self.colourImgData[indexPath.row]
            
            if iData["image"] == ""
            {
                cellA.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = iData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellA.img_Product.af_setImage(withURL: url as URL)
                if iData["variant_name_color"] == color {
                    cellA.isSelected = true
                }
            }
            return cellA
        }
        else if collectionView == self.collectionViewSOption
        {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "shippingOptionsCell", for: indexPath) as! shippingOptionsCell
            
            let shippingData = self.shippingOptionsData[indexPath.row]
            
            let extraChargeData = shippingData["extry_charges"] as? [[String:String]]
            
            if shippingData["image"] as? String == ""
            {
                cellB.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = shippingData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellB.img_Product.af_setImage(withURL: url as URL)
            }
            
            cellB.lbl_transit_time.text = shippingData["transit_time"] as? String
            cellB.selectBorder = UIColor(red: 53/255, green: 186/255, blue: 224/255, alpha: 1.0)
            cellB.deselectBorder = UIColor(red: 220/255, green: 219/255, blue: 220/255, alpha: 1.0)
            
            return cellB
        }
        else
        {
            let cellC = collectionView.dequeueReusableCell(withReuseIdentifier: "similarProductCell", for: indexPath) as! similarProductCell
            
            let sData = self.similarProductData[indexPath.row]
            cellC.lbl_product_title.text = sData["name"] ?? ""
            cellC.lbl_product_price.text = "$\(sData["price"] ?? "")"
            cellC.rating_product.rating = (sData["rating"]! as NSString).floatValue
            
            let strLogo1 = sData["logo"]!
            let urlLogo1:NSURL = NSURL(string :"\(imageURL)\(String(describing: strLogo1))")!
            cellC.img_logo.af_setImage(withURL: urlLogo1 as URL)
            
            if sData["image"] == ""
            {
                cellC.img_Product.image = UIImage(named:"noimage")
            }else{
                let str = sData["image"]!
                let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: str))")!
                cellC.img_Product.af_setImage(withURL: url as URL)
            }
            
            return cellC
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionViewPColor
        {
//            let defaultData = self.colourImgData[indexPath.row]
//            let controller = self.storyboard?.instantiateViewController(withIdentifier:"ProductOptionVC") as! ProductOptionVC
//            controller.product_ID = self.product_ID
//            controller.product_variant_ID = defaultData["product_variant_id"] ?? ""
//            self.product_variant_ID = defaultData["product_variant_id"] ?? ""
//            self.fetchFeaturedRecentWebServiceCall()
//            controller.selectedDictblock = { dict in
//                NSLog("Selected Dict-->\(dict)")
//                self.productOptionData = dict
//                self.isFromProductOption = "true"
//                self.product_variant_ID = self.productOptionData["product_variant_id"] ?? ""
//                self.fetchFeaturedRecentWebServiceCall()
//
//            }
//            controller.push()
            
            let defaultData = self.colourImgData[indexPath.row]
            self.product_variant_ID = defaultData["product_variant_id"] ?? ""
            self.fetchFeaturedRecentWebServiceCall()
         }
        else if collectionView == self.collectionViewSOption
        {
            let qty = self.lblSelectQty.text?.replacingOccurrences(of: "QTY", with: "", options: .literal, range: nil)
            let basicPrice = (self.originalPrice as NSString).doubleValue * (qty! as NSString).doubleValue
            var shippingData = self.shippingOptionsData[indexPath.row]
            shippingID = shippingData["id"] as! String
            if shippingData["free_shipping"] as! String == "0" {
                self.lblPrice.text = "$\(basicPrice)"
                
            }
            else{
                let extraChargeData = shippingData["extry_charges"] as? [[String:String]]
                
                if extraChargeData?.count == 0{
                    self.lblPrice.text = "$\(basicPrice)"
                }
                
                for (index, element) in (extraChargeData?.enumerated())! {
                    print("Item \(index): \(element)")
                    let str1 = (element["from_price"]! as NSString).doubleValue
                    let str2 = (element["to_price"]! as NSString).doubleValue
                    
                    if basicPrice >= str1 && basicPrice <= str2  {
                        
                        let  extraCharge = (element["price"]! as NSString).doubleValue
                        let temp = basicPrice + extraCharge
                        self.lblPrice.text = "$\(temp)"
                        
                    }
                }
            }
        }
        else if collectionView == self.collectionViewSProduct {
            let similarData = self.similarProductData[indexPath.row]
            self.product_variant_ID = similarData["product_variant_id"] ?? ""
            self.product_ID = similarData["product_id"] ?? ""
            self.fetchFeaturedRecentWebServiceCall()
            self.mailScrollView.setContentOffset(CGPoint.zero, animated: true)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewProductImg
        {
            return collectionView.bounds.size
        }else if collectionView == collectionViewSProduct
        {
            let width = (self.view.frame.size.width - 40) / 2.1
            let height =  CGFloat(180)
            return CGSize(width: width, height: height)
        }
        else if collectionView == collectionViewPColor
        {
            let width = (self.view.frame.size.width - 40) / 3.5
            let height = CGFloat(85)
            return CGSize(width: width, height: height)
        }
        else
        {
            let width = (self.view.frame.size.width - 30) / 3.5
            let height = CGFloat(90)
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == collectionViewSOption
        {
            return 5.0
        }else{
            return 10.0
        }
    }
    
    
    // MARK: Web-Service Methods
    func fetchFeaturedRecentWebServiceCall() {
        
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "product_id":product_ID,
                    "product_variant_id":product_variant_ID]
        
        webService_obj.fetchDataFromServer(header:"productDetail", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                
                self.productCompleteData = responce.value(forKey: "data") as! [String:Any]
                
                let defaultData = self.productCompleteData["default"]  as! [String:Any]
                self.originalPrice = defaultData["price"] as! String
                self.defaultImgData = defaultData["image"]  as? [[String:String]] ?? []
                self.colourImgData = self.productCompleteData["image"]  as! [[String:String]]
                self.similarProductData = self.productCompleteData["similer_product"]  as! [[String:String]]
                self.shippingOptionsData = self.productCompleteData["shipping"] as! [[String:Any]]
                self.lbl_ProductName.text = defaultData["name"] as? String ?? ""
                self.lblProductId.text = "#\(defaultData["product_id"] as? String ?? "")"
                self.viewRating.rating = (defaultData["rating"] as! NSString).floatValue
                self.lblStock.text = "\((defaultData["qty"] as? String)?.integerValue() ?? 0)"
                self.lblMimimumQty.text = "1"
                self.lblMaximumQty.text = "3"
                
                self.lbl_colorsName.text = defaultData["variant_name_color"] as? String ?? ""
                self.pageControl.numberOfPages = self.defaultImgData.count
                self.pageControl.currentPage = 0
                
                
                do {
                    let str = try NSAttributedString(data:(defaultData["description"] as? String ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                    self.lblDescription.attributedText = str
                } catch {
                    print(error)
                }
                
                
                do {
                    let str1 = try NSAttributedString(data:(defaultData["specifications"] as? String ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                    self.lblSpecifications.attributedText = str1
                } catch {
                    print(error)
                }
                
                do {
                    let str1 = try NSAttributedString(data:(defaultData["return_policies"] as? String ?? "").data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                    self.lbl_ReturnPolicy.attributedText = str1
                } catch {
                    print(error)
                }
                
                
                
                if (defaultData["flash_deal"] as! NSString).intValue == 1
                {
                    self.btn_FlashDeal.isHidden = false
                }
                else{
                    self.btn_FlashDeal.isHidden = true
                }
                
                if (defaultData["is_favorite"] as! NSString).intValue == 1
                {
                    self.isFavSelected = true
                    self.btn_Fav.setImage(UIImage(named: "favorites"), for: .normal)
                }
                else{
                    self.isFavSelected = false
                    self.btn_Fav.setImage(UIImage(named: "favirot"), for: .normal)
                }
                
                
                if self.isFromProductOption == "true" {
                  //  self.lblPrice.text = "$\(self.productOptionData["FinalPrice"] ?? "")"
                     self.lblPrice.text = "$\(defaultData["price"] as? String ?? "")"
                    self.lblSelectQty.text = "QTY \(self.productOptionData["qty"] ?? "0")"
                }
                else{
                    self.lblSelectQty.text = "QTY 1"
                    self.lblPrice.text = "$\(defaultData["price"] as? String ?? "")"
                }
                
                if defaultData["is_variant_color"] as! String == "1" {
                    if defaultData["is_variant_size"] as! String == "1" {
                        
                    }
                    else{
                        self.sizeHeight.constant = 0
                    }
                }
                else {
                    self.colorViewHeight.constant = 0
                    if defaultData["is_variant_size"] as! String == "1" {
                        
                    }
                    else {
                        self.sizeHeight.constant = 0
                    }
                    
                }
                
                if self.similarProductData.count == 0 {
                    self.collectionViewSProduct.isHidden = true
                    self.similarViewHeight.constant = 100
                    self.lbl_NoSimilarProduct.isHidden = false
                    
                }
                else{
                    self.lbl_NoSimilarProduct.isHidden = true
                }
                self.collectionViewPColor.reloadData()
                self.collectionViewSOption.reloadData()
                self.collectionViewSProduct.reloadData()
                self.collectionViewProductImg.reloadData()
            }
        }
    }
    
    
}
