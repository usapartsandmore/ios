//  ParentsTableViewCell.swift
//  Expandable3
//  Created by MAC241 on 11/05/17.
//  Copyright © 2017 KiranJasvanee. All rights reserved.


import UIKit

class SubCategoryCell: UITableViewCell {

    @IBOutlet weak var imageviewBackground: UIImageView!
    @IBOutlet weak var constraintLeadingLabelParent: NSLayoutConstraint!
    @IBOutlet weak var labelCategoryName: UILabel!
    
   // @IBOutlet weak var btnAddLocker: UIButton!
   // @IBOutlet weak var labelIndex: UILabel!
   // SubCategoryCell
    
    
    @IBOutlet weak var buttonState: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
       // labelParentCell.font = UIFont(name: "HelveticaNeue-Regular", size: 15)
        //labelIndex.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        
        
    }
    
    func cellFillUp(indexParam: String, tupleCount: NSInteger) {
        if tupleCount == 1 {
            
            labelCategoryName.text = "Anchor & Docking"
            imageviewBackground.image = UIImage(named: "1st")
            //constraintLeadingLabelParent.constant = 16
            
        }else{
            
            labelCategoryName.text = "Docking"
            imageviewBackground.image = nil
            //constraintLeadingLabelParent.constant = 16
        }
        labelCategoryName.textColor = UIColor.darkGray
        //labelIndex.textColor = UIColor.white
        
        //labelIndex.text = "Index: \(indexParam)"
    }

    
    func cellFillUp(indexParam: String) {
        
        //labelCategoryName.textColor = UIColor.white
       // labelCategoryName.text = "Locker  \(indexParam)"
        labelCategoryName.text = "Anchoring"
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
