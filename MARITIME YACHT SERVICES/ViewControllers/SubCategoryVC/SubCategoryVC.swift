//  SubCategoryVC.swift
//  MARITIME YACHT SERVICES
//  Created by Octal on 28/08/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.


import UIKit

class SubCategoriesCell: UITableViewCell {
    
}

class SubCategoryVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var top_img: UIImageView!
    @IBOutlet var lbl_cat_Name: UILabel!
    var subCategoryData = [[String: String]]()
    
    var cat_id = ""
    var cat_name = ""
    var cat_img_str = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        self.getCategoriesWebServiceMethod()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lbl_cat_Name.text = cat_name
        let url:NSURL = NSURL(string :"\(imageURL)\(String(describing: cat_img_str))")!
        self.top_img.af_setImage(withURL: url as URL)
        
     }
    
    // MARK: Button Click
    @IBAction func cartBtnClick(_ sender: Any) {
        let st = UIStoryboard.init(name: "Cart", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
        controller.push()
    }
    
    @IBAction func sideMenuPressed(_ sender: UIButton) {
        
        sideMenuVC.toggleMenu()
        
    }
    
    @IBAction func btnBackPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated:true)
    }
    
     // MARK: TableView Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoriesCell", for: indexPath) as! SubCategoriesCell
        let data = subCategoryData[indexPath.row]
        cell.textLabel?.text = data["name"]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let data = subCategoryData[indexPath.row]
        
        let st = UIStoryboard.init(name: "Product", bundle: nil)
        let controller = st.instantiateViewController(withIdentifier:"NewArrivalsVC") as! NewArrivalsVC
        controller.subcat_id = data["id"]!
        controller.subcat_name = data["name"]!
        controller.type = "Sub Category Product"
        controller.push()
    }
    
     func getCategoriesWebServiceMethod () {
        
        PostData = ["cat_id":cat_id]
        
        webService_obj.fetchDataFromServer(header:"productSubCat", withParameter: PostData as NSDictionary, inVC: self, showHud: true){ (responce,message,staus) in
            if staus{
                self.subCategoryData = responce.value(forKey: "data") as! [[String:String]]
                
                self.tableView.reloadData()
             }
        }
    }
    
}




//    @IBOutlet weak var tblListing: UITableView!
//    // KJ Tree instances -------------------------
//    var arrayTree:[Parent] = []
//    var kjtreeInstance: KJTree = KJTree()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Do any additional setup after loading the view, typically from a nib.
//        
//        let filepath: String? = Bundle.main.path(forResource: "Tree", ofType: "json")
//        let url = URL(fileURLWithPath: filepath ?? "")
//        
//        var jsonData: Data?
//        do {
//            jsonData = try Data(contentsOf: url)
//        }catch{
//            print("error")
//        }
//        
//        var jsonDictionary: NSDictionary?
//        do {
//            jsonDictionary = try JSONSerialization.jsonObject(with: jsonData!, options: .init(rawValue: 0)) as? NSDictionary
//        }catch{
//            print("error")
//        }
//        
//        var arrayParents: NSArray?
//        if let treeDictionary = jsonDictionary?.object(forKey: "Tree") as? NSDictionary {
//            if let arrayOfParents = treeDictionary.object(forKey: "Parents") as? NSArray {
//                arrayParents = arrayOfParents
//            }
//        }
//        
//        if let arrayOfParents = arrayParents
//        {
//            kjtreeInstance = KJTree(parents: arrayOfParents, childrenKey: "Children", expandableKey: "Expanded", key: "Id")
//        }
//        
//        kjtreeInstance.isInitiallyExpanded = true
//        kjtreeInstance.animation = .fade
//        
//        
//        tblListing.delegate = self
//        tblListing.dataSource = self
//        tblListing.rowHeight = UITableViewAutomaticDimension
//        tblListing.estimatedRowHeight = 44
//    }
//    
//    
//    override func didReceiveMemoryWarning()
//    {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    

//    
//    @IBAction func btnBackPressed(_ sender: UIButton) {
//        _ = self.navigationController?.popViewController(animated:true)
//    }
//    @IBAction func cartBtnClick(_ sender: Any) {
//        let st = UIStoryboard.init(name: "Cart", bundle: nil)
//        let controller = st.instantiateViewController(withIdentifier:"ServicesShoppingListVC")
//        controller.push()
//    }
//  }
//
//
//
//extension SubCategoryVC{
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let total = kjtreeInstance.tableView(tableView, numberOfRowsInSection: section)
//        return total
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let node = kjtreeInstance.cellIdentifierUsingTableView(tableView, cellForRowAt: indexPath)
//        let indexTuples = node.index.components(separatedBy: ".")
//        
//        if indexTuples.count == 1   {
//            
//            // Parents
//            let cellIdentifierParents = "SubCategoryCell"
//            var cellParents: SubCategoryCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifierParents) as? SubCategoryCell
//            if cellParents == nil {
//                tableView.register(UINib(nibName: "SubCategoryCell", bundle: nil), forCellReuseIdentifier: cellIdentifierParents)
//                cellParents = tableView.dequeueReusableCell(withIdentifier: cellIdentifierParents) as? SubCategoryCell
//            }
//            cellParents?.cellFillUp(indexParam: node.index, tupleCount: indexTuples.count)
//            cellParents?.selectionStyle = .none
//            
//            if node.state == .open {
//                
//                cellParents?.buttonState.setImage(UIImage(named: "productarrow"), for: .normal)
//                
//            }else if node.state == .close {
//                
//                cellParents?.buttonState.setImage(UIImage(named: "productarrowroroate"), for: .normal)
//                
//            }else{
//                
//                cellParents?.buttonState.setImage(nil, for: .normal)
//            }
//            
//            if indexPath.row == 0
//            {
//                cellParents?.labelCategoryName.text = "Anchor & Docking"
//            }
//            else if indexPath.row == 1
//            {
//                cellParents?.labelCategoryName.text = "Docking"
//            }
//            else
//            {
//                cellParents?.labelCategoryName.text = "Mooring Gears"
//            }
//            
//            
//            cellParents?.labelCategoryName.textColor = UIColor.black
//            
//            return cellParents!
//            
//        }else if indexTuples.count == 2{
//            
//           
//            let cellIdentifierChilds = "SubCategoryCell"
//            var cellChild: SubCategoryCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? SubCategoryCell
//            if cellChild == nil {
//                tableView.register(UINib(nibName: "SubCategoryCell", bundle: nil), forCellReuseIdentifier: cellIdentifierChilds)
//                cellChild = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? SubCategoryCell
//            }
//            cellChild?.cellFillUp(indexParam: node.index)
//            cellChild?.selectionStyle = .none
//            
//            if node.state == .open {
//                
//                cellChild?.buttonState.setImage(UIImage(named: "productarrow"), for: .normal)
//                
//            }else if node.state == .close {
//                
//                cellChild?.buttonState.setImage(UIImage(named: "productarrowroroate"), for: .normal)
//            }else{
//                
//                cellChild?.buttonState.setImage(nil, for: .normal)
//            }
//            
//           cellChild?.labelCategoryName.text = "Anchoring"
//           cellChild?.labelCategoryName.textColor = UIColor.black
//            
//            return cellChild!
//            
//        }else if indexTuples.count == 3{
//            
//            // Parents
//            let cellIdentifierChilds = "SubCategoryCell"
//            var cellChild: SubCategoryCell? = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? SubCategoryCell
//            if cellChild == nil {
//                tableView.register(UINib(nibName: "SubCategoryCell", bundle: nil), forCellReuseIdentifier: cellIdentifierChilds)
//                cellChild = tableView.dequeueReusableCell(withIdentifier: cellIdentifierChilds) as? SubCategoryCell
//            }
//            cellChild?.cellFillUp(indexParam: node.index)
//            cellChild?.selectionStyle = .none
//            
//            if node.state == .open {
//                cellChild?.buttonState.setImage(UIImage(named: "productarrow"), for: .normal)
//            }else if node.state == .close {
//                cellChild?.buttonState.setImage(UIImage(named: "productarrowroroate"), for: .normal)
//            }else{
//                cellChild?.buttonState.setImage(nil, for: .normal)
//            }
//            
//             cellChild?.labelCategoryName.text = "Anchor Lines"
//            cellChild?.labelCategoryName.textColor = UIColor.gray
//            return cellChild!
//            
//        }else{
//            
//            // Childs
//            // grab cell
//            var tableviewcell = tableView.dequeueReusableCell(withIdentifier: "cellidentity")
//            if tableviewcell == nil {
//                tableviewcell = UITableViewCell(style: .default, reuseIdentifier: "cellidentity")
//            }
//            tableviewcell?.textLabel?.text = node.index
//            tableviewcell?.backgroundColor = UIColor.yellow
//            tableviewcell?.selectionStyle = .none
//            return tableviewcell!
//        }
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let node = kjtreeInstance.tableView(tableView, didSelectRowAt: indexPath)
//        print(node.index)
//        print(node.key)
//        // if you've added any identifier or used indexing format
//        print(node.givenIndex)
//        
//        let indexTuples = node.index.components(separatedBy: ".")
//        
//        if indexTuples.count == 3   {
//         let st = UIStoryboard.init(name: "Section5", bundle: nil)
//        let controller = st.instantiateViewController(withIdentifier:"ProductsVC")
//        controller.push()
//        }
//
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 50.0;
//    }
//
//    
//     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        
//        return 1
//    }
//    
//    
//}
