//
//  MessageCell.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit on 05/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//
import UIKit

enum MessageType{
    case incoming
    case outgoing
    case none
}

class MessageCell: UITableViewCell {
    
    static let textFont = UIFont.systemFont(ofSize: 15)

    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageTimeStampLabel: UILabel!
    
    var type: MessageType{
    
        set{
            if(newValue == .incoming){
                messageLabel.superview?.backgroundColor = UIColor.white
                messageLabel.textColor = UIColor.darkGray
            }else{
                messageLabel.superview?.backgroundColor = UIColor(colorLiteralRed: 47.0/255.0, green: 157.0/255.0, blue: 1.0, alpha: 1.0)
                messageLabel.textColor = UIColor.white
            }
        }
        
        get{
            return .none
        }
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        messageLabel.superview?.layer.borderColor = UIColor.lightGray.cgColor
        messageLabel.superview?.layer.cornerRadius = 5.0
        messageLabel.superview?.layer.borderWidth = 0.5
        messageLabel.superview?.layer.masksToBounds = true
    }

}
