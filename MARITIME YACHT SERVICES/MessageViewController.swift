//
//  MessageViewController.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit on 05/09/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import UIKit
import NextGrowingTextView
import IQKeyboardManagerSwift

class MessageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var textView: NextGrowingTextView!
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var lbl_DriverName: UILabel!
    @IBOutlet weak var tblview: UITableView!
    
    @IBOutlet weak var btn_SendMessage: UIButton!
    
    @IBOutlet weak var lbl_noDataMessage: UILabel!
    
    var vendor_id = ""
    var vendor_name = ""
    
    var array: [[String:Any]]!
    
    var timer: Timer?
    
    var isFirstTime = Bool()
    
    let paragraphStyle: NSMutableParagraphStyle = {
        let style = NSMutableParagraphStyle()
        style.lineBreakMode = .byCharWrapping
        style.lineSpacing = 3.0
        return style
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        lbl_noDataMessage.isHidden = true
        isFirstTime = true
        
        array = []
        
        lbl_DriverName.text = self.vendor_name
        
        self.textView.layer.cornerRadius = 4
        self.textView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.textView.textContainerInset = UIEdgeInsets(top: 8, left: 0, bottom: 4, right: 0)
        self.textView.placeholderAttributedText = NSAttributedString(string: "Type your message",
                                                attributes: [NSFontAttributeName: MessageCell.textFont,
                                                            NSForegroundColorAttributeName: UIColor.gray])
        inputContainerView.layer.masksToBounds = false
        inputContainerView.layer.shadowRadius = 2.5
        inputContainerView.layer.shadowOpacity = 0.5
        inputContainerView.layer.shadowOffset = CGSize(width: 0, height: -3)
        inputContainerView.layer.shadowColor = UIColor.gray.cgColor
        
        self.recivedMessageWebService()
        
        
        //Dynamic row height
        tblview.rowHeight = UITableViewAutomaticDimension
        tblview.estimatedRowHeight = 300
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enable = false
        var frame = textView.superview!.frame
        frame.size.width = view.bounds.width
        textView.superview!.frame = frame
        textView.superview?.layoutIfNeeded()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
  
    
        timer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(MessageViewController.recivedMessageWebService), userInfo: nil, repeats: true)
   
        if self.array.count > 0{
            self.tblview.scrollToRow(at: [0, self.array.count-1], at: .bottom, animated: true)
        }
    }
    
    deinit {
        print("")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        timer?.invalidate()
        timer = nil
        
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let msgdict = array[indexPath.row]
        
        if msgdict["sender"] as! String == "1"  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftCell") as! MessageCell
            var message = msgdict["msg"] as! String
            message = message.decodeEmojiText() ?? ""
            let attrString = NSMutableAttributedString(attributedString: message.attrStr())
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 1
            attrString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, message.length))
            
            attrString.addAttribute(NSFontAttributeName, value: UIFont(name: "Poppins", size: 15.0)!, range:NSRange(location: 0,length: message.characters.count))
            
            cell.messageLabel.text = nil
            cell.messageLabel.attributedText = attrString
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: msgdict["created"] as! String)
            dateFormatter.dateFormat = "MMM dd,hh:mm aa"
            cell.messageTimeStampLabel.text = dateFormatter.string(from: myDate!)
            
            cell.type = .incoming
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RightCell") as! MessageCell
            var message = msgdict["msg"] as! String
            message = message.decodeEmojiText() ?? ""
            let attrString = NSMutableAttributedString(attributedString: message.attrStr())
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 1
            attrString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, message.length))
            attrString.addAttribute(NSFontAttributeName, value: UIFont(name: "Poppins", size: 15.0)!, range:NSRange(location: 0,length: message.characters.count))
            cell.messageLabel.text = nil
            cell.messageLabel.attributedText = attrString
            
            //Convert in date format
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let myDate = dateFormatter.date(from: msgdict["created"] as! String)
            dateFormatter.dateFormat = "MMM dd,hh:mm aa"
            cell.messageTimeStampLabel.text = dateFormatter.string(from: myDate!)
            
            cell.type = .outgoing
            return cell
        }
    }
    
    func getHeight(for text: NSAttributedString) -> CGFloat {
        
        let constraints = CGSize(width: view.bounds.width*0.75, height: CGFloat.greatestFiniteMagnitude)
        let context = NSStringDrawingContext()
        let bound: CGRect = text.boundingRect(with: constraints, options: [.usesLineFragmentOrigin], context: context)
        return ceil(bound.height)+45;
    }

    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.inputContainerViewBottom.constant =  0
                //textViewBottomConstraint.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant = keyboardHeight
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { _ in
                    if self.array.count > 0{
                        self.tblview.scrollToRow(at: [0, self.array.count-1], at: .bottom, animated: true)
                    }
                })
            }
        }
    }
    
    @IBAction func onBackClickEvent(sender: UIButton){
        _=self.navigationController?.popViewController(animated: true)
    }
    
     @IBAction func onSendMessageClickEvent(sender: UIButton){
        self.btn_SendMessage.isEnabled = false
        
        textView.text! = textView.text!.trimmingCharacters(in: .whitespaces)
        
        if textView.text! .isEmpty
        {
            self.btn_SendMessage.isEnabled = true
        }else
        {
            PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                        "user_id":self.vendor_id,
                        "msg":textView.text!.encodedEmojiText() ?? ""]
            
            webService_obj.fetchDataFromServer(alertMsg: false, header:"message_send", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
                self.btn_SendMessage.isEnabled = true
                if staus{
                    self.isFirstTime = true
                    self.recivedMessageWebService()
                    
//                    let dataMsg = responce.value(forKey: "data") as! [[String:Any]]
//                    
                     self.textView.text = ""
//                    
//                    DispatchQueue.main.async {
//                        self.array = dataMsg
//                        self.tblview.reloadData()
//                        
//                        if self.array.count > 0{
//                            self.tblview.scrollToRow(at: [0, self.array.count-1], at: .bottom, animated: true)
//                        }
//                    }
                }
            }
        }
    }
    
    
    func recivedMessageWebService()
    {
         
        PostData = ["customer_id":webService_obj.Retrive("User_Id") as! String,
                    "user_id":self.vendor_id]
        
        webService_obj.fetchDataFromServer(alertMsg: false, header:"message_data", withParameter: PostData as NSDictionary, inVC: self, showHud: false){ (responce,message,staus) in
            if staus{
                let dataMsg = responce.value(forKey: "data") as! [[String:Any]]
                
              //  DispatchQueue.main.async {
                    self.array = dataMsg
                    self.tblview.reloadData()
                    
                    if self.array.count > 0{
                        if self.isFirstTime == true{
                             self.isFirstTime = false
                            self.tblview.scrollToRow(at: [0, self.array.count-1], at: .bottom, animated: true)
                        }
                        self.lbl_noDataMessage.isHidden = true
                    }else{
                        self.lbl_noDataMessage.isHidden = false
                    }
             //   }
              }
            if self.array.count > 0{
                
                if self.isFirstTime == true{
                    self.isFirstTime = false
                    self.tblview.scrollToRow(at: [0, self.array.count-1], at: .bottom, animated: true)
                }
            }
         }
    }

}
