//
//  Constant.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 03/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import Foundation
import UIKit

//MARK: File Instance
var selectedIndex = 0

//MARK: UIConstant values
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

//MARK: APP Fonts
let kAPPFontRegular = "ProximaNova-Reg"
let kAPPFontLight = "ProximaNova-Light"
let kAPPFontBold = "ProximaNova-Bold"

//WebServices URL
let baseURL = URL(string: "https://56.octallabs.com/mys/businessadmin/web_services/")!
//let baseURL = URL(string: "http://maritimeyachtservices.com/businessadmin/web_services/")!

let imageURL = "https://56.octallabs.com/mys/businessadmin/"
//let imageURL = "http://maritimeyachtservices.com/businessadmin/"

#if DEBUG
    ///itunes App ID
    public let ITUNES_APP_ID = 512939461
#else
    public let ITUNES_APP_ID = 1332605553
    func print(_ items: Any...){
        //nothing will print in production mode
    }
#endif

let PAYPAL_CLIENT_ID_Sanbox = "AW8LIDTi63OCSwhHg1976HaM1gPo-znpuV9uxlcsiONOukqWdYc4QPdF8ZtqB8LxKmXeQiftFEtcB9PX"
//"Abnrs0CTlTb_FGuzzdAgK-fGR6PjApBYsm62IyJBCAiezoyAtiwWIw5Hc4cdt7Qutss1QtKxdNr5uxqd"

let PAYPAL_CLIENT_SECRET_Sandbox = "EALA-xtcoo6TbHWuExHgCJqMYSaY6MwLvxnSzI3QLu1HV6_gcIVJrLK3wHIHn8jk_kcFKBFIdhJyxB95"
//"EDY4yrIiFpe-sddcOmozj3pvPl2GwkuXsx9VP8jpVMjgYtPc80xMu8CDFiVN1w8y73HkyDsyyLwGAIhE"

let PAYPAL_CLIENT_ID_Live = "AQFFyOHPFe6dRAPnAAdBFieblcvn2f29o3lYmErfIsdYWmxmLKmqslituXG8bi_EiaEWKUdo-zz5f_Ky"

//let addBluePrintUrl = "addBluePrint"

let addBluePrintUrl = "add_blue_print_image"
let getAllDeckRecordUrl = "getBluePrintList"
let addPinToDeck = "addDeckPin"
let editPinToDeck = "editDeckPin"
let removeDeckPin = "deleteDeckPin"
let addLockerApi = "addLocker"
let addBoxApi = "addBox"
let getDeckRecordForId = "getBluePrint"
let getVesselSizeUrl = "get_profile"
let addUpdateVesselSizeUrl = "customer_vessel_update"
 
let ACCEPTABLE_ALLCHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@#"
let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_.@"
let ACCEPTABLE_FORCONTACT = "0123456789"


//MARK: AppConstant Text
let kAPPName = "MARITIME YACHT SERVICES"
let kAPPUSERDATA = "userData"
let kAPP_DEVICE_ID = "Device_token"
let kAPP_DEVICETYPE = "iPhone"
let k_MAKKAH_LAT = "21.4225"
let k_MAKKAH_LONG = "39.8262"

//MARK: File Instance
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let defaults = UserDefaults.standard
let webService_obj = Config()
let load = Loader()
let kAPPLOGIN_IN = "is_loggedIn"

@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}

extension String{
    func encodedEmojiText() -> String? {
        if let data = self.data(using: .nonLossyASCII){
            return String(data: data, encoding: .utf8)
        }
        return nil
    }
    
    func decodeEmojiText() -> String? {
        
        if let data = self.data(using: .utf8){
            return String(data: data, encoding: .nonLossyASCII)
        }
        return nil
    }
    
    func attrStr() -> NSAttributedString {
        
        if let data = self.data(using: .utf16, allowLossyConversion: false){
            
            let attributed = try? NSAttributedString(data: data, options: [ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType ], documentAttributes: nil)
            return attributed ?? NSAttributedString()
        }
        return NSAttributedString()
    }
}

extension UIViewController{
    @IBAction func onMenuClickEvent(_ sender: UIButton) {
        sideMenuVC.openMenu()
    }
    
    func setShadow(on navigationBarView: UIView) {
        // navigationBarView.superview?.bringSubview(toFront: navigationBarView)
        navigationBarView.layer.shadowColor = UIColor.black.cgColor
        navigationBarView.layer.shadowOpacity = 0.5
        navigationBarView.layer.shadowOffset = CGSize.zero
        navigationBarView.layer.shadowRadius = 8
    }
    
    func push() {
        if let navigationController = sideMenuVC.mainViewController as? UINavigationController{
            navigationController.pushViewController(self, animated: true)
            sideMenuVC.closeMenu()
        }
    }
    
    @IBAction func onMenuBackClickEvent(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: true)
    }
    
    func AlertMessage(_ alert :String)
    {
        let objAlertController = UIAlertController(title: "", message:alert, preferredStyle: UIAlertControllerStyle.alert)
        present(objAlertController, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC))
        {
            () -> Void in
            objAlertController .dismiss(animated: true, completion: nil)
        }
    }
    
    var userId:String {
        get{
        
            return webService_obj.Retrive("User_Id") as! String
        }
    }
}

