//
//  Loader.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 03/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//


import UIKit

class Loader: NSObject {

//    var load  = MBProgressHUD()
    func show(views: UIView)
    {
        let loadingNotification = MBProgressHUD.showAdded(to: views, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        
    }
    
    func hide(delegate:UIViewController) {
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: delegate.view, animated: true)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
       
    }
}
