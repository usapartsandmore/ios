//
//  Pin.swift
//  PinDropOnImage
//
//  Created by Mac114 on 28/09/17.
//  Copyright © 2017 Mac114. All rights reserved.
//

import Foundation
import UIKit

class ImagePin {
    
    var pinDescription:String? = " "
    var pinCurrentPointLocation:CGPoint? = nil
    var pinLocationOnOriginalScale:CGPoint? = nil
    var pinSuperviewOriginalSize:CGSize? = nil
    
    func setCurrentPointLocation(currentLocation:CGPoint){
        pinCurrentPointLocation = currentLocation
    }
    
    func setLocationAtOriginalScale(initialScaleLocation:CGPoint){
        pinLocationOnOriginalScale = initialScaleLocation
    }
    
    func setDescription(description:String){
        pinDescription = description
    }
    
    func setPinOriginalViewSize(originalViewSize:CGSize){
        pinSuperviewOriginalSize = originalViewSize
    }
    
}
