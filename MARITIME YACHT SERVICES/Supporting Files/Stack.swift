//
//  Stack.swift
//  PinDropOnImage
//
//  Created by Mac114 on 28/09/17.
//  Copyright © 2017 Mac114. All rights reserved.
//

import Foundation
import UIKit
struct ImageSizeNode {
    var imageSize:CGSize? = nil
    var imageScale:CGFloat? = nil
    var imageBoundsSize:CGSize? = nil
}

class Stack {
    
    var arrPins = [ImageSizeNode]()
    
    func push(newImageSizeNode:ImageSizeNode){
        arrPins.append(newImageSizeNode)
    }
    
    func pop() -> ImageSizeNode?
    {
        if arrPins.last != nil {
            let lastImgSize = arrPins.last!
            arrPins.removeLast()
            return lastImgSize
        }
        else
        {
            return nil
        }
    }
    
    var isFull: Bool{
        if arrPins.count == 9 {
            return true
        }
        else
        {
            return false
        }
    }

}
