//
//  Config.swift
//  MARITIME YACHT SERVICES
//
//  Created by Rachit Sharma on 03/04/17.
//  Copyright © 2017 OctalSoftware. All rights reserved.
//

import Foundation
import Alamofire

enum JSONError : Error {
    case notArray
    case notNSDictionary
}

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}

extension UIImage{
    var fixed: UIImage?{
        if (self.imageOrientation == UIImageOrientation.up ) { return self }
        UIGraphicsBeginImageContext(self.size)
        draw(in: CGRect(origin: CGPoint.zero, size: size))
        let copy = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return copy
    }
}

extension Dictionary {
    func toJson() {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print("JSON string = \(jsonString)")
    }
}


class Config: NSObject, UITextFieldDelegate {
    
    var imageCache = NSCache<AnyObject, AnyObject>()
    var imageURL  = String()
    var imageviews = UIImageView()
    var label = UILabel()
    let prefs = UserDefaults.standard
    
    //Webserice Method
    
   // MARK: 1.Normal Webserice Method
    func fetchDataFromServer(alertMsg: Bool = true,header: String, withParameter parameter: NSDictionary, inVC vc: UIViewController, showHud hud: Bool, completion: @escaping (_ responce : NSDictionary,_ message : NSString, _ status : Bool) -> ()) {
        
        if NetReachability.isConnectedToNetwork() {
            print("URL->\(baseURL)\(header)")
            print("Request Params->\((parameter as! Dictionary<String, Any>).toJson())")
            
            if hud{
                load.show(views: vc.view)
            }
            
            let headers: HTTPHeaders = [
                "Authorization": "XXXXXX",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            
            guard let url = URL(string: "\(baseURL)\(header)"), var request = try? URLRequest(url: url, method: .post, headers: headers) else{
                completion(NSDictionary(), "URLRequest Error", true)
                return
            }
            
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameter, options: [])
            request.timeoutInterval = 60
            
            Alamofire.request(request)
                .responseJSON { (response:DataResponse<Any>) in
                    
                    //        Alamofire.request("\(baseURL)\(header)", method: .post, parameters:parameter as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    if hud{
                        load.hide(delegate: vc)
                    }
                    
                    switch(response.result)
                    {
                    case .success(_):
                        
                        print("Responce->\(response.result.value!)")
                        
                        if response.result.value != nil
                        {
                            let JSON = response.result.value! as AnyObject
                            let responce = JSON["response"] as! NSDictionary
                            
                            let sucess = responce.value(forKey: "status")
                            let success_message = responce.value(forKey: "success_message") as! String
                            let error_message = responce.value(forKey: "error_message") as! String
                            
                            
                            if sucess as! String == "OK"
                            {
                                // let data = responce.value(forKey: "data") as! NSDictionary
                                completion(responce.replacingNullsWithBlanks() as NSDictionary, success_message as NSString, true)
                            }
                            else
                            {
                                print("Fails")
                                completion([:], error_message as NSString, false)
                                
                                if alertMsg
                                {
                                    //self.presentAlertWithTitle(title: kAPPName, message: error_message, vc: vc)
                                    self.presentAlertWithTitle(title: "Alert", message: error_message, vc: vc)
                                }
                            }
                        }
                        break
                        
                    case .failure(_):
                        print(response.result.error ?? "Fail")
                
                        self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                        break
                        
                    }
            }
        }else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }
    
    // MARK: 2.Normal Webserice Method for calendar and Task
    func fetchCalendarDataFromServer(alertMsg: Bool = true,header: String, withParameter parameter: NSDictionary, inVC vc: UIViewController, showHud hud: Bool, completion: @escaping (_ responce : NSDictionary,_ message : NSString, _ status : Bool) -> ()) {
        if NetReachability.isConnectedToNetwork() {
            
            print("URL->\(baseURL)\(header)")
            print("Request Params->\((parameter as! Dictionary<String, Any>).toJson())")
            
            if hud{
                load.show(views: vc.view)
            }
            
            let headers: HTTPHeaders = [
                "Authorization": "XXXXXX",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            
            guard let url = URL(string: "\(baseURL)\(header)"), var request = try? URLRequest(url: url, method: .post, headers: headers) else{
                completion(NSDictionary(), "URLRequest Error", true)
                return
            }
            
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameter, options: [])
            request.timeoutInterval = 60
            
            Alamofire.request(request)
                .responseJSON { (response:DataResponse<Any>) in
                    
                    //        Alamofire.request("\(baseURL)\(header)", method: .post, parameters:parameter as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
                    
                    if hud{
                        load.hide(delegate: vc)
                    }
                    
                    switch(response.result)
                    {
                    case .success(_):
                        
                        print("Responce->\(response.result.value!)")
                        
                        if response.result.value != nil
                        {
                            let JSON = response.result.value! as AnyObject
                            let responce = JSON["response"] as! NSDictionary
                            
                            let sucess = responce.value(forKey: "status")
                            let success_message = responce.value(forKey: "success_message") as! String
                            let error_message = responce.value(forKey: "error_message") as! String
                            
                            
                            if sucess as! String == "OK"
                            {
                                // let data = responce.value(forKey: "data") as! NSDictionary
                                completion(responce.replacingNullsWithBlanks() as NSDictionary, success_message as NSString, true)
                            }
                            else
                            {
                                print("Fails")
                                //completion([:], error_message as NSString, false)
                                
                                completion(responce.replacingNullsWithBlanks() as NSDictionary, success_message as NSString, false)
                                
                                if alertMsg
                                {
                                    //self.presentAlertWithTitle(title: kAPPName, message: error_message, vc: vc)
                                    self.presentAlertWithTitle(title: "Alert", message: error_message, vc: vc)
                                }
                            }
                        }
                        break
                        
                    case .failure(_):
                        print(response.result.error ?? "Fail")
                        
                        if (response.result.error?.localizedDescription)!.range(of:"The network connection was lost.") != nil {
                            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
                        }else{
                            self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                        }
                        break
                        
                    }
            }
        }
        else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }

    
    
    func uploadMultipleImages(alertMsg: Bool = true,header: String,
                     withParameter parameter: [String: Any],
                     images: [String:UIImage] = [:],
                     compressRatio: CGFloat = 0.5,
                     inVC vc: UIViewController,
                     showHud hud: Bool = true,
                     completion: @escaping (_ responce : NSDictionary,_ message : NSString, _ status : Bool) -> ()) {
        if NetReachability.isConnectedToNetwork() {
            
            print("URL->\(baseURL)\(header)")
            
            if hud
            {
                load.show(views: vc.view)
            }
            
            let urlString = "\(baseURL)\(header)"
            let jsonData  = try! JSONSerialization.data(withJSONObject: parameter, options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print("JSON Params->\(jsonString)")
            
            let headers: HTTPHeaders = [
                "Authorization": "XXXXXX",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            
            let url = try! URLRequest(url: URL(string:urlString)!, method: .post, headers: headers)
            
            
            Alamofire.upload(multipartFormData: { multipart in
                
                var keys = [String]()
                
                for (key, img) in images{
                    if let imageData = UIImageJPEGRepresentation(img, compressRatio){
                        multipart.append(imageData, withName: key, fileName: key+".png", mimeType: "image/png")
                        keys.append(key)
                    }
                }
                
                let nameJSON = try! JSONSerialization.data(withJSONObject: ["count": keys.count], options: .prettyPrinted)
                
                multipart.append(nameJSON, withName: "names")
                multipart.append(jsonData, withName: "data")
                
            }, with: url) { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON{ responce in
                        
                        if hud{
                            load.hide(delegate: vc)
                        }
                        
                        switch responce.result{
                        case .success(_):
                            
                            print("Responce->\(responce.result.value!)")
                            
                            if responce.result.value != nil
                            {
                                let JSON = responce.result.value! as AnyObject
                                let responce = JSON["response"] as! NSDictionary
                                
                                let sucess = responce.value(forKey: "status")
                                let success_message = responce.value(forKey: "success_message") as! String
                                let error_message = responce.value(forKey: "error_message") as! String
                                
                                
                                if sucess as! String == "OK"
                                {
                                    // let data = responce.value(forKey: "data") as! NSDictionary
                                    completion(responce.replacingNullsWithBlanks() as NSDictionary, success_message as NSString, true)
                                }
                                else
                                {
                                    print("Fails")
                                    completion([:], error_message as NSString, false)
                                    
                                    if alertMsg
                                    {
                                        //self.presentAlertWithTitle(title: kAPPName, message: error_message, vc: vc)
                                        self.presentAlertWithTitle(title: "Alert", message: error_message, vc: vc)
                                    }
                                }
                            }
                            break
                            
                        case .failure(_):
                            print(responce.result.error ?? "Fail")
                            self.presentAlertWithTitle(title: "Error!", message: (responce.result.error?.localizedDescription)!, vc: vc)
                            break
                            
                        }
                    }
                case .failure(let error):
                    self.presentAlertWithTitle(title: "Error!", message: error.localizedDescription, vc: vc)
                }
            }
        }else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }

    
     //MARK: 4.Profile Upload Webserice Method
    func profileDataFromServer(header: String, withParameter parameter: NSDictionary,coverdata: UIImage, inVC vc: UIViewController, showHud hud: Bool, completion: @escaping (_ responce : NSDictionary, _ status : Bool) -> ()) {
        if NetReachability.isConnectedToNetwork() {
            
            print("URL->\(baseURL)\(header)")
            
            if hud
            {
                load.show(views: vc.view)
            }
            
            let urlString = "\(baseURL)\(header)"
            let jsonData  = try! JSONSerialization.data(withJSONObject: parameter, options: .prettyPrinted)
            
            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
            print("JSON Params->\(jsonString)")
            
            let headers: HTTPHeaders = [
                "Authorization": "XXXXXX",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            
            let url = try! URLRequest(url: URL(string:urlString)!, method: .post, headers: headers)
            
            let coverimageData = UIImageJPEGRepresentation(coverdata, 0.8)
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(coverimageData!, withName: "image", fileName: "cover.png", mimeType: "image/png")
                    multipartFormData.append(jsonData, withName: "data")
            },
                with: url,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON
                            { response in
                                if hud{
                                    load.hide(delegate: vc)
                                }
                                if response.result.value != nil
                                {
                                    let JSON = response.result.value! as AnyObject
                                    let responce = JSON["response"] as! NSDictionary
                                    
                                    let sucess = responce.value(forKey: "status")
                                    let success_message = responce.value(forKey: "success_message") as! String
                                    let error_message = responce.value(forKey: "error_message") as! String
                                    
                                    print("JSON Responce->\(responce)")
                                    
                                    if sucess as! String == "OK"
                                    {
                                        // let data = responce.value(forKey: "data") as! NSDictionary
                                        completion(responce.replacingNullsWithBlanks() as NSDictionary, true)
                                    }else
                                    {
                                        print("Fails")
                                        completion([:], false)
                                        self.presentAlertWithTitle(title: kAPPName, message: error_message, vc: vc)
                                    }
                                }
                        }
                        break
                    case .failure( _):
                        self.presentAlertWithTitle(title: "Error!", message: "", vc: vc)
                        break
                    }
            })
        }else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }

    
    
    //MARK:- Method to post data along with images
    func callAPiFor(alertMsg: Bool = true,subApiName: String, parameter: [String: String], images: [String: UIImage?], view: UIView?, inVC vc: UIViewController, completion: @escaping (_ responce : NSDictionary?,_ message : NSString, _ status : Bool) -> ()) {
        if NetReachability.isConnectedToNetwork() {
            
            print("URL->\(baseURL)\(subApiName)")
            print("Request Params->\((parameter as [String: String]).toJson())")
            
            
            let compressRatio: CGFloat = 0.35
            
            if let view = vc.view {
                let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
            let urlString = "\(baseURL)\(subApiName)"
            let jsonData  = try! JSONSerialization.data(withJSONObject: parameter, options: .prettyPrinted)
            
            let headers: HTTPHeaders = [
                "Authorization": "XXXXXX",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            
            let url = try! URLRequest(url: URL(string:urlString)!, method: .post, headers: headers)
            
            Alamofire.upload(multipartFormData: { multipart in
                for (key, img) in images{
                    if let imageData = UIImageJPEGRepresentation(img!, compressRatio){
                        multipart.append(imageData, withName: key, fileName: key+".png", mimeType: "image/png")
                    }
                }
                multipart.append(jsonData, withName: "data")
            }, with: url) { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON{ responce in
                        
                        if let view = vc.view {
                            MBProgressHUD.hide(for: view, animated: true)
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        }
                        
                        switch responce.result{
                        case .success:
                            if responce.result.value != nil{
                                let JSON = responce.result.value! as AnyObject
                                let responce = JSON["response"] as! NSDictionary
                                
                                
                                
                                let sucess = responce.value(forKey: "status")
                                let success_message = responce.value(forKey: "success_message") as! String
                                let error_message = responce.value(forKey: "error_message") as! String
                                
                                print("Responce->\(JSON)")
                                
                                if sucess as! String == "OK"
                                {
                                    // let data = responce.value(forKey: "data") as! NSDictionary
                                    completion(responce.replacingNullsWithBlanks() as NSDictionary, success_message as NSString, true)
                                }
                                else
                                {
                                    print("Fails")
                                    completion([:], error_message as NSString, false)
                                    
                                    if alertMsg
                                    {
                                        //self.presentAlertWithTitle(title: kAPPName, message: error_message, vc: vc)
                                        self.presentAlertWithTitle(title: "Alert", message: error_message, vc: vc)
                                    }
                                }
                            }
                            
                        case .failure(let error):
                            completion(nil, error.localizedDescription as NSString, false)
                        }
                    }
                case .failure(let error):
                    completion(nil, error.localizedDescription as NSString, false)
                }
            }
        }else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }
    
    //MARK:- API Call method using both BaseUrl and SubUrl
    func fetchDataFromServerForBothUrl(alertMsg: Bool = true, completeUrl: String, withParameter parameter: NSDictionary, inVC vc: UIViewController, showHud hud: Bool, completion: @escaping (_ responce : NSDictionary,_ message : NSString, _ status : Bool) -> ()) {
        if NetReachability.isConnectedToNetwork() {
            
            print("URL->\(completeUrl)")
            print("Request Params->\((parameter as! Dictionary<String, Any>).toJson())")
            
            if hud{
                load.show(views: vc.view)
            }
            
            let headers: HTTPHeaders = [
                "Authorization": "XXXXXX",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            Alamofire.request("\(completeUrl)", method: .post, parameters:parameter as? Parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                
                if hud{
                    load.hide(delegate: vc)
                }
                
                switch(response.result)
                {
                case .success(_):
                    
                    print("Responce->\(response.result.value!)")
                    
                    if response.result.value != nil
                    {
                        let JSON = response.result.value! as AnyObject
                        let responce = JSON["response"] as! NSDictionary
                        
                        let sucess = responce.value(forKey: "status")
                        let success_message = responce.value(forKey: "success_message") as! String
                        let error_message = responce.value(forKey: "error_message") as! String
                        
                        
                        if sucess as! String == "OK"
                        {
                            // let data = responce.value(forKey: "data") as! NSDictionary
                            completion(responce.replacingNullsWithBlanks() as NSDictionary, success_message as NSString, true)
                        }
                        else
                        {
                            print("Fails")
                            completion([:], error_message as NSString, false)
                            
                            if alertMsg
                            {
                                //self.presentAlertWithTitle(title: kAPPName, message: error_message, vc: vc)
                                self.presentAlertWithTitle(title: "Alert", message: error_message, vc: vc)
                            }
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                    
                }
            }
        }else {
            self.presentAlertWithTitle(title: "Error!", message: "Please check network connectivity.", vc: vc)
        }
    }
    
    
    
    // MARK: NSUserDefault
     func Save(_ str:String,keyname:String)
    {
        prefs.setValue(str, forKey: keyname)
        prefs.synchronize()
    }
    
    func Retrive(_ str:String)-> AnyObject
    {
        let retrivevalue =  prefs.value(forKey: str)
        
        if retrivevalue != nil
        {
            return retrivevalue! as AnyObject
        }
        return "" as AnyObject
    }
    
    func RemoveKey(_ str:String)
    {
        prefs.removeObject(forKey: str)
    }
    
    func alertempty(_ message:String,view:UIView)
    {
        label = UILabel(frame:CGRect(x: 0,y: SCREEN_HEIGHT/2-100,width: SCREEN_WIDTH,height: 35))
        label.text = message
        label.textAlignment = .center
        view.addSubview(label)
    }
    
    func alertemptyNew(_ message:String,view:UIView)
    {
        label = UILabel(frame:CGRect(x: 0,y: 50,width: SCREEN_WIDTH,height: 35))
        label.text = message
        label.textAlignment = .center
        view.addSubview(label)
    }
    
    func alertemptyNewRemove(_ view:UIView)
    {
        // view.removeFromSuperview()
        label.willRemoveSubview(view)
    }
     
    
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func getStringAfterTriming(text: String) -> NSString {
        return text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
    }
}

extension UITextField
{
    func setBottomBorder(borderColor: UIColor)
    {
        
        self.borderStyle = UITextBorderStyle.none
        self.backgroundColor = UIColor.clear
        let width = 1.0
        
        let borderLine = UIView()
        borderLine.tag = 1
        self.layoutIfNeeded()
        borderLine.frame = CGRect(x: 0, y: Double(self.frame.height) - width, width: Double(self.frame.width), height: width)
        
        borderLine.backgroundColor = borderColor
        self.addSubview(borderLine)
    }
    
    func setBorder(borderColor: UIColor)
    {
        
        self.borderStyle = UITextBorderStyle.none
        self.backgroundColor = UIColor.clear
        let width = 1.0
        
        let borderLine = UIView()
        borderLine.tag = 1
        self.layoutIfNeeded()
        borderLine.frame = CGRect(x: 0, y: 0, width: Double(self.frame.width), height: Double(self.frame.height))
        borderLine.backgroundColor = borderColor
        self.addSubview(borderLine)
    }
    
    func addPaddingView(width: Double, onSide side: NSInteger)
    {
        let paddingView = UIView()
        //self.layoutIfNeeded()
        paddingView.frame = CGRect(x: 0, y: 0, width: width, height: Double(self.frame.height))
        paddingView.backgroundColor = UIColor.clear
        if side == 0{
            self.leftView = paddingView
            self.leftViewMode = .always
        }else{
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
    
}
extension UITextView
{
    
    func addPaddingView(width: Double, onSide side: NSInteger)
    {
        let paddingView = UIView()
        //self.layoutIfNeeded()
        paddingView.frame = CGRect(x: 0, y: 0, width: width, height: Double(self.frame.height))
        paddingView.backgroundColor = UIColor.clear
        if side == 0{
            self.addSubview(paddingView)
            //self.leftViewMode = .always
        }else{
            self.addSubview(paddingView)
            // self.rightViewMode = .always
        }
    }
    
}
extension UIButton {
    
    func alignImageAndTitleVertically(padding: CGFloat = 6.0) {
        let imageSize = self.imageView!.frame.size
        let titleSize = self.titleLabel!.frame.size
        let totalHeight = imageSize.height + titleSize.height + padding
        
        self.imageEdgeInsets = UIEdgeInsets(
            top: -(totalHeight - imageSize.height),
            left: 0,
            bottom: 0,
            right: -titleSize.width
        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: -imageSize.width,
            bottom: -(totalHeight - titleSize.height),
            right: 0
        )
    }
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { () -> Void in
                self.imageView?.image = UIImage(data: data)
            }
        }
    }
    func downloadBGImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { () -> Void in
                self.setBackgroundImage(UIImage(data: data), for: .normal)
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func roundButton(_ radius: Float) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = CGFloat(radius)
        self.clipsToBounds = true
    }
    
}
extension UITableView
{
    func reloadTableAnimation()
    {
        let sectionIndex = IndexSet(integer: 0)
        self.reloadSections(sectionIndex, with: .automatic)
        
    }
}


extension UIImage {
    var uncompressedPNGData: Foundation.Data?      { return UIImagePNGRepresentation(self)        }
    var highestQualityJPEGNSData: Foundation.Data? { return UIImageJPEGRepresentation(self, 1.0)  }
    var highQualityJPEGNSData: Foundation.Data?    { return UIImageJPEGRepresentation(self, 0.75) }
    var mediumQualityJPEGNSData: Foundation.Data?  { return UIImageJPEGRepresentation(self, 0.5)  }
    var lowQualityJPEGNSData: Foundation.Data?     { return UIImageJPEGRepresentation(self, 0.25) }
    var lowestQualityJPEGNSData:Foundation.Data?   { return UIImageJPEGRepresentation(self, 0.0)  }
}
extension String {
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
}


