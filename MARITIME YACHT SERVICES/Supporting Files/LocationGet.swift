//
//  LocationGet.swift
//  Location
//
//  Created by Admin on 28/06/16.
//  Copyright © 2016 Rachit Sharma. All rights reserved.
//

import UIKit
import CoreLocation

class LocationGet: NSObject,CLLocationManagerDelegate
{
    
    var locationManager:CLLocationManager = CLLocationManager()
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        locationManager.allowsBackgroundLocationUpdates = false
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.startUpdatingLocation()
    }
    
    
    func displayLocationInfo(_ placemark: CLPlacemark?) {
        if let containsPlacemark = placemark {
            locationManager.startUpdatingLocation()
            _ = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            _ = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            _ = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            _ = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            let lat:String = locationManager.location!.coordinate.latitude.description
            let long:String = locationManager.location!.coordinate.longitude.description
            
            print(lat)
            print(long)
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("didChangeAuthorizationStatus")
        
        switch status {
        case .notDetermined:
            print(".NotDetermined")
            break
            
        case .authorizedAlways:
            print(".Authorized")
            self.locationManager.startUpdatingLocation()
            break
            
        case .denied:
            print(".Denied")
            break
            
        default:
            print("Unhandled authorization status")
            break
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, e) -> Void in
            if e != nil {
                // print("Error:  \(e!.localizedDescription)")
            } else {
                let placemark = placemarks!.last! as CLPlacemark
                
                //yatindra
                
                if((placemark.subLocality) != nil){
                    
                    let userInfo = [
                        "latitude":"\(location.coordinate.latitude)",
                        "longitude":"\(location.coordinate.longitude)",
                        "Area":     placemark.subLocality! ,
                        "city":     placemark.locality!,
                        "state":    placemark.administrativeArea!,
                        "country":  placemark.country!,
                        "name": placemark.name!,
                        // "thoroughfare":placemark .thoroughfare!,
                        "subAdministrativeArea":  placemark.subAdministrativeArea ?? "",
                        "postalCode": placemark.postalCode ?? "",
                        "ISOcountryCode":placemark .isoCountryCode!,
                        ] as NSMutableDictionary
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil, userInfo: (userInfo as AnyObject) as? [AnyHashable : Any])
                    
                } else {
                    
                    let userInfo = [
                        "latitude":"\(location.coordinate.latitude)",
                        "longitude":"\(location.coordinate.longitude)",
                        "Area":     "" ,
                        "city":     placemark.locality!,
                        "state":    placemark.administrativeArea!,
                        "country":  placemark.country!,
                        "name": placemark.name!,
                        // "thoroughfare":placemark .thoroughfare!,
                        "subAdministrativeArea":  placemark.subAdministrativeArea ?? "",
                        "postalCode": placemark.postalCode ?? "",
                        "ISOcountryCode":placemark .isoCountryCode!,
                        ] as NSMutableDictionary
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LOCATION_AVAILABLE"), object: nil, userInfo: (userInfo as AnyObject) as? [AnyHashable : Any])
                    
                }
                
                
            }
        })
    }
}
